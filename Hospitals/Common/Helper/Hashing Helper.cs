﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Common.Helper
{
    public class HashingHelper
    {
        public static string Sha256Hash(string value)
        {
            var stringBuilder = new StringBuilder();

            var enc = Encoding.UTF8;
            using var hash = SHA256.Create();
            var result = hash.ComputeHash(enc.GetBytes(value));
            foreach (var b in result)
                stringBuilder.Append(b.ToString("x2"));

            return stringBuilder.ToString();
        }

        public static string FormatDataForHashing(Dictionary<string, string> data, string pass)
        {
            var orderedData = data.OrderBy(x => x.Key);
            // var items = from pair in data
            //             orderby pair.Value ascending
            //             select pair;
            var tmp = new ArrayList { pass };
            foreach (var item in orderedData)
            {
                if (item.Key.ToString() != "signature")
                {
                    tmp.Add(item.Key.ToString() + "=" + item.Value.ToString());
                }
            }

            tmp.Add(pass);
            var requestParameters = tmp.ToArray(typeof(string)) as string[];
            var dataFormatted = string.Join("", requestParameters);
            return dataFormatted;
        }
    }
}
