﻿using Common.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helper
{
    public class HttpConnector
    {
        protected readonly HttpClient client;

        public HttpConnector(HttpClient client)
        {
            this.client = client;
            this.client.DefaultRequestHeaders.Accept.Clear();
            this.client.DefaultRequestHeaders.Add("Accept", "application/json");
        }

        #region GetListFrom
        public async Task<IEnumerable<T>> GetListFrom<T>(string baseAddress, string url)
        {
            string request = baseAddress + url;

            LoggingHelper.WriteLog(request + " : " + "start");

            List<T> list = new List<T>();
            try
            {
                var urirequest = new Uri(request);
                LoggingHelper.WriteLog(request + " : " + "start using ");
                try
                {
                    LoggingHelper.WriteLog("start url" + request);
                    using (HttpResponseMessage response = await client.GetAsync(urirequest))
                    {
                        LoggingHelper.WriteLog("END url" + request);

                        if (response.IsSuccessStatusCode)
                        {
                            LoggingHelper.WriteLog(request + " : " + "Begin Result");
                            using (HttpContent content = response.Content)
                            {
                                string result = await content.ReadAsStringAsync().ConfigureAwait(false);
                                list = JsonConvert.DeserializeObject<List<T>>(result);
                                LoggingHelper.WriteLog(request + " : " + "result");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggingHelper.WriteLog(request + "  " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                LoggingHelper.WriteLog(request + "  " + ex.Message);
            }
            LoggingHelper.WriteLog(request + " : " + "END");

            return list;
        }
        #endregion

        #region GetFrom
        public async Task<T> GetFromAsync<T>(string baseAddress, string url)
        {
            string request = baseAddress + url;


            var obj = Activator.CreateInstance(typeof(T));
            try
            {
                LoggingHelper.WriteLog(request + " : " + "start");
                var urirequest = new Uri(request);
                try
                {
                    LoggingHelper.WriteLog("start url" + request);
                    using (HttpResponseMessage response = await client.GetAsync(urirequest).ConfigureAwait(false))
                    {
                        LoggingHelper.WriteLog("END url" + request);

                        if (response.IsSuccessStatusCode)
                        {
                            LoggingHelper.WriteLog(request + " : " + "Begin Result");
                            using (HttpContent content = response.Content)
                            {
                                string result = await content.ReadAsStringAsync();
                                obj = JsonConvert.DeserializeObject<T>(result);
                                LoggingHelper.WriteLog(request + " : " + "End Result");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggingHelper.WriteLog(request + "  " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                LoggingHelper.WriteLog(request + "  " + ex.Message);
            }
            LoggingHelper.WriteLog(request + " : " + "END");
            return (T)obj;
        }

        public T GetFrom<T>(string baseAddress, string url)
        {
            string request = baseAddress + url;

            var obj = Activator.CreateInstance(typeof(T));
            try
            {
                LoggingHelper.WriteLog(request + " : " + "start");
                var urirequest = new Uri(request);
                try
                {
                    LoggingHelper.WriteLog("start url" + request);
                    using (HttpResponseMessage response = client.GetAsync(urirequest).Result)
                    {
                        LoggingHelper.WriteLog("END url" + request);

                        if (response.IsSuccessStatusCode)
                        {
                            LoggingHelper.WriteLog(request + " : " + "Begin Result");
                            using (HttpContent content = response.Content)
                            {
                                string result = content.ReadAsStringAsync().Result;
                                obj = JsonConvert.DeserializeObject<T>(result);
                                LoggingHelper.WriteLog(request + " : " + "End Result");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggingHelper.WriteLog(request + "  " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                LoggingHelper.WriteLog(request + "  " + ex.Message);
            }
            LoggingHelper.WriteLog(request + " : " + "END");
            return (T)obj;
        }
        public string GetStringFrom(string baseAddress, string url)
        {
            string request = baseAddress + url;

            string obj = null;

            try
            {
                LoggingHelper.WriteLog(request + " : " + "start");
                var urirequest = new Uri(request);
                try
                {
                    LoggingHelper.WriteLog("start url" + request);
                    using (HttpResponseMessage response = client.GetAsync(urirequest).Result)
                    {
                        LoggingHelper.WriteLog("END url" + request);

                        if (response.IsSuccessStatusCode)
                        {
                            LoggingHelper.WriteLog(request + " : " + "Begin Result");
                            using (HttpContent content = response.Content)
                            {
                                obj = content.ReadAsStringAsync().Result;

                                LoggingHelper.WriteLog(request + " : " + "End Result");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggingHelper.WriteLog(request + "  " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                LoggingHelper.WriteLog(request + "  " + ex.Message);
            }
            LoggingHelper.WriteLog(request + " : " + "END");
            return obj;
        }
        #endregion

        #region GetFromResponseResult
        public async Task<T> GetFromResponseResult<T>(string baseAddress, string url)
        {
            string request = baseAddress + url;


            var obj = Activator.CreateInstance(typeof(T));

            try
            {
                LoggingHelper.WriteLog(request + " : " + "start");
                var urirequest = new Uri(request);
                try
                {


                    LoggingHelper.WriteLog("start url" + request);
                    using (HttpResponseMessage response = await client.GetAsync(urirequest).ConfigureAwait(false))
                    {
                        LoggingHelper.WriteLog("END url" + request);

                        if (response.IsSuccessStatusCode)
                        {
                            LoggingHelper.WriteLog(request + " : " + "Begin Result");
                            using (HttpContent content = response.Content)
                            {
                                string result = await content.ReadAsStringAsync();
                                obj = JsonConvert.DeserializeObject<ResponseResult<T>>(result).Data;
                                LoggingHelper.WriteLog(request + " : " + "End Result");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggingHelper.WriteLog(request + "  " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                LoggingHelper.WriteLog(request + "  " + ex.Message);
            }
            LoggingHelper.WriteLog(request + " : " + "END");
            return (T)obj;
        }
        #endregion

        #region PostListFrom
        public async Task<IEnumerable<T>> PostListFrom<T>(string baseAddress, string url, HttpContent content)
        {
            List<T> list = null;
            string request = baseAddress + url;
            LoggingHelper.WriteLog(request + " : " + "start");

            try
            {
                var urirequest = new Uri(request);
                LoggingHelper.WriteLog("start url" + request);
                HttpResponseMessage response = await client.PostAsync(urirequest, content).ConfigureAwait(false);
                LoggingHelper.WriteLog("END url" + request);

                if (response.IsSuccessStatusCode)
                {
                    LoggingHelper.WriteLog(request + " : " + "Begin Result");
                    string result = await response.Content.ReadAsStringAsync();
                    list = JsonConvert.DeserializeObject<List<T>>(result);
                    LoggingHelper.WriteLog(request + " : " + "End Result");
                }
            }
            catch (Exception ex)
            {
                LoggingHelper.WriteLog(request + "  " + ex.Message);
            }

            return list;
        }
        #endregion

        #region PostFrom
        public async Task<T> PostFrom<T>(string baseAddress, string url, string requestbody)
        {
            var obj = Activator.CreateInstance(typeof(T));
            string request = baseAddress + url;
            LoggingHelper.WriteLog(request + " : " + "start");

            try
            {
                //Create the Uri
                var urirequest = new Uri(request);
                StringContent stringContent = new StringContent(requestbody, Encoding.UTF8, "application/json");
                LoggingHelper.WriteLog("start url" + request);

                using (HttpResponseMessage response = await client.PostAsync(urirequest, stringContent).ConfigureAwait(false))
                {
                    using (HttpContent content = response.Content)
                    {
                        var json = content.ReadAsStringAsync().Result;

                        LoggingHelper.WriteLog(json + " : " + "Begin Result");
                        if (response.IsSuccessStatusCode)
                        {
                            string result = await response.Content.ReadAsStringAsync();
                            obj = JsonConvert.DeserializeObject<T>(result);
                            LoggingHelper.WriteLog(request + " : " + "End Result");
                        }
                    }
                }

                LoggingHelper.WriteLog("END url" + request);
            }
            catch (Exception ex)
            {
                LoggingHelper.WriteLog(request + "  " + ex.Message);
            }

            return (T)obj;
        }
        #endregion

        #region PutFrom
        public async Task<T> PutFrom<T>(string baseAddress, string url, string requestbody)
        {
            var obj = Activator.CreateInstance(typeof(T));
            string request = baseAddress + url;
            LoggingHelper.WriteLog(request + " : " + "start");

            try
            {
                //Create the Uri
                var urirequest = new Uri(request);
                StringContent stringContent = new StringContent(requestbody, Encoding.UTF8, "application/json");
                LoggingHelper.WriteLog("start url" + request);

                using (HttpResponseMessage response = await client.PutAsync(urirequest, stringContent).ConfigureAwait(false))
                {
                    using (HttpContent content = response.Content)
                    {
                        var json = content.ReadAsStringAsync().Result;

                        LoggingHelper.WriteLog(json + " : " + "Begin Result");
                        if (response.IsSuccessStatusCode)
                        {
                            string result = await response.Content.ReadAsStringAsync();
                            obj = JsonConvert.DeserializeObject<T>(result);
                            LoggingHelper.WriteLog(request + " : " + "End Result");
                        }
                    }
                }

                LoggingHelper.WriteLog("END url" + request);
            }
            catch (Exception ex)
            {
                LoggingHelper.WriteLog(request + "  " + ex.Message);
            }

            return (T)obj;
        }
        #endregion

        #region DeleteFrom
        public async Task<T> DeleteFrom<T>(string baseAddress, string url)
        {
            var obj = Activator.CreateInstance(typeof(T));
            string request = baseAddress + url;
            LoggingHelper.WriteLog(request + " : " + "start");
            try
            {
                var urirequest = new Uri(request);
                LoggingHelper.WriteLog("start url" + request);
                using (HttpResponseMessage response = await client.DeleteAsync(urirequest).ConfigureAwait(false))
                {
                    using (HttpContent content = response.Content)
                    {
                        var json = content.ReadAsStringAsync().Result;

                        LoggingHelper.WriteLog(json + " : " + "Begin Result");
                        if (response.IsSuccessStatusCode)
                        {
                            string result = await response.Content.ReadAsStringAsync();
                            obj = JsonConvert.DeserializeObject<T>(result);
                            LoggingHelper.WriteLog(request + " : " + "End Result");
                        }
                    }
                }

                LoggingHelper.WriteLog("END url" + request);
            }
            catch (Exception ex)
            {
                LoggingHelper.WriteLog(request + "  " + ex.Message);
            }

            return (T)obj;
        }
        #endregion
    }
}
