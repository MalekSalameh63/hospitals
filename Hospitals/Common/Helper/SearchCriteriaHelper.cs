﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.Helper
{
    public class SearchCriteriaHelper
    {
        private const string _AndChar = "&";
        private const string _EqualsChar = "=";
        public static string CreateSearchQuery(IDictionary<string, object> criteria)
        {
            bool isFirstProperty = true;
            string searchQuery = string.Empty;

            if (criteria != null)
            {
                foreach (KeyValuePair<string, object> entry in criteria)
                {
                    if (entry.Value != null)
                    {
                        if (isFirstProperty)
                        {
                            searchQuery += entry.Key + _EqualsChar + entry.Value;
                            isFirstProperty = false;
                        }
                        else
                        {
                            searchQuery += _AndChar + entry.Key + _EqualsChar + entry.Value;
                        }
                    }
                }
            }

            return searchQuery;
        }

        // Converts any object to dictionary to be used in CreateSearchQuery() via serialization.
        public static string CreateSearchQueryFromObject(object payload)
        {
            try
            {
                string jsonPayload = JsonConvert.SerializeObject(payload);
                Dictionary<string, object> dictionaryPayload = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonPayload);
                return CreateSearchQuery(dictionaryPayload);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
