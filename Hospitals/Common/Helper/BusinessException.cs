﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Helper
{
    public class BusinessException : Exception
    {
        public FriendlyStatus BusinessMessage { get; set; }

        public BusinessException(FriendlyStatus businessMessage)
        {
            BusinessMessage = businessMessage;
        }
    }
}
