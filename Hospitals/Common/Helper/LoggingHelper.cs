﻿using Common.Common.Configuration;
using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

namespace Common.Helper
{
    /// <summary>
    /// Helper class for logging
    /// </summary>
    public static class LoggingHelper
    {
        #region WriteLog
        [System.Runtime.CompilerServices.MethodImpl(MethodImplOptions.Synchronized)]
        public static void WriteLog(params string[] strLogs)
        {
            try
            {
                string strPath = AppDomain.CurrentDomain.BaseDirectory + "Log/";//SharedSettings.ErrorLogPath;
                if (strLogs[0].Contains("[SUBFOLDER]"))
                    strPath += strLogs[0].Replace("[SUBFOLDER]", "");


                string[] arrPaths = new string[] { strPath };
                int[][] FileCount = null;

                FileCount = new int[1][];
                for (int i = 0; i < 1; i++)
                {
                    FileCount[i] = new int[1];
                }

                int Index = 0;
                int RegionIndex = 0;
                string strLog;

                StringBuilder Bldr = new StringBuilder();
                int _index = 0;
                if (strLogs[0].Contains("[SUBFOLDER]"))
                    _index = 1;
                for (int i = _index; i < strLogs.Length; i++)
                    Bldr.Append(strLogs[i]);
                strLog = Bldr.ToString();

                string RegionPath = arrPaths[Index];
                string fname = DateTime.Now.ToString("yyyy-MM-dd");

                if (!Directory.Exists(arrPaths[Index]))
                    Directory.CreateDirectory(arrPaths[Index]);

                if (!Directory.Exists(RegionPath))
                    Directory.CreateDirectory(RegionPath);

                if (!System.IO.File.Exists(RegionPath + fname + "_000.log"))
                    FileCount[RegionIndex][Index] = 0;

                string fileName = RegionPath + fname + "_" + FileCount[RegionIndex][Index].ToString("000") + ".log";
                System.IO.FileInfo info = new System.IO.FileInfo(fileName);

                while (info.Exists && info.Length > 1024 * 1024 * 2)
                {
                    FileCount[RegionIndex][Index]++;
                    fileName = RegionPath + fname + "_" + FileCount[RegionIndex][Index].ToString("000") + ".log";
                    info = new FileInfo(fileName);
                }

                using (StreamWriter sw = new StreamWriter(fileName, true))
                {
                    sw.WriteLine(">>" + DateTime.Now.ToLongTimeString() + "\t:" + strLog);
                    sw.Flush();
                    sw.Close();
                }
            }
            catch (NotSupportedException ex)
            {
                string errmsg = ex.Message;
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message;
            }
        }
        #endregion

    }
}
