﻿using System;
using System.Globalization;
using System.Linq;

namespace Common.Helper
{
    public static class DateHelper
    {

        /// <summary>
        /// Convert datetime to millisecends
        /// </summary>
        /// <param name="date">Date to be convert</param>
        /// <param name="time">Time to be added to date</param>
        /// <returns>Millisecends as long</returns>
        public static long GetDateMillisecends(DateTime? date, TimeSpan? time)
        {
            if (date != null & time != null)
            {
                date.Value.Add(time.Value);
                return (long)(date - new DateTime(1970, 1, 1)).Value.TotalMilliseconds;
            }
            else if (date != null)
            {
                return (long)(date - new DateTime(1970, 1, 1, 0, 0, 0)).Value.TotalMilliseconds;
            }
            else
                return 0;

        }

        /// <summary>
        /// Convert millisecends to datetime
        /// </summary>
        /// <param name="timeString">millisecends to be convert</param>
        /// <returns>DateTime</returns>
        public static DateTime GetDate(string timeString)
        {
            var date = (new DateTime(1970, 1, 1)).AddMilliseconds(Double.Parse(timeString));
            return date;
        }
        /// <summary>
        /// Convert milliseconds to datetime
        /// </summary>
        /// <param name="timeString">millisecends to be convert</param>
        /// <returns>DateTime</returns>
        public static DateTime GetDate(long timeString)
        {
            var date = (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).AddMilliseconds(timeString);
            var utcDate = DateTime.SpecifyKind(date, DateTimeKind.Utc);
            return utcDate;
        }

        /// <summary>
        /// Calculates Age from date of birth
        /// </summary>
        /// <param name="dateOfBirth"></param>
        /// <returns></returns>
        public static long GetAge(long? dateOfBirth)
        {
            if (dateOfBirth.HasValue)
            {
                return DateTime.Today.Year - GetDate(dateOfBirth.Value).Year;
            }

            return 0;
        }

        /// <summary>
        /// Extracts epoch time from E-service time slot
        /// </summary>
        /// <param name="slot"></param>
        /// <returns></returns>
        public static long AppointmentConvertDate(string slot)
        {
            var extractedSlot = slot.Split('(', ')')[1];
            return Convert.ToInt64(extractedSlot.Substring(0, extractedSlot.IndexOf('+') - 1) + "0");
        }

        public static string ConvertDateCalendar(DateTime dateConv, string calendar, string dateLangCulture)
        {
            dateLangCulture = dateLangCulture.ToLower();

            if (calendar == "Hijri" && dateLangCulture.StartsWith("en-"))
            {
                dateLangCulture = "ar-sa";
            }

            var dtFormat = new CultureInfo(dateLangCulture, false).DateTimeFormat;

            switch (calendar)
            {
                case "Hijri":
                    dtFormat.Calendar = new HijriCalendar();
                    break;

                case "Gregorian":
                    dtFormat.Calendar = new GregorianCalendar();
                    break;

                default:
                    return "";
            }

            dtFormat.ShortDatePattern = "dd/MM/yyyy";
            return (dateConv.Date.ToString("dd/MM/yyyy", dtFormat));
        }

        /// <summary>
        /// Converts Microsoft (old) epoch format to DateTimeOffset with timezone.
        /// </summary>
        /// <param name="ins"></param>
        /// <returns></returns>
        public static DateTimeOffset MsEpochToDateTimeOffset(string ins)
        {
            // UTC + timezone offset format
            if (ins.Count() == 26)
            {
                long Utime = long.Parse(ins.Substring(6, 13));
                int HourOffset = int.Parse(ins.Substring(20, 2));
                int MinuteOffset = int.Parse(ins.Substring(22, 2));
                char Offset = Convert.ToChar(ins.Substring(19, 1));
                DateTimeOffset Result = DateTimeOffset.FromUnixTimeMilliseconds(Utime);

                if (Offset == '+')
                    Result = Result.ToOffset(new TimeSpan(HourOffset, MinuteOffset, 0));
                else if (Offset == '-')
                    Result = Result.ToOffset(new TimeSpan(-HourOffset, -MinuteOffset, 0));

                return Result;
            }
            // UTC format
            else if (ins.Count() == 21)
            {
                long Utime = long.Parse(ins.Substring(6, 13));
                return DateTimeOffset.FromUnixTimeMilliseconds(Utime);
            }
            // Negative format
            else if (ins.Count() == 23)
            {
                long Utime = long.Parse(ins.Substring(7, 14));
                return DateTimeOffset.FromUnixTimeMilliseconds(-Utime);
            }
            else
                return default;
        }
    }
}
