﻿using Common.Authorization;
using Common.Clients;
using Common.Common.Configuration;
using Common.DTOs;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helper
{
    /// <summary>
    /// A generic wrapper class to REST API calls
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static class HttpClientWrapper<T> where T : class
    {
        /// <summary>
        /// For getting the resources from a web api
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <returns>A Task with result object of type T</returns>
        public static async Task<T> GetRequestAsync(MicroserviceKeyDTO microserviceKey, string baseUrl, string requestAddress)
        {
            T result = null;
            HttpClientHandler clientHandler = null;
            HttpContextAccessor httpContextAccessor = new HttpContextAccessor();
            string EMRToken = httpContextAccessor.HttpContext.Request.Headers[SystemConstants.EMRAuthorization];

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);

                using (var httpClient = new CustomHttpClient(microserviceKey, clientHandler))
                {
                    // EMR token
                    if (!string.IsNullOrEmpty(EMRToken))
                        httpClient.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                    var response = await httpClient.GetAsync(new Uri(baseUrl + requestAddress));

                    response.EnsureSuccessStatusCode();

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);
                }
            }
            else
            {
                using (var httpClient = new CustomHttpClient(microserviceKey))
                {
                    // EMR token
                    if (!string.IsNullOrEmpty(EMRToken))
                        httpClient.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                    var response = await httpClient.GetAsync(new Uri(baseUrl + requestAddress));

                    response.EnsureSuccessStatusCode();

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);
                }
            }
            return result;
        }
        /// <summary>
        /// For getting the resources from a web api
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <returns>A Task with result object of type T</returns>
        public static async Task<T> GetRequestAsyncTest(MicroserviceKeyDTO microserviceKey, string baseUrl, string requestAddress)
        {
            T result = null;
            HttpClientHandler clientHandler = null;
            //HttpContextAccessor httpContextAccessor = new HttpContextAccessor();
            //string EMRToken = httpContextAccessor.HttpContext.Request.Headers[SystemConstants.EMRAuthorization];

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);

                using (var httpClient = new CustomHttpClient(microserviceKey, clientHandler))
                {
                    // EMR token
                    //if (!string.IsNullOrEmpty(EMRToken))
                    //    httpClient.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                    var response = await httpClient.GetAsync(new Uri(baseUrl + requestAddress));

                    response.EnsureSuccessStatusCode();

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);
                }
            }
            else
            {
                using (var httpClient = new CustomHttpClient(microserviceKey))
                {
                    // EMR token
                    //if (!string.IsNullOrEmpty(EMRToken))
                    //    httpClient.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                    var response = await httpClient.GetAsync(new Uri(baseUrl + requestAddress));

                    response.EnsureSuccessStatusCode();

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);
                }
            }
            return result;
        }

        // DEV-NOTE: Not asynchronous.
        /// <summary>
        /// For getting the resources from a web api
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <returns>A Task with result object of type T</returns>
        public static async Task<T> GetRequestAsync(string baseUrl, string requestAddress)
        {
            T result = null;
            HttpContextAccessor httpContextAccessor = new HttpContextAccessor();
            string EMRToken = httpContextAccessor.HttpContext.Request.Headers[SystemConstants.EMRAuthorization];

            try
            {
                HttpClientHandler clientHandler = null;

                if (SharedSettings.SSLEnabled)
                {
                    X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                    clientHandler = new HttpClientHandler()
                    {
                        ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                        ClientCertificateOptions = ClientCertificateOption.Manual,
                        SslProtocols = SslProtocols.Tls12
                    };
                    clientHandler.ClientCertificates.Add(clientCert);

                    using (var httpClient = new CustomHttpClient(null, clientHandler))
                    {
                        // EMR Token
                        if (!string.IsNullOrEmpty(EMRToken))
                            httpClient.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                        var response = httpClient.GetAsync(new Uri(baseUrl + requestAddress)).Result;

                        response.EnsureSuccessStatusCode();

                        string content = response.Content.ReadAsStringAsync().Result;

                        result = JsonConvert.DeserializeObject<T>(content);
                    }
                }
                else
                {
                    using (var httpClient = new CustomHttpClient(null))
                    {
                        // EMR Token
                        if (!string.IsNullOrEmpty(EMRToken))
                            httpClient.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                        var response = httpClient.GetAsync(new Uri(baseUrl + requestAddress)).Result;

                        response.EnsureSuccessStatusCode();

                        string content = response.Content.ReadAsStringAsync().Result;

                        result = JsonConvert.DeserializeObject<T>(content);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                Console.Error.Write("Exception Error: " + ex.Message + " / " + " baseUrl: " + baseUrl + " / " + " requestAddress: " + requestAddress);
                return result;
            }

        }

        /// <summary>
        /// For creating a new item over a web api using POST
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <param name="postObject"></param>
        /// <returns>Response with the status of the request</returns>
        public static async Task<T> PostRequestAsync(MicroserviceKeyDTO microserviceKey, string baseUrl, string requestAddress, object postObject)
        {
            T result = null;
            HttpClientHandler clientHandler = null;
            HttpContextAccessor httpContextAccessor = new HttpContextAccessor();
            string EMRToken = httpContextAccessor.HttpContext.Request.Headers[SystemConstants.EMRAuthorization];

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);
                using (var client = new CustomHttpClient(microserviceKey, clientHandler))
                {
                    // EMR Token
                    if (!string.IsNullOrEmpty(EMRToken))
                        client.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                    string jsonPostObject = JsonConvert.SerializeObject(postObject);

                    StringContent stringContent = new StringContent(jsonPostObject, Encoding.UTF8, "application/json");

                    var response = await client.PostAsync(baseUrl + requestAddress, stringContent).ConfigureAwait(false);

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);

                }
            }
            else
            {
                using (var client = new CustomHttpClient(microserviceKey))
                {
                    // EMR Token
                    if (!string.IsNullOrEmpty(EMRToken))
                        client.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                    string jsonPostObject = JsonConvert.SerializeObject(postObject);

                    StringContent stringContent = new StringContent(jsonPostObject, Encoding.UTF8, "application/json");

                    var response = await client.PostAsync(baseUrl + requestAddress, stringContent).ConfigureAwait(false);

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);

                }
            }

            return result;
        }
        /// <summary>
        /// For creating a new item over a web api using POST
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <param name="postObject"></param>
        /// <returns>Response with the status of the request</returns>
        public static async Task<T> PostRequestAsyncTest(MicroserviceKeyDTO microserviceKey, string baseUrl, string requestAddress, object postObject)
        {
            T result = null;
            HttpClientHandler clientHandler = null;
            //HttpContextAccessor httpContextAccessor = new HttpContextAccessor();
            //string EMRToken = httpContextAccessor.HttpContext.Request.Headers[SystemConstants.EMRAuthorization];

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);
                using (var client = new CustomHttpClient(microserviceKey, clientHandler))
                {
                    // EMR Token
                    //if (!string.IsNullOrEmpty(EMRToken))
                    //    client.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                    string jsonPostObject = JsonConvert.SerializeObject(postObject);

                    StringContent stringContent = new StringContent(jsonPostObject, Encoding.UTF8, "application/json");

                    var response = await client.PostAsync(baseUrl + requestAddress, stringContent).ConfigureAwait(false);

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);

                }
            }
            else
            {
                using (var client = new CustomHttpClient(microserviceKey))
                {
                    // EMR Token
                    //if (!string.IsNullOrEmpty(EMRToken))
                    //    client.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                    string jsonPostObject = JsonConvert.SerializeObject(postObject);

                    StringContent stringContent = new StringContent(jsonPostObject, Encoding.UTF8, "application/json");

                    var response = await client.PostAsync(baseUrl + requestAddress, stringContent).ConfigureAwait(false);

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);

                }
            }

            return result;
        }

        /// <summary>
        /// For creating a new item over a web api using POST
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <param name="postObject"></param>
        /// <returns>Response with the status of the request</returns>
        public static async Task<T> PostRequestAsync(string baseUrl, string requestAddress, object postObject)
        {
            T result = null;
            HttpClientHandler clientHandler = null;
            HttpContextAccessor httpContextAccessor = new HttpContextAccessor();
            string EMRToken = httpContextAccessor.HttpContext.Request.Headers[SystemConstants.EMRAuthorization];

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);
                using (var client = new CustomHttpClient(null, clientHandler))
                {
                    // EMR Token
                    if (!string.IsNullOrEmpty(EMRToken))
                        client.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                    string jsonPostObject = JsonConvert.SerializeObject(postObject);

                    StringContent stringContent = new StringContent(jsonPostObject, Encoding.UTF8, "application/json");

                    var response = await client.PostAsync(baseUrl + requestAddress, stringContent).ConfigureAwait(false);

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);

                }
            }
            else
            {
                using (var client = new CustomHttpClient(null))
                {
                    // EMR Token
                    if (!string.IsNullOrEmpty(EMRToken))
                        client.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                    string jsonPostObject = JsonConvert.SerializeObject(postObject);

                    StringContent stringContent = new StringContent(jsonPostObject, Encoding.UTF8, "application/json");

                    var response = await client.PostAsync(baseUrl + requestAddress, stringContent).ConfigureAwait(false);

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);

                }
            }
            return result;
        }

        /// <summary>
        /// For updating an existing item over a web api using PUT
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <param name="putObject">The object to be edited</param>
        /// <returns>Response with the status of the request</returns>
        public static async Task<T> PutRequestAsync(MicroserviceKeyDTO microserviceKey, string baseUrl, string requestAddress, object putObject)
        {
            T result = null;
            HttpClientHandler clientHandler = null;
            HttpContextAccessor httpContextAccessor = new HttpContextAccessor();
            string EMRToken = httpContextAccessor.HttpContext.Request.Headers[SystemConstants.EMRAuthorization];

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);
                using (var client = new CustomHttpClient(microserviceKey, clientHandler))
                {
                    string jsonPostObject = JsonConvert.SerializeObject(putObject);

                    StringContent stringContent = new StringContent(jsonPostObject, Encoding.UTF8, "application/json");

                    var response = await client.PutAsync(baseUrl + requestAddress, stringContent).ConfigureAwait(false);

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);

                }
            }
            else
            {
                using (var client = new CustomHttpClient(microserviceKey))
                {
                    string jsonPostObject = JsonConvert.SerializeObject(putObject);

                    StringContent stringContent = new StringContent(jsonPostObject, Encoding.UTF8, "application/json");

                    var response = await client.PutAsync(baseUrl + requestAddress, stringContent).ConfigureAwait(false);

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);

                }
            }



            return result;
        }

        /// <summary>
        /// For updating an existing item over a web api using PUT
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <param name="putObject">The object to be edited</param>
        /// <returns>Response with the status of the request</returns>
        public static async Task<T> PutRequestAsync(string baseUrl, string requestAddress, object putObject)
        {
            T result = null;
            HttpClientHandler clientHandler = null;
            HttpContextAccessor httpContextAccessor = new HttpContextAccessor();
            string EMRToken = httpContextAccessor.HttpContext.Request.Headers[SystemConstants.EMRAuthorization];

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);
                using (var client = new CustomHttpClient(null, clientHandler))
                {
                    // EMR Token
                    if (!string.IsNullOrEmpty(EMRToken))
                        client.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                    string jsonPostObject = JsonConvert.SerializeObject(putObject);

                    StringContent stringContent = new StringContent(jsonPostObject, Encoding.UTF8, "application/json");

                    var response = await client.PutAsync(baseUrl + requestAddress, stringContent).ConfigureAwait(false);

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);

                }
            }
            else
            {
                using (var client = new CustomHttpClient(null))
                {
                    // EMR Token
                    if (!string.IsNullOrEmpty(EMRToken))
                        client.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                    string jsonPostObject = JsonConvert.SerializeObject(putObject);

                    StringContent stringContent = new StringContent(jsonPostObject, Encoding.UTF8, "application/json");

                    var response = await client.PutAsync(baseUrl + requestAddress, stringContent).ConfigureAwait(false);

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);

                }
            }

            return result;
        }

        /// <summary>
        /// For deleting an entry through a web api
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <returns>Response with the status of the request</returns>
        public static async Task<T> DeleteRequestAsync(MicroserviceKeyDTO microserviceKey, string baseUrl, string requestAddress)
        {
            T result = null;
            HttpClientHandler clientHandler = null;
            HttpContextAccessor httpContextAccessor = new HttpContextAccessor();
            string EMRToken = httpContextAccessor.HttpContext.Request.Headers[SystemConstants.EMRAuthorization];

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);
                using (var httpClient = new CustomHttpClient(microserviceKey, clientHandler))
                {
                    // EMR Token
                    if (!string.IsNullOrEmpty(EMRToken))
                        httpClient.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                    var response = await httpClient.DeleteAsync(new Uri(baseUrl + requestAddress));

                    response.EnsureSuccessStatusCode();

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);
                }
            }
            else
            {

                using (var httpClient = new CustomHttpClient(microserviceKey))
                {
                    // EMR Token
                    if (!string.IsNullOrEmpty(EMRToken))
                        httpClient.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                    var response = await httpClient.DeleteAsync(new Uri(baseUrl + requestAddress));

                    response.EnsureSuccessStatusCode();

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);
                }
            }

            return result;
        }

        /// <summary>
        /// For deleting an entry through a web api
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <returns>Response with the status of the request</returns>
        public static async Task<T> DeleteRequestAsync(string baseUrl, string requestAddress)
        {
            T result = null;
            HttpClientHandler clientHandler = null;
            HttpContextAccessor httpContextAccessor = new HttpContextAccessor();
            string EMRToken = httpContextAccessor.HttpContext.Request.Headers[SystemConstants.EMRAuthorization];

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);
                using (var httpClient = new CustomHttpClient(null, clientHandler))
                {
                    // EMR Token
                    if (!string.IsNullOrEmpty(EMRToken))
                        httpClient.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                    var response = await httpClient.DeleteAsync(new Uri(baseUrl + requestAddress));

                    response.EnsureSuccessStatusCode();

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);
                }
            }
            else
            {
                using (var httpClient = new CustomHttpClient(null))
                {
                    // EMR Token
                    if (!string.IsNullOrEmpty(EMRToken))
                        httpClient.DefaultRequestHeaders.Add(SystemConstants.EMRAuthorization, EMRToken);

                    var response = await httpClient.DeleteAsync(new Uri(baseUrl + requestAddress));

                    response.EnsureSuccessStatusCode();

                    string content = await response.Content.ReadAsStringAsync();

                    result = JsonConvert.DeserializeObject<T>(content);
                }
            }


            return result;
        }

        /// <summary>
        /// For getting the resources from a web api
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <returns>A method with result object of type T</returns>
        public static T GetRequest(MicroserviceKeyDTO microserviceKey, string baseUrl, string requestAddress)
        {
            T result = null;
            HttpClientHandler clientHandler = null;

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);
                using (var httpClient = new CustomHttpClient(microserviceKey, clientHandler))
                {
                    var response = httpClient.GetAsync(new Uri(baseUrl + requestAddress)).Result;

                    response.EnsureSuccessStatusCode();

                    string content = response.Content.ReadAsStringAsync().Result;

                    result = JsonConvert.DeserializeObject<T>(content);
                }
            }
            else
            {
                using (var httpClient = new CustomHttpClient(microserviceKey))
                {
                    var response = httpClient.GetAsync(new Uri(baseUrl + requestAddress)).Result;

                    response.EnsureSuccessStatusCode();

                    string content = response.Content.ReadAsStringAsync().Result;

                    result = JsonConvert.DeserializeObject<T>(content);
                }
            }


            return result;
        }

        /// <summary>
        /// For getting the resources from a web api
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <returns>A method with result object of type T</returns>
        public static T GetRequest(string baseUrl, string requestAddress)
        {
            T result = null;
            HttpClientHandler clientHandler = null;

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);

                using (var httpClient = new CustomHttpClient(null, clientHandler))
                {
                    var response = httpClient.GetAsync(new Uri(baseUrl + requestAddress)).Result;

                    response.EnsureSuccessStatusCode();

                    string content = response.Content.ReadAsStringAsync().Result;

                    result = JsonConvert.DeserializeObject<T>(content);
                }
            }
            else
            {

                using (var httpClient = new CustomHttpClient(null))
                {
                    var response = httpClient.GetAsync(new Uri(baseUrl + requestAddress)).Result;

                    response.EnsureSuccessStatusCode();

                    string content = response.Content.ReadAsStringAsync().Result;

                    result = JsonConvert.DeserializeObject<T>(content);
                }
            }


            return result;
        }

        /// <summary>
        /// For creating a new item over a web api using POST
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <param name="postObject">The object to be created</param>
        /// <returns>Response with the status of the request</returns>
        public static void PostRequest(MicroserviceKeyDTO microserviceKey, string baseUrl, string requestAddress, T postObject)
        {
            HttpClientHandler clientHandler = null;

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);

                using (var client = new CustomHttpClient(microserviceKey, clientHandler))
                {
                    string jsonPostObject = JsonConvert.SerializeObject(postObject);

                    StringContent stringContent = new StringContent(jsonPostObject, Encoding.UTF8, "application/json");

                    var response = client.PostAsync(baseUrl + requestAddress, stringContent).Result;

                    response.EnsureSuccessStatusCode();
                }
            }
            else
            {
                using (var client = new CustomHttpClient(microserviceKey))
                {
                    string jsonPostObject = JsonConvert.SerializeObject(postObject);

                    StringContent stringContent = new StringContent(jsonPostObject, Encoding.UTF8, "application/json");

                    var response = client.PostAsync(baseUrl + requestAddress, stringContent).Result;

                    response.EnsureSuccessStatusCode();
                }
            }

        }

        /// <summary>
        /// For creating a new item over a web api using POST
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <param name="postObject">The object to be created</param>
        /// <returns>Response with the status of the request</returns>
        public static void PostRequest(string baseUrl, string requestAddress, T postObject)
        {
            HttpClientHandler clientHandler = null;

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);
                using (var client = new CustomHttpClient(null, clientHandler))
                {
                    string jsonPostObject = JsonConvert.SerializeObject(postObject);

                    StringContent stringContent = new StringContent(jsonPostObject, Encoding.UTF8, "application/json");

                    var response = client.PostAsync(baseUrl + requestAddress, stringContent).Result;

                    response.EnsureSuccessStatusCode();
                }
            }
            else
            {
                using (var client = new CustomHttpClient(null))
                {
                    string jsonPostObject = JsonConvert.SerializeObject(postObject);

                    StringContent stringContent = new StringContent(jsonPostObject, Encoding.UTF8, "application/json");

                    var response = client.PostAsync(baseUrl + requestAddress, stringContent).Result;

                    response.EnsureSuccessStatusCode();
                }
            }
        }

        /// <summary>
        /// For updating an existing item over a web api using PUT
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <param name="putObject">The object to be edited</param>
        public static void PutRequest(MicroserviceKeyDTO microserviceKey, string baseUrl, string requestAddress, T putObject)
        {
            HttpClientHandler clientHandler = null;

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);
                using (var client = new CustomHttpClient(microserviceKey, clientHandler))
                {
                    string jsonPutObject = JsonConvert.SerializeObject(putObject);

                    StringContent stringContent = new StringContent(jsonPutObject, Encoding.UTF8, "application/json");

                    var response = client.PutAsync(baseUrl + requestAddress, stringContent).Result;

                    response.EnsureSuccessStatusCode();

                }
            }
            else
            {
                using (var client = new CustomHttpClient(microserviceKey))
                {
                    string jsonPutObject = JsonConvert.SerializeObject(putObject);

                    StringContent stringContent = new StringContent(jsonPutObject, Encoding.UTF8, "application/json");

                    var response = client.PutAsync(baseUrl + requestAddress, stringContent).Result;

                    response.EnsureSuccessStatusCode();

                }
            }

        }

        /// <summary>
        /// For updating an existing item over a web api using PUT
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <param name="putObject">The object to be edited</param>
        public static void PutRequest(string baseUrl, string requestAddress, T putObject)
        {
            HttpClientHandler clientHandler = null;

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);

                using (var client = new CustomHttpClient(null, clientHandler))
                {
                    string jsonPutObject = JsonConvert.SerializeObject(putObject);

                    StringContent stringContent = new StringContent(jsonPutObject, Encoding.UTF8, "application/json");

                    var response = client.PutAsync(baseUrl + requestAddress, stringContent).Result;

                    response.EnsureSuccessStatusCode();

                }
            }
            else
            {
                using (var client = new CustomHttpClient(null))
                {
                    string jsonPutObject = JsonConvert.SerializeObject(putObject);

                    StringContent stringContent = new StringContent(jsonPutObject, Encoding.UTF8, "application/json");

                    var response = client.PutAsync(baseUrl + requestAddress, stringContent).Result;

                    response.EnsureSuccessStatusCode();

                }
            }
        }

        /// <summary>
        /// For deleting an existing item over a web api using Delete
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <returns>Response with the status of the request</returns>
        public static T DeleteRequest(MicroserviceKeyDTO microserviceKey, string baseUrl, string requestAddress)
        {
            T result = null;

            HttpClientHandler clientHandler = null;

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);
                using (var httpClient = new CustomHttpClient(microserviceKey, clientHandler))
                {
                    var response = httpClient.DeleteAsync(new Uri(baseUrl + requestAddress)).Result;

                    response.EnsureSuccessStatusCode();

                    string content = response.Content.ReadAsStringAsync().Result;

                    result = JsonConvert.DeserializeObject<T>(content);
                }

            }
            else
            {
                using (var httpClient = new CustomHttpClient(microserviceKey))
                {
                    var response = httpClient.DeleteAsync(new Uri(baseUrl + requestAddress)).Result;

                    response.EnsureSuccessStatusCode();

                    string content = response.Content.ReadAsStringAsync().Result;

                    result = JsonConvert.DeserializeObject<T>(content);
                }

            }

            return result;
        }

        /// <summary>
        /// For deleting an existing item over a web api using Delete
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="requestAddress"></param>
        /// <returns>Response with the status of the request</returns>
        public static T DeleteRequest(string baseUrl, string requestAddress)
        {
            T result = null;

            HttpClientHandler clientHandler = null;

            if (SharedSettings.SSLEnabled)
            {
                X509Certificate2 clientCert = new X509Certificate2(SharedSettings.SSLCertificate.CertificateName, SharedSettings.SSLCertificate.Password);
                clientHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (sender, clientCert, chain, sslPolicyErrors) => { return true; },
                    ClientCertificateOptions = ClientCertificateOption.Manual,
                    SslProtocols = SslProtocols.Tls12
                };
                clientHandler.ClientCertificates.Add(clientCert);

                using (var httpClient = new CustomHttpClient(null, clientHandler))
                {
                    var response = httpClient.DeleteAsync(new Uri(baseUrl + requestAddress)).Result;

                    response.EnsureSuccessStatusCode();

                    string content = response.Content.ReadAsStringAsync().Result;

                    result = JsonConvert.DeserializeObject<T>(content);
                }
            }
            else
            {
                using (var httpClient = new CustomHttpClient(null))
                {
                    var response = httpClient.DeleteAsync(new Uri(baseUrl + requestAddress)).Result;

                    response.EnsureSuccessStatusCode();

                    string content = response.Content.ReadAsStringAsync().Result;

                    result = JsonConvert.DeserializeObject<T>(content);
                }
            }
            return result;
        }
    }
}
