﻿namespace Common.Common
{
    public class VideoCallSummaryTemplate
    {
        public string Name { get; set; }
        public string Amount { get; set; }
        public string Tax { get; set; }
        public string Total { get; set; }
        public string Date { get; set; }
    }
}
