﻿namespace Common.Common
{
    public static class MicroserviceIdentification
    {
        public static string Name { get; set; }
        public static string Id { get; set; }
        public static string Environment { get; set; }
    }
}
