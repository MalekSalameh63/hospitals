﻿using System;
using Common.VOs;

namespace Common.Common
{
    public class EmailActivationRequest
    {

        public EmailVo Email { get; set; }
        public TextVO Token { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? NumberOfSentEmails { get; set; }
    }
}
