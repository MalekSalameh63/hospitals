﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Common.Common
{
    public class TransactionFeedback
    {
        [JsonProperty("amount")]
        public float Amount { get; set; }

        [JsonProperty("response_code")]
        public string ResponseCode { get; set; }

        [JsonProperty("card_number")]
        public string CardNumber { get; set; }

        [JsonProperty("card_holder_name")]
        public string CardHolderName { get; set; }

        [JsonProperty("signature")]
        public string Signature { get; set; }

        [JsonProperty("payment_option")]
        public string PaymentOption { get; set; }

        [JsonProperty("expiry_date")]
        public long ExpiryDate { get; set; }

        [JsonProperty("customer_ip")]
        public string CustomerIp { get; set; }

        [JsonProperty("eci")]
        public string Eci { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; }

        [Key]
        [JsonProperty("fort_id")]
        public string FortId { get; set; }

        [JsonProperty("command")]
        public string Command { get; set; }
        [JsonProperty("merchant_extra")]
        public string MerchantExtra { get; set; }

        [JsonProperty("response_message")]
        public string ResponseMessage { get; set; }

        [JsonProperty("sdk_token")]
        public string SdkToken { get; set; }

        [JsonProperty("authorization_code")]
        public long AuthorizationCode { get; set; }

        [JsonProperty("merchant_reference")]
        public string MerchantReference { get; set; }

        [JsonProperty("customer_email")]
        public string CustomerEmail { get; set; }

        [JsonProperty("token_name")]
        public string TokenName { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
