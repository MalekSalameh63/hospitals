﻿namespace Common.Common
{
    public class ChangePassword
    {
        public string UserId { get; set; }
        public string OldPassword { set; get; }
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }

    }
}
