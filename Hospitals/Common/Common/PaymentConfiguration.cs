﻿using Newtonsoft.Json;

namespace Common.Common
{
    public class PaymentConfiguration
    {
        [JsonProperty("language")]
        public string Language { get; set; }
        [JsonProperty("merchant_identifier")]
        public string MerchantIdentifier { get; set; }
        [JsonProperty("access_code")]
        public string AccessCode { get; set; }
        [JsonProperty("sha_request_phrase")]
        public string ShaRequestPhrase { get; set; }
        [JsonProperty("currency")]
        public string Currency { get; set; }
        [JsonProperty("customer_email")]
        public string CustomerEmail { get; set; }
        [JsonProperty("merchant_reference")]
        public string MerchantReference { get; set; }
        [JsonProperty("currency_code")]
        public int CurrencyCode { get; set; }
        [JsonProperty("currency_label")]
        public string CurrencyLabel { get; set; }
    }
}
