﻿using System;

namespace Common.Common
{
    public class BaseModel
    {
        public Guid Id { get; set; }
        public string CreationUser { get; set; }
        public DateTime CreationDate { get; set; }
        public string ModificationUser { get; set; }
        public DateTime? ModificationDate { get; set; }
    }
}
