﻿using System.Collections.Generic;

namespace Common.Common
{
    public static class CachingSettingsDto
    {
        public static bool CacheEnabled { get; set; }
        public static List<CachingProfileConfiguration> CachingProfiles { get; set; }
    }
    public class CachingSettings
    {
        public bool CacheEnabled { get; set; }
        public List<CachingProfileConfiguration> CachingProfiles { get; set; }
    }
}
