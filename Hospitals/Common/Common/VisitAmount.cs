﻿using Newtonsoft.Json;

namespace Common.Common
{
    public class VisitAmount
    {
        [JsonProperty("cost")]
        public float Cost { get; set; }
        [JsonProperty("pay_value")]

        public string PayValue { get; set; }
    }
}
