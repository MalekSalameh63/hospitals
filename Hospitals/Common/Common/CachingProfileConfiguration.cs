﻿using Microsoft.AspNetCore.Mvc;

namespace Common.Common
{
    public class CachingProfileConfiguration
    {
        public string ProfileName { get; set; }
        public int Duration { get; set; }
        public ResponseCacheLocation Location { get; set; }
        public string[] VaryByQueryKeys { get; set; }
        public bool Enabled { get; set; }
    }
}
