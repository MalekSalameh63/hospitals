﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Common
{
    public class AppointmentPrice
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public long ClinicId { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }

    }
}
