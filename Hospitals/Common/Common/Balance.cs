﻿using Common.VOs;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Common.Common
{
    public class Balance
    {
        public Balance()
        {
        }

        public Balance(string walletId, float amount, float reserved, string currency)
        {
            WalletId = walletId;
            Amount = amount;
            Reserved = reserved;
            Currency = new CurrencyVo(currency).Currency;
        }

        [Key]
        public int Id { get; set; }
        public string Currency { get; set; }
        public float Amount { get; set; }
        public float Reserved { get; set; }
        public string WalletId { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [ForeignKey("WalletId")]
        public Wallet Wallet { get; set; }
    }
}
