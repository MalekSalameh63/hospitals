﻿using System;

namespace Common.Common
{
    public class PromotionCodeRequest
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}
