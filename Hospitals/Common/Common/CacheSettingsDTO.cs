﻿using Common.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class CacheSettingsDTO
    {
        [JsonProperty("application_identifier")]
        public ApplicationIdentityDTO ApplicationIdentifier { get; set; }

        [JsonProperty("cache_enabled")]
        public bool CacheEnabled { get; set; }

        [JsonProperty("caching_profiles")]
        public List<CachingProfileConfiguration> CachingProfiles { get; set; }
    }
}
