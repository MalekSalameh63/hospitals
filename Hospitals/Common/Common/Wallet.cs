﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Common.Common
{
    public class Wallet
    {
        [Key]
        public string UserId { get; set; }
        public Balance Balance { get; set; }
        public float Points { get; set; }
        public DateTime LastUpdate { get; set; }
        [JsonIgnore]
        public List<Transaction> Transactions { get; set; }
    }
}
