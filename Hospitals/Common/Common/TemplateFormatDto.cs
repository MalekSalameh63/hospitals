﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Common.Common
{
    public class TemplateFormatDto
    {
        [BsonId]
        public Guid Id { get; set; }
        public int TemplateId { get; set; }
        public string TemplateName { get; set; }
        public string Title { get; set; }
        public string Language { get; set; }
        public string EmailTemplate { get; set; }
        public string SmsTemplate { get; set; }
        public string NotificationTemplate { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
