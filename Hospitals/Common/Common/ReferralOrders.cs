﻿using Common.DTOs.EMR.MedicalRecord.Prescription;
using Common.DTOs.EMR.MedicalRecord.Procedure;
using System.Collections.Generic;

namespace Common.Common
{
    public class ReferralOrders
    {
        public List<OrderedProceduresResponse> LabOrders { get; set; }
        public List<OrderedProceduresResponse> RadiologyOrder { get; set; }
        public List<Prescription> Prescriptions { get; set; }
    }
}
