﻿using Common.Enums;
using Common.Interfaces.Shared;
using Newtonsoft.Json;

namespace Common.Common
{
    public class ResponseResult<T> : IResponseResult<T>
    {
        private ResponseResult()
        { }

        [JsonProperty("global_status")]
        public GlobalStatus GlobalStatus { get; set; }

        [JsonProperty("api_status")]
        public ApiStatus ApiStatus { get; set; }

        public FriendlyStatus BusinessError { get; set; }

        [JsonProperty("exception")]
        public string ExceptionMessage { get; set; }

        [JsonProperty("data")]
        public T Data { get; set; }

        public ResponseResult(GlobalStatus globalStatus, ApiStatus apiStatus, T data, FriendlyStatus businessError, string exceptionMessage = null)
        {
            GlobalStatus = globalStatus;
            ApiStatus = apiStatus;
            BusinessError = businessError;
            Data = data;
            ExceptionMessage = exceptionMessage == null ? null : $"Microservice Name : {MicroserviceIdentification.Name} => {exceptionMessage} ";
        }

        /// <summary>
        /// Checks if GlobalStatus is success, ApiStatus is ok, and data was returned. Otherwise, return false.
        /// </summary>
        /// <returns></returns>
        public bool AllGreen()
        {
            if (this.GlobalStatus == GlobalStatus.Success &&
                this.ApiStatus == ApiStatus.Ok &&
                this.Data != null)
                return true;
            else
                return false;
        }

    }
}
