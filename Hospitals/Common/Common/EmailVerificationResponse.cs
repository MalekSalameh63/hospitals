﻿namespace Common.Common
{
    public class EmailVerificationResponse
    {
        public string Link { get; set; }
    }
}
