﻿namespace Common.Common
{
    public class Attachments
    {
        public string Name { get; set; }
        public string Content { get; set; }
        public string MimeType { get; set; }
    }
}
