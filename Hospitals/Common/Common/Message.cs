﻿using Common.DTOs;
using Newtonsoft.Json;
using System;

namespace Common.Common
{
    public class Message
    {
        [JsonProperty("registration_ids")]
        public string[] RegistrationIds { get; }
        public Notification Notification { get; }
        public Data Data { get; }
        private Message()
        {

        }
        public Message(NotificationMessageDto notificationMessageDto)
        {
            if (notificationMessageDto.RegistrationIds != null)
            {
                if (notificationMessageDto.RegistrationIds.Length <= 0) throw new Exception("Device token in required");
            }
            RegistrationIds = notificationMessageDto.RegistrationIds;

            var text = notificationMessageDto.NotificationDto.Text;
            var clickAction = notificationMessageDto.NotificationDto.ClickAction;
            var icon = notificationMessageDto.NotificationDto.Icon;
            var androidChannelId = notificationMessageDto.NotificationDto.AndroidChannelId;
            Notification = new Notification(notificationMessageDto.NotificationDto.Title, text, clickAction, icon, androidChannelId);

            var pageName = notificationMessageDto.NotificationDataDto.PageName;
            var userId = notificationMessageDto.NotificationDataDto.UserId;
            var creationDate = notificationMessageDto.NotificationDataDto.CreationDate;
            Data = new Data(creationDate, pageName, userId);
        }
    }
    public class Notification
    {
        [JsonProperty("title")]
        public string Title { get; private set; }

        [JsonProperty("text")]
        public string Text { get; private set; }

        [JsonProperty("click_action")]
        public string ClickAction { get; private set; }

        [JsonProperty("icon")]
        public string Icon { get; private set; }

        [JsonProperty("android_channel_id")]
        public string AndroidChannelId { get; private set; }

        private Notification()
        {

        }
        public Notification(string notificationTitle, string notificationText, string notificationClickAction, string notificationIconUrl, string notificationAndroidChannelId)
        {
            Title = notificationTitle;
            Text = notificationText;
            ClickAction = notificationClickAction;
            Icon = notificationIconUrl;
            AndroidChannelId = notificationAndroidChannelId;
        }
    }
    public class Data
    {
        [JsonProperty("page_name")]
        public string PageName { get; private set; }
        public string UserName { get; private set; }
        public string CreationDate { get; private set; }
        public string UserId { get; private set; }
        public Data(string creationDate, string pageName, string userId = null)
        {
            CreationDate = creationDate;
            PageName = pageName;
            UserId = userId;
        }
    }


}