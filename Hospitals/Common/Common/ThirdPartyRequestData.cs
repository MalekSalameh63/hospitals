﻿using System;

namespace Common.Common
{
    public class ThirdPartyRequestData
    {
        public DateTime RequestTime { get; set; }
        public object Request { get; set; }
        public object Response { get; set; }
        public string RequestUrl { get; set; }
    }
}
