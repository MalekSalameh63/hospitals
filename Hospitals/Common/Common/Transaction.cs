﻿using Common.DTOs;
using Common.Enums;
using Common.Helper;
using Common.VOs;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Common.Common
{
    public class Transaction
    {
        public Transaction()
        {
        }

        public Transaction(TransactionRequestDto transactionRequestDto, TransactionType transactionType)
        {
            Date = DateHelper.GetDateMillisecends(DateTime.UtcNow, DateTime.Now.TimeOfDay);
            TransactionType = transactionType;
            Amount = transactionRequestDto.Amount;
            WalletId = transactionRequestDto.UserId;
            Currency = new CurrencyVo(transactionRequestDto.Currency).Currency;
            PaymentMethod = transactionRequestDto.PaymentMethod;
            ServiceType = transactionRequestDto.ServiceType;
            ServiceId = transactionRequestDto.ServiceId;
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public string Id { get; set; }
        public long? Date { get; set; }
        public TransactionType TransactionType { get; set; }
        public float Amount { get; set; }
        public string Currency { get; set; }
        public float AvailableBalance { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public ServiceType ServiceType { get; set; }
        public string ServiceId { get; set; }
        public string WalletId { get; set; }
        [ForeignKey("WalletId")]
        [JsonIgnore]
        [IgnoreDataMember]
        public Wallet Wallet { get; set; }

        public TransactionResponseDto GenerateResponseDto()
        {
            return new TransactionResponseDto()
            {
                Id = this.Id,
                Date = this.Date,
                Amount = this.Amount,
                Currency = this.Currency,
                PaymentMethod = this.PaymentMethod.ToString(),
                TransactionType = this.TransactionType.ToString(),
                Balance = this.AvailableBalance,
                UserId = this.WalletId,
                ServiceId = this.ServiceId,
                ServiceType = this.ServiceType
            };
        }
    }
}
