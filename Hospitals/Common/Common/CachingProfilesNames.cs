﻿namespace Common.Common
{
    public static class CachingProfilesNames
    {
        #region Controllers
        public const string AppSettingsController = "AppSettingsController";
        public const string BusinessValidationController = "BusinessValidationController";
        public const string ChiefComplaintController = "ChiefComplaintController";
        public const string ComplexLookupsController = "ComplexLookupsController";
        public const string ConfigurationController = "ConfigurationController";
        public const string DictionaryController = "DictionaryController";
        public const string EmailController = "EmailController";
        public const string EmrController = "EmrController";
        public const string FileUploadController = "FileUploadController";
        public const string InAppNotificationController = "InAppNotificationController";
        public const string LookupController = "LookupController";
        public const string MyWalletController = "MyWalletController";
        public const string PatientController = "PatientController";
        public const string PaymentController = "PaymentController";
        public const string PhysicianController = "PhysicianController";
        public const string PricingController = "PricingController";
        public const string PushNotificationController = "PushNotificationController";
        public const string RolesController = "RolesController";
        public const string SmsController = "SmsController";
        public const string UsersController = "UsersController";
        #endregion

        #region ConfigurationService
        public const string ConfigurationServiceForms = "ConfigurationServiceForms";
        public const string ConfigurationServiceScreen = "ConfigurationServiceScreen";
        public const string ConfigurationServicePatientWaitingCall = "ConfigurationServicePatientWaitingCall";
        public const string ConfigurationServicePhysicianWaitingCall = "ConfigurationServicePhysicianWaitingCall";
        public const string ConfigurationServicePhysicianInfo = "ConfigurationServicePhysicianInfo";
        public const string ConfigurationServicePatientQueue = "ConfigurationServicePatientQueue";
        public const string ConfigurationServiceNotificationPage = "ConfigurationServiceNotificationPage";
        public const string ConfigurationServiceWorkflow = "ConfigurationServiceWorkflow";
        public const string ConfigurationServiceNavigationPanels = "ConfigurationServiceNavigationPanels";
        public const string ConfigurationServiceInformativeScreens = "ConfigurationServiceInformativeScreens";
        public const string ConfigurationServicePaymentMethodScreen = "ConfigurationServicePaymentMethodScreen";
        public const string ConfigurationServiceWalletScreen = "ConfigurationServiceWalletScreen";
        public const string ConfigurationServicePhysicianWalletScreen = "ConfigurationServicePhysicianWalletScreen";
        public const string ConfigurationServiceInitiatePatientCallScreen = "ConfigurationServiceInitiatePatientCallScreen";
        public const string ConfigurationServiceInitiatePhysicianWaitingCall = "ConfigurationServiceInitiatePhysicianWaitingCall";
        public const string ConfigurationServiceApplicationSettings = "ConfigurationServiceApplicationSettings";
        public const string ConfigurationServicePaymentSummaryScreen = "ConfigurationServicePaymentSummaryScreen";
        public const string ConfigurationServiceTransactionDetailsScreen = "ConfigurationServiceTransactionDetailsScreen";
        public const string ConfigurationServiceDoctorsListScreen = "ConfigurationServiceDoctorsListScreen";
        public const string ConfigurationServiceCallAssessmentScreen = "ConfigurationServiceCallAssessmentScreen";
        public const string ConfigurationServiceAppointmentHistoryScreen = "ConfigurationServiceAppointmentHistoryScreen";
        public const string ConfigurationServiceAppointmentHomeScreen = "ConfigurationServiceAppointmentHomeScreen";
        public const string ConfigurationServiceAppointmentConfirmationScreen = "ConfigurationServiceAppointmentConfirmationScreen";
        public const string ConfigurationServiceClinicsScreen = "ConfigurationServiceClinicsScreen";
        public const string ConfigurationServiceDoctorsListFilterScreen = "ConfigurationServiceDoctorsListFilterScreen";
        public const string ConfigurationServicePatientCallSummary = "ConfigurationServicePatientCallSummary";
        public const string ConfigurationServicePhysicianAvailableDates = "ConfigurationServicePhysicianAvailableDates";
        public const string ConfigurationServicePhysicianInfoScreen = "ConfigurationServicePhysicianInfoScreen";
        public const string ConfigurationServiceSoap = "ConfigurationServiceSoap";
        public const string ConfigurationServiceUpcomingAppointmentsScreen = "ConfigurationServiceUpcomingAppointmentsScreen";
        public const string ConfigurationServiceUsersSettingScreen = "ConfigurationServiceUsersSettingScreen";
        public const string ConfigurationServicePhysicianCallAssessmentScreen = "ConfigurationServicePhysicianCallAssessmentScreen";

        #endregion

        #region ComplexLookups

        public const string ComplexLookupsCountry = "ComplexLookupsCountry";
        public const string ComplexLookupsCulture = "ComplexLookupsCulture";
        public const string ComplexLookupsCurrency = "ComplexLookupsCurrency";
        public const string ComplexLookupsDateFormat = "ComplexLookupsDateFormat";
        public const string ComplexLookupsLanguage = "ComplexLookupsLanguage";
        public const string ComplexLookupsMonthNameFormat = "ComplexLookupsMonthNameFormat";
        public const string ComplexLookupsNumeric = "ComplexLookupsNumeric";
        public const string ComplexLookupsSpecialty = "ComplexLookupsSpecialty";
        public const string ComplexLookupsUnit = "ComplexLookupsUnit";
        public const string ComplexLookupsCity = "ComplexLookupsCity";
        public const string ComplexLookupsRank = "ComplexLookupsRank";
        public const string ComplexLookupsArea = "ComplexLookupsArea";

        #endregion

        #region Emr
        public const string GetMasterData = "GetMasterData";
        #endregion

        #region Appointment
        public const string SearchDoctorByTime = "SearchDoctorByTime";
        public const string GetClinics = "GetClinics";
        #endregion

    }
}