﻿namespace Common.Common
{
    public class EmailActivationBody
    {
        public string Link { get; set; }
    }
}
