﻿namespace Common.Common
{
    public class AppointmentsConfiguration
    {
        public long PatientOutSA { get; set; }
        public string activationCode { get; set; }
        public bool IsSilentLogin { get; set; }
        public long PatientMobileNumber { get; set; }
        public string ZipCode { get; set; }
        public int SearchType { get; set; }
        public string PatientIdentificationID { get; set; }
        public bool isRegister { get; set; }
        public double VersionID { get; set; }
        public int Channel { get; set; }
        public int LanguageID { get; set; }
        public string IPAdress { get; set; }
        public string generalid { get; set; }
        public string SessionID { get; set; }
        public bool isDentalAllowedBackend { get; set; }
        public int DeviceTypeID { get; set; }
        public int ClinicId { get; set; }
        public long PatientID { get; set; }
        public long DoctorId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public bool ContinueDentalPlan { get; set; }
        public bool IsSearchAppointmnetByClinicID { get; set; }
        public bool IsGetNearAppointment { get; set; }
        public bool License { get; set; }
        public int ProjectID { get; set; }
        public int OriginalClinicID { get; set; }
        public int days { get; set; }
        public bool isReschadual { get; set; }
        public int Age { get; set; }
        public int PatientTypeID { get; set; }
        public int PatientType { get; set; }
        public bool IsForLiveCare { get; set; }
        public int InitialSlotDuration { get; set; }
        public bool IsVirtualCare { get; set; }
        public string DeviceType { get; set; }
        public int VisitType { get; set; }
        public int VisitFor { get; set; }
        public int BookedBy { get; set; }
        public int IsHijri { get; set; }
        public int Region { get; set; }
        public string SetupID { get; set; }
        public string MasterKey { get; set; }
        public long CompanyId { get; set; }
        public int SubCategoryId { get; set; }


    }
}
