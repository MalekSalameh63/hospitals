﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Common.Configuration
{
    public class OpenTokSettings
    {
        public int ApiKey { get; set; }
        public string ApiSecret { get; set; }
        public int CallDuration { get; set; }
        public int WarningDuration { get; set; }

    }
}
