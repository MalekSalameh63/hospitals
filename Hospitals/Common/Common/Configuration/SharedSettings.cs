﻿using Common.DTOs;
using Common.VOs;

namespace Common.Common.Configuration
{
    public static class SharedSettings
    {
        public static string MongoConnection { get; set; }
        public static string MongoDatabaseName { get; set; }
        public static string Secret { get; set; }
        public static string ErrorLogPath { get; set; }
        public static string RedisConnection { get; set; }
        public static int RedisDatabaseId { get; set; }
        public static string DictionaryServiceBaseUrl { get; set; }
        public static string LookupServiceBaseUrl { get; set; }
        public static string ComplexLookupServiceBaseUrl { get; set; }
        public static string UserMicroserviceBaseUrl { get; set; }
        public static string EMRMicroserviceBaseUrl { get; set; }
        public static bool DirectLogin { get; set; }
        public static bool SessionEnabled { get; set; }
        public static bool EMRSessionEnabled { get; set; }
        public static bool CacheEnabled { get; set; }
        public static int CacheDuration { get; set; }
        public static string PostgresConnectionString { get; set; }
        public static string WebEMRUrl { get; set; }
        public static MicroserviceEncryptionDTO MicroserviceEncryption { get; set; }
        public static SSLCertificateVO SSLCertificate { get; set; }
        public static bool SSLEnabled { get; set; }
        public static bool ElasticApmEnable { get; set; }

    }
}
