﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Common.Configuration
{
    public static class ContextInfo
    {
        public static string Language { get; set; }  
        public static string UserId { get; set; }
        public static int? UserTypeId { get; set; }
    }
}
