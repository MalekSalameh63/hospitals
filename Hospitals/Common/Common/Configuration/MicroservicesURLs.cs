﻿using Common.DTOs;

namespace Common.Common.Configuration
{
    /// <summary>
    /// Class MicroservicesURLs.
    /// </summary>
    public class MicroservicesURLs
    {
        public string NotificationApiUrl { get; set; }
        public string ConfigurationApiUrl { get; set; }
        public string EmailApiUrl { get; set; }
        public string EmailActivationUrl { get; set; }
        public string SMSApiUrl { get; set; }
        public string PatientApiUrl { get; set; }
        public string PhysicianApiUrl { get; set; }
        public string UserApiUrl { get; set; }
        public string DictionaryApiUrl { get; set; }
        public string OtpVerificationUrl { get; set; }
        public string ChiefComplaintUr { get; set; }
        public string FileUploadUrl { get; set; }
        public string WebSocketUrl { get; set; }
        public string MyWalletUrl { get; set; }
        public string LookupApiUrl { get; set; }
        public string EmrUrl { get; set; }
        public string PromotionCodeUrl { get; set; }
        public string ComplexLookupApiUrl { get; set; }
        public string TemplateFormatUrl { get; set; }
    }
}
