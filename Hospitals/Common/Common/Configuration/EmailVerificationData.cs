﻿namespace Common.Common.Configuration
{
    public class EmailVerificationData
    {
        public static int ActivationRequestsToBlock { get; set; }
        public static int EmailActivationLinkExpiration { get; set; }
        public static int UnBlockUserPeriod { get; set; }
        public static string EncryptionKey { get; set; }
        public static string EmailActivationLink { get; set; }
        public static string SendEmailBaseUrl { get; set; }
        public static int EmailAllowedPeriodInDays { get; set; }     
    }
}
