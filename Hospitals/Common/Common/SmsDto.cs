﻿namespace Common.Common
{
    public class SmsDto
    {
        public string Message { get; set; }
        public string[] To { get; set; }
    }
}
