﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace Common.Common
{
    public class Visit
    {
        [BsonId]
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public string Mrn { get; set; }
        public string Gender { get; set; }
        public string DoctorName { get; set; }
        public DateTime VisitDate { get; set; }
        public string ChiefComplaint { get; set; }
        public string TransactionId { get; set; }

    }
}
