﻿namespace Common.Common
{
    public class AwsS3ServiceConfiguration
    {
        public static string BucketName { get; set; }
        public static string AwsAccessKeyId { get; set; }
        public static string AwsSecretAccessKey { get; set; }
    }
}
