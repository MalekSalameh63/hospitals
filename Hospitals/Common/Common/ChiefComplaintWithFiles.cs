﻿using Common.DTOs.File;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.Common
{
    public class ChiefComplaintWithFilesDto
    {
        public ChiefComplaintWithFiles Data { get; set; }
    }

    public class ChiefComplaintWithFiles
    {
        [JsonProperty("complaint")]
        public string Complaint { get; set; }
        [JsonProperty("tags")]
        public List<string> Tags { get; set; }
        [JsonProperty("appointment_number")]
        public long AppointmentNumber { get; set; }
        [JsonProperty("episode_number")]
        public long EpisodeNumber { get; set; }

        [JsonProperty("files")]
        public List<DocumentDTO> Files { get; set; }
    }
}
