﻿using Common.Enums;

namespace Common.Common
{
    public class FileFilter
    {
        public string UserId { get; set; }
        public FileType FileType { get; set; }
        public FileParentType ParentType { get; set; }
        public string ParentId { get; set; }
        public bool Approved { get; set; }
    }
}
