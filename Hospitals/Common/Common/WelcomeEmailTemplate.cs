﻿namespace Common.Common
{
    public class WelcomeEmailTemplate
    {
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNumber { get; set; }
        public string Gender { get; set; }
        //public string NationalId { get; set; }
    }
}
