﻿namespace Common.Common
{
    public class SendEmailByTemplateId
    {
        public int TemplateId { get; set; }
        public string Message { get; set; }
        public string UserId { get; set; }

    }
}
