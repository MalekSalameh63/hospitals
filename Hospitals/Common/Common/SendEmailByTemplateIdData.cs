﻿namespace Common.Common
{
    public class SendEmailByTemplateIdData
    {
        public string Message { get; set; }
        public string UserName { get; set; }
    }
}
