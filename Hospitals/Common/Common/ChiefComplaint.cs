﻿using Common.DTOs;
using Common.Enums;
using Common.VOs;
using System;
using System.Collections.Generic;

namespace Common.Common
{
    public class ChiefComplaint
    {
        private ChiefComplaint()
        { }

        public ChiefComplaint(ChiefComplaintDto chiefComplaintDto)
        {
            ComplaintId = new GuidVo(Guid.NewGuid().ToString());
            Complaint = new NTextVO(chiefComplaintDto.Complaint);
            Tags = chiefComplaintDto.Tags;
            UserId = new GuidVo(chiefComplaintDto.UserId.ToString());
            CreationDate = DateTime.Now;
            AppointmentNumber = chiefComplaintDto.AppointmentNumber;
            EpisodeNumber = chiefComplaintDto.EpisodeNumber;
            PhysicianId = chiefComplaintDto.PhysicianId;
            CallStart = chiefComplaintDto.CallStart;
            CallEnd = chiefComplaintDto.CallEnd;
            VisitType = chiefComplaintDto.VisitType;
            EndBy = chiefComplaintDto.EndBy;
        }

        public GuidVo ComplaintId { get; set; }
        public NTextVO Complaint { get; }
        public List<string> Tags { get; set; }
        public GuidVo UserId { get; }
        public long AppointmentNumber { get; set; }
        public long EpisodeNumber { get; set; }
        public string PhysicianId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime CallStart { get; set; }
        public DateTime CallEnd { get; set; }
        public VisitType VisitType { get; set; }
        public UserType EndBy { get; set; }
        public bool IsDeleted { get; set; }
        public ChiefComplaintDto GenerateDto()
        {
            var dto = new ChiefComplaintDto(this);
            return dto;
        }
    }
}
