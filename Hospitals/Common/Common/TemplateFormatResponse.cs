﻿namespace Common.Common
{
    public class TemplateFormatResponse
    {
        public string TemplateName { get; set; }
        public string Title { get; set; }
        public dynamic EmailTemplate { get; set; }
        public string SmsTemplate { get; set; }
        public string NotificationTemplate { get; set; }
    }
}
