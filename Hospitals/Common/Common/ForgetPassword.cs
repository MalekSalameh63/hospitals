﻿using Newtonsoft.Json;

namespace Common.Common
{
    public class ForgetPassword
    {
        [JsonProperty("country_code")]
        public string CountryCode { get; set; }
        [JsonProperty("phonenumber")]
        public string Phone { set; get; }
        public string Password { get; set; }
    }
}
