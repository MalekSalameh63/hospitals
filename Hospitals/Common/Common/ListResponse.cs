﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Common
{
    public class DataList<T> where T : class
    {
        [JsonProperty("actions")]
        public List<T> Data { get; set; }
    }
}
