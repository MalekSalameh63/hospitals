﻿namespace Common.Common
{
    public class EmailActivationDynamicLinkTemplateData
    {
        public string Message { get; set; }
        public int Status { get; set; }
    }
}
