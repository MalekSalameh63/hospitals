﻿using System;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace Common.Common
{
    public class PromotionCode
    {
        [BsonId]
        [JsonIgnore]
        public Guid Id { get; set; }
        public string Code { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}
