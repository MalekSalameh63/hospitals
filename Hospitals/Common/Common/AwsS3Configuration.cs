﻿namespace Common.Common
{
    public class AwsS3Configuration
    {
        public string BucketName { get; set; }
        public string AwsAccessKeyId { get; set; }
        public string AwsSecretAccessKey { get; set; }
    }
}
