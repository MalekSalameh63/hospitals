﻿namespace Common.Common
{
    public class EmailSetting
    {
        public static int SubjectMinLength { get; set; }
        public static int SubjectMaxLength { get; set; }
        public static int BodyMinLength { get; set; }
        public static int BodyMaxLength { get; set; }
        public static int AttachmentsMaxSize { get; set; }
        public static int AttachmentMaxNumber { get; set; }
        public static int AttachmentMaxSize { get; set; }


    }
}
