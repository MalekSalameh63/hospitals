﻿using Common.DTOs;
using Common.VOs;
using System.Collections.Generic;
using System.Linq;

namespace Common.Common
{
    public class EmailMessage
    {
        public EmailMessage(EmailMessageDto message)
        {
            Subject = message.Subject;
            Body = message.Body;
            From = message.From;
            To = message.To.Select(x => new EmailVo(x)).ToList();
            Cc = message.Cc.Select(x => new EmailVo(x)).ToList();


            if (message.Attachments.Any())
            {
                Attachments = message.Attachments.Select(x =>
                        new AttachmentVo(x.Content, new TextVO(x.Name), new MimeTypeVo(x.MimeType)))
                    .ToList();
                //if (Attachments.Sum(x => x.AttachmentSize) > EmailSetting.AttachmentMaxSize)
                //    throw new Exception("Exceeded maximum size of attachments");
            }


            IsBodyHtml = message.IsBodyHtml;
            Type = message.Type;
        }
        public string Subject { get; }
        public string Body { get; }
        public string From { get; set; }
        public List<EmailVo> To { get; }
        public List<EmailVo> Cc { get; }
        public List<AttachmentVo> Attachments { get; } = new List<AttachmentVo>();
        public bool IsBodyHtml { get; }
        public int? Type { get; }
    }
}