﻿using Common.Enums;

namespace Common.Common
{
    public class ReferralOrdersRequest
    {
        public int AppointmentNumber { get; set; }
        public int PatientMrn { get; set; }
        public int Episode { get; set; }
        public ReferralUserType UserType { get; set; }
        public string Otp { get; set; }
    }
}
