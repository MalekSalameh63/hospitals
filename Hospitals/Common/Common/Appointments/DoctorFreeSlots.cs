﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class DoctorFreeSlots : AppointmentRequestBase
    {
        [JsonProperty("DoctorID")]
        public long DoctorId { get; set; }

        [JsonProperty("IsBookingForLiveCare")]
        public long IsBookingForLiveCare { get; set; }

        [JsonProperty("ClinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("OriginalClinicID")]
        public long OriginalClinicId { get; set; }

    }
}
