﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class ConfirmAppointment : AppointmentRequestBase
    {
        [JsonProperty("AppointmentNumber")]
        public long AppointmentNumber { get; set; }

        [JsonProperty("ClinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("ConfirmationBy")]
        public long ConfirmationBy { get; set; }

        [JsonProperty("PatientID")]
        public long PatientId { get; set; }

    }
}
