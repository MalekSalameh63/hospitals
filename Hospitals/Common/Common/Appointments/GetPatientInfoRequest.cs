﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class GetPatientInfoRequest : AppointmentRequestBase
    {
        [JsonProperty("PatientIdentificationID")]
        public string PatientIdentificationID { get; set; }

        [JsonProperty("DOB")]
        public string DOB { get; set; }

        [JsonProperty("IsHijri")]
        public int IsHijri { get; set; }

        [JsonProperty("Region")]
        public int Region { get; set; }
    }
}
