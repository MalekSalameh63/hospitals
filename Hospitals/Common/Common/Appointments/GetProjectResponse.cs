﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.Common.Appointments
{
    public class GetProjectResponse : AppointmentResponseBase
    {
        [JsonProperty("ListProject")]
        public List<ListProject> ListProject { get; set; }

        [JsonProperty("ListProjectWithDistance")]
        public object ListProjectWithDistance { get; set; }

    }

    public class ListProject
    {
        [JsonProperty("Desciption")]
        public string Desciption { get; set; }

        [JsonProperty("DesciptionN")]
        public object DesciptionN { get; set; }

        [JsonProperty("ID")]
        public long Id { get; set; }

        [JsonProperty("LegalName")]
        public string LegalName { get; set; }

        [JsonProperty("LegalNameN")]
        public string LegalNameN { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("NameN")]
        public object NameN { get; set; }

        [JsonProperty("PhoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("SetupID")]
        public string SetupId { get; set; }

        [JsonProperty("DistanceInKilometers")]
        public long DistanceInKilometers { get; set; }

        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }

        [JsonProperty("Latitude")]
        public string Latitude { get; set; }

        [JsonProperty("Longitude")]
        public string Longitude { get; set; }

        [JsonProperty("MainProjectID")]
        public long MainProjectId { get; set; }

        [JsonProperty("ProjectOutSA")]
        public object ProjectOutSa { get; set; }

        [JsonProperty("UsingInDoctorApp")]
        public bool UsingInDoctorApp { get; set; }
    }
}
