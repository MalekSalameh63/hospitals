﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class GetPatientInfoResponse : AppointmentRequestBase
    {
        [JsonProperty("Date")]
        public object Date { get; set; }

        [JsonProperty("ServiceName")]
        public long ServiceName { get; set; }

        [JsonProperty("Time")]
        public object Time { get; set; }

        [JsonProperty("AndroidLink")]
        public object AndroidLink { get; set; }

        [JsonProperty("AuthenticationTokenID")]
        public object AuthenticationTokenId { get; set; }

        [JsonProperty("Data")]
        public object Data { get; set; }

        [JsonProperty("DietType")]
        public long DietType { get; set; }

        [JsonProperty("ErrorCode")]
        public object ErrorCode { get; set; }

        [JsonProperty("ErrorEndUserMessage")]
        public object ErrorEndUserMessage { get; set; }

        [JsonProperty("ErrorEndUserMessageN")]
        public object ErrorEndUserMessageN { get; set; }

        [JsonProperty("ErrorMessage")]
        public object ErrorMessage { get; set; }

        [JsonProperty("ErrorType")]
        public long ErrorType { get; set; }

        [JsonProperty("FoodCategory")]
        public long FoodCategory { get; set; }

        [JsonProperty("IOSLink")]
        public object IosLink { get; set; }

        [JsonProperty("IsAuthenticated")]
        public bool IsAuthenticated { get; set; }

        [JsonProperty("MealOrderStatus")]
        public long MealOrderStatus { get; set; }

        [JsonProperty("MealType")]
        public long MealType { get; set; }

        [JsonProperty("MessageStatus")]
        public long MessageStatus { get; set; }

        [JsonProperty("NumberOfResultRecords")]
        public long NumberOfResultRecords { get; set; }

        [JsonProperty("PatientBlodType")]
        public object PatientBlodType { get; set; }

        [JsonProperty("SuccessMsg")]
        public object SuccessMsg { get; set; }

        [JsonProperty("SuccessMsgN")]
        public object SuccessMsgN { get; set; }

        [JsonProperty("AccessTokenObject")]
        public object AccessTokenObject { get; set; }

        [JsonProperty("Age")]
        public long Age { get; set; }

        [JsonProperty("ClientIdentifierId")]
        public object ClientIdentifierId { get; set; }

        [JsonProperty("CreatedBy")]
        public long CreatedBy { get; set; }

        [JsonProperty("DateOfBirth")]
        public string DateOfBirth { get; set; }

        [JsonProperty("FirstNameAr")]
        public string FirstNameAr { get; set; }

        [JsonProperty("FirstNameEn")]
        public string FirstNameEn { get; set; }

        [JsonProperty("Gender")]
        public string Gender { get; set; }

        [JsonProperty("GenderAr")]
        public object GenderAr { get; set; }

        [JsonProperty("GenderEn")]
        public object GenderEn { get; set; }

        [JsonProperty("HealthId")]
        public string HealthId { get; set; }

        [JsonProperty("IdNumber")]
        public string IdNumber { get; set; }

        [JsonProperty("IdType")]
        public string IdType { get; set; }

        [JsonProperty("IsHijri")]
        public bool IsHijri { get; set; }

        [JsonProperty("IsInstertedOrUpdated")]
        public long IsInstertedOrUpdated { get; set; }

        [JsonProperty("IsNull")]
        public long IsNull { get; set; }

        [JsonProperty("IsPatientExistNHIC")]
        public long IsPatientExistNhic { get; set; }

        [JsonProperty("LastNameAr")]
        public string LastNameAr { get; set; }

        [JsonProperty("LastNameEn")]
        public string LastNameEn { get; set; }

        [JsonProperty("List_ActiveAccessToken")]
        public object ListActiveAccessToken { get; set; }

        [JsonProperty("MaritalStatus")]
        public string MaritalStatus { get; set; }

        [JsonProperty("MaritalStatusCode")]
        public string MaritalStatusCode { get; set; }

        [JsonProperty("Nationality")]
        public string Nationality { get; set; }

        [JsonProperty("NationalityCode")]
        public string NationalityCode { get; set; }

        [JsonProperty("Occupation")]
        public string Occupation { get; set; }

        [JsonProperty("PatientStatus")]
        public string PatientStatus { get; set; }

        [JsonProperty("PlaceofBirth")]
        public string PlaceofBirth { get; set; }

        [JsonProperty("PractitionerStatusCode")]
        public object PractitionerStatusCode { get; set; }

        [JsonProperty("PractitionerStatusDescAr")]
        public object PractitionerStatusDescAr { get; set; }

        [JsonProperty("PractitionerStatusDescEn")]
        public object PractitionerStatusDescEn { get; set; }

        [JsonProperty("SecondNameAr")]
        public string SecondNameAr { get; set; }

        [JsonProperty("SecondNameEn")]
        public string SecondNameEn { get; set; }

        [JsonProperty("ThirdNameAr")]
        public string ThirdNameAr { get; set; }

        [JsonProperty("ThirdNameEn")]
        public string ThirdNameEn { get; set; }

        [JsonProperty("accessToken")]
        public object AccessToken { get; set; }

        [JsonProperty("categoryCode")]
        public long CategoryCode { get; set; }

        [JsonProperty("categoryNameAr")]
        public object CategoryNameAr { get; set; }

        [JsonProperty("categoryNameEn")]
        public object CategoryNameEn { get; set; }

        [JsonProperty("constraintCode")]
        public long ConstraintCode { get; set; }

        [JsonProperty("constraintNameAr")]
        public object ConstraintNameAr { get; set; }

        [JsonProperty("constraintNameEn")]
        public object ConstraintNameEn { get; set; }

        [JsonProperty("content")]
        public object Content { get; set; }

        [JsonProperty("licenseExpiryDate")]
        public object LicenseExpiryDate { get; set; }

        [JsonProperty("licenseIssuedDate")]
        public object LicenseIssuedDate { get; set; }

        [JsonProperty("licenseStatusCode")]
        public object LicenseStatusCode { get; set; }

        [JsonProperty("licenseStatusDescAr")]
        public object LicenseStatusDescAr { get; set; }

        [JsonProperty("licenseStatusDescEn")]
        public object LicenseStatusDescEn { get; set; }

        [JsonProperty("organizations")]
        public object Organizations { get; set; }

        [JsonProperty("registrationNumber")]
        public object RegistrationNumber { get; set; }

        [JsonProperty("specialtyCode")]
        public long SpecialtyCode { get; set; }

        [JsonProperty("specialtyNameAr")]
        public object SpecialtyNameAr { get; set; }

        [JsonProperty("specialtyNameEn")]
        public object SpecialtyNameEn { get; set; }
    }
}

