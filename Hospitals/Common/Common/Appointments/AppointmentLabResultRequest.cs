﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class AppointmentLabResultRequest : AppointmentRequestBase
    {
        [JsonProperty("AppointmentNo")]
        public long AppointmentNo { get; set; }
        [JsonProperty("ClinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("PatientID")]
        public long PatientId { get; set; }
    }
}
