﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.Common.Appointments
{
    public class OnlineCheckInAppointment
    {
        [JsonProperty("AdvanceNumber")]
        public long AdvanceNumber { get; set; }

        [JsonProperty("AppointmentDate")]
        public string AppointmentDate { get; set; }

        [JsonProperty("AppointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("CashPrice")]
        public long CashPrice { get; set; }

        [JsonProperty("CashPriceTax")]
        public long CashPriceTax { get; set; }

        [JsonProperty("CashPriceWithTax")]
        public long CashPriceWithTax { get; set; }

        [JsonProperty("ClinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("ClinicName")]
        public string ClinicName { get; set; }

        [JsonProperty("CompanyId")]
        public long CompanyId { get; set; }

        [JsonProperty("CompanyName")]
        public string CompanyName { get; set; }

        [JsonProperty("CompanyShareWithTax")]
        public long CompanyShareWithTax { get; set; }

        [JsonProperty("DoctorID")]
        public long DoctorId { get; set; }

        [JsonProperty("DoctorImageURL")]
        public Uri DoctorImageUrl { get; set; }

        [JsonProperty("DoctorNameObj")]
        public string DoctorNameObj { get; set; }

        [JsonProperty("DoctorSpeciality")]
        public List<string> DoctorSpeciality { get; set; }

        [JsonProperty("ErrCode")]
        public object ErrCode { get; set; }

        [JsonProperty("GroupID")]
        public long GroupId { get; set; }

        [JsonProperty("ISAllowOnlineCheckedIN")]
        public bool IsAllowOnlineCheckedIn { get; set; }

        [JsonProperty("InsurancePolicyNo")]
        public object InsurancePolicyNo { get; set; }

        [JsonProperty("IsExcludedForOnlineCheckin")]
        public bool IsExcludedForOnlineCheckin { get; set; }

        [JsonProperty("IsFollowup")]
        public long IsFollowup { get; set; }

        [JsonProperty("IsLiveCareAppointment")]
        public bool IsLiveCareAppointment { get; set; }

        [JsonProperty("IsOnlineCheckedIN")]
        public bool IsOnlineCheckedIn { get; set; }

        [JsonProperty("Message")]
        public string Message { get; set; }

        [JsonProperty("NextAction")]
        public long NextAction { get; set; }

        [JsonProperty("PatientCardID")]
        public object PatientCardId { get; set; }

        [JsonProperty("PatientID")]
        public long PatientId { get; set; }

        [JsonProperty("PatientShare")]
        public long PatientShare { get; set; }

        [JsonProperty("PatientShareWithTax")]
        public double PatientShareWithTax { get; set; }

        [JsonProperty("PatientStatusType")]
        public long PatientStatusType { get; set; }

        [JsonProperty("PatientTaxAmount")]
        public double PatientTaxAmount { get; set; }

        [JsonProperty("PatientType")]
        public string PatientType { get; set; }

        [JsonProperty("PaymentAmount")]
        public long PaymentAmount { get; set; }

        [JsonProperty("PaymentDate")]
        public string PaymentDate { get; set; }

        [JsonProperty("PaymentMethodName")]
        public object PaymentMethodName { get; set; }

        [JsonProperty("PaymentReferenceNumber")]
        public object PaymentReferenceNumber { get; set; }

        [JsonProperty("PolicyId")]
        public long PolicyId { get; set; }

        [JsonProperty("PolicyName")]
        public string PolicyName { get; set; }

        [JsonProperty("ProcedureName")]
        public string ProcedureName { get; set; }

        [JsonProperty("ProjectID")]
        public long ProjectId { get; set; }

        [JsonProperty("ProjectName")]
        public string ProjectName { get; set; }

        [JsonProperty("ServiceID")]
        public long ServiceId { get; set; }

        [JsonProperty("SetupID")]
        public object SetupId { get; set; }

        [JsonProperty("SourceType")]
        public long SourceType { get; set; }

        [JsonProperty("StartTime")]
        public string StartTime { get; set; }

        [JsonProperty("Status")]
        public long Status { get; set; }

        [JsonProperty("StatusCode")]
        public long StatusCode { get; set; }

        [JsonProperty("StatusDesc")]
        public object StatusDesc { get; set; }

        [JsonProperty("SubPolicyNo")]
        public object SubPolicyNo { get; set; }

        [JsonProperty("Tax")]
        public long Tax { get; set; }

        [JsonProperty("UserID")]
        public long UserId { get; set; }
    }
}
