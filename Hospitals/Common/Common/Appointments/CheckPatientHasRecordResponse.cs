﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class CheckPatientHasRecordResponse : AppointmentResponseBase
    {
        [JsonProperty("AuthenticationTokenID")]
        public string AuthenticationTokenId { get; set; }

        [JsonProperty("IsAuthenticated")]
        public bool IsAuthenticated { get; set; }
    }
}
