﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class SearchDoctorByTime : AppointmentRequestBase
    {
        [JsonProperty("ClinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("ContinueDentalPlan")]
        public bool ContinueDentalPlan { get; set; }

        [JsonProperty("IsSearchAppointmnetByClinicID")]
        public bool IsSearchAppointmnetByClinicId { get; set; }

        [JsonProperty("PatientID")]
        public long PatientId { get; set; }

        [JsonProperty("gender")]
        public long Gender { get; set; }

        [JsonProperty("IsGetNearAppointment")]
        public bool IsGetNearAppointment { get; set; }

        [JsonProperty("Latitude")]
        public long Latitude { get; set; }

        [JsonProperty("Longitude")]
        public long Longitude { get; set; }

        [JsonProperty("License")]
        public bool License { get; set; }

    }
}
