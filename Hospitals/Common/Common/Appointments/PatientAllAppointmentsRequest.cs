﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class PatientAllAppointmentsRequest : AppointmentRequestBase
    {
        [JsonProperty("PatientID")]
        public long PatientId { get; set; }

        [JsonProperty("Status")]
        public long Status { get; set; }
    }
}