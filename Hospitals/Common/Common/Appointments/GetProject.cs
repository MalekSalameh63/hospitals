﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class GetProject : AppointmentRequestBase
    {
        [JsonProperty("Latitude")]
        public long Latitude { get; set; }

        [JsonProperty("Longitude")]
        public long Longitude { get; set; }

    }
}
