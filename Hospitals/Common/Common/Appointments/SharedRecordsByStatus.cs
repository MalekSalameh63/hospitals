﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class SharedRecordsByStatus
    {
        [JsonProperty("VersionID")]
        public double VersionId { get; set; }

        [JsonProperty("Channel")]
        public long Channel { get; set; }

        [JsonProperty("LanguageID")]
        public long LanguageId { get; set; }

        [JsonProperty("IPAdress")]
        public string IpAdress { get; set; }

        [JsonProperty("generalid")]
        public string Generalid { get; set; }

        [JsonProperty("PatientOutSA")]
        public long PatientOutSa { get; set; }

        [JsonProperty("SessionID")]
        public string SessionId { get; set; }

        [JsonProperty("isDentalAllowedBackend")]
        public bool IsDentalAllowedBackend { get; set; }

        [JsonProperty("DeviceTypeID")]
        public long DeviceTypeId { get; set; }

        [JsonProperty("PatientID")]
        public long PatientId { get; set; }

        [JsonProperty("TokenID")]
        public string TokenId { get; set; }

        [JsonProperty("PatientTypeID")]
        public long PatientTypeId { get; set; }

        [JsonProperty("PatientType")]
        public long PatientType { get; set; }

        [JsonProperty("Status")]
        public long Status { get; set; }
    }
}
