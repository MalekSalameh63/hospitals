﻿using Common.DTOs.EMR.MedicalRecord.Procedure;

namespace Common.Common.Appointments
{
    public class RadiologyOrderWithResults
    {
        public OrderedProceduresResponse Order { get; set; }
        public FinalRadiologyList Result { get; set; }
    }
}
