﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class GetCheckInScreenAppointmentDetailsByAppointmentNo : AppointmentRequestBase
    {
        [JsonProperty("PatientID")]
        public long PatientId { get; set; }
        [JsonProperty("ClinicID")]
        public long ClinicId { get; set; }
        [JsonProperty("AppointmentNo")]
        public string AppointmentNo { get; set; }
        [JsonProperty("IsActiveAppointment")]
        public bool IsActiveAppointment { get; set; }
    }
}
