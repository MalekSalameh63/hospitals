﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class GetDoctorProfile : AppointmentRequestBase
    {
        [JsonProperty("doctorID")]
        public long DoctorId { get; set; }

        [JsonProperty("License")]
        public bool License { get; set; }

        [JsonProperty("IsRegistered")]
        public bool IsRegistered { get; set; }

        [JsonProperty("ClinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("PatientID")]
        public long PatientId { get; set; }

    }
}
