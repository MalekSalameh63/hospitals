﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.Common.Appointments
{

    public class AppointmentResponse : AppointmentResponseBase
    {
        [JsonProperty("AppointmentActiveNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long AppointmentActiveNumber { get; set; }

        [JsonProperty("AppointmentNo", NullValueHandling = NullValueHandling.Ignore)]
        public long AppointmentNo { get; set; }

        [JsonProperty("DoctorList", NullValueHandling = NullValueHandling.Ignore)]
        public List<DoctorList> DoctorList { get; set; }

        [JsonProperty("DoctorProfileList", NullValueHandling = NullValueHandling.Ignore)]
        public List<DoctorProfileList> DoctorProfileList { get; set; }

        [JsonProperty("ErrorSearchMsg", NullValueHandling = NullValueHandling.Ignore)]
        public object ErrorSearchMsg { get; set; }

        [JsonProperty("FreeTimeSlots", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> FreeTimeSlots { get; set; }

        [JsonProperty("IsDoctorExist", NullValueHandling = NullValueHandling.Ignore)]
        public bool IsDoctorExist { get; set; }

        [JsonProperty("IsSlotAvailable", NullValueHandling = NullValueHandling.Ignore)]
        public long IsSlotAvailable { get; set; }

        [JsonProperty("SlotDuration", NullValueHandling = NullValueHandling.Ignore)]
        public long SlotDuration { get; set; }

        [JsonProperty("List_DoctorWorkingHoursTable", NullValueHandling = NullValueHandling.Ignore)]
        public List<DoctorWorkingHours> ListDoctorWorkingHours { get; set; }

        [JsonProperty("AppoimentAllHistoryResultList", NullValueHandling = NullValueHandling.Ignore)]
        public List<AppoimentAllHistoryResultList> AppoimentAllHistoryResultList { get; set; }

        [JsonProperty("OnlineCheckInAppointments", NullValueHandling = NullValueHandling.Ignore)]
        public List<OnlineCheckInAppointment> OnlineCheckInAppointments { get; set; }
        [JsonProperty("HIS_GetAllAppoimentHistoryList", NullValueHandling = NullValueHandling.Ignore)]
        public List<AppoimentAllHistoryResultList> HIS_GetAllAppoimentHistoryList { get; set; }

    }

    public class TestHistory
    {
        public long Date { get; set; }
    }
}
