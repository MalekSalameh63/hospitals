﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.Common.Appointments
{
    public class AppointmentPrescriptionResponse : AppointmentResponseBase
    {
        [JsonProperty("ListPRM")]
        public List<ListPrm> ListPrm { get; set; }
    }
    public class ListPrm
    {
        [JsonProperty("Address")]
        public string Address { get; set; }

        [JsonProperty("AppointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("Clinic")]
        public string Clinic { get; set; }

        [JsonProperty("CompanyName")]
        public object CompanyName { get; set; }

        [JsonProperty("Days")]
        public long Days { get; set; }

        [JsonProperty("DoctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("DoseDailyQuantity")]
        public long DoseDailyQuantity { get; set; }

        [JsonProperty("Frequency")]
        public string Frequency { get; set; }

        [JsonProperty("FrequencyNumber")]
        public long FrequencyNumber { get; set; }

        [JsonProperty("Image")]
        public object Image { get; set; }

        [JsonProperty("ImageExtension")]
        public object ImageExtension { get; set; }

        [JsonProperty("ImageSRCUrl")]
        public Uri ImageSrcUrl { get; set; }

        [JsonProperty("ImageString")]
        public object ImageString { get; set; }

        [JsonProperty("ImageThumbUrl")]
        public Uri ImageThumbUrl { get; set; }

        [JsonProperty("IsCovered")]
        public string IsCovered { get; set; }

        [JsonProperty("ItemDescription")]
        public string ItemDescription { get; set; }

        [JsonProperty("ItemID")]
        public long ItemId { get; set; }

        [JsonProperty("OrderDate")]
        public string OrderDate { get; set; }

        [JsonProperty("PatientID")]
        public long PatientId { get; set; }

        [JsonProperty("PatientName")]
        public string PatientName { get; set; }

        [JsonProperty("PhoneOffice1")]
        public string PhoneOffice1 { get; set; }

        [JsonProperty("PrescriptionQR")]
        public object PrescriptionQr { get; set; }

        [JsonProperty("PrescriptionTimes")]
        public long PrescriptionTimes { get; set; }

        [JsonProperty("ProductImage")]
        public object ProductImage { get; set; }

        [JsonProperty("ProductImageBase64")]
        public object ProductImageBase64 { get; set; }

        [JsonProperty("ProductImageString")]
        public string ProductImageString { get; set; }

        [JsonProperty("ProjectID")]
        public long ProjectId { get; set; }

        [JsonProperty("ProjectName")]
        public string ProjectName { get; set; }

        [JsonProperty("Remarks")]
        public string Remarks { get; set; }

        [JsonProperty("Route")]
        public string Route { get; set; }

        [JsonProperty("SKU")]
        public string Sku { get; set; }

        [JsonProperty("ScaleOffset")]
        public long ScaleOffset { get; set; }

        [JsonProperty("StartDate")]
        public string StartDate { get; set; }
    }
}
