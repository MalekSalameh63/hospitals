﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class ConfirmAppointmentResponse : AppointmentResponseBase
    {
        [JsonProperty("Date")]
        public object Date { get; set; }

        [JsonProperty("LanguageID")]
        public long LanguageId { get; set; }

        [JsonProperty("ServiceName")]
        public long ServiceName { get; set; }

        [JsonProperty("Time")]
        public object Time { get; set; }

        [JsonProperty("AndroidLink")]
        public object AndroidLink { get; set; }

        [JsonProperty("AuthenticationTokenID")]
        public object AuthenticationTokenId { get; set; }

        [JsonProperty("DietType")]
        public long DietType { get; set; }

        [JsonProperty("FoodCategory")]
        public long FoodCategory { get; set; }

        [JsonProperty("IOSLink")]
        public object IosLink { get; set; }

        [JsonProperty("IsAuthenticated")]
        public bool IsAuthenticated { get; set; }

        [JsonProperty("MealOrderStatus")]
        public long MealOrderStatus { get; set; }

        [JsonProperty("MealType")]
        public long MealType { get; set; }

        [JsonProperty("PatientBlodType")]
        public object PatientBlodType { get; set; }

        [JsonProperty("AppointmentDetails")]
        public object AppointmentDetails { get; set; }

        [JsonProperty("Askh_List_GetAllNotificationsFromPool")]
        public object AskhListGetAllNotificationsFromPool { get; set; }

        [JsonProperty("List_Askh_MessagesFromNotificationPool")]
        public object ListAskhMessagesFromNotificationPool { get; set; }

        [JsonProperty("List_DailyPushNotificationReport")]
        public object ListDailyPushNotificationReport { get; set; }

        [JsonProperty("List_GetAllNotificationsFromLabAndRad")]
        public object ListGetAllNotificationsFromLabAndRad { get; set; }

        [JsonProperty("List_GetAllNotificationsFromPool")]
        public object ListGetAllNotificationsFromPool { get; set; }

        [JsonProperty("List_GetInteractiveMasterData")]
        public object ListGetInteractiveMasterData { get; set; }

        [JsonProperty("List_GetPatientDeviceInformation")]
        public object ListGetPatientDeviceInformation { get; set; }

        [JsonProperty("List_GetPushNotificationSatisfactionModel")]
        public object ListGetPushNotificationSatisfactionModel { get; set; }

        [JsonProperty("List_MessagesFromNotificationPool")]
        public object ListMessagesFromNotificationPool { get; set; }

        [JsonProperty("List_MessagesFromPool")]
        public object ListMessagesFromPool { get; set; }

        [JsonProperty("List_NewRequests")]
        public object ListNewRequests { get; set; }

        [JsonProperty("List_Notification_GetTodayNotificationsModel")]
        public object ListNotificationGetTodayNotificationsModel { get; set; }

        [JsonProperty("List_Notification_TomorrowAppointments")]
        public object ListNotificationTomorrowAppointments { get; set; }

        [JsonProperty("List_Notification_TomorrowAppointmentsByAppNum")]
        public object ListNotificationTomorrowAppointmentsByAppNum { get; set; }

        [JsonProperty("List_PushNotificationData")]
        public object ListPushNotificationData { get; set; }

        [JsonProperty("List_PushNotificationFaidUsersMessage")]
        public object ListPushNotificationFaidUsersMessage { get; set; }

        [JsonProperty("List_PushNotificationReportHistory")]
        public object ListPushNotificationReportHistory { get; set; }

        [JsonProperty("List_PushNotificationStatusID")]
        public object ListPushNotificationStatusId { get; set; }

        [JsonProperty("List_PushNotificationUsersNo")]
        public object ListPushNotificationUsersNo { get; set; }

        [JsonProperty("List_PushNotification_CurrentDataHistory")]
        public object ListPushNotificationCurrentDataHistory { get; set; }

        [JsonProperty("List_PushNotification_GetDashboardData")]
        public object ListPushNotificationGetDashboardData { get; set; }

        [JsonProperty("List_PushNotification_GetDashboardDataForMap")]
        public object ListPushNotificationGetDashboardDataForMap { get; set; }

        [JsonProperty("List_PushNotification_GetDashboardDataForMessages")]
        public object ListPushNotificationGetDashboardDataForMessages { get; set; }

        [JsonProperty("List_PushNotification_GetDashboardDataForNewUsers")]
        public object ListPushNotificationGetDashboardDataForNewUsers { get; set; }

        [JsonProperty("List_PushNotification_GetDashboardDataForPeriod")]
        public object ListPushNotificationGetDashboardDataForPeriod { get; set; }

        [JsonProperty("List_PushNotification_GetUploadStatistics")]
        public object ListPushNotificationGetUploadStatistics { get; set; }

        [JsonProperty("List_PushNotification_Member")]
        public object ListPushNotificationMember { get; set; }

        [JsonProperty("List_SelectByID_PatientMobileHistoryModel")]
        public object ListSelectByIdPatientMobileHistoryModel { get; set; }

        [JsonProperty("List_ePhrPushNotificationData")]
        public object ListEPhrPushNotificationData { get; set; }

        [JsonProperty("ReturnValue")]
        public long ReturnValue { get; set; }

        [JsonProperty("RowCount")]
        public long RowCount { get; set; }

        [JsonProperty("UnReadNotificationsCount")]
        public long UnReadNotificationsCount { get; set; }
    }
}
