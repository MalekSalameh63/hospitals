﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.Common.Appointments
{
    public class PatientAppointmentHistoryResponse
    {
    }

    public class AppoimentAllHistoryResultList
    {
        [JsonProperty("SetupID")]
        public string SetupId { get; set; }

        [JsonProperty("ProjectID")]
        public long ProjectId { get; set; }

        [JsonProperty("AppointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("AppointmentDate")]
        public string AppointmentDate { get; set; }

        [JsonProperty("AppointmentDateN")]
        public object AppointmentDateN { get; set; }

        [JsonProperty("AppointmentType")]
        public long AppointmentType { get; set; }

        [JsonProperty("BookDate")]
        public string BookDate { get; set; }

        [JsonProperty("PatientType")]
        public long PatientType { get; set; }

        [JsonProperty("PatientID")]
        public long PatientId { get; set; }

        [JsonProperty("ClinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("DoctorID")]
        public long DoctorId { get; set; }

        [JsonProperty("EndDate")]
        public string EndDate { get; set; }

        [JsonProperty("StartTime")]
        public string StartTime { get; set; }

        [JsonProperty("EndTime")]
        public string EndTime { get; set; }

        [JsonProperty("Status")]
        public long Status { get; set; }

        [JsonProperty("VisitType")]
        public long VisitType { get; set; }

        [JsonProperty("VisitFor")]
        public long VisitFor { get; set; }

        [JsonProperty("PatientStatusType")]
        public long PatientStatusType { get; set; }

        [JsonProperty("CompanyID")]
        public long CompanyId { get; set; }

        [JsonProperty("BookedBy")]
        public long BookedBy { get; set; }

        [JsonProperty("BookedOn")]
        public string BookedOn { get; set; }

        [JsonProperty("ConfirmedBy")]
        public long? ConfirmedBy { get; set; }

        [JsonProperty("ConfirmedOn")]
        public string ConfirmedOn { get; set; }

        [JsonProperty("ArrivalChangedBy")]
        public long? ArrivalChangedBy { get; set; }

        [JsonProperty("ArrivedOn")]
        public string ArrivedOn { get; set; }

        [JsonProperty("EditedBy")]
        public object EditedBy { get; set; }

        [JsonProperty("EditedOn")]
        public object EditedOn { get; set; }

        [JsonProperty("DoctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("DoctorNameN")]
        public string DoctorNameN { get; set; }

        [JsonProperty("StatusDesc")]
        public string StatusDesc { get; set; }

        [JsonProperty("StatusDescN")]
        public object StatusDescN { get; set; }

        [JsonProperty("VitalStatus")]
        public bool VitalStatus { get; set; }

        [JsonProperty("VitalSignAppointmentNo")]
        public object VitalSignAppointmentNo { get; set; }

        [JsonProperty("EpisodeID")]
        public long EpisodeId { get; set; }

        [JsonProperty("ActualDoctorRate")]
        public long ActualDoctorRate { get; set; }

        [JsonProperty("ClinicName")]
        public string ClinicName { get; set; }

        [JsonProperty("CompanyName")]
        public object CompanyName { get; set; }

        [JsonProperty("ComplainExists")]
        public bool ComplainExists { get; set; }

        [JsonProperty("DoctorImageURL")]
        public string DoctorImageUrl { get; set; }

        [JsonProperty("DoctorNameObj")]
        public string DoctorNameObj { get; set; }

        [JsonProperty("DoctorRate")]
        public long DoctorRate { get; set; }

        [JsonProperty("DoctorSpeciality")]
        public List<string> DoctorSpeciality { get; set; }

        [JsonProperty("DoctorTitle")]
        public string DoctorTitle { get; set; }

        [JsonProperty("Gender")]
        public long Gender { get; set; }

        [JsonProperty("GenderDescription")]
        public string GenderDescription { get; set; }

        [JsonProperty("ISAllowOnlineCheckedIN")]
        public bool IsAllowOnlineCheckedIn { get; set; }

        [JsonProperty("IsActiveDoctor")]
        public bool IsActiveDoctor { get; set; }

        [JsonProperty("IsActiveDoctorProfile")]
        public bool IsActiveDoctorProfile { get; set; }

        [JsonProperty("IsDoctorAllowVedioCall")]
        public bool IsDoctorAllowVedioCall { get; set; }

        [JsonProperty("IsExecludeDoctor")]
        public bool IsExecludeDoctor { get; set; }

        [JsonProperty("IsFollowup")]
        public long IsFollowup { get; set; }

        [JsonProperty("IsLiveCareAppointment")]
        public bool IsLiveCareAppointment { get; set; }

        [JsonProperty("IsMedicalReportRequested")]
        public bool IsMedicalReportRequested { get; set; }

        [JsonProperty("IsOnlineCheckedIN")]
        public bool IsOnlineCheckedIn { get; set; }

        [JsonProperty("Latitude")]
        public string Latitude { get; set; }

        [JsonProperty("List_HIS_GetContactLensPerscription")]
        public object ListHisGetContactLensPerscription { get; set; }

        [JsonProperty("List_HIS_GetGlassPerscription")]
        public object ListHisGetGlassPerscription { get; set; }

        [JsonProperty("Longitude")]
        public string Longitude { get; set; }

        [JsonProperty("NextAction")]
        public long NextAction { get; set; }

        [JsonProperty("NoOfPatientsRate")]
        public long NoOfPatientsRate { get; set; }

        [JsonProperty("OriginalClinicID")]
        public long OriginalClinicId { get; set; }

        [JsonProperty("OriginalProjectID")]
        public long OriginalProjectId { get; set; }

        [JsonProperty("PatientShare")]
        public long PatientShare { get; set; }

        [JsonProperty("PatientShareWithTax")]
        public long PatientShareWithTax { get; set; }

        [JsonProperty("ProjectName")]
        public string ProjectName { get; set; }

        [JsonProperty("QR")]
        public string Qr { get; set; }

        [JsonProperty("RemaniningHoursTocanPay")]
        public long RemaniningHoursTocanPay { get; set; }

        [JsonProperty("SMSButtonVisable")]
        public bool SmsButtonVisable { get; set; }

        [JsonProperty("Tax")]
        public long Tax { get; set; }

        [JsonProperty("clinicID")]
        public long AppoimentAllHistoryResultListClinicId { get; set; }

        [JsonProperty("patientType")]
        public object AppoimentAllHistoryResultListPatientType { get; set; }

        [JsonProperty("status")]
        public object AppoimentAllHistoryResultListStatus { get; set; }

        [JsonProperty("visitType")]
        public object AppoimentAllHistoryResultListVisitType { get; set; }
    }
}
