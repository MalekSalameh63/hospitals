﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class AppointmentRequestBase
    {
        [JsonProperty("DeviceType")]
        public string DeviceType { get; set; }

        [JsonProperty("TokenID")]
        public string TokenId { get; set; }
        [JsonProperty("VersionID")]
        public double VersionId { get; set; }

        [JsonProperty("Channel")]
        public long Channel { get; set; }

        [JsonProperty("LanguageID")]
        public long LanguageId { get; set; }

        [JsonProperty("IPAdress")]
        public string IpAdress { get; set; }

        [JsonProperty("generalid")]
        public string Generalid { get; set; }

        [JsonProperty("PatientOutSA")]
        public long PatientOutSa { get; set; }

        [JsonProperty("SessionID")]
        public object SessionId { get; set; }

        [JsonProperty("Age")]
        public long Age { get; set; }
        [JsonProperty("days")]
        public int Days { get; set; }

        [JsonProperty("isDentalAllowedBackend")]
        public bool IsDentalAllowedBackend { get; set; }

        [JsonProperty("DeviceTypeID")]
        public long DeviceTypeId { get; set; }

        [JsonProperty("IsSilentLogin")]
        public bool IsSilentLogin { get; set; }
        [JsonProperty("SearchType")]
        public long SearchType { get; set; }
        [JsonProperty("IsForLiveCare")]
        public bool IsForLiveCare { get; set; }

        [JsonProperty("PatientTypeID")]
        public long PatientTypeId { get; set; }

        [JsonProperty("PatientType")]
        public long PatientType { get; set; }

        [JsonProperty("BookedBy")]
        public long BookedBy { get; set; }

        [JsonProperty("VisitType")]
        public long VisitType { get; set; }

        [JsonProperty("VisitFor")]
        public long VisitFor { get; set; }
        [JsonProperty("ProjectID")]
        public long ProjectId { get; set; }
        [JsonProperty("SetupID")]
        public string SetupId { get; set; }
        [JsonProperty("MasterKey")]
        public string MasterKey { get; set; }
        [JsonProperty("IsVirtualCare")]
        public bool IsVirtualCare { get; set; }
    }
}
