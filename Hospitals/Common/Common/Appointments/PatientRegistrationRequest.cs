﻿using Newtonsoft.Json;
using System;

namespace Common.Common.Appointments
{
    public class PatientRegistrationRequest
    {
        [JsonProperty("Patientobject")]
        public PatientObject PatientObject { get; set; }

        [JsonProperty("PatientIdentificationID")]
        public string PatientIdentificationId { get; set; }

        [JsonProperty("PatientMobileNumber")]
        public long PatientMobileNumber { get; set; }

        [JsonProperty("LogInTokenID")]
        public string LogInTokenId { get; set; }

        [JsonProperty("VersionID")]
        public double VersionId { get; set; }

        [JsonProperty("Channel")]
        public long Channel { get; set; }

        [JsonProperty("LanguageID")]
        public long LanguageId { get; set; }

        [JsonProperty("IPAdress")]
        public string IpAdress { get; set; }

        [JsonProperty("generalid")]
        public string Generalid { get; set; }

        [JsonProperty("PatientOutSA")]
        public long PatientOutSa { get; set; }

        [JsonProperty("SessionID")]
        public object SessionId { get; set; }

        [JsonProperty("isDentalAllowedBackend")]
        public bool IsDentalAllowedBackend { get; set; }

        [JsonProperty("DeviceTypeID")]
        public long DeviceTypeId { get; set; }

        [JsonProperty("TokenID")]
        public string TokenId { get; set; }
    }
    public class PatientObject
    {
        [JsonProperty("TempValue")]
        public bool TempValue { get; set; }

        [JsonProperty("PatientIdentificationType")]
        public long PatientIdentificationType { get; set; }

        [JsonProperty("PatientIdentificationNo")]
        public string PatientIdentificationNo { get; set; }

        [JsonProperty("MobileNumber")]
        public long MobileNumber { get; set; }

        [JsonProperty("PatientOutSA")]
        public long PatientOutSa { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("MiddleName")]
        public string MiddleName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("StrDateofBirth")]
        public DateTimeOffset StrDateofBirth { get; set; }

        [JsonProperty("DateofBirth")]
        public string DateofBirth { get; set; }

        [JsonProperty("Gender")]
        public long Gender { get; set; }

        [JsonProperty("NationalityID")]
        public string NationalityId { get; set; }

        [JsonProperty("DateofBirthN")]
        public string DateofBirthN { get; set; }

        [JsonProperty("EmailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("SourceType")]
        public string SourceType { get; set; }

        [JsonProperty("PreferredLanguage")]
        public string PreferredLanguage { get; set; }

        [JsonProperty("Marital")]
        public string Marital { get; set; }
    }
}
