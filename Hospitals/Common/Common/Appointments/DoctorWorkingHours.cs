﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class DoctorWorkingHours
    {
        [JsonProperty("Date")]
        public string Date { get; set; }

        [JsonProperty("DayName")]
        public string DayName { get; set; }

        [JsonProperty("WorkingHours")]
        public string WorkingHours { get; set; }
    }
}
