﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class AppointmentResponseBase
    {
        [JsonProperty("Data", NullValueHandling = NullValueHandling.Ignore)]
        public object Data { get; set; }

        [JsonProperty("Dataw", NullValueHandling = NullValueHandling.Ignore)]
        public bool Dataw { get; set; }

        [JsonProperty("ErrorCode", NullValueHandling = NullValueHandling.Ignore)]
        public object ErrorCode { get; set; }

        [JsonProperty("ErrorEndUserMessage", NullValueHandling = NullValueHandling.Ignore)]
        public string ErrorEndUserMessage { get; set; }

        [JsonProperty("ErrorEndUserMessageN", NullValueHandling = NullValueHandling.Ignore)]
        public object ErrorEndUserMessageN { get; set; }

        [JsonProperty("ErrorMessage", NullValueHandling = NullValueHandling.Ignore)]
        public object ErrorMessage { get; set; }

        [JsonProperty("ErrorType", NullValueHandling = NullValueHandling.Ignore)]
        public long ErrorType { get; set; }

        [JsonProperty("MessageStatus", NullValueHandling = NullValueHandling.Ignore)]
        public long MessageStatus { get; set; }

        [JsonProperty("NumberOfResultRecords", NullValueHandling = NullValueHandling.Ignore)]
        public long NumberOfResultRecords { get; set; }

        [JsonProperty("SuccessMsg")]
        public object SuccessMsg { get; set; }

        [JsonProperty("SuccessMsgN")]
        public object SuccessMsgN { get; set; }


    }
}
