﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class CheckForUsageAgreementResponse
    {
        [JsonProperty("Date")]
        public object Date { get; set; }

        [JsonProperty("LanguageID")]
        public long LanguageId { get; set; }

        [JsonProperty("ServiceName")]
        public long ServiceName { get; set; }

        [JsonProperty("Time")]
        public object Time { get; set; }

        [JsonProperty("AndroidLink")]
        public object AndroidLink { get; set; }

        [JsonProperty("AuthenticationTokenID")]
        public object AuthenticationTokenId { get; set; }

        [JsonProperty("Data")]
        public object Data { get; set; }

        [JsonProperty("Dataw")]
        public bool Dataw { get; set; }

        [JsonProperty("DietType")]
        public long DietType { get; set; }

        [JsonProperty("ErrorCode")]
        public object ErrorCode { get; set; }

        [JsonProperty("ErrorEndUserMessage")]
        public object ErrorEndUserMessage { get; set; }

        [JsonProperty("ErrorEndUserMessageN")]
        public object ErrorEndUserMessageN { get; set; }

        [JsonProperty("ErrorMessage")]
        public object ErrorMessage { get; set; }

        [JsonProperty("ErrorType")]
        public long ErrorType { get; set; }

        [JsonProperty("FoodCategory")]
        public long FoodCategory { get; set; }

        [JsonProperty("IOSLink")]
        public object IosLink { get; set; }

        [JsonProperty("IsAuthenticated")]
        public bool IsAuthenticated { get; set; }

        [JsonProperty("MealOrderStatus")]
        public long MealOrderStatus { get; set; }

        [JsonProperty("MealType")]
        public long MealType { get; set; }

        [JsonProperty("MessageStatus")]
        public long MessageStatus { get; set; }

        [JsonProperty("NumberOfResultRecords")]
        public long NumberOfResultRecords { get; set; }

        [JsonProperty("PatientBlodType")]
        public object PatientBlodType { get; set; }

        [JsonProperty("SuccessMsg")]
        public object SuccessMsg { get; set; }

        [JsonProperty("SuccessMsgN")]
        public object SuccessMsgN { get; set; }

        [JsonProperty("AccountStatus")]
        public long AccountStatus { get; set; }

        [JsonProperty("ActiveArchiveObject")]
        public object ActiveArchiveObject { get; set; }

        [JsonProperty("ActiveMedicationCount")]
        public long ActiveMedicationCount { get; set; }

        [JsonProperty("AllMedicationTakenDuringAdmission_List")]
        public object AllMedicationTakenDuringAdmissionList { get; set; }

        [JsonProperty("AppointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("ArePatientsOnlineList")]
        public object ArePatientsOnlineList { get; set; }

        [JsonProperty("BalanceAmount")]
        public object BalanceAmount { get; set; }

        [JsonProperty("BloodGroupList")]
        public object BloodGroupList { get; set; }

        [JsonProperty("CVI_UnreadCount")]
        public long CviUnreadCount { get; set; }

        [JsonProperty("CheckUserHasAccount")]
        public object CheckUserHasAccount { get; set; }

        [JsonProperty("ComplaintNo")]
        public long ComplaintNo { get; set; }

        [JsonProperty("DischargeList")]
        public object DischargeList { get; set; }

        [JsonProperty("EpisodeID")]
        public long EpisodeId { get; set; }

        [JsonProperty("FinalRadiologyList")]
        public object FinalRadiologyList { get; set; }

        [JsonProperty("FullName")]
        public object FullName { get; set; }

        [JsonProperty("GeoF_PointsList")]
        public object GeoFPointsList { get; set; }

        [JsonProperty("GeoGetPateintInfo")]
        public object GeoGetPateintInfo { get; set; }

        [JsonProperty("GetAllDoctorsByProjectAndClinicList")]
        public object GetAllDoctorsByProjectAndClinicList { get; set; }

        [JsonProperty("GetAppointmentNumbersForDoctorList")]
        public object GetAppointmentNumbersForDoctorList { get; set; }

        [JsonProperty("GetCheckUpItemsList")]
        public object GetCheckUpItemsList { get; set; }

        [JsonProperty("GetCosmeticConferenceForTodayList")]
        public object GetCosmeticConferenceForTodayList { get; set; }

        [JsonProperty("GetDoctorERClinicResult")]
        public object GetDoctorErClinicResult { get; set; }

        [JsonProperty("GetInvoiceApprovalList")]
        public object GetInvoiceApprovalList { get; set; }

        [JsonProperty("GetNearestProjectList")]
        public object GetNearestProjectList { get; set; }

        [JsonProperty("GetPatientAdmissionOrAppoinmentNo_List")]
        public object GetPatientAdmissionOrAppoinmentNoList { get; set; }

        [JsonProperty("GetPatientBloodType")]
        public object GetPatientBloodType { get; set; }

        [JsonProperty("GetPatientInsuranceCardStatusStatisticsList")]
        public object GetPatientInsuranceCardStatusStatisticsList { get; set; }

        [JsonProperty("GetSurveyList")]
        public object GetSurveyList { get; set; }

        [JsonProperty("GetTotalRegisteredPatientList")]
        public object GetTotalRegisteredPatientList { get; set; }

        [JsonProperty("GetUserDetailsList")]
        public object GetUserDetailsList { get; set; }

        [JsonProperty("Get_CustomerPointInfo")]
        public object GetCustomerPointInfo { get; set; }

        [JsonProperty("HIS_Approval_List")]
        public object HisApprovalList { get; set; }

        [JsonProperty("HIS_InpAdmission_List")]
        public object HisInpAdmissionList { get; set; }

        [JsonProperty("HIS_ProgNoteAssesmentModel_List")]
        public object HisProgNoteAssesmentModelList { get; set; }

        [JsonProperty("HMG_GetAllOffersList")]
        public object HmgGetAllOffersList { get; set; }

        [JsonProperty("Has_Approval")]
        public bool HasApproval { get; set; }

        [JsonProperty("Has_Consultation")]
        public bool HasConsultation { get; set; }

        [JsonProperty("Has_Dental")]
        public bool HasDental { get; set; }

        [JsonProperty("Has_Lab")]
        public bool HasLab { get; set; }

        [JsonProperty("Has_Pharmacy")]
        public bool HasPharmacy { get; set; }

        [JsonProperty("Has_Rad")]
        public bool HasRad { get; set; }

        [JsonProperty("Hmg_SMS_Get_By_ProjectID_And_PatientIDList")]
        public object HmgSmsGetByProjectIdAndPatientIdList { get; set; }

        [JsonProperty("HoursLeft")]
        public long HoursLeft { get; set; }

        [JsonProperty("INPM_GetAllAdmission_List")]
        public object InpmGetAllAdmissionList { get; set; }

        [JsonProperty("INPM_GetPatientInfoForSickLeaveReport_List")]
        public object InpmGetPatientInfoForSickLeaveReportList { get; set; }

        [JsonProperty("INPM_HIS_PatientMedicalStatus_UnreadCount")]
        public object InpmHisPatientMedicalStatusUnreadCount { get; set; }

        [JsonProperty("INPM_LAB_GetPatientLabOrdersResults_List")]
        public object InpmLabGetPatientLabOrdersResultsList { get; set; }

        [JsonProperty("INPM_LAB_GetPatientLabResults_List")]
        public object InpmLabGetPatientLabResultsList { get; set; }

        [JsonProperty("INPM_LAB_GetPatientRADReport_List")]
        public object InpmLabGetPatientRadReportList { get; set; }

        [JsonProperty("INPM_LAB_GetPatientRadResults_List")]
        public object InpmLabGetPatientRadResultsList { get; set; }

        [JsonProperty("INPM_Rad_GetPatientRadOrders_CVI_List")]
        public object InpmRadGetPatientRadOrdersCviList { get; set; }

        [JsonProperty("INPM_Rad_GetPatientRadOrders_List")]
        public object InpmRadGetPatientRadOrdersList { get; set; }

        [JsonProperty("INPM_Rad_GetRadMedicalRecords_List")]
        public object InpmRadGetRadMedicalRecordsList { get; set; }

        [JsonProperty("INP_GetPrescriptionDischarges_List")]
        public object InpGetPrescriptionDischargesList { get; set; }

        [JsonProperty("INP_GetPrescriptionReport_List")]
        public object InpGetPrescriptionReportList { get; set; }

        [JsonProperty("IdentificationNo")]
        public object IdentificationNo { get; set; }

        [JsonProperty("IsHomeMedicineDeliverySupported")]
        public bool IsHomeMedicineDeliverySupported { get; set; }

        [JsonProperty("IsInsertedOrUpdated")]
        public long IsInsertedOrUpdated { get; set; }

        [JsonProperty("IsMainAcoountEqualPatienID")]
        public bool IsMainAcoountEqualPatienId { get; set; }

        [JsonProperty("IsPatientAlreadyAgreed")]
        public bool IsPatientAlreadyAgreed { get; set; }

        [JsonProperty("IsPatientCallBackBlackList")]
        public bool IsPatientCallBackBlackList { get; set; }

        [JsonProperty("IsPatientHaveFingerPrint")]
        public bool IsPatientHaveFingerPrint { get; set; }

        [JsonProperty("IsPatientOnline")]
        public bool IsPatientOnline { get; set; }

        [JsonProperty("IsPatientTokenRemoved")]
        public bool IsPatientTokenRemoved { get; set; }

        [JsonProperty("IsPaused")]
        public bool IsPaused { get; set; }

        [JsonProperty("IsProjectWorkingHours")]
        public bool IsProjectWorkingHours { get; set; }

        [JsonProperty("IsStoreRateAllowed")]
        public object IsStoreRateAllowed { get; set; }

        [JsonProperty("IsStoreRateInserted")]
        public object IsStoreRateInserted { get; set; }

        [JsonProperty("IsStoreRateUpdated")]
        public object IsStoreRateUpdated { get; set; }

        [JsonProperty("LabRadUpdatedToRead")]
        public long LabRadUpdatedToRead { get; set; }

        [JsonProperty("LabReportUnreadNo")]
        public long LabReportUnreadNo { get; set; }

        [JsonProperty("LakumInquiryInformationObj")]
        public object LakumInquiryInformationObj { get; set; }

        [JsonProperty("LakumInquiryInformationObjVersion")]
        public object LakumInquiryInformationObjVersion { get; set; }

        [JsonProperty("LakumResponseList")]
        public object LakumResponseList { get; set; }

        [JsonProperty("Laser_GetBodyPartsByCategoryList")]
        public object LaserGetBodyPartsByCategoryList { get; set; }

        [JsonProperty("Laser_GetCategoriesList")]
        public object LaserGetCategoriesList { get; set; }

        [JsonProperty("List")]
        public object List { get; set; }

        [JsonProperty("ListCount")]
        public long ListCount { get; set; }

        [JsonProperty("ListDeviceInfo")]
        public object ListDeviceInfo { get; set; }

        [JsonProperty("ListFamilyAppointments")]
        public object ListFamilyAppointments { get; set; }

        [JsonProperty("ListLabResultsByAppNo")]
        public object ListLabResultsByAppNo { get; set; }

        [JsonProperty("ListLakumInquiryInformationObj")]
        public object ListLakumInquiryInformationObj { get; set; }

        [JsonProperty("ListOpinion_GetAllPeriod")]
        public object ListOpinionGetAllPeriod { get; set; }

        [JsonProperty("ListOpinion_GetAllServices")]
        public object ListOpinionGetAllServices { get; set; }

        [JsonProperty("ListOpinion_GetIsAgreeValue")]
        public object ListOpinionGetIsAgreeValue { get; set; }

        [JsonProperty("ListOpinion_GetOpinionLogin")]
        public object ListOpinionGetOpinionLogin { get; set; }

        [JsonProperty("ListOpinion_GetRequestedSerives")]
        public object ListOpinionGetRequestedSerives { get; set; }

        [JsonProperty("ListOpinion_GetShareServicesDetails")]
        public object ListOpinionGetShareServicesDetails { get; set; }

        [JsonProperty("ListOpinion_UserTerms")]
        public object ListOpinionUserTerms { get; set; }

        [JsonProperty("ListPLO")]
        public object ListPlo { get; set; }

        [JsonProperty("ListPLR")]
        public object ListPlr { get; set; }

        [JsonProperty("ListPLSR")]
        public object ListPlsr { get; set; }

        [JsonProperty("ListPRM")]
        public object ListPrm { get; set; }

        [JsonProperty("ListPatientFamilyFiles")]
        public object ListPatientFamilyFiles { get; set; }

        [JsonProperty("ListPatientFileInfo")]
        public object ListPatientFileInfo { get; set; }

        [JsonProperty("ListRAD")]
        public object ListRad { get; set; }

        [JsonProperty("ListRADAPI")]
        public object ListRadapi { get; set; }

        [JsonProperty("List_ActiveGetPrescriptionReportByPatientID")]
        public object ListActiveGetPrescriptionReportByPatientId { get; set; }

        [JsonProperty("List_BabyInfoResult")]
        public object ListBabyInfoResult { get; set; }

        [JsonProperty("List_CheckInsuranceCoverage")]
        public object ListCheckInsuranceCoverage { get; set; }

        [JsonProperty("List_CompanyClass")]
        public object ListCompanyClass { get; set; }

        [JsonProperty("List_ConsentMedicalReport")]
        public object ListConsentMedicalReport { get; set; }

        [JsonProperty("List_DeviceTokenIDByAppointmentNo")]
        public object ListDeviceTokenIdByAppointmentNo { get; set; }

        [JsonProperty("List_DischargeDiagnosis")]
        public object ListDischargeDiagnosis { get; set; }

        [JsonProperty("List_DischargeMedicine")]
        public object ListDischargeMedicine { get; set; }

        [JsonProperty("List_DischargeSummary")]
        public object ListDischargeSummary { get; set; }

        [JsonProperty("List_DoctorResponse")]
        public object ListDoctorResponse { get; set; }

        [JsonProperty("List_DoneVaccines")]
        public object ListDoneVaccines { get; set; }

        [JsonProperty("List_EReferralResult")]
        public object ListEReferralResult { get; set; }

        [JsonProperty("List_EReferrals")]
        public object ListEReferrals { get; set; }

        [JsonProperty("List_GetAllPatients_LiveCare_Admin")]
        public object ListGetAllPatientsLiveCareAdmin { get; set; }

        [JsonProperty("List_GetDataForExcel")]
        public object ListGetDataForExcel { get; set; }

        [JsonProperty("List_GetMainCountID")]
        public object ListGetMainCountId { get; set; }

        [JsonProperty("List_GetPrescriptionReportByPatientID")]
        public object ListGetPrescriptionReportByPatientId { get; set; }

        [JsonProperty("List_GetSickLeave")]
        public object ListGetSickLeave { get; set; }

        [JsonProperty("List_HISInvoice")]
        public object ListHisInvoice { get; set; }

        [JsonProperty("List_HISInvoiceProcedures")]
        public object ListHisInvoiceProcedures { get; set; }

        [JsonProperty("List_InpatientInvoices")]
        public object ListInpatientInvoices { get; set; }

        [JsonProperty("List_InsuranceCheckList")]
        public object ListInsuranceCheckList { get; set; }

        [JsonProperty("List_InsuranceCompanies")]
        public object ListInsuranceCompanies { get; set; }

        [JsonProperty("List_InsuranceCompaniesGroup")]
        public object ListInsuranceCompaniesGroup { get; set; }

        [JsonProperty("List_InsuranceUpdateDetails")]
        public object ListInsuranceUpdateDetails { get; set; }

        [JsonProperty("List_InvoiceApprovalProcedureInfo")]
        public object ListInvoiceApprovalProcedureInfo { get; set; }

        [JsonProperty("List_IsLastSatisfactionSurveyReviewedModel")]
        public object ListIsLastSatisfactionSurveyReviewedModel { get; set; }

        [JsonProperty("List_LabOrderDetailsModel")]
        public object ListLabOrderDetailsModel { get; set; }

        [JsonProperty("List_MedicalReport")]
        public object ListMedicalReport { get; set; }

        [JsonProperty("List_MedicalReportApprovals")]
        public object ListMedicalReportApprovals { get; set; }

        [JsonProperty("List_MedicalReportStatus")]
        public object ListMedicalReportStatus { get; set; }

        [JsonProperty("List_MonthBloodPressureResult")]
        public object ListMonthBloodPressureResult { get; set; }

        [JsonProperty("List_MonthBloodPressureResultAverage")]
        public object ListMonthBloodPressureResultAverage { get; set; }

        [JsonProperty("List_MonthDiabtecPatientResult")]
        public object ListMonthDiabtecPatientResult { get; set; }

        [JsonProperty("List_MonthDiabtectResultAverage")]
        public object ListMonthDiabtectResultAverage { get; set; }

        [JsonProperty("List_MonthWeightMeasurementResult")]
        public object ListMonthWeightMeasurementResult { get; set; }

        [JsonProperty("List_MonthWeightMeasurementResultAverage")]
        public object ListMonthWeightMeasurementResultAverage { get; set; }

        [JsonProperty("List_OnlinePrescriptionResult")]
        public object ListOnlinePrescriptionResult { get; set; }

        [JsonProperty("List_OutPatientInvoices")]
        public object ListOutPatientInvoices { get; set; }

        [JsonProperty("List_PHRInvoice")]
        public object ListPhrInvoice { get; set; }

        [JsonProperty("List_PHRInvoiceItems")]
        public object ListPhrInvoiceItems { get; set; }

        [JsonProperty("List_PHRPaymentMethods")]
        public object ListPhrPaymentMethods { get; set; }

        [JsonProperty("List_PateintDetails")]
        public object ListPateintDetails { get; set; }

        [JsonProperty("List_PateintInformation")]
        public object ListPateintInformation { get; set; }

        [JsonProperty("List_PatientAdmissionInfo")]
        public object ListPatientAdmissionInfo { get; set; }

        [JsonProperty("List_PatientAdvanceBalanceAmount")]
        public object ListPatientAdvanceBalanceAmount { get; set; }

        [JsonProperty("List_PatientCallBackLogs")]
        public object ListPatientCallBackLogs { get; set; }

        [JsonProperty("List_PatientCallBackToUpdateFromICServer")]
        public object ListPatientCallBackToUpdateFromIcServer { get; set; }

        [JsonProperty("List_PatientCount")]
        public object ListPatientCount { get; set; }

        [JsonProperty("List_PatientDashboard")]
        public object ListPatientDashboard { get; set; }

        [JsonProperty("List_PatientER_GetAdminClinicsModel")]
        public object ListPatientErGetAdminClinicsModel { get; set; }

        [JsonProperty("List_PatientER_GetAdminProjectsModel")]
        public object ListPatientErGetAdminProjectsModel { get; set; }

        [JsonProperty("List_PatientER_GetAllClinicsModel")]
        public object ListPatientErGetAllClinicsModel { get; set; }

        [JsonProperty("List_PatientHISInvoices")]
        public object ListPatientHisInvoices { get; set; }

        [JsonProperty("List_PatientICProjects")]
        public object ListPatientIcProjects { get; set; }

        [JsonProperty("List_PatientICProjectsByID")]
        public object ListPatientIcProjectsById { get; set; }

        [JsonProperty("List_PatientICProjectsTimings")]
        public object ListPatientIcProjectsTimings { get; set; }

        [JsonProperty("List_PatientIDByUID")]
        public object ListPatientIdByUid { get; set; }

        [JsonProperty("List_PatientIDForSurveyResult")]
        public object ListPatientIdForSurveyResult { get; set; }

        [JsonProperty("List_PatientInfo")]
        public object ListPatientInfo { get; set; }

        [JsonProperty("List_PatientInfoForDDScreen")]
        public object ListPatientInfoForDdScreen { get; set; }

        [JsonProperty("List_PatientInfoForSickleaveReport")]
        public object ListPatientInfoForSickleaveReport { get; set; }

        [JsonProperty("List_PatientInsuranceCard")]
        public object ListPatientInsuranceCard { get; set; }

        [JsonProperty("List_PatientInsuranceCardHistory")]
        public object ListPatientInsuranceCardHistory { get; set; }

        [JsonProperty("List_PatientInsuranceDetails")]
        public object ListPatientInsuranceDetails { get; set; }

        [JsonProperty("List_PatientPHRInvoices")]
        public object ListPatientPhrInvoices { get; set; }

        [JsonProperty("List_PatientServicePoint")]
        public object ListPatientServicePoint { get; set; }

        [JsonProperty("List_PatientStatusCount")]
        public object ListPatientStatusCount { get; set; }

        [JsonProperty("List_Patient_ChatRequestMapModel")]
        public object ListPatientChatRequestMapModel { get; set; }

        [JsonProperty("List_Patient_ChatRequestModel")]
        public object ListPatientChatRequestModel { get; set; }

        [JsonProperty("List_Patient_ChatRequestVCModel")]
        public object ListPatientChatRequestVcModel { get; set; }

        [JsonProperty("List_PaymentMethods")]
        public object ListPaymentMethods { get; set; }

        [JsonProperty("List_PointServices")]
        public object ListPointServices { get; set; }

        [JsonProperty("List_PregnancyStagesInfo")]
        public object ListPregnancyStagesInfo { get; set; }

        [JsonProperty("List_ProjectAvgERWaitingTime")]
        public object ListProjectAvgErWaitingTime { get; set; }

        [JsonProperty("List_ProjectAvgERWaitingTimeHourly")]
        public object ListProjectAvgErWaitingTimeHourly { get; set; }

        [JsonProperty("List_RadMedicalRecords")]
        public object ListRadMedicalRecords { get; set; }

        [JsonProperty("List_RadMedicalRecordsAPI")]
        public object ListRadMedicalRecordsApi { get; set; }

        [JsonProperty("List_RadMedicalRecordsCVI")]
        public object ListRadMedicalRecordsCvi { get; set; }

        [JsonProperty("List_RadMedicalRecordsCVIAPI")]
        public object ListRadMedicalRecordsCviapi { get; set; }

        [JsonProperty("List_RadMedicalRecordsResults")]
        public object ListRadMedicalRecordsResults { get; set; }

        [JsonProperty("List_SickLeave")]
        public object ListSickLeave { get; set; }

        [JsonProperty("List_Transaction")]
        public object ListTransaction { get; set; }

        [JsonProperty("List_VideoConferenceSessions")]
        public object ListVideoConferenceSessions { get; set; }

        [JsonProperty("List_WeekBloodPressureResult")]
        public object ListWeekBloodPressureResult { get; set; }

        [JsonProperty("List_WeekBloodPressureResultAverage")]
        public object ListWeekBloodPressureResultAverage { get; set; }

        [JsonProperty("List_WeekDiabtecPatientResult")]
        public object ListWeekDiabtecPatientResult { get; set; }

        [JsonProperty("List_WeekDiabtectResultAverage")]
        public object ListWeekDiabtectResultAverage { get; set; }

        [JsonProperty("List_WeekWeightMeasurementResult")]
        public object ListWeekWeightMeasurementResult { get; set; }

        [JsonProperty("List_WeekWeightMeasurementResultAverage")]
        public object ListWeekWeightMeasurementResultAverage { get; set; }

        [JsonProperty("List_YearBloodPressureResult")]
        public object ListYearBloodPressureResult { get; set; }

        [JsonProperty("List_YearBloodPressureResultAverage")]
        public object ListYearBloodPressureResultAverage { get; set; }

        [JsonProperty("List_YearDiabtecPatientResult")]
        public object ListYearDiabtecPatientResult { get; set; }

        [JsonProperty("List_YearDiabtecResultAverage")]
        public object ListYearDiabtecResultAverage { get; set; }

        [JsonProperty("List_YearWeightMeasurementResult")]
        public object ListYearWeightMeasurementResult { get; set; }

        [JsonProperty("List_YearWeightMeasurementResultAverage")]
        public object ListYearWeightMeasurementResultAverage { get; set; }

        [JsonProperty("List_eInvoiceForDental")]
        public object ListEInvoiceForDental { get; set; }

        [JsonProperty("List_eInvoiceForOnlineCheckIn")]
        public object ListEInvoiceForOnlineCheckIn { get; set; }

        [JsonProperty("Med_GetActivitiesTransactionsStsList")]
        public object MedGetActivitiesTransactionsStsList { get; set; }

        [JsonProperty("Med_GetAvgMonthTransactionsStsList")]
        public object MedGetAvgMonthTransactionsStsList { get; set; }

        [JsonProperty("Med_GetAvgWeekTransactionsStsList")]
        public object MedGetAvgWeekTransactionsStsList { get; set; }

        [JsonProperty("Med_GetCategoriesList")]
        public object MedGetCategoriesList { get; set; }

        [JsonProperty("Med_GetMonthActivitiesTransactionsStsList")]
        public object MedGetMonthActivitiesTransactionsStsList { get; set; }

        [JsonProperty("Med_GetMonthStepsTransactionsStsList")]
        public object MedGetMonthStepsTransactionsStsList { get; set; }

        [JsonProperty("Med_GetMonthTransactionsStsList")]
        public object MedGetMonthTransactionsStsList { get; set; }

        [JsonProperty("Med_GetPatientLastRecordList")]
        public object MedGetPatientLastRecordList { get; set; }

        [JsonProperty("Med_GetSubCategoriesList")]
        public object MedGetSubCategoriesList { get; set; }

        [JsonProperty("Med_GetTransactionsAndActTransactionsResult")]
        public object MedGetTransactionsAndActTransactionsResult { get; set; }

        [JsonProperty("Med_GetTransactionsList")]
        public object MedGetTransactionsList { get; set; }

        [JsonProperty("Med_GetWeekActivitiesTransactionsStsList")]
        public object MedGetWeekActivitiesTransactionsStsList { get; set; }

        [JsonProperty("Med_GetWeekStepsTransactionsStsList")]
        public object MedGetWeekStepsTransactionsStsList { get; set; }

        [JsonProperty("Med_GetWeekTransactionsStsList")]
        public object MedGetWeekTransactionsStsList { get; set; }

        [JsonProperty("Med_GetYearActivitiesTransactionsStsList")]
        public object MedGetYearActivitiesTransactionsStsList { get; set; }

        [JsonProperty("Med_GetYearSleepTransactionsStsList")]
        public object MedGetYearSleepTransactionsStsList { get; set; }

        [JsonProperty("Med_GetYearStepsTransactionsStsList")]
        public object MedGetYearStepsTransactionsStsList { get; set; }

        [JsonProperty("Med_GetYearTransactionsStsList")]
        public object MedGetYearTransactionsStsList { get; set; }

        [JsonProperty("Med_InsertTransactionsOutputsList")]
        public object MedInsertTransactionsOutputsList { get; set; }

        [JsonProperty("MedicalRecordImages")]
        public object MedicalRecordImages { get; set; }

        [JsonProperty("MedicalReportToRead")]
        public long MedicalReportToRead { get; set; }

        [JsonProperty("MedicalReportUnreadNo")]
        public long MedicalReportUnreadNo { get; set; }

        [JsonProperty("Missing_IDCardAttachment")]
        public bool MissingIdCardAttachment { get; set; }

        [JsonProperty("Missing_InsuranceCardAttachment")]
        public bool MissingInsuranceCardAttachment { get; set; }

        [JsonProperty("Missing_MedicalReportAttachment")]
        public bool MissingMedicalReportAttachment { get; set; }

        [JsonProperty("Missing_OtherRelationship")]
        public bool MissingOtherRelationship { get; set; }

        [JsonProperty("Missing_PatientContactNo")]
        public bool MissingPatientContactNo { get; set; }

        [JsonProperty("Missing_PatientId")]
        public bool MissingPatientId { get; set; }

        [JsonProperty("Missing_PatientIdentityNumber")]
        public bool MissingPatientIdentityNumber { get; set; }

        [JsonProperty("Missing_PatientName")]
        public bool MissingPatientName { get; set; }

        [JsonProperty("Missing_ReferralContactNo")]
        public bool MissingReferralContactNo { get; set; }

        [JsonProperty("Missing_ReferralRelationship")]
        public bool MissingReferralRelationship { get; set; }

        [JsonProperty("Missing_ReferralRequesterName")]
        public bool MissingReferralRequesterName { get; set; }

        [JsonProperty("MobileNumber")]
        public object MobileNumber { get; set; }

        [JsonProperty("NationalityNumber")]
        public long NationalityNumber { get; set; }

        [JsonProperty("OnlineCheckInAppointments")]
        public object OnlineCheckInAppointments { get; set; }

        [JsonProperty("Opinion_UserAgreementContent")]
        public object OpinionUserAgreementContent { get; set; }

        [JsonProperty("OrderInsert")]
        public bool OrderInsert { get; set; }

        [JsonProperty("PateintInfoForUpdateList")]
        public object PateintInfoForUpdateList { get; set; }

        [JsonProperty("PateintUpatedList")]
        public object PateintUpatedList { get; set; }

        [JsonProperty("PatientBirthdayCertificate")]
        public object PatientBirthdayCertificate { get; set; }

        [JsonProperty("PatientER_CMCRequestSummaryByProject")]
        public object PatientErCmcRequestSummaryByProject { get; set; }

        [JsonProperty("PatientER_CMCRequestWithTotal")]
        public object PatientErCmcRequestWithTotal { get; set; }

        [JsonProperty("PatientER_CMC_GetAllServicesList")]
        public object PatientErCmcGetAllServicesList { get; set; }

        [JsonProperty("PatientER_CMC_GetTransactionsForOrderList")]
        public object PatientErCmcGetTransactionsForOrderList { get; set; }

        [JsonProperty("PatientER_Coordinates")]
        public object PatientErCoordinates { get; set; }

        [JsonProperty("PatientER_CountsForApprovalOffice")]
        public object PatientErCountsForApprovalOffice { get; set; }

        [JsonProperty("PatientER_DeleteOldCurrentDoctorsOutputsList")]
        public object PatientErDeleteOldCurrentDoctorsOutputsList { get; set; }

        [JsonProperty("PatientER_Delivery_GetAllDeliverdOrderList")]
        public object PatientErDeliveryGetAllDeliverdOrderList { get; set; }

        [JsonProperty("PatientER_Delivery_GetAllOrderList")]
        public object PatientErDeliveryGetAllOrderList { get; set; }

        [JsonProperty("PatientER_Delivery_IsOrderInserted")]
        public bool PatientErDeliveryIsOrderInserted { get; set; }

        [JsonProperty("PatientER_Delivery_IsOrderUpdated")]
        public bool PatientErDeliveryIsOrderUpdated { get; set; }

        [JsonProperty("PatientER_Delivery_IsPausedChanged")]
        public bool PatientErDeliveryIsPausedChanged { get; set; }

        [JsonProperty("PatientER_Delivery_NextOrder")]
        public object PatientErDeliveryNextOrder { get; set; }

        [JsonProperty("PatientER_Delivery_OrderInsert")]
        public long PatientErDeliveryOrderInsert { get; set; }

        [JsonProperty("PatientER_Delivery_UpdateOrderStatus")]
        public long PatientErDeliveryUpdateOrderStatus { get; set; }

        [JsonProperty("PatientER_Exacart_CheckIsDispenseAccpetableList")]
        public object PatientErExacartCheckIsDispenseAccpetableList { get; set; }

        [JsonProperty("PatientER_Exacart_GetDispenseQuantitiesByOrderIDList")]
        public object PatientErExacartGetDispenseQuantitiesByOrderIdList { get; set; }

        [JsonProperty("PatientER_Exacart_GetOrderDetailsByePharmacyOrderNoList")]
        public object PatientErExacartGetOrderDetailsByePharmacyOrderNoList { get; set; }

        [JsonProperty("PatientER_Exacart_GetOrderDetailsList")]
        public object PatientErExacartGetOrderDetailsList { get; set; }

        [JsonProperty("PatientER_Exacart_IsDispenseAdded")]
        public bool PatientErExacartIsDispenseAdded { get; set; }

        [JsonProperty("PatientER_Exacart_IsDispenseAddedList")]
        public object PatientErExacartIsDispenseAddedList { get; set; }

        [JsonProperty("PatientER_Exacart_IsOrderCompleted")]
        public bool PatientErExacartIsOrderCompleted { get; set; }

        [JsonProperty("PatientER_GetAdminByProjectAndRoleList")]
        public object PatientErGetAdminByProjectAndRoleList { get; set; }

        [JsonProperty("PatientER_GetAdminProjectsList")]
        public object PatientErGetAdminProjectsList { get; set; }

        [JsonProperty("PatientER_GetAllNeedAproveStatusList")]
        public object PatientErGetAllNeedAproveStatusList { get; set; }

        [JsonProperty("PatientER_GetAllPresOrdersStatusList")]
        public object PatientErGetAllPresOrdersStatusList { get; set; }

        [JsonProperty("PatientER_GetAllProjectsList")]
        public object PatientErGetAllProjectsList { get; set; }

        [JsonProperty("PatientER_GetArchiveInformation_List")]
        public object PatientErGetArchiveInformationList { get; set; }

        [JsonProperty("PatientER_GetAskDoctorTotalByDateFilterList")]
        public object PatientErGetAskDoctorTotalByDateFilterList { get; set; }

        [JsonProperty("PatientER_GetBookScheduleConfigsList")]
        public object PatientErGetBookScheduleConfigsList { get; set; }

        [JsonProperty("PatientER_GetClinicAndTimeAndEpisodeForAppointmentList")]
        public object PatientErGetClinicAndTimeAndEpisodeForAppointmentList { get; set; }

        [JsonProperty("PatientER_GetClinicAndTimeForDischargeList")]
        public object PatientErGetClinicAndTimeForDischargeList { get; set; }

        [JsonProperty("PatientER_GetDashboardDataforApporvalSectionForAdminList")]
        public object PatientErGetDashboardDataforApporvalSectionForAdminList { get; set; }

        [JsonProperty("PatientER_GetDashboardDataforApporvalSectionList")]
        public object PatientErGetDashboardDataforApporvalSectionList { get; set; }

        [JsonProperty("PatientER_GetDashboardDataforHHCSectionForAdminList")]
        public object PatientErGetDashboardDataforHhcSectionForAdminList { get; set; }

        [JsonProperty("PatientER_GetDashboardDataforHHCSectionList")]
        public object PatientErGetDashboardDataforHhcSectionList { get; set; }

        [JsonProperty("PatientER_GetDashboardDataforPrescriptionSectionForAdminList")]
        public object PatientErGetDashboardDataforPrescriptionSectionForAdminList { get; set; }

        [JsonProperty("PatientER_GetDashboardDataforPrescriptionSectionList")]
        public object PatientErGetDashboardDataforPrescriptionSectionList { get; set; }

        [JsonProperty("PatientER_GetDoctorDashboardDataModelList")]
        public object PatientErGetDoctorDashboardDataModelList { get; set; }

        [JsonProperty("PatientER_GetInsuranceCardRequestByDateFilterList")]
        public object PatientErGetInsuranceCardRequestByDateFilterList { get; set; }

        [JsonProperty("PatientER_GetLiveCareSummaryBookedAppoinmentStatusList")]
        public object PatientErGetLiveCareSummaryBookedAppoinmentStatusList { get; set; }

        [JsonProperty("PatientER_GetLiveCareSummaryCovidList")]
        public object PatientErGetLiveCareSummaryCovidList { get; set; }

        [JsonProperty("PatientER_GetLiveCareSummaryForCMCList")]
        public object PatientErGetLiveCareSummaryForCmcList { get; set; }

        [JsonProperty("PatientER_GetLiveCareSummaryForHHCList")]
        public object PatientErGetLiveCareSummaryForHhcList { get; set; }

        [JsonProperty("PatientER_GetLiveCareSummaryForHomeDeliveryList")]
        public object PatientErGetLiveCareSummaryForHomeDeliveryList { get; set; }

        [JsonProperty("PatientER_GetLiveCareSummaryForInsuranceCardRequestList")]
        public object PatientErGetLiveCareSummaryForInsuranceCardRequestList { get; set; }

        [JsonProperty("PatientER_GetLiveCareSummaryForNewFilesList")]
        public object PatientErGetLiveCareSummaryForNewFilesList { get; set; }

        [JsonProperty("PatientER_GetLiveCareSummaryForOnlinePaymetRequestList")]
        public object PatientErGetLiveCareSummaryForOnlinePaymetRequestList { get; set; }

        [JsonProperty("PatientER_GetLiveCareSummaryForOnlinePharmacyOrdersList")]
        public object PatientErGetLiveCareSummaryForOnlinePharmacyOrdersList { get; set; }

        [JsonProperty("PatientER_GetLiveCareSummaryForTrasnportationList")]
        public object PatientErGetLiveCareSummaryForTrasnportationList { get; set; }

        [JsonProperty("PatientER_GetLiveCareSummaryLiveCareCountsList")]
        public object PatientErGetLiveCareSummaryLiveCareCountsList { get; set; }

        [JsonProperty("PatientER_GetMedicalRequestTotalByDateFilterList")]
        public object PatientErGetMedicalRequestTotalByDateFilterList { get; set; }

        [JsonProperty("PatientER_GetNearestPendingOrdersList")]
        public object PatientErGetNearestPendingOrdersList { get; set; }

        [JsonProperty("PatientER_GetNeedAproveHistoryForOrderList")]
        public object PatientErGetNeedAproveHistoryForOrderList { get; set; }

        [JsonProperty("PatientER_GetNeedAprovePendingOrdersList")]
        public object PatientErGetNeedAprovePendingOrdersList { get; set; }

        [JsonProperty("PatientER_GetNeedAproveStatusStatisticsList")]
        public object PatientErGetNeedAproveStatusStatisticsList { get; set; }

        [JsonProperty("PatientER_GetPatientAllPresOrdersList")]
        public object PatientErGetPatientAllPresOrdersList { get; set; }

        [JsonProperty("PatientER_GetPendingPatientsCountList")]
        public object PatientErGetPendingPatientsCountList { get; set; }

        [JsonProperty("PatientER_GetPresOrdersHistoryForAdminList")]
        public object PatientErGetPresOrdersHistoryForAdminList { get; set; }

        [JsonProperty("PatientER_GetPresOrdersHistoryForOrderList")]
        public object PatientErGetPresOrdersHistoryForOrderList { get; set; }

        [JsonProperty("PatientER_GetPresOrdersStatusStatisticsList")]
        public object PatientErGetPresOrdersStatusStatisticsList { get; set; }

        [JsonProperty("PatientER_HHCRequest")]
        public object PatientErHhcRequest { get; set; }

        [JsonProperty("PatientER_HHCRequestSummaryByProject")]
        public object PatientErHhcRequestSummaryByProject { get; set; }

        [JsonProperty("PatientER_HHCRequestWithTotal")]
        public object PatientErHhcRequestWithTotal { get; set; }

        [JsonProperty("PatientER_HHC_GetAllServicesList")]
        public object PatientErHhcGetAllServicesList { get; set; }

        [JsonProperty("PatientER_HHC_GetTransactionsForOrderList")]
        public object PatientErHhcGetTransactionsForOrderList { get; set; }

        [JsonProperty("PatientER_HomeDeliveryCounts")]
        public object PatientErHomeDeliveryCounts { get; set; }

        [JsonProperty("PatientER_InsertNewCurrentDoctorsOutputsList")]
        public object PatientErInsertNewCurrentDoctorsOutputsList { get; set; }

        [JsonProperty("PatientER_InsuranceStatusCountList")]
        public object PatientErInsuranceStatusCountList { get; set; }

        [JsonProperty("PatientER_IsNearestProjectUpdated")]
        public bool PatientErIsNearestProjectUpdated { get; set; }

        [JsonProperty("PatientER_IsNeedAproveReturnedToQueue")]
        public bool PatientErIsNeedAproveReturnedToQueue { get; set; }

        [JsonProperty("PatientER_IsNeedAproveUpdated")]
        public bool PatientErIsNeedAproveUpdated { get; set; }

        [JsonProperty("PatientER_IsOrderClientRequestUpdated")]
        public bool PatientErIsOrderClientRequestUpdated { get; set; }

        [JsonProperty("PatientER_IsOrderReturnedToQueue")]
        public bool PatientErIsOrderReturnedToQueue { get; set; }

        [JsonProperty("PatientER_IsPresOrderInserted")]
        public bool PatientErIsPresOrderInserted { get; set; }

        [JsonProperty("PatientER_IsPresOrderUpdated")]
        public bool PatientErIsPresOrderUpdated { get; set; }

        [JsonProperty("PatientER_IsProjectUpdated")]
        public bool PatientErIsProjectUpdated { get; set; }

        [JsonProperty("PatientER_NotCompletedDetails")]
        public object PatientErNotCompletedDetails { get; set; }

        [JsonProperty("PatientER_PatientsCountByCallStatus")]
        public object PatientErPatientsCountByCallStatus { get; set; }

        [JsonProperty("PatientER_PeakHourCounts")]
        public object PatientErPeakHourCounts { get; set; }

        [JsonProperty("PatientER_PresOrderInfo")]
        public object PatientErPresOrderInfo { get; set; }

        [JsonProperty("PatientER_PrescriptionCounts")]
        public object PatientErPrescriptionCounts { get; set; }

        [JsonProperty("PatientER_ProjectsContribution")]
        public object PatientErProjectsContribution { get; set; }

        [JsonProperty("PatientER_RRT_GetAllQuestionsList")]
        public object PatientErRrtGetAllQuestionsList { get; set; }

        [JsonProperty("PatientER_RRT_GetAllTransportationMethodList")]
        public object PatientErRrtGetAllTransportationMethodList { get; set; }

        [JsonProperty("PatientER_RRT_GetPickUpRequestByPresOrderIDList")]
        public object PatientErRrtGetPickUpRequestByPresOrderIdList { get; set; }

        [JsonProperty("PatientER_RealRRT_GetAllServicesList")]
        public object PatientErRealRrtGetAllServicesList { get; set; }

        [JsonProperty("PatientER_RealRRT_GetOrderDetailsList")]
        public object PatientErRealRrtGetOrderDetailsList { get; set; }

        [JsonProperty("PatientER_RealRRT_GetTransactionsForOrderList")]
        public object PatientErRealRrtGetTransactionsForOrderList { get; set; }

        [JsonProperty("PatientER_RealRRT_IsTransInserted")]
        public bool PatientErRealRrtIsTransInserted { get; set; }

        [JsonProperty("PatientER_RequestList")]
        public object PatientErRequestList { get; set; }

        [JsonProperty("PatientER_TransportationRequestWithTotal")]
        public object PatientErTransportationRequestWithTotal { get; set; }

        [JsonProperty("PatientE_RealRRT_GetServicePriceList")]
        public object PatientERealRrtGetServicePriceList { get; set; }

        [JsonProperty("PatientInfoByAdmissionNo_List")]
        public object PatientInfoByAdmissionNoList { get; set; }

        [JsonProperty("PatientMonitor_GetPatientHeartRate")]
        public object PatientMonitorGetPatientHeartRate { get; set; }

        [JsonProperty("PatientNotServedCounts")]
        public long PatientNotServedCounts { get; set; }

        [JsonProperty("PatientPrescriptionList")]
        public object PatientPrescriptionList { get; set; }

        [JsonProperty("Patient_Allergies")]
        public object PatientAllergies { get; set; }

        [JsonProperty("Patient_CheckAppointmentValidationList")]
        public object PatientCheckAppointmentValidationList { get; set; }

        [JsonProperty("Patient_LoginTokenList")]
        public object PatientLoginTokenList { get; set; }

        [JsonProperty("Patient_QRLoginInfoList")]
        public object PatientQrLoginInfoList { get; set; }

        [JsonProperty("Patient_SELECTDeviceIMEIbyIMEIList")]
        public object PatientSelectDeviceImeIbyImeiList { get; set; }

        [JsonProperty("PharmList")]
        public object PharmList { get; set; }

        [JsonProperty("PrefLang")]
        public object PrefLang { get; set; }

        [JsonProperty("RadReportUnreadNo")]
        public long RadReportUnreadNo { get; set; }

        [JsonProperty("Rad_GetPatientRadOrdersForDental_List")]
        public object RadGetPatientRadOrdersForDentalList { get; set; }

        [JsonProperty("ReferralNumber")]
        public long ReferralNumber { get; set; }

        [JsonProperty("ReminderConfigurations")]
        public object ReminderConfigurations { get; set; }

        [JsonProperty("RequestNo")]
        public object RequestNo { get; set; }

        [JsonProperty("RowCount")]
        public long RowCount { get; set; }

        [JsonProperty("ServicePrivilegeList")]
        public object ServicePrivilegeList { get; set; }

        [JsonProperty("ShareFamilyFileObj")]
        public object ShareFamilyFileObj { get; set; }

        [JsonProperty("Status")]
        public object Status { get; set; }

        [JsonProperty("SuccessCode")]
        public long SuccessCode { get; set; }

        [JsonProperty("SurveyRate")]
        public object SurveyRate { get; set; }

        [JsonProperty("SymptomChecker_ConditionList")]
        public object SymptomCheckerConditionList { get; set; }

        [JsonProperty("SymptomChecker_GetAllDefaultQuestionsList")]
        public object SymptomCheckerGetAllDefaultQuestionsList { get; set; }

        [JsonProperty("SymptomChecker_GetBodyPartSymptomsList")]
        public object SymptomCheckerGetBodyPartSymptomsList { get; set; }

        [JsonProperty("SymptomChecker_GetBodyPartsByCodeList")]
        public object SymptomCheckerGetBodyPartsByCodeList { get; set; }

        [JsonProperty("SymptomChecker_GetBodyPartsList")]
        public object SymptomCheckerGetBodyPartsList { get; set; }

        [JsonProperty("SymptomChecker_JsonResponseInString")]
        public object SymptomCheckerJsonResponseInString { get; set; }

        [JsonProperty("TimerTime")]
        public long TimerTime { get; set; }

        [JsonProperty("TotalAdvanceBalanceAmount")]
        public long TotalAdvanceBalanceAmount { get; set; }

        [JsonProperty("TotalPatientsCount")]
        public long TotalPatientsCount { get; set; }

        [JsonProperty("TotalPendingApprovalCount")]
        public long TotalPendingApprovalCount { get; set; }

        [JsonProperty("TotalUnUsedCount")]
        public long TotalUnUsedCount { get; set; }

        [JsonProperty("TransactionNo")]
        public long TransactionNo { get; set; }

        [JsonProperty("UnReadCounts")]
        public long UnReadCounts { get; set; }

        [JsonProperty("UpdateStatus")]
        public bool UpdateStatus { get; set; }

        [JsonProperty("UserAgreementContent")]
        public object UserAgreementContent { get; set; }

        [JsonProperty("YahalaAccountNo")]
        public long YahalaAccountNo { get; set; }

        [JsonProperty("check24HourComplaint")]
        public bool Check24HourComplaint { get; set; }

        [JsonProperty("currency")]
        public object Currency { get; set; }

        [JsonProperty("message")]
        public object Message { get; set; }

        [JsonProperty("patientID")]
        public long PatientId { get; set; }

        [JsonProperty("returnValue")]
        public long ReturnValue { get; set; }

        [JsonProperty("returnValueStr")]
        public object ReturnValueStr { get; set; }

        [JsonProperty("statusCode")]
        public long StatusCode { get; set; }
    }
}
