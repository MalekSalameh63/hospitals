﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class CheckActivationCode : AppointmentRequestBase
    {

        [JsonProperty("activationCode")]
        public string ActivationCode { get; set; }

        [JsonProperty("PatientMobileNumber")]
        public long PatientMobileNumber { get; set; }

        [JsonProperty("ZipCode")]
        public string ZipCode { get; set; }

        [JsonProperty("PatientID")]
        public long PatientId { get; set; }

        [JsonProperty("PatientIdentificationID")]
        public string PatientIdentificationId { get; set; }

        [JsonProperty("IsRegistered")]
        public bool IsRegistered { get; set; }
    }
}
