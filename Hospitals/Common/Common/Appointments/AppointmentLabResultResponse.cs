﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.Common.Appointments
{
    public class AppointmentLabResultResponse : AppointmentResponseBase
    {
        [JsonProperty("ListLabResultsByAppNo")]
        public List<ListLabResultsByAppNo> ListPlo { get; set; }
    }
    public class ListLabResultsByAppNo
    {
        [JsonProperty("SetupID")]
        public long SetupId { get; set; }

        [JsonProperty("OrderNo")]
        public long OrderNo { get; set; }

        [JsonProperty("InvoiceNo")]
        public long InvoiceNo { get; set; }

        [JsonProperty("PatientOutSA")]
        public object PatientOutSa { get; set; }

        [JsonProperty("patientLabResultList")]
        public List<PatientLabResultList> PatientLabResultList { get; set; }

        [JsonProperty("patientSpecialLabResultList")]
        public object PatientSpecialLabResultList { get; set; }
    }

    public class PatientLabResultList
    {
        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("FemaleInterpretativeData")]
        public object FemaleInterpretativeData { get; set; }

        [JsonProperty("Gender")]
        public long Gender { get; set; }

        [JsonProperty("LineItemNo")]
        public long LineItemNo { get; set; }

        [JsonProperty("MaleInterpretativeData")]
        public object MaleInterpretativeData { get; set; }

        [JsonProperty("Notes")]
        public object Notes { get; set; }

        [JsonProperty("PackageID")]
        public string PackageId { get; set; }

        [JsonProperty("PatientID")]
        public long PatientId { get; set; }

        [JsonProperty("ProjectID")]
        public string ProjectId { get; set; }

        [JsonProperty("ReferanceRange")]
        public string ReferanceRange { get; set; }

        [JsonProperty("ResultValue")]
        public string ResultValue { get; set; }

        [JsonProperty("SampleCollectedOn")]
        public string SampleCollectedOn { get; set; }

        [JsonProperty("SampleReceivedOn")]
        public string SampleReceivedOn { get; set; }

        [JsonProperty("SetupID")]
        public string SetupId { get; set; }

        [JsonProperty("SuperVerifiedOn")]
        public object SuperVerifiedOn { get; set; }

        [JsonProperty("TestCode")]
        public string TestCode { get; set; }

        [JsonProperty("UOM")]
        public string Uom { get; set; }

        [JsonProperty("VerifiedOn")]
        public string VerifiedOn { get; set; }

        [JsonProperty("VerifiedOnDateTime")]
        public object VerifiedOnDateTime { get; set; }
    }

}
