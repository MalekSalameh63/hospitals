﻿using Common.DTOs.Appointments;
using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class RescheduleAppointment : InsertAppointmentDto
    {
        [JsonProperty("old_appointment_number")]
        public long OldAppointmentNumber { get; set; }
    }
}