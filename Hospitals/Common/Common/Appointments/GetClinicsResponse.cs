﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.Common.Appointments
{
    public class GetClinicsResponse : AppointmentResponseBase
    {
        [JsonProperty("ListClinicCentralized")]
        public List<ListClinicCentralized> ListClinicCentralized { get; set; }

        [JsonProperty("List_GetBookScheduleConfigsList")]
        public List<ListGetBookScheduleConfigsList> ListGetBookScheduleConfigsList { get; set; }

    }
    public class ListClinicCentralized
    {
        [JsonProperty("ClinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("ClinicDescription")]
        public string ClinicDescription { get; set; }

        [JsonProperty("ClinicDescriptionN")]
        public object ClinicDescriptionN { get; set; }

        [JsonProperty("Age")]
        public long Age { get; set; }

        [JsonProperty("Gender")]
        public long Gender { get; set; }

        [JsonProperty("IsLiveCareClinicAndOnline")]
        public bool IsLiveCareClinicAndOnline { get; set; }

        [JsonProperty("LiveCareClinicID")]
        public long LiveCareClinicId { get; set; }

        [JsonProperty("LiveCareServiceID")]
        public long LiveCareServiceId { get; set; }
    }
    public partial class ListGetBookScheduleConfigsList
    {
        [JsonProperty("ID")]
        public long Id { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("DescriptionN")]
        public object DescriptionN { get; set; }

        [JsonProperty("Value")]
        public bool Value { get; set; }

        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }
    }
}
