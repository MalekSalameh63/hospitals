﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.Common.Appointments
{
    public class PatientShareResponse : AppointmentResponseBase
    {
        [JsonProperty("List_InsuranceCheckList")]
        public List<ListInsuranceCheckList> ListInsuranceCheckList { get; set; }
    }
    public partial class ListInsuranceCheckList
    {
        [JsonProperty("CompanyID")]
        public long CompanyId { get; set; }

        [JsonProperty("SubCategoryID")]
        public long SubCategoryId { get; set; }

        [JsonProperty("CheckList")]
        public string CheckList { get; set; }

        [JsonProperty("CheckListN")]
        public object CheckListN { get; set; }

        [JsonProperty("CheckListTXT")]
        public string CheckListTxt { get; set; }
    }
}
