﻿using Newtonsoft.Json;
using System;

namespace Common.Common.Appointments
{
    public class CancelAppointment : AppointmentRequestBase
    {
        [JsonProperty("AppointmentID")]
        public long AppointmentId { get; set; }

        [JsonProperty("DoctorID")]
        public long DoctorId { get; set; }

        [JsonProperty("StartTime")]
        public DateTimeOffset StartTime { get; set; }

        [JsonProperty("EndTime")]
        public DateTimeOffset EndTime { get; set; }

        [JsonProperty("OriginalClinicID")]
        public long OriginalClinicId { get; set; }

        [JsonProperty("OriginalProjectID")]
        public long OriginalProjectId { get; set; }

        [JsonProperty("StrAppointmentDate")]
        public string StrAppointmentDate { get; set; }

    }
}
