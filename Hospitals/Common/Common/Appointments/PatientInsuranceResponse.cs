﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.Common.Appointments
{
    public class PatientInsuranceResponse : AppointmentResponseBase
    {
        [JsonProperty("List_PatientInsuranceDetails")]
        public List<ListPatientInsuranceDetail> ListPatientInsuranceDetails { get; set; }
    }
    public class ListPatientInsuranceDetail
    {
        [JsonProperty("ApprovalLimit")]
        public long ApprovalLimit { get; set; }

        [JsonProperty("CompanyGroupID")]
        public long CompanyGroupId { get; set; }

        [JsonProperty("CompanyName")]
        public string CompanyName { get; set; }

        [JsonProperty("CompanyRemarks")]
        public string CompanyRemarks { get; set; }

        [JsonProperty("DOB")]
        public string Dob { get; set; }

        [JsonProperty("Deductible")]
        public long Deductible { get; set; }

        [JsonProperty("EffectiveFrom")]
        public string EffectiveFrom { get; set; }

        [JsonProperty("EffectiveTo")]
        public string EffectiveTo { get; set; }

        [JsonProperty("EligabilityFailureReason")]
        public string EligabilityFailureReason { get; set; }

        [JsonProperty("IsMemberEligible")]
        public bool IsMemberEligible { get; set; }

        [JsonProperty("MemberID")]
        public string MemberId { get; set; }

        [JsonProperty("MemberName")]
        public string MemberName { get; set; }

        [JsonProperty("MemberStatus")]
        public string MemberStatus { get; set; }

        [JsonProperty("Message")]
        public object Message { get; set; }

        [JsonProperty("NationalId")]
        public string NationalId { get; set; }

        [JsonProperty("PolicyNumber")]
        public string PolicyNumber { get; set; }

        [JsonProperty("ResponseMessage")]
        public string ResponseMessage { get; set; }

        [JsonProperty("Room")]
        public string Room { get; set; }

        [JsonProperty("SubCategory")]
        public string SubCategory { get; set; }
    }
}


