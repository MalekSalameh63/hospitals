﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class CheckPatientAuthentication : AppointmentRequestBase
    {
        [JsonProperty("PatientMobileNumber")]
        public long PatientMobileNumber { get; set; }

        [JsonProperty("ZipCode")]
        public long ZipCode { get; set; }

        [JsonProperty("isRegister")]
        public bool IsRegister { get; set; }

        [JsonProperty("PatientID")]
        public long PatientId { get; set; }

        [JsonProperty("PatientIdentificationID")]
        public string PatientIdentificationId { get; set; }

    }
}
