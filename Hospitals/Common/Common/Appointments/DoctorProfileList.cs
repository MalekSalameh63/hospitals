﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.Common.Appointments
{
    public class DoctorProfileList
    {
        [JsonProperty("DoctorID")]
        public long DoctorId { get; set; }

        [JsonProperty("DoctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("DoctorNameN")]
        public object DoctorNameN { get; set; }

        [JsonProperty("ClinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("ClinicDescription")]
        public string ClinicDescription { get; set; }

        [JsonProperty("ClinicDescriptionN")]
        public object ClinicDescriptionN { get; set; }

        [JsonProperty("LicenseExpiry")]
        public object LicenseExpiry { get; set; }

        [JsonProperty("EmploymentType")]
        public long EmploymentType { get; set; }

        [JsonProperty("SetupID")]
        public object SetupId { get; set; }

        [JsonProperty("ProjectID")]
        public long ProjectId { get; set; }

        [JsonProperty("ProjectName")]
        public string ProjectName { get; set; }

        [JsonProperty("NationalityID")]
        public string NationalityId { get; set; }

        [JsonProperty("NationalityName")]
        public string NationalityName { get; set; }

        [JsonProperty("NationalityNameN")]
        public object NationalityNameN { get; set; }

        [JsonProperty("Gender")]
        public long Gender { get; set; }

        [JsonProperty("Gender_Description")]
        public string GenderDescription { get; set; }

        [JsonProperty("Gender_DescriptionN")]
        public object GenderDescriptionN { get; set; }

        [JsonProperty("DoctorTitle")]
        public object DoctorTitle { get; set; }

        [JsonProperty("ProjectNameN")]
        public object ProjectNameN { get; set; }

        [JsonProperty("IsAllowWaitList")]
        public bool IsAllowWaitList { get; set; }

        [JsonProperty("Title_Description")]
        public string TitleDescription { get; set; }

        [JsonProperty("Title_DescriptionN")]
        public object TitleDescriptionN { get; set; }

        [JsonProperty("IsRegistered")]
        public object IsRegistered { get; set; }

        [JsonProperty("IsDoctorDummy")]
        public object IsDoctorDummy { get; set; }

        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }

        [JsonProperty("IsDoctorAppointmentDisplayed")]
        public object IsDoctorAppointmentDisplayed { get; set; }

        [JsonProperty("DoctorClinicActive")]
        public bool DoctorClinicActive { get; set; }

        [JsonProperty("IsbookingAllowed")]
        public object IsbookingAllowed { get; set; }

        [JsonProperty("DoctorCases")]
        public string DoctorCases { get; set; }

        [JsonProperty("DoctorPicture")]
        public object DoctorPicture { get; set; }

        [JsonProperty("DoctorProfileInfo")]
        public string DoctorProfileInfo { get; set; }

        [JsonProperty("Specialty")]
        public List<string> Specialty { get; set; }

        [JsonProperty("ActualDoctorRate")]
        public long ActualDoctorRate { get; set; }

        [JsonProperty("DoctorImageURL")]
        public Uri DoctorImageUrl { get; set; }

        [JsonProperty("DoctorRate")]
        public long DoctorRate { get; set; }

        [JsonProperty("DoctorTitleForProfile")]
        public string DoctorTitleForProfile { get; set; }

        [JsonProperty("IsAppointmentAllowed")]
        public bool IsAppointmentAllowed { get; set; }

        [JsonProperty("NationalityFlagURL")]
        public Uri NationalityFlagUrl { get; set; }

        [JsonProperty("NoOfPatientsRate")]
        public long NoOfPatientsRate { get; set; }

        [JsonProperty("QR")]
        public string Qr { get; set; }

        [JsonProperty("ServiceID")]
        public long ServiceId { get; set; }
    }
}
