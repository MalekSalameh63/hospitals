﻿namespace Common.Common.Appointments
{
    public class ValidateCheckInScreenAppointmentDetailsByAppointmentNo
    {
        public string VersionID { get; set; }

        public long Channel { get; set; }

        public long LanguageId { get; set; }

        public string IpAdress { get; set; }

        public string Generalid { get; set; }

        public long PatientOutSa { get; set; }

        public string SessionId { get; set; }

        public bool IsDentalAllowedBackend { get; set; }

        public long DeviceTypeId { get; set; }

        public long PatientId { get; set; }

        public string TokenId { get; set; }

        public long PatientTypeId { get; set; }

        public long PatientType { get; set; }

        public long ClinicId { get; set; }

        public long ProjectId { get; set; }

        public long AppointmentNo { get; set; }
    }
}
