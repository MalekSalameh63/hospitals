﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class AppointmentPrescriptionRequest : AppointmentRequestBase
    {
        [JsonProperty("PatientID")]
        public long PatientId { get; set; }

        [JsonProperty("AppointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("EpisodeID")]
        public long EpisodeId { get; set; }

        [JsonProperty("ClinicID")]
        public long ClinicId { get; set; }
    }
}
