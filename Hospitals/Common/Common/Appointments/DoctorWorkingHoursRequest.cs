﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class DoctorWorkingHoursRequest : AppointmentRequestBase
    {

        [JsonProperty("ClinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("DoctorID")]
        public long DoctorId { get; set; }

        [JsonProperty("DoctorWorkingHoursDays")]
        public int DoctorWorkingHoursDays { get; set; }
    }
}
