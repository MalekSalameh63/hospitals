﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.Common.Appointments
{
    public class AppointmentRadiologyResultResponse : AppointmentResponseBase
    {
        [JsonProperty("FinalRadiologyList")]
        public List<FinalRadiologyList> FinalRadiologyList { get; set; }
    }
    public class FinalRadiologyList
    {
        [JsonProperty("SetupID")]
        public string SetupId { get; set; }

        [JsonProperty("ProjectID")]
        public long ProjectId { get; set; }

        [JsonProperty("PatientID")]
        public string PatientId { get; set; }

        [JsonProperty("InvoiceLineItemNo")]
        public long InvoiceLineItemNo { get; set; }

        [JsonProperty("InvoiceNo")]
        public long InvoiceNo { get; set; }

        [JsonProperty("DoctorID")]
        public long DoctorId { get; set; }

        [JsonProperty("ClinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("OrderDate")]
        public string OrderDate { get; set; }

        [JsonProperty("ReportData")]
        public string ReportData { get; set; }

        [JsonProperty("ImageURL")]
        public string ImageUrl { get; set; }

        [JsonProperty("ProcedureID")]
        public string ProcedureId { get; set; }

        [JsonProperty("AppointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("DIAPacsURL")]
        public object DiaPacsUrl { get; set; }

        [JsonProperty("IsRead")]
        public bool IsRead { get; set; }

        [JsonProperty("ReadOn")]
        public string ReadOn { get; set; }

        [JsonProperty("AdmissionNo")]
        public object AdmissionNo { get; set; }

        [JsonProperty("IsInOutPatient")]
        public bool IsInOutPatient { get; set; }

        [JsonProperty("ActualDoctorRate")]
        public long ActualDoctorRate { get; set; }

        [JsonProperty("ClinicDescription")]
        public string ClinicDescription { get; set; }

        [JsonProperty("DIA_PACS_URL")]
        public string FinalRadiologyListDiaPacsUrl { get; set; }

        [JsonProperty("DoctorImageURL")]
        public string DoctorImageUrl { get; set; }

        [JsonProperty("DoctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("DoctorRate")]
        public long DoctorRate { get; set; }

        [JsonProperty("DoctorTitle")]
        public string DoctorTitle { get; set; }

        [JsonProperty("Gender")]
        public long Gender { get; set; }

        [JsonProperty("GenderDescription")]
        public string GenderDescription { get; set; }

        [JsonProperty("IsActiveDoctorProfile")]
        public bool IsActiveDoctorProfile { get; set; }

        [JsonProperty("IsExecludeDoctor")]
        public bool IsExecludeDoctor { get; set; }

        [JsonProperty("IsInOutPatientDescription")]
        public string IsInOutPatientDescription { get; set; }

        [JsonProperty("IsInOutPatientDescriptionN")]
        public string IsInOutPatientDescriptionN { get; set; }

        [JsonProperty("NationalityFlagURL")]
        public string NationalityFlagUrl { get; set; }

        [JsonProperty("NoOfPatientsRate")]
        public long NoOfPatientsRate { get; set; }

        [JsonProperty("OrderNo")]
        public long OrderNo { get; set; }

        [JsonProperty("ProjectName")]
        public string ProjectName { get; set; }

        [JsonProperty("QR")]
        public string Qr { get; set; }

        [JsonProperty("ReportDataHTML")]
        public string ReportDataHtml { get; set; }

        [JsonProperty("ReportDataTextString")]
        public string ReportDataTextString { get; set; }

        [JsonProperty("Speciality")]
        public List<string> Speciality { get; set; }

        [JsonProperty("isCVI")]
        public bool IsCvi { get; set; }

        [JsonProperty("isRadMedicalReport")]
        public bool IsRadMedicalReport { get; set; }
    }
}
