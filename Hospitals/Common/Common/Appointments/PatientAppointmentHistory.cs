﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class PatientAppointmentHistory : AppointmentRequestBase
    {

        [JsonProperty("PatientID")]
        public long PatientId { get; set; }
        [JsonProperty("IsActiveAppointment")]
        public bool IsActiveAppointment { get; set; }
    }
}
