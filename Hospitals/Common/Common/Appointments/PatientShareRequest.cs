﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class PatientShareRequest : AppointmentRequestBase
    {
        [JsonProperty("CompanyID")]
        public long CompanyId { get; set; }

        [JsonProperty("SubCategoryID")]
        public long SubCategoryId { get; set; }

        [JsonProperty("PatientID")]
        public long PatientId { get; set; }


    }
}
