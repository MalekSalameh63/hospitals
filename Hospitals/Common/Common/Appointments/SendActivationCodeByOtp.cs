﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class SendActivationCodeByOtp
    {
        [JsonProperty("PatientMobileNumber")]
        public long PatientMobileNumber { get; set; }

        [JsonProperty("MobileNo")]
        public string MobileNo { get; set; }

        [JsonProperty("ProjectOutSA")]
        public long ProjectOutSa { get; set; }

        [JsonProperty("LoginType")]
        public long LoginType { get; set; }

        [JsonProperty("ZipCode")]
        public long ZipCode { get; set; }

        [JsonProperty("isRegister")]
        public bool IsRegister { get; set; }

        [JsonProperty("LogInTokenID")]
        public string LogInTokenId { get; set; }

        [JsonProperty("SearchType")]
        public long SearchType { get; set; }

        [JsonProperty("PatientID")]
        public long PatientId { get; set; }

        [JsonProperty("NationalID")]
        public string NationalId { get; set; }

        [JsonProperty("PatientIdentificationID")]
        public string PatientIdentificationId { get; set; }

        [JsonProperty("OTP_SendType")]
        public long OtpSendType { get; set; }

        [JsonProperty("LanguageID")]
        public long LanguageId { get; set; }

        [JsonProperty("VersionID")]
        public double VersionId { get; set; }

        [JsonProperty("Channel")]
        public long Channel { get; set; }

        [JsonProperty("IPAdress")]
        public string IpAdress { get; set; }

        [JsonProperty("generalid")]
        public string Generalid { get; set; }

        [JsonProperty("PatientOutSA")]
        public long PatientOutSa { get; set; }

        [JsonProperty("SessionID")]
        public object SessionId { get; set; }

        [JsonProperty("isDentalAllowedBackend")]
        public bool IsDentalAllowedBackend { get; set; }

        [JsonProperty("DeviceTypeID")]
        public long DeviceTypeId { get; set; }
    }
}
