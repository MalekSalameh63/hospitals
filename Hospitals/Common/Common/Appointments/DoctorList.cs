﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.Common.Appointments
{
    public class DoctorList
    {
        [JsonProperty("ClinicID")]
        public int ClinicId { get; set; }

        [JsonProperty("ClinicName")]
        public string ClinicName { get; set; }

        [JsonProperty("DoctorTitle")]
        public string DoctorTitle { get; set; }

        [JsonProperty("ID")]
        public long Id { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("ProjectID")]
        public int ProjectId { get; set; }

        [JsonProperty("ProjectName")]
        public string ProjectName { get; set; }

        [JsonProperty("ActualDoctorRate")]
        public long ActualDoctorRate { get; set; }

        [JsonProperty("ClinicRoomNo")]
        public long ClinicRoomNo { get; set; }

        [JsonProperty("Date")]
        public object Date { get; set; }

        [JsonProperty("DayName")]
        public object DayName { get; set; }

        [JsonProperty("DoctorID")]
        public long DoctorId { get; set; }

        [JsonProperty("DoctorImageURL")]
        public string DoctorImageUrl { get; set; }

        [JsonProperty("DoctorProfile")]
        public object DoctorProfile { get; set; }

        [JsonProperty("DoctorProfileInfo")]
        public object DoctorProfileInfo { get; set; }

        [JsonProperty("DoctorRate")]
        public long DoctorRate { get; set; }

        [JsonProperty("Gender")]
        public long Gender { get; set; }

        [JsonProperty("GenderDescription")]
        public string GenderDescription { get; set; }

        [JsonProperty("IsAppointmentAllowed")]
        public bool IsAppointmentAllowed { get; set; }

        [JsonProperty("IsDoctorAllowVedioCall")]
        public bool IsDoctorAllowVedioCall { get; set; }

        [JsonProperty("IsDoctorDummy")]
        public bool IsDoctorDummy { get; set; }

        [JsonProperty("IsLiveCare")]
        public bool IsLiveCare { get; set; }

        //[JsonProperty("Latitude")]
        //public string Latitude { get; set; }

        //[JsonProperty("Longitude")]
        //public string Longitude { get; set; }

        [JsonProperty("NationalityFlagURL")]
        public Uri NationalityFlagUrl { get; set; }

        [JsonProperty("NationalityID")]
        public string NationalityId { get; set; }

        [JsonProperty("NationalityName")]
        public string NationalityName { get; set; }

        [JsonProperty("NearestFreeSlot")]
        public object NearestFreeSlot { get; set; }

        [JsonProperty("NoOfPatientsRate")]
        public long NoOfPatientsRate { get; set; }

        [JsonProperty("OriginalClinicID")]
        public object OriginalClinicId { get; set; }

        [JsonProperty("PersonRate")]
        public long PersonRate { get; set; }

        [JsonProperty("ProjectDistanceInKiloMeters")]
        public long ProjectDistanceInKiloMeters { get; set; }

        //[JsonProperty("QR")]
        //public string Qr { get; set; }

        //[JsonProperty("QRString")]
        //public object QrString { get; set; }

        [JsonProperty("RateNumber")]
        public long RateNumber { get; set; }

        [JsonProperty("ServiceID")]
        public object ServiceId { get; set; }

        [JsonProperty("SetupID")]
        public string SetupId { get; set; }

        [JsonProperty("Speciality")]
        public List<string> Speciality { get; set; }

        [JsonProperty("WorkingHours")]
        public object WorkingHours { get; set; }
    }
}
