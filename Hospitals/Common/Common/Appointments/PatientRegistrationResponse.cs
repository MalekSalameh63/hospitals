﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.Common.Appointments
{
    public class PatientRegistrationResponse : AppointmentResponseBase
    {
        [JsonProperty("Date")]
        public object Date { get; set; }

        [JsonProperty("LanguageID")]
        public long LanguageId { get; set; }

        [JsonProperty("ServiceName")]
        public long ServiceName { get; set; }

        [JsonProperty("Time")]
        public object Time { get; set; }

        [JsonProperty("AndroidLink")]
        public object AndroidLink { get; set; }

        [JsonProperty("AuthenticationTokenID")]
        public string AuthenticationTokenId { get; set; }

        [JsonProperty("DietType")]
        public long DietType { get; set; }

        [JsonProperty("FoodCategory")]
        public long FoodCategory { get; set; }

        [JsonProperty("IOSLink")]
        public object IosLink { get; set; }

        [JsonProperty("IsAuthenticated")]
        public bool IsAuthenticated { get; set; }

        [JsonProperty("MealOrderStatus")]
        public long MealOrderStatus { get; set; }

        [JsonProperty("MealType")]
        public long MealType { get; set; }

        [JsonProperty("PatientBlodType")]
        public object PatientBlodType { get; set; }

        [JsonProperty("DoctorInformation_List")]
        public object DoctorInformationList { get; set; }

        [JsonProperty("GetAllPendingRecordsList")]
        public object GetAllPendingRecordsList { get; set; }

        [JsonProperty("GetAllSharedRecordsByStatusList")]
        public object GetAllSharedRecordsByStatusList { get; set; }

        [JsonProperty("GetResponseFileList")]
        public object GetResponseFileList { get; set; }

        [JsonProperty("IsHMGPatient")]
        public bool IsHmgPatient { get; set; }

        [JsonProperty("IsLoginSuccessfully")]
        public bool IsLoginSuccessfully { get; set; }

        [JsonProperty("IsNeedUpdateIdintificationNo")]
        public bool IsNeedUpdateIdintificationNo { get; set; }

        [JsonProperty("KioskSendSMS")]
        public bool KioskSendSms { get; set; }

        [JsonProperty("List")]
        public List<object> List { get; set; }

        [JsonProperty("List_AskHabibMobileLoginInfo")]
        public object ListAskHabibMobileLoginInfo { get; set; }

        [JsonProperty("List_AskHabibPatientFile")]
        public object ListAskHabibPatientFile { get; set; }

        [JsonProperty("List_MergeFiles")]
        public object ListMergeFiles { get; set; }

        [JsonProperty("List_MobileLoginInfo")]
        public object ListMobileLoginInfo { get; set; }

        [JsonProperty("List_PatientCount")]
        public object ListPatientCount { get; set; }

        [JsonProperty("LogInTokenID")]
        public object LogInTokenId { get; set; }

        [JsonProperty("MohemmPrivilege_List")]
        public object MohemmPrivilegeList { get; set; }

        [JsonProperty("PateintID")]
        public long PateintId { get; set; }

        [JsonProperty("PatientBloodType")]
        public object PatientBloodType { get; set; }

        [JsonProperty("PatientER_AdminDriverFileList")]
        public object PatientErAdminDriverFileList { get; set; }

        [JsonProperty("PatientER_AdminFile")]
        public object PatientErAdminFile { get; set; }

        [JsonProperty("PatientER_DriverFile")]
        public object PatientErDriverFile { get; set; }

        [JsonProperty("PatientER_DriverFileList")]
        public object PatientErDriverFileList { get; set; }

        [JsonProperty("PatientHasFile")]
        public bool PatientHasFile { get; set; }

        [JsonProperty("PatientMergedIDs")]
        public object PatientMergedIDs { get; set; }

        [JsonProperty("PatientOutSA")]
        public bool PatientOutSa { get; set; }

        [JsonProperty("PatientShareRequestID")]
        public long PatientShareRequestId { get; set; }

        [JsonProperty("PatientType")]
        public long PatientType { get; set; }

        [JsonProperty("ProjectIDOut")]
        public long ProjectIdOut { get; set; }

        [JsonProperty("ReturnMessage")]
        public object ReturnMessage { get; set; }

        [JsonProperty("SMSLoginRequired")]
        public bool SmsLoginRequired { get; set; }

        [JsonProperty("ServicePrivilege_List")]
        public List<object> ServicePrivilegeList { get; set; }

        [JsonProperty("SharePatientName")]
        public object SharePatientName { get; set; }

        [JsonProperty("VerificationCode")]
        public object VerificationCode { get; set; }

        [JsonProperty("email")]
        public object Email { get; set; }

        [JsonProperty("errorList")]
        public List<ErrorList> ErrorList { get; set; }

        [JsonProperty("hasFile")]
        public bool HasFile { get; set; }

        [JsonProperty("isActiveCode")]
        public bool IsActiveCode { get; set; }

        [JsonProperty("isMerged")]
        public bool IsMerged { get; set; }

        [JsonProperty("isNeedUserAgreement")]
        public bool IsNeedUserAgreement { get; set; }

        [JsonProperty("isSMSSent")]
        public bool IsSmsSent { get; set; }

        [JsonProperty("memberList")]
        public object MemberList { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("statusCode")]
        public long StatusCode { get; set; }
    }
    public class ErrorList
    {
        [JsonProperty("VidaErrorCode")]
        public string VidaErrorCode { get; set; }

        [JsonProperty("VidaErrorMessage")]
        public string VidaErrorMessage { get; set; }
    }
}
