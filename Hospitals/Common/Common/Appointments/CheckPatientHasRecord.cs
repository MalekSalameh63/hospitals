﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class CheckPatientHasRecord : AppointmentRequestBase
    {
        [JsonProperty("PatientIdentificationID")]
        public string PatientIdentificationId { get; set; }

        [JsonProperty("PatientMobileNumber")]
        public long PatientMobileNumber { get; set; }

        [JsonProperty("PatientID")]
        public long PatientId { get; set; }

    }
}
