﻿using Common.Common.Appointments;
using Newtonsoft.Json;

namespace Common.Common
{
    public class InsertAppointmentRequest : AppointmentRequestBase
    {
        [JsonProperty("PatientID")]
        public long PatientID { get; set; }

        [JsonProperty("ClinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("DoctorID")]
        public long DoctorId { get; set; }

        [JsonProperty("StartTime")]
        public string StartTime { get; set; }

        [JsonProperty("SelectedTime")]
        public string SelectedTime { get; set; }

        [JsonProperty("EndTime")]
        public string EndTime { get; set; }

        [JsonProperty("InitialSlotDuration")]
        public long InitialSlotDuration { get; set; }

        [JsonProperty("StrAppointmentDate")]
        public string StrAppointmentDate { get; set; }


    }
}
