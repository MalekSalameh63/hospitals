﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class AppointmentRadiologyResultRequest : AppointmentRequestBase
    {
        [JsonProperty("PatientID")]
        public long PatientId { get; set; }

        [JsonProperty("AppointmentNo")]
        public long AppointmentNo { get; set; }
    }
}
