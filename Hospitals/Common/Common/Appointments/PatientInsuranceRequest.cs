﻿using Newtonsoft.Json;

namespace Common.Common.Appointments
{
    public class PatientInsuranceRequest : AppointmentRequestBase
    {
        [JsonProperty("PatientIdentificationID")]
        public string PatientIdentificationID { get; set; }

        [JsonProperty("PatientID")]
        public long PatientId { get; set; }
    }
}
