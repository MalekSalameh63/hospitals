﻿using Common.DTOs.EMR.MedicalRecord.Procedure;
using System.Collections.Generic;

namespace Common.Common.Appointments
{
    public class LabOrderWithResults
    {
        public OrderedProceduresResponse Order { get; set; }
        public List<PatientLabResultList> Results { get; set; }
    }
}
