﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Common.VOs
{
    public class TextVO
    {
        public string TextValue { get; protected set; }
        public int TextMinLength { get; protected set; }
        public int TextMaxLength { get; protected set; }
        public Regex TextRegex { get; protected set; }
        protected TextVO()
        {

        }
        public TextVO(string inputText, int minLength = 3, int maxLength = 256, Regex regex = null)
        {
            if (minLength > 0 && minLength <= maxLength && maxLength <= Math.Pow(2, 8))
            {
                TextValue = inputText != null ?  inputText.Trim(): inputText;
                TextMinLength = minLength;
                TextMaxLength = maxLength;
                CheckTextValueLength();
            }
            else
            {
                throw new Exception("Passed parameters are invalid");
            }
            if (regex != null)
            {
                TextRegex = regex;
                ValidateTextValueRegex(regex);
            }
        }
        protected void CheckTextValueLength()
        {
            if (this.TextValue == null || this.TextValue == "" || this.TextValue.Length == 0 || this.TextValue.Length < this.TextMinLength || this.TextValue.Length > this.TextMaxLength)
            {
                throw new Exception("Input text is either empty or exceeds text length boundaries. Input text: " + this.TextValue);
            }
        }
        protected void ValidateTextValueRegex(Regex regex)
        {
            if (!regex.Match(this.TextValue).Success)
            {
                throw new Exception("Input text does not match specified regular expression. Input text: " + this.TextValue);
            }
        }
        public override string ToString()
        {
            return TextValue;
        }
        public static bool operator !=(TextVO firstTextVO, TextVO secondTextVO)
        {
            return firstTextVO.TextValue != secondTextVO.TextValue;
        }

        public static bool operator ==(TextVO firstTextVO, TextVO secondTextVO)
        {
            return firstTextVO.TextValue == secondTextVO.TextValue;
        }

        public override int GetHashCode()
        {
            return TextValue.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj as TextVO);
        }
    }

}
