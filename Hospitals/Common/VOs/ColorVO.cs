﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Common.VOs
{
    public class ColorVO
    {
        public string ColorValue { get; private set; }
        public Regex ColorRegex { get; private set; }

        protected ColorVO()
        {

        }
        public ColorVO(string inputColor, Regex regex = null)
        {
            ColorValue = inputColor;
            if (regex == null)
            {
                ColorRegex = new Regex("^#(0x|0X)?[a-fA-F0-9]+$");
            }
            else
            {
                ColorRegex = regex;
            }
            ValidateColorRegex();
        }

        protected void ValidateColorRegex()
        {
            if (!ColorRegex.Match(this.ColorValue).Success)
            {
                throw new Exception("Input value does not match color regex");
            }
        }

        public static bool operator !=(ColorVO firstColorVO, ColorVO secondColorVO)
        {
            return firstColorVO.ColorValue != secondColorVO.ColorValue;
        }

        public static bool operator ==(ColorVO firstColorVO, ColorVO secondColorVO)
        {
            return firstColorVO.ColorValue == secondColorVO.ColorValue;
        }

        public override int GetHashCode()
        {
            return ColorValue.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj as ColorVO);
        }
    }
}
