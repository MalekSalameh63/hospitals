﻿using System;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using static System.Decimal;

namespace Common.VOs
{
    public class AttachmentVo
    {
        public Attachment Attachment { get; }
        public string Content { get; }
        public string Name { get; }
        public string MimeType { get; }
        public float AttachmentSize { get; }

        private AttachmentVo()
        {
        }

        public AttachmentVo(string content, TextVO name, MimeTypeVo mimeType, int maxSize = 200)
        {
            if (string.IsNullOrWhiteSpace(content))
                throw new Exception("Attachment is empty.");

            //Calculate attachment size
            AttachmentSize = GetSize(content);

            //Check if attachment size exceeds allowable limit
            if (AttachmentSize > maxSize)
                throw new Exception("Attachment size exceed allowable limit.");

            var bytes = Convert.FromBase64String(content);
            Stream stream = new MemoryStream(bytes);

            //Create email attachment
            var attachment = new Attachment(stream, name.TextValue, mimeType.ToString());

            ContentDisposition disposition = attachment.ContentDisposition;
            attachment.TransferEncoding = TransferEncoding.Base64;
            attachment.ContentId = name.ToString();
            attachment.ContentDisposition.Inline = true;

            Attachment = attachment;
            Content = content;
            Name = name.ToString();
            MimeType = mimeType.ToString();
        }
        protected static float GetSize(string content)
        {
            var totalBytes = content.Length;
            var fileSize = Divide(totalBytes, 1048576);
            return (float)(fileSize / 4 * 3);
        }
    }
}
