﻿using Common.Enums;
using Common.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.VOs
{
    public class NEpochDateOfBirthVO
    {
        public long? DateOfBirth { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public bool HasValue { get; }

        private NEpochDateOfBirthVO()
        {
        }

        public NEpochDateOfBirthVO(long? dateOfBirth, int minAge = 0, int maxAge = 100)
        {
            if (dateOfBirth != null)
            {
                HasValue = true;
                this.DateOfBirth = dateOfBirth;

                DateTime DateOfBirth = DateHelper.GetDate(dateOfBirth.ToString());
                if (DateOfBirth.AddYears(minAge) > DateTime.Now)
                    throw new BusinessException(FriendlyStatus.AgeCouldNotBeLessThanMinAge);

                if (!(DateOfBirth.AddYears(maxAge) > DateTime.Now))
                    throw new BusinessException(FriendlyStatus.AgeCouldNotBeMoreThanMaxAge);
            }
            else
            {
                HasValue = false;
            }
        }

        public static bool operator ==(NEpochDateOfBirthVO firstDate, NEpochDateOfBirthVO secondDate)
        {
            return firstDate.DateOfBirth == secondDate.DateOfBirth;
        }

        public static bool operator !=(NEpochDateOfBirthVO firstDate, NEpochDateOfBirthVO secondDate)
        {
            return firstDate.DateOfBirth != secondDate.DateOfBirth;
        }

        public override int GetHashCode()
        {
            return DateOfBirth.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj as NEpochDateOfBirthVO);
        }
    }
}
