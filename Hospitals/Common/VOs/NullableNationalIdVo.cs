﻿using Common.Enums;
using Common.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Common.VOs
{
    public class NullableNationalIdVo
    {
        public string NationalId { get; set; }
        public bool HasValue { get; }
        private NullableNationalIdVo()
        {
        }

        public NullableNationalIdVo(string nationalId)
        {
            if (!string.IsNullOrEmpty(nationalId))
            {
                HasValue = true;

                nationalId = nationalId.Trim();

                if (string.IsNullOrWhiteSpace(nationalId))
                    throw new BusinessException(FriendlyStatus.NationalIdCanNotBeEmpty);

                if (!Regex.IsMatch(nationalId, @"[0-9]+"))
                    throw new BusinessException(FriendlyStatus.NationalIdShouldBeOnlyNumbers);

                if (nationalId.Length != 10)
                    throw new BusinessException(FriendlyStatus.NationalIdLengthShouldBe10Numbers);

                NationalId = nationalId;
            }
            else
            {
                HasValue = false;
                NationalId = null;
            }
        }

        public static bool operator ==(NullableNationalIdVo firstNationalIdVo, NullableNationalIdVo secondNationalIdVo)
        {
            return firstNationalIdVo.NationalId == secondNationalIdVo.NationalId;
        }

        public static bool operator !=(NullableNationalIdVo firstNationalIdVo, NullableNationalIdVo secondNationalIdVo)
        {
            return firstNationalIdVo.NationalId != secondNationalIdVo.NationalId;
        }

        public override int GetHashCode()
        {
            return NationalId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj as NullableNationalIdVo);
        }
    }
}
