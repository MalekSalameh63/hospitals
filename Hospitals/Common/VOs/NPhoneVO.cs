﻿using Common.Enums;
using Common.Helper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace Common.VOs
{
    public class NPhoneVO
    {
        public string PhoneValue { get; private set; }
        public Regex PhoneRegex { get; private set; }
        public bool HasValue { get; }
        protected NPhoneVO()
        {

        }
        public NPhoneVO(string phoneValue, Regex regex = null)
        {
            if (phoneValue != null && phoneValue != "")
            {
                HasValue = true;
                PhoneValue = phoneValue;

                if (regex == null)
                {
                    PhoneRegex = new Regex("^\\+?(\\d[\\d-. ]+)?(\\([\\d-. ]+\\))?[\\d-. ]+\\d$");
                }

                else
                {
                    PhoneRegex = regex;
                }

                ValidateTextValueRegex();
            }
            else
            {
                HasValue = false;
                PhoneValue = null;
            }        
        }
        protected void ValidateTextValueRegex()
        {
            if (!PhoneRegex.Match(this.PhoneValue).Success)
            {
                throw new BusinessException(FriendlyStatus.PhoneNumberDoesNotMatchRegularExpression);
            }
        }

        public static bool operator !=(NPhoneVO firstPhoneVO, NPhoneVO secondPhoneVO)
        {

            return firstPhoneVO.PhoneValue != secondPhoneVO.PhoneValue;
        }
        public static bool operator ==(NPhoneVO firstPhoneVO, NPhoneVO secondPhoneVO)
        {
            return firstPhoneVO.PhoneValue == secondPhoneVO.PhoneValue;
        }
        public override int GetHashCode()
        {
            return PhoneValue.GetHashCode();

        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj as NPhoneVO);
        }
    }
}
