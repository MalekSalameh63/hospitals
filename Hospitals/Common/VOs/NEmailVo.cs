﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Common.VOs
{
    public class NEmailVo
    {
        public string EmailAddress { get; }
        public bool HasValue { get; }
        private NEmailVo()
        {
        }

        public NEmailVo(string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                HasValue = false;
            }
            else
            {
                var emailRegex = new Regex(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
                if (!emailRegex.Match(emailAddress).Success)
                    throw new Exception("The specified string is not in the form required for an e-mail address.");

                EmailAddress = emailAddress;
                HasValue = true;
            }
        }
        public override string ToString()
        {
            return EmailAddress;
        }
        public static bool operator !=(NEmailVo firstEmailVo, NEmailVo secondEmailVo)
        {
            return firstEmailVo.EmailAddress != secondEmailVo.EmailAddress;
        }
        public static bool operator ==(NEmailVo firstEmailVo, NEmailVo secondEmailVo)
        {
            return firstEmailVo.EmailAddress == secondEmailVo.EmailAddress;
        }
        public override int GetHashCode()
        {
            return EmailAddress.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj as NEmailVo);
        }
    }
}
