﻿using System;

namespace Common.VOs
{
    public class PortVo
    {
        public override bool Equals(object obj)
        {
            return Equals((PortVo)obj);
        }

        protected bool Equals(PortVo other)
        {
            return _port == other._port;
        }
        private readonly int _port;

        private PortVo()
        {
        }

        public PortVo(int port)
        {
            if (port < 0 || port > 65535)
                throw new Exception("Invalid port number.");

            _port = port;
        }

        public override string ToString()
        {
            return _port.ToString();
        }

        public int ToInt()
        {
            return _port;
        }
        public static bool operator !=(PortVo firstPortVo, PortVo secondPortVo)
        {
            return firstPortVo._port != secondPortVo._port;
        }
        public static bool operator ==(PortVo firstPortVo, PortVo secondPortVo)
        {
            return firstPortVo._port == secondPortVo._port;
        }
        public override int GetHashCode() => _port.GetHashCode();
    }
}
