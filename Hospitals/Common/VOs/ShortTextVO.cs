﻿using System;
using System.Text.RegularExpressions;

namespace Common.VOs
{
    public class ShortTextVO : TextVO
    {
        protected ShortTextVO()
        {

        }
        public ShortTextVO(string inputText, int minLength = 3, int maxLength = 256, Regex regex = null)
        {
            if (minLength > 0 && minLength <= maxLength && maxLength <= Math.Pow(2, 8))
            {
                TextValue = inputText.Trim();
                TextMinLength = minLength;
                TextMaxLength = maxLength;
                CheckTextValueLength();
            }
            else
            {
                throw new Exception("Passed parameters are invalid");
            }

            if (regex != null)
            {
                TextRegex = regex;
                ValidateTextValueRegex(regex);
            }
        }

        protected new void CheckTextValueLength()
        {
            if (string.IsNullOrEmpty(this.TextValue) || this.TextValue.Length == 0 || this.TextValue.Length < this.TextMinLength)
            {
                throw new Exception("Input text is either empty or the input text is less than minimum length. Input text: " + this.TextValue);
            }
            else if (this.TextValue.Length > this.TextMaxLength)
            {
                this.TextValue = this.TextValue.Substring(0, this.TextMaxLength);
            }
        }
    }
}
