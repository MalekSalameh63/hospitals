﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Common.VOs
{
    public class OrderVO
    {
        public int Value { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public OrderVO(int value, int minValue = 0, int maxValue = 100)
        {
            if (minValue >= 0 && maxValue <= int.MaxValue && minValue <= maxValue)
            {
                Value = value;
                MinValue = minValue;
                MaxValue = maxValue;
                ValidateValue();
            }
            else
            {
                throw new Exception("Passed parameters are invalid");
            }
        }
        protected void ValidateValue()
        {
            if (Value > MaxValue || Value < MinValue)
            {
                throw new Exception("Value is outside the range of acceptable values");
            }
        }
    }
}
