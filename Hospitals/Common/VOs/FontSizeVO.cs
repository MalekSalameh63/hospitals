﻿using System;

namespace Common.VOs
{
    public class FontSizeVO
    {
        public double Value { get; set; }
        public double MinValue { get; set; }
        public double MaxValue { get; set; }
        public FontSizeVO(double value, double minValue = 0.00, double maxValue = 5000.00)
        {
            if (minValue >= 0.00 && maxValue <= double.MaxValue && minValue <= maxValue)
            {
                Value = value;
                MinValue = minValue;
                MaxValue = maxValue;
                ValidateValue();
            }
            else
            {
                throw new Exception("Passed parameters are invalid");
            }
        }
        protected void ValidateValue()
        {
            if (Value > MaxValue || Value < MinValue)
            {
                throw new Exception("Value is outside the range of acceptable values");
            }
        }
    }
}
