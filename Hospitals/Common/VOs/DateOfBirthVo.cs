﻿using System;

namespace Common.VOs
{
    public class DateOfBirthVo
    {
        public override bool Equals(object obj)
        {
            return Equals((DateOfBirthVo)obj);
        }

        protected bool Equals(DateOfBirthVo other)
        {
            return DateOfBirth.Equals(other.DateOfBirth) && MinAge == other.MinAge && MaxAge == other.MaxAge;
        }


        public DateTime DateOfBirth { get; }
        public int MinAge { get; }
        public int MaxAge { get; }

        private DateOfBirthVo()
        {
        }

        public DateOfBirthVo(DateTime dateOfBirth, int minAge = 0, int maxAge = 150)
        {
            if (dateOfBirth == null)
                throw new Exception("Date of birth can not be null");

            if (dateOfBirth.AddYears(minAge) > DateTime.Now)
                throw new Exception("Age could not be less than " + minAge + " Years");

            if (!(dateOfBirth.AddYears(maxAge) > DateTime.Now))
                throw new Exception("Age could not be more than " + maxAge + " Years");

            DateOfBirth = dateOfBirth;

        }

        public static bool operator ==(DateOfBirthVo firstDate, DateOfBirthVo secondDate)
        {
            return firstDate.DateOfBirth.Date == secondDate.DateOfBirth.Date;
        }

        public static bool operator !=(DateOfBirthVo firstDate, DateOfBirthVo secondDate)
        {
            return firstDate.DateOfBirth.Date != secondDate.DateOfBirth.Date;
        }

        public override int GetHashCode() => DateOfBirth.GetHashCode();
    }
}
