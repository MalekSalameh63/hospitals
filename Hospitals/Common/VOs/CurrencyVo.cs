﻿using System;

namespace Common.VOs
{
    public class CurrencyVo
    {
        public override bool Equals(object obj)
        {
            return Equals((CurrencyVo)obj);
        }

        protected bool Equals(CurrencyVo other)
        {
            return Currency == other.Currency;
        }

        public string Currency { get; }

        private CurrencyVo()
        {
        }

        public CurrencyVo(string currency)
        {
            if (string.IsNullOrEmpty(currency))
                throw new Exception("The specified string is not in the form required for a currency.");
            if (currency.Length > 3)
                throw new Exception("The specified string is not in the form required for a currency.");
            Currency = currency;
        }
        public override string ToString()
        {
            return Currency;
        }
        public static bool operator !=(CurrencyVo firstCurrencyVo, CurrencyVo secondCurrencyVoVo)
        {
            return firstCurrencyVo.Currency != secondCurrencyVoVo.Currency;
        }
        public static bool operator ==(CurrencyVo firstCurrencyVo, CurrencyVo secondCurrencyVoVo)
        {
            return firstCurrencyVo.Currency == secondCurrencyVoVo.Currency;
        }
        public override int GetHashCode() => Currency.GetHashCode();
    }
}
