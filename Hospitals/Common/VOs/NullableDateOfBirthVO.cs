﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.VOs
{
    public class NullableDateOfBirthVO
    {
        public DateTime DateOfBirth { get; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public bool HasValue { get; }

        private NullableDateOfBirthVO()
        {
        }

        public NullableDateOfBirthVO(DateTime? dateOfBirth, int minAge = 0, int maxAge = 150)
        {
            if (dateOfBirth != null)
            {
                DateOfBirth = (DateTime)dateOfBirth;
                HasValue = true;

                if (DateOfBirth.AddYears(minAge) > DateTime.Now)
                    throw new Exception("Age could not be less than " + minAge + " Years");

                if (!(DateOfBirth.AddYears(maxAge) > DateTime.Now))
                    throw new Exception("Age could not be more than " + maxAge + " Years");
            }
            else
            {
                HasValue = false;
            }
        }

        public static bool operator ==(NullableDateOfBirthVO firstDate, NullableDateOfBirthVO secondDate)
        {
            return firstDate.DateOfBirth.Date == secondDate.DateOfBirth.Date;
        }

        public static bool operator !=(NullableDateOfBirthVO firstDate, NullableDateOfBirthVO secondDate)
        {
            return firstDate.DateOfBirth.Date != secondDate.DateOfBirth.Date;
        }

        public override int GetHashCode()
        {
            return DateOfBirth.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj as NullableDateOfBirthVO);
        }
    }
}
