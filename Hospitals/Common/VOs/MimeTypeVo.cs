﻿using System;
using System.Collections.Generic;

namespace Common.VOs
{
    public class MimeTypeVo
    {
        public override bool Equals(object obj)
        {
            return Equals((MimeTypeVo)obj);
        }

        protected bool Equals(MimeTypeVo other)
        {
            return _mimeType == other._mimeType;
        }

        private readonly string _mimeType;

        private MimeTypeVo()
        {
        }

        public MimeTypeVo(string mimeType)
        {
            if (string.IsNullOrWhiteSpace(mimeType))
                throw new Exception(mimeType + " Mime type is not supported.");

            var supportedMimeTypes = new List<string>()
            {
                "application/msword",
                "image/jpeg",
                "application/pdf",
                "image/png"
            };

            if (!supportedMimeTypes.Contains(mimeType))
                throw new Exception(mimeType + " Mime type is not supported.");

            _mimeType = mimeType;
        }

        public override string ToString()
        {
            return _mimeType;
        }
        public static bool operator !=(MimeTypeVo firstMimeTypeVo, MimeTypeVo secondMimeTypeVo)
        {
            return firstMimeTypeVo._mimeType != secondMimeTypeVo._mimeType;
        }

        public static bool operator ==(MimeTypeVo firstMimeTypeVo, MimeTypeVo secondMimeTypeVo)
        {
            return firstMimeTypeVo._mimeType == secondMimeTypeVo._mimeType;
        }

        public override int GetHashCode() => _mimeType.GetHashCode();
    }
}