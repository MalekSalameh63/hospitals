﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Common.VOs
{
    public class PhoneVO
    {
        public string PhoneValue { get; private set; }
        public Regex PhoneRegex { get; private set; }
        protected PhoneVO()
        {

        }
        public PhoneVO(string phoneValue, Regex regex = null)
        {
            if (phoneValue != null && phoneValue != "")
            {
                PhoneValue = phoneValue; 

                if (regex == null)
                {
                    PhoneRegex = new Regex("^\\+?(\\d[\\d-. ]+)?(\\([\\d-. ]+\\))?[\\d-. ]+\\d$");
                }

                else
                {
                    PhoneRegex = regex;
                }

                ValidateTextValueRegex();
            }
            else

            {
                throw new Exception("Phone number is required");
            }
        }
        protected void ValidateTextValueRegex()
        {
            if (!PhoneRegex.Match(this.PhoneValue).Success)
            {
                throw new Exception("Phone number does not match regular expression");
            }
        }

        public static bool operator !=(PhoneVO firstPhoneVO, PhoneVO secondPhoneVO)
        {

            return firstPhoneVO.PhoneValue != secondPhoneVO.PhoneValue;
        }
        public static bool operator ==(PhoneVO firstPhoneVO, PhoneVO secondPhoneVO)
        {
            return firstPhoneVO.PhoneValue == secondPhoneVO.PhoneValue;
        }
        public override int GetHashCode()
        {
            return PhoneValue.GetHashCode();

        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj as PhoneVO);
        }
    }
}
