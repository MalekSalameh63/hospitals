﻿using System;
using System.Text.RegularExpressions;

namespace Common.VOs
{
    public class EmailVo
    {
        public override bool Equals(object obj)
        {
            return Equals((EmailVo)obj);
        }

        protected bool Equals(EmailVo other)
        {
            return EmailAddress == other.EmailAddress;
        }


        public string EmailAddress { get; }

        private EmailVo()
        {
        }

        public EmailVo(string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
                throw new Exception("The specified string is not in the form required for an e-mail address.");

            var emailRegex = new Regex(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
            if (!emailRegex.Match(emailAddress).Success)
                throw new Exception("The specified string is not in the form required for an e-mail address.");

            EmailAddress = emailAddress;
        }
        public override string ToString()
        {
            return EmailAddress;
        }
        public static bool operator !=(EmailVo firstEmailVo, EmailVo secondEmailVo)
        {
            return firstEmailVo.EmailAddress != secondEmailVo.EmailAddress;
        }
        public static bool operator ==(EmailVo firstEmailVo, EmailVo secondEmailVo)
        {
            return firstEmailVo.EmailAddress == secondEmailVo.EmailAddress;
        }
        public override int GetHashCode() => EmailAddress.GetHashCode();
    }
}