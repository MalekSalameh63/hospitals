﻿using Common.Enums;
using Common.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.VOs
{
    public class NGenderVO
    {
        public int? Value { get; protected set; }
        public bool HasValue { get; protected set; }

        protected NGenderVO()
        {

        }
        public NGenderVO(int? value)
        {
            if (value != null)
            {
                HasValue = true;
                Value = value;
                CheckGenderValue();
            }
            else
            {
                HasValue = false;
            }
        }

        protected void CheckGenderValue()
        {
            if (Value != (int)Gender.Female && Value != (int)Gender.Male)
            {
                throw new BusinessException(FriendlyStatus.InvalidGenderValue);
            }
        }
    }
}
