﻿using System;
using System.Text.RegularExpressions;

namespace Common.VOs
{
    public class NationalIdVo
    {
        public override bool Equals(object obj)
        {
            return Equals((NationalIdVo)obj);
        }

        protected bool Equals(NationalIdVo other)
        {
            return NationalId == other.NationalId;
        }
        public string NationalId { get; }
        private NationalIdVo()
        {
        }

        public NationalIdVo(string nationalId)
        {
            nationalId = nationalId.Trim();

            if (string.IsNullOrWhiteSpace(nationalId))
                throw new Exception("NationalId can not be empty");

            if (!Regex.IsMatch(nationalId, @"[0-9]+"))
                throw new Exception("NationalId should be only numbers");

            if (nationalId.Length != 10)
                throw new Exception("NationalId should length should be 10 numbers");

            NationalId = nationalId;
        }

        public static bool operator ==(NationalIdVo firstNationalIdVo, NationalIdVo secondNationalIdVo)
        {
            return firstNationalIdVo.NationalId == secondNationalIdVo.NationalId;
        }

        public static bool operator !=(NationalIdVo firstNationalIdVo, NationalIdVo secondNationalIdVo)
        {
            return firstNationalIdVo.NationalId != secondNationalIdVo.NationalId;
        }

        public override int GetHashCode() => NationalId.GetHashCode();
    }
}
