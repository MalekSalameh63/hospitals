﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Common.VOs
{
    public class SSLCertificateVO
    {
        public byte[] Certificate { get; set; }
        public string Password { get; set; }
        public bool Enabled { get; set; }
        public string CertificateName { get; set; }
        public int SSLPort { get; set; }
    }
}
