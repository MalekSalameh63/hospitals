﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Common.VOs
{
    public class DateVO
    {
        public string DateValue { get; private set; }
        public string DateFormat { get; private set; }
        public Regex DateRegex { get; private set; }
        protected DateVO()
        {

        }
        public DateVO(TextVO inputDate, TextVO format, Regex regex = null)
        {
            DateValue = inputDate.TextValue;

            if (regex != null)
            {
                DateRegex = regex;
                ValidateTextValueRegex(regex);
            }

            DateTime FormattedDate = ValidateDateFormat(format);
            this.DateValue = FormattedDate.ToString();
        }

        private DateTime ValidateDateFormat(TextVO format)
        {
            DateTime dDate;
            if (DateTime.TryParse(DateValue, out dDate))
            {
                String.Format(format.TextValue, dDate);
                return dDate;
            }
            else
            {
                throw new Exception("Input date does not match specified date format");
            }
        }

        private void ValidateTextValueRegex(Regex regex)
        {
            if (!regex.Match(DateValue.ToString()).Success)
            {
                throw new Exception("Input text does not match specified regular expression");
            }
        }

        public static bool operator !=(DateVO firstDateVO, DateVO secondDateVO)
        {
            return firstDateVO.DateValue != secondDateVO.DateValue;
        }

        public static bool operator ==(DateVO firstDateVO, DateVO secondDateVO)
        {
            return firstDateVO.DateValue == secondDateVO.DateValue;
        }

        public override int GetHashCode()
        {
            return DateValue.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj as DateVO);
        }
    }
}
