﻿using System;
using System.Text.RegularExpressions;

namespace Common.VOs
{
    public class NShortTextVo : TextVO
    {
        public bool HasValue { get; }
        protected NShortTextVo()
        {

        }
        public NShortTextVo(string inputText, int minLength = 3, int maxLength = 256, Regex regex = null)
        {
            if (!string.IsNullOrEmpty(inputText))
            {
                HasValue = true;
                if (minLength > 0 && minLength <= maxLength && maxLength <= Math.Pow(2, 8))
                {
                    TextValue = inputText.Trim();
                    TextMinLength = minLength;
                    TextMaxLength = maxLength;
                    CheckTextValueLength();
                }
                else
                {
                    throw new Exception("Passed parameters are invalid");
                }

                if (regex != null)
                {
                    TextRegex = regex;
                    ValidateTextValueRegex(regex);
                }
            }
            else
            {
                TextValue = null;
                HasValue = false;
            }
        }

        protected new void CheckTextValueLength()
        {
            if (this.TextValue == null || this.TextValue == "" || this.TextValue.Length == 0 || this.TextValue.Length < this.TextMinLength)
            {
                throw new Exception("Input text is either empty or the input text is less than minimum length. Input text: " + this.TextValue);
            }
            else if (this.TextValue.Length > this.TextMaxLength)
            {
                this.TextValue = this.TextValue.Substring(0, this.TextMaxLength);
            }
        }
    }
}
