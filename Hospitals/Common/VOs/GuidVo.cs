﻿using System;
using System.Text.RegularExpressions;

namespace Common.VOs
{
    public class GuidVo
    {
        public override bool Equals(object obj)
        {
            return Equals((GuidVo)obj);
        }

        protected bool Equals(GuidVo other)
        {
            return Guid.Equals(other.Guid);
        }

        public Guid Guid { get; }

        private GuidVo()
        {
        }

        public GuidVo(string guid)
        {
            var guidRegex = new Regex(
                @"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$",
                RegexOptions.Compiled);
            if (!guidRegex.Match(guid).Success)
                throw new Exception("The specified string is not in the form required for a guid");

            Guid = new Guid(guid);
        }

        public static bool operator !=(GuidVo firstGuidVo, GuidVo secondGuidVo)
        {
            return firstGuidVo.Guid != secondGuidVo.Guid;
        }
        public static bool operator ==(GuidVo firstGuidVo, GuidVo secondGuidVo)
        {
            return firstGuidVo.Guid == secondGuidVo.Guid;
        }

        public override int GetHashCode() => Guid.GetHashCode();
    }
}