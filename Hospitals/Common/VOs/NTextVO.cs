﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Common.VOs
{
    /// <summary>
    /// Nullable Text VO
    /// </summary>
    public class NTextVO
    {
        public string TextValue { get; protected set; }
        public int TextMinLength { get; protected set; }
        public int TextMaxLength { get; protected set; }
        public Regex TextRegex { get; protected set; }
        public bool HasValue { get; }
        protected NTextVO()
        {

        }
        public NTextVO(string inputText, int minLength = 1, int maxLength = 256, Regex regex = null)
        {
            if (!string.IsNullOrEmpty(inputText))
            {
                HasValue = true;
                if (minLength > 0 && minLength <= maxLength && maxLength <= Math.Pow(2, 8))
                {
                    TextValue = inputText.Trim();
                    TextMinLength = minLength;
                    TextMaxLength = maxLength;
                    CheckTextValueLength();
                }
                else
                {
                    throw new Exception("Passed parameters are invalid");
                }
                if (regex != null)
                {
                    TextRegex = regex;
                    ValidateTextValueRegex(regex);
                }
            }
            else
            {
                HasValue = false;
                TextValue = null;
                TextMinLength = 0;
                TextMaxLength = 256;
            }
        }
        protected void CheckTextValueLength()
        {
            if (this.TextValue == null || this.TextValue == "" || this.TextValue.Length == 0 || this.TextValue.Length < this.TextMinLength || this.TextValue.Length > this.TextMaxLength)
            {
                throw new Exception("Input text is either empty or exceeds text length boundaries. Input text: " + this.TextValue);
            }
        }
        protected void ValidateTextValueRegex(Regex regex)
        {
            if (!regex.Match(this.TextValue).Success)
            {
                throw new Exception("Input text does not match specified regular expression. Input text: " + this.TextValue);
            }
        }

    }
}
