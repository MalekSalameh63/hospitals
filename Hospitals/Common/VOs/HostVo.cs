﻿using System;
using System.Text.RegularExpressions;

namespace Common.VOs
{
    public class HostVo
    {
        public override bool Equals(object obj)
        {
            return Equals((HostVo)obj);
        }

        protected bool Equals(HostVo other)
        {
            return _host == other._host;
        }

        private readonly string _host;

        private HostVo()
        {
        }

        public HostVo(string host)
        {
            if (string.IsNullOrWhiteSpace(host))
                throw new Exception("Host is not valid");

            var hostRegex =
                new Regex(
                    @"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");
            var ipRegex =
                new Regex(@"^(?=.{1,254}$)((?=[a-z0-9-]{1,63}\.)(xn--+)?[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,63}$");

            if (!hostRegex.Match(host).Success && !ipRegex.Match(host).Success)
                throw new Exception("Host is not valid");

            _host = host;
        }

        public override string ToString()
        {
            return _host;
        }
        public static bool operator !=(HostVo firstHostVo, HostVo secondHostVo)
        {
            return firstHostVo._host != secondHostVo._host;
        }

        public static bool operator ==(HostVo firstHostVo, HostVo secondHostVo)
        {
            return firstHostVo._host == secondHostVo._host;
        }

        public override int GetHashCode() => _host.GetHashCode();
    }
}