﻿namespace Common.Authorization.Models
{
    public class ContextUser
    {
        public string Id { get; set; }
        public string UserName { get; set; }
    }
}
