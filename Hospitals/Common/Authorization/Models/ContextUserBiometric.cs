﻿namespace Common.Authorization.Models
{
    public class ContextUserBiometric
    {
        public string Id { get; set; }
        public int? TypeId { get; set; }
        public string DeviceId { get; set; }
        public string Mrn { get; set; }
        public long? DoctorId { get; set; }
        public int? ClinicId { get; set; }
    }
}
