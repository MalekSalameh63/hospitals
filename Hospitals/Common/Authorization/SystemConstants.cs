﻿namespace Common.Authorization
{
    public static class SystemConstants
    {
        public const string SessionUserKey = "__SessionUser";
        public const string Language = "Language";
        public const string Authorization = "MicroserviceToken";
        public const string ApiGatewayAuthorization = "Authorization";
        public const string EMRAuthorization = "_EMRAuthorization";
    }
}
