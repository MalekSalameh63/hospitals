﻿using Common.Authorization.Models;
using Common.Common.Configuration;
using Common.DTOs;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Common.Authorization
{
    public class JwtTokenManager
    {
        public static string GenerateJWEToken(MicroserviceKeyDTO microserviceKey, string user)
        {

            var handler = new JwtSecurityTokenHandler();

            var securityKey = new SymmetricSecurityKey(microserviceKey.PublicKey);

            if (string.IsNullOrEmpty(user))
            {
                user = string.Empty;
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Audience = SharedSettings.MicroserviceEncryption.Audience,
                Issuer = SharedSettings.MicroserviceEncryption.Issuer,
                Expires = DateTime.UtcNow.AddMinutes(SharedSettings.MicroserviceEncryption.TokenExpiry),
                Subject = new ClaimsIdentity(new List<Claim> { new Claim(nameof(ContextUser), user) }),
                EncryptingCredentials = new X509EncryptingCredentials(new X509Certificate2(microserviceKey.PrivateKey)),
                SigningCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature)
            };

            string token = handler.CreateEncodedJwt(tokenDescriptor);

            return token;
        }

        public static JwtSecurityToken ValidateToken(MicroserviceKeyDTO microserviceKey, string token)
        {
            var handler = new JwtSecurityTokenHandler();

            var securityKey = new SymmetricSecurityKey(microserviceKey.PublicKey);

            try
            {
                var claimsPrincipal = handler.ValidateToken(
                    token,
                    new TokenValidationParameters
                    {
                        ValidAudience = SharedSettings.MicroserviceEncryption.Audience,
                        ValidIssuer = SharedSettings.MicroserviceEncryption.Issuer,
                        RequireSignedTokens = true,
                        TokenDecryptionKey = new X509EncryptingCredentials(new X509Certificate2(microserviceKey.PrivateKey)).Key,
                        IssuerSigningKey = securityKey
                    },
                    out SecurityToken securityToken);

                return securityToken as JwtSecurityToken;
            }
            catch
            {
                return null;
            }
        }
        public static string GenerateJWEBiometricToken(MicroserviceKeyDTO microserviceKey, string user)
        {

            var handler = new JwtSecurityTokenHandler();

            var securityKey = new SymmetricSecurityKey(microserviceKey.PublicKey);

            if (string.IsNullOrEmpty(user))
            {
                user = string.Empty;
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Audience = SharedSettings.MicroserviceEncryption.Audience,
                Issuer = SharedSettings.MicroserviceEncryption.Issuer,
                Expires = DateTime.UtcNow.AddYears(10),
                Subject = new ClaimsIdentity(new List<Claim> { new Claim(nameof(ContextUserBiometric), user)}),
                EncryptingCredentials = new X509EncryptingCredentials(new X509Certificate2(microserviceKey.PrivateKey)),
                SigningCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature)
            };

            string token = handler.CreateEncodedJwt(tokenDescriptor);

            return token;
        }
    }
}
