﻿using Common.Authorization.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Authorization
{
    public interface IUserContext
    {
        ContextUser TryGetCurrentUser();
        string GetSessionLanguage();
    }
}
