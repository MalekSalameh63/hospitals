﻿using Common.Authorization.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;

namespace Common.Authorization
{
    public class UserContext : IUserContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly bool _sessionEnabled = false;
        public UserContext(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string GetSessionLanguage()
        {
            if (_httpContextAccessor.HttpContext != null && _httpContextAccessor.HttpContext.Request != null)
            {
                if (_httpContextAccessor.HttpContext.Request.Headers.ContainsKey(SystemConstants.Language))
                {
                    return _httpContextAccessor.HttpContext.Request.Headers[SystemConstants.Language].ToString().ToLower();
                }
            }
            return string.Empty;
        }

        public ContextUser TryGetCurrentUser()
        {
            if (_httpContextAccessor.HttpContext.Session != null && _httpContextAccessor.HttpContext.User != null)
            {
                string jsonUser = _httpContextAccessor.HttpContext.User?.Claims?.FirstOrDefault()?.Value;

                if (jsonUser != null)
                    return JsonConvert.DeserializeObject<ContextUser>(jsonUser);
            }

            return new ContextUser();
        }

        public static string GetUserFromToken(JwtSecurityToken token)
        {
            if (token.Payload.TryGetValue(nameof(ContextUser), out object jsonUser))
                return jsonUser as string;

            return null;
        }

        public static string GetUserFromBiometricToken(JwtSecurityToken token)
        {
            if (token.Payload.TryGetValue(nameof(ContextUserBiometric), out object jsonUser))
                return jsonUser as string;

            return null;
        }

        public static ContextUser GetContextUserFromToken(JwtSecurityToken token)
        {
            if (token.Payload.TryGetValue(nameof(ContextUser), out object jsonUser))
            {
                return JsonConvert.DeserializeObject<ContextUser>(jsonUser.ToString());
            }

            return null;
        }
    }
}
