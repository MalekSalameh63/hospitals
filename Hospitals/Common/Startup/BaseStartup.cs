﻿using Common.Common;
using Common.Common.Configuration;
using Common.DTOs;
using Common.Enums;
using Common.Filters;
using Common.GlobalErrorHandling;
using Common.Helper;
using Elastic.Apm.NetCoreAll;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.Linq;
using System.Net.Mime;
namespace Common.Startup
{
    public class BaseStartup
    {
        protected IWebHostEnvironment Environment { get; set; }
        protected IConfiguration Configuration { get; }
        public BaseStartup(IWebHostEnvironment env)
        {
            Environment = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        protected void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            var pathBase = Configuration["PATH_BASE"];

            ILogger logger = loggerFactory.CreateLogger<BaseStartup>();

            app.ConfigureExceptionHandler(logger);

            if (!string.IsNullOrEmpty(pathBase))
            {
                logger.LogDebug("Using PATH BASE '{pathBase}'", pathBase);
                app.UsePathBase(pathBase);
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                app.UseHttpsRedirection();
            }

            if (SharedSettings.ElasticApmEnable)
            {
                app.UseAllElasticApm(Configuration);
            }

            app.UseRouting();

            app.UseHealthChecks("/healthcheck",
                new HealthCheckOptions
                {
                    ResponseWriter = async (context, report) =>
                    {
                        var result = JsonConvert.SerializeObject(
                            new
                            {
                                status = report.Status.ToString(),
                                errors = report.Entries.Select(e => new { key = e.Key, value = Enum.GetName(typeof(HealthStatus), e.Value.Status) })
                            });
                        context.Response.ContentType = MediaTypeNames.Application.Json;
                        await context.Response.WriteAsync(result);
                    }
                });

            //app.UseMvc();

            #region swagger
            app.UseSwagger(c =>
            {
                c.RouteTemplate = "swagger/{documentName}/swagger.json";
            });
            app.UseSwaggerUI(s =>
            {
                s.SwaggerEndpoint($"{ (!string.IsNullOrEmpty(pathBase) ? pathBase : string.Empty) }/swagger/v1/swagger.json", "AppSettings Microservice Documentation API v1.0");
                s.DocExpansion(DocExpansion.None);
            });
            #endregion

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }

    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddAppSettings(this IServiceCollection services, IConfiguration configuration)
        {
            #region Configuration

            #region Microservice Identification
            MicroserviceIdentification.Name = configuration.GetSection("MicroserviceIdentification").GetValue<string>("Name");
            MicroserviceIdentification.Id = configuration.GetSection("MicroserviceIdentification").GetValue<string>("Id");
            MicroserviceIdentification.Environment = configuration.GetSection("MicroserviceIdentification").GetValue<string>("Environment");
            SharedSettings.SSLEnabled = configuration.GetSection("SSLCertificateSettings").GetValue<bool>("Enabled");
            #endregion

            try
            {
                string url = configuration.GetSection("MicroservicesURLs").GetValue<string>("AppsettingsUrl");

                var appSettingsResult = HttpClientWrapper<ResponseResult<AppSettingsConfiguration>>
                    .GetRequest(url, "api/AppSettings/" + MicroserviceIdentification.Id.ToLower() + "/" + MicroserviceIdentification.Environment.ToLower());

                if (appSettingsResult == null || appSettingsResult.Data == null || appSettingsResult.ApiStatus != ApiStatus.Ok)
                {
                    throw new Exception("Error retrieving AppSettings or AppSettings Empty");
                }

                AppSettingsConfiguration AppSettings = appSettingsResult.Data;

                var encryptionResult = HttpClientWrapper<ResponseResult<MicroserviceEncryptionDTO>>.GetRequest(url, "api/MicroserviceEncryption");

                if (encryptionResult == null || encryptionResult.Data == null || encryptionResult.ApiStatus != ApiStatus.Ok)
                {
                    throw new Exception("Error retrieving Encryption Keys or Encryption Keys Empty");
                }

                SharedSettings.MicroserviceEncryption = encryptionResult.Data;

                if (AppSettings != null)
                {

                    #region ElasticApmSettings                   
                    if (AppSettings.ElasticApmSettings != null)
                    {
                        SharedSettings.ElasticApmEnable = AppSettings.ElasticApmSettings.ElasticApmEnable;
                        configuration["ElasticApm:SecretToken"] = AppSettings.ElasticApmSettings.SecretToken;
                        configuration["ElasticApm:ServerUrls"] = AppSettings.ElasticApmSettings.ServerUrls;
                        configuration["ElasticApm:ServiceName"] = AppSettings.ElasticApmSettings.ServiceName;
                    }
                    #endregion

                    if (AppSettings.MongoSettings != null)
                    {
                        SharedSettings.MongoConnection = AppSettings?.MongoSettings?.Connection;
                        SharedSettings.MongoDatabaseName = AppSettings?.MongoSettings?.DatabaseName;
                        services.AddSingleton<MongoSettingsDTO>(AppSettings.MongoSettings);
                    }

                    if (AppSettings.RedisSettings != null)
                    {
                        SharedSettings.RedisConnection = AppSettings.RedisSettings.Connection;
                        SharedSettings.RedisDatabaseId = AppSettings.RedisSettings.DatabaseId;

                        services.AddSingleton<RedisSettingsDTO>(AppSettings.RedisSettings);
                    }
                    if (AppSettings.NavigationSettings != null)
                    {
                        SharedSettings.DictionaryServiceBaseUrl = AppSettings.NavigationSettings.DictionaryApiUrl;
                        SharedSettings.LookupServiceBaseUrl = AppSettings.NavigationSettings.LookupApiUrl;
                        SharedSettings.WebEMRUrl = AppSettings.NavigationSettings.WebEMRUrl;
                        services.AddSingleton<NavigationSettingsDTO>(AppSettings.NavigationSettings);
                    }
                    if (AppSettings.EmailVerificationSettings != null)
                    {
                        EmailVerificationData.ActivationRequestsToBlock = AppSettings.EmailVerificationSettings.ActivationRequestsToBlock;
                        EmailVerificationData.EmailActivationLinkExpiration = AppSettings.EmailVerificationSettings.EmailActivationLinkExpiration;
                        EmailVerificationData.UnBlockUserPeriod = AppSettings.EmailVerificationSettings.UnBlockUserPeriod;
                        EmailVerificationData.EncryptionKey = AppSettings.EmailVerificationSettings.EncryptionKey;
                        EmailVerificationData.EmailAllowedPeriodInDays = AppSettings.EmailVerificationSettings.EmailAllowedPeriodInDays;
                        EmailVerificationData.EmailActivationLink = AppSettings.EmailVerificationSettings.EmailActivationLink;

                        services.AddSingleton<EmailVerificationSettingsDTO>(AppSettings.EmailVerificationSettings);
                    }
                    if (AppSettings.CachingSettings != null)
                    {
                        CachingSettingsDto.CachingProfiles = AppSettings.CachingSettings.CachingProfiles;
                        CachingSettingsDto.CacheEnabled = AppSettings.CachingSettings.CacheEnabled;
                    }
                    if (AppSettings.PostgresSettings != null)
                    {
                        SharedSettings.PostgresConnectionString = AppSettings.PostgresSettings.Connection;
                        services.AddSingleton<PostgresSettingsDTO>(AppSettings.PostgresSettings);
                    }
                    if (AppSettings.PaymentSettings != null)
                    {
                        services.AddSingleton<PaymentSettingsDTO>(AppSettings.PaymentSettings);
                    }
                    if (AppSettings.NotificationSettings != null)
                    {
                        services.AddSingleton<NotificationSettingsDTO>(AppSettings.NotificationSettings);
                    }
                    if (AppSettings.OpenTokSettings != null)
                    {
                        services.AddSingleton<OpenTokSettingsDTO>(AppSettings.OpenTokSettings);
                    }
                    if (AppSettings.SMSSettings != null)
                    {
                        services.AddSingleton<SMSSettingsDTO>(AppSettings.SMSSettings);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while adding AppSettings. \n Microservice Name : " + MicroserviceIdentification.Name + ". \n Exception : " + ex.Message);
            }
            #endregion

            return services;
        }
        public static IServiceCollection AddCachingProfiles(this IServiceCollection services)
        {
            var cacheEnabled = CachingSettingsDto.CacheEnabled;
            var cachingProfilesConfiguration = CachingSettingsDto.CachingProfiles;
            services.AddControllers(config =>
                {
                    if (cacheEnabled)
                    {
                        foreach (var cachingProfile in cachingProfilesConfiguration)
                        {
                            config.CacheProfiles.Add(
                                cachingProfile.ProfileName,
                                new CacheProfile()
                                {
                                    Duration = cachingProfile.Duration * 60,
                                    Location = cachingProfile.Location,
                                    VaryByQueryKeys = cachingProfile.VaryByQueryKeys,
                                    VaryByHeader = "language",
                                    NoStore = !cachingProfile.Enabled
                                });

                        }
                    }
                    else
                    {
                        foreach (var cachingProfile in cachingProfilesConfiguration)
                        {
                            config.CacheProfiles.Add(
                                cachingProfile.ProfileName,
                                new CacheProfile()
                                {
                                    Duration = 0,
                                    Location = ResponseCacheLocation.None,
                                    NoStore = true
                                });

                        }
                    }

                }
                );


            return services;
        }
    }
}
