﻿using Common.Common;
using Common.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.IO;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Common.Middleware
{
    public class ResponseRewindMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly RecyclableMemoryStreamManager _recyclableMemoryStreamManager;


        public ResponseRewindMiddleware(RequestDelegate next)
        {
            this._next = next;
            _recyclableMemoryStreamManager = new RecyclableMemoryStreamManager();

        }

        public async Task Invoke(HttpContext context)
        {
            await Request(context);
            await CacheResponse(context);
        }

        private async Task Request(HttpContext context)
        {
            try
            {
                context.Request.EnableBuffering();

                await using var requestStream = _recyclableMemoryStreamManager.GetStream();
                await context.Request.Body.CopyToAsync(requestStream);


                context.Request.Body.Position = 0;
            }
            catch (Exception ex)
            {
                Console.Error.Write("ResponseRewindMiddleware >> Request >> exception Error: " + ex.Message + " / " + " StackTrace: " + ex.StackTrace);
            }
        }

        private static string ReadStreamInChunks(Stream stream)
        {
            const int readChunkBufferLength = 4096;

            stream.Seek(0, SeekOrigin.Begin);

            using var textWriter = new StringWriter();
            using var reader = new StreamReader(stream);

            var readChunk = new char[readChunkBufferLength];
            int readChunkLength;

            do
            {
                readChunkLength = reader.ReadBlock(readChunk,
                    0,
                    readChunkBufferLength);
                textWriter.Write(readChunk, 0, readChunkLength);
            } while (readChunkLength > 0);

            return textWriter.ToString();
        }
        private async Task CacheResponse(HttpContext context)
        {
            var originalBodyStream = context.Response.Body;

            await using var responseBody = _recyclableMemoryStreamManager.GetStream();
            context.Response.Body = responseBody;
            await _next(context);

            context.Response.Body.Seek(0, SeekOrigin.Begin);
            var text = await new StreamReader(context.Response.Body).ReadToEndAsync();
            try
            {
                var responseObject =
                    JsonConvert.DeserializeObject<ResponseResult<object>>(text);

                if (responseObject?.GlobalStatus != GlobalStatus.Success ||
                    responseObject?.ApiStatus != ApiStatus.Ok)
                {
                    context.Response.Headers.Remove("Cache-Control");
                    context.Response.Headers.Add("Cache-Control", "no-cache, no-store, must-revalidate, public, max-age=0");
                    context.Response.Headers.Remove("Pragma");
                    context.Response.Headers.Add("Pragma", "no-cache");
                    context.Response.Headers.Remove("Expires");
                    context.Response.Headers.Add("Expires", "0");
                }
            }
            catch
            {
                // ignored
            }


            context.Response.Body.Seek(0, SeekOrigin.Begin);
            await responseBody.CopyToAsync(originalBodyStream);
        }
    }
}
