﻿namespace Common
{
    public class EmailActivationResponse
    {
        public string Email { get; set; }
        public string Language { get; set; }
    }
}
