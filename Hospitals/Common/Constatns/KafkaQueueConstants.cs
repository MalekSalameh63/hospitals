﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Common.Constatns
{
    public class KafkaQueueConstants
    {
        /// <summary>
        /// Patient queue name
        /// </summary>
        public const string PatientQueue = "patientqueue";

        /// <summary>
        /// Physician queue name
        /// </summary>
        public const string PhysicianQueue = "physicianqueue";

        /// <summary>
        /// Notification queue name
        /// </summary>
        public const string NotificationQueue = "notificationqueue";
    }
}