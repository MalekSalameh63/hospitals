﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Constatns
{
    public class ValidationErrors
    {
        public const string NameIsRequired = "Name_Is_Required";
        public const string NationalityIsRequired = "Nationality_Is_Required";
        public const string MaritalStatusIsRequired = "MaritalStatus_Is_Required";
        public const string DOBIsRequired = "DOB_Is_Required";
        public const string EmailConfirmedIsRequired = "Email_Confirmed_Is_Required";
        public const string NationalIdIsRequired = "National_Id_Is_Required";
        public const string PatientIdentificationNoAlreadyExist = "Patient_Identification_No_Already_Exist";
    }
}
