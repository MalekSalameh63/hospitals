﻿using Newtonsoft.Json;

namespace Common.DTOs.InitiatePatientCall
{
    public class PatientInfoDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("age")]
        public int Age { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("user_type")]
        public int? UserType { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("age_label")]
        public string AgeLabel { get; set; }
        [JsonProperty("image")]
        public string Image { get; set; }
        [JsonProperty("specialties")]
        public string Specialties { get; set; }

    }
}
