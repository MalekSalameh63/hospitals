﻿using Newtonsoft.Json;

namespace Common.DTOs.InitiatePatientCall
{
    public class CallConfigurationDto
    {
        [JsonProperty("call_duration")]
        public long CallDuration { get; set; }

        [JsonProperty("warning_duration")]
        public long WarningDuration { get; set; }
        [JsonProperty("timer_text")]
        public string TimerText { get; set; }
        [JsonProperty("remaining_time")]
        public string RemainingTime { get; set; }
    }
}
