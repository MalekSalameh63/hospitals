﻿using Newtonsoft.Json;

namespace Common.DTOs.InitiatePatientCall
{
    public class DialogConfigurationDto
    {
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("ok_button")]
        public string OkButton { get; set; }
        [JsonProperty("cancel_button")]
        public string CancelButton { get; set; }
    }
}
