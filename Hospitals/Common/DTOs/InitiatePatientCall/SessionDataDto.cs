﻿using Newtonsoft.Json;

namespace Common.DTOs.InitiatePatientCall
{
    public class SessionDataDto
    {
        [JsonProperty("key_api_key")]
        public long KeyApiKey { get; set; }

        [JsonProperty("key_session_id")]
        public string KeySessionId { get; set; }

        [JsonProperty("key_token")]
        public string KeyToken { get; set; }
    }
}
