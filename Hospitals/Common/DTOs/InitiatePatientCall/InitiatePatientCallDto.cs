﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Common.DTOs.InitiatePatientCall
{
    public class InitiatePatientCallDto
    {
        [Key]
        [JsonProperty("id")]
        public Guid Id { get; set; }
        [JsonProperty("user_id")]
        public string PhysicianId { get; set; }
        [JsonProperty("session_data")]
        public SessionDataDto SessionData { get; set; }
        [JsonProperty("user_info")]
        public PatientInfoDto PatientInfo { get; set; }
        [JsonProperty("call_configuration")]
        public CallConfigurationDto CallConfiguration { get; set; }

        [JsonProperty("dialog_config")]
        public DialogConfigurationDto DialogConfiguration { get; set; }
    }
}
