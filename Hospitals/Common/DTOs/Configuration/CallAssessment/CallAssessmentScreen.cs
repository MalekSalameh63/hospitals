﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Configuration.CallAssessment
{
    public class CallAssessmentScreenDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("screen_config")]
        public AssessmentSectionDTO AssessmentSection { get; set; }

        [JsonProperty("questions_list")]
        public List<QuestionDTO> Questions { get; set; }
    }
}
