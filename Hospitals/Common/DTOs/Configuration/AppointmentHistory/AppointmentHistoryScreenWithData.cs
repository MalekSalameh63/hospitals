﻿using Common.DTOs.Configuration.UpcomingAppointments;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.AppointmentHistory
{
    public class AppointmentHistoryScreenWithData
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
        [JsonProperty("route")]
        public string Route { get; set; }
        [JsonProperty("appointment_details_configuration")]
        public CustomThemeDTO CustomTheme { get; set; }
        [JsonProperty("appointment_history_configuration")]
        public AppointmentHistoryConfigurationDto AppointmentsConfiguration { get; set; }
        [JsonProperty("appointments_list")]
        public List<UpcomingAppointmentDetailsDto> AppointmentsList { get; set; }
    }
}
