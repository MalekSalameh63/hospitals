﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.Configuration.AppointmentHistory
{
    public class AppointmentHistoryScreenDto
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
        [JsonProperty("route")]
        public string Route { get; set; }
        [JsonProperty("appointment_details_configuration")]
        public CustomThemeDTO CustomTheme { get; set; }
        [JsonProperty("appointment_history_configuration")]
        public AppointmentHistoryConfigurationDto AppointmentsConfiguration { get; set; }

    }
}
