﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.AppointmentHistory
{
    public class AppointmentHistoryConfigurationDto
    {
        [JsonProperty("call_to_action_button_icon")]
        public string CallToActionButtonIcon { get; set; }
        [JsonProperty("call_to_action_button_icon_color")]
        public string CallToActionButtonIconColor { get; set; }
        [JsonProperty("appointment_icon")]
        public string AppointmentIcon { get; set; }
        [JsonProperty("more_icon")]
        public string MoreRoute { get; set; }
    }
}

