﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.AppointmentHomeScreen
{
    public class AppointmentHomeScreenServiceDto
    {
        [JsonProperty("appointment_service")]
        public ApplicationServiceDTO AppointmentService { get; set; }
        [JsonProperty("route")]
        public string Route { get; set; }
    }
}
