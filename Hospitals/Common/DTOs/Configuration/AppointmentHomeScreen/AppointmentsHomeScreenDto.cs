﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.AppointmentHomeScreen
{
    public class AppointmentsHomeScreenDto
    {

        [JsonProperty("appointment_services")]
        public List<ApplicationServiceDTO> AppointmentServices { get; set; }
    }
}
