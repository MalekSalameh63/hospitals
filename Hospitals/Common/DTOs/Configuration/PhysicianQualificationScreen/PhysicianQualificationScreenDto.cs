﻿namespace Common.DTOs.Configuration
{
    public class PhysicianQualificationScreenDto
    {
        public int Status { get; set; }
        public string QuestionTitle { get; set; }
        public string SubmitTitle { get; set; }
        public string Confirmation { get; set; }
        public string RequiredText { get; set; }
        public string InvalidInputText { get; set; }
        public string RankTitle { get; set; }
        public string QualificationTitle { get; set; }
        public string ClinicTitle { get; set; }
        public string SpecialtyTitle { get; set; }
        public string LanguageTitle { get; set; }
        public string BioTitle { get; set; }
        public string LicenseNumberTitle { get; set; }
        public string LicenseExpiryTitle { get; set; }
        public string AttachmentsTitle { get; set; }
        public string AttachmentsValue { get; set; }

    }
}
