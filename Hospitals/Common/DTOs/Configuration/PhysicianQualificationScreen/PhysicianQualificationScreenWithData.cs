﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs.Configuration
{
    public class PhysicianQualificationScreenWithData
    {
        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("question")]
        public string Question { get; set; }

        [JsonProperty("submit")]
        public string Submit { get; set; }

        [JsonProperty("confirmation")]
        public string Confirmation { get; set; }

        [JsonProperty("required_text")]
        public string RequiredText { get; set; }

        [JsonProperty("invalid_input_text")]
        public string InvalidInputText { get; set; }

        [JsonProperty("rank")]
        public RankString Rank { get; set; }

        [JsonProperty("qualification")]
        public BioInt Qualification { get; set; }

        [JsonProperty("clinic")]
        public RankString Clinic { get; set; }

        [JsonProperty("speciality")]
        public Languages Speciality { get; set; }

        [JsonProperty("languages")]
        public Languages Languages { get; set; }

        [JsonProperty("bio")]
        public BioInt BioInt { get; set; }

        [JsonProperty("license_number")]
        public BioInt LicenseNumber { get; set; }

        [JsonProperty("license_expiry")]
        public LicenseExpiry LicenseExpiry { get; set; }

        [JsonProperty("attachments")]
        public PhysicianQualificationAttachments PhysicianQualificationAttachments { get; set; }
    }
    public partial class PhysicianQualificationAttachments
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public partial class BioInt
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("value")]
        public dynamic Value { get; set; }

        [JsonProperty("more")]
        public BioMoreInt MoreInt { get; set; }
    }
    public partial class BioString
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("more")]
        public BioMoreString MoreInt { get; set; }
    }
    public partial class RankInt
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("value")]
        public int Value { get; set; }

        [JsonProperty("more")]
        public BioMoreInt MoreInt { get; set; }
    }
    public partial class RankString
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("value")]
        public dynamic Value { get; set; }

        [JsonProperty("more")]
        public BioMoreString MoreInt { get; set; }
    }

    public partial class BioMoreInt
    {
        [JsonProperty("list")]
        public List<PhysicianQualificationIntList> List { get; set; }
    }
    public partial class BioMoreString
    {
        [JsonProperty("list")]
        public List<PhysicianQualificationStringList> List { get; set; }
    }

    public partial class PhysicianQualificationIntList
    {
        [JsonProperty("key")]
        public int Key { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
    }
    public partial class PhysicianQualificationStringList
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("value")]
        public dynamic Value { get; set; }
    }

    public partial class Languages
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("value")]
        public List<int> Value { get; set; }

        [JsonProperty("more")]
        public LanguagesMore More { get; set; }
    }

    public partial class LanguagesMore
    {
        [JsonProperty("list")]
        public List<FluffyList> List { get; set; }
    }

    public partial class FluffyList
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("value")]
        public long Value { get; set; }
    }


    public partial class LicenseExpiry
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("value")]
        public long? Value { get; set; }

        [JsonProperty("more")]
        public object More { get; set; }
    }
}
