﻿using Common.Common.Appointments;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.AppointmentHistoryDetailsScreen
{
    public class AppointmentHistoryDetailsScreenRadiologyConfiguration : AppointmentHistoryDetailsScreenTabConfigurationDto
    {
        public List<RadiologyOrderWithResults> RadiologyOrders { get; set; }
    }
}
