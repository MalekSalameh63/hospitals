﻿using Common.DTOs.Configuration.DoctorsList;
using Common.DTOs.Configuration.UpcomingAppointmentsDetailsScreen;

namespace Common.DTOs.Configuration
{
    public class AppointmentHistoryDetailsScreenCardDto : UpcomingAppointmentsDetailsCardDto
    {
        public CallToActionButtonDto CallToActionButton { get; set; }

    }
}
