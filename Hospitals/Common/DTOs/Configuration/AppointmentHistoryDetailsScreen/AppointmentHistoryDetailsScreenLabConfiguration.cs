﻿using Common.Common.Appointments;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.AppointmentHistoryDetailsScreen
{
    public class AppointmentHistoryDetailsScreenLabConfiguration : AppointmentHistoryDetailsScreenTabConfigurationDto
    {
        public List<LabOrderWithResults> LabOrders { get; set; }
    }
}
