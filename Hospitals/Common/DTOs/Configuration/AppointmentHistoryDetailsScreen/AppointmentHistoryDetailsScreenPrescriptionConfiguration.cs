﻿using Common.DTOs.EMR.MedicalRecord.Prescription;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.AppointmentHistoryDetailsScreen
{
    public class AppointmentHistoryDetailsScreenPrescriptionConfiguration : AppointmentHistoryDetailsScreenTabConfigurationDto
    {
        public List<Prescription> Prescriptions { get; set; }
    }
}
