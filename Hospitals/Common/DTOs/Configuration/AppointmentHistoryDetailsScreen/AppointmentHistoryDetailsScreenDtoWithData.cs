﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.AppointmentHistoryDetailsScreen
{
    public class AppointmentHistoryDetailsScreenDtoWithData
    {
        [JsonProperty("appointment_card")]
        public AppointmentHistoryDetailsScreenCardDto Card { get; set; }
        [JsonProperty("appointment_history_details_tabs")]
        public List<ApplicationServiceDTO> AppointmentHistoryDetailsTabs { get; set; }
        [JsonProperty("lab")]
        public AppointmentHistoryDetailsScreenLabConfiguration Lab { get; set; }
        [JsonProperty("radiology")]
        public AppointmentHistoryDetailsScreenRadiologyConfiguration Radiology { get; set; }
        [JsonProperty("prescription")]
        public AppointmentHistoryDetailsScreenPrescriptionConfiguration Prescription { get; set; }
        [JsonProperty("services_custom_theme")]
        public CustomThemeDTO ServiceCustomTheme { get; set; }
    }
}
