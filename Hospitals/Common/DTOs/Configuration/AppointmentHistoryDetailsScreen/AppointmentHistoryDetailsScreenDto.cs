﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs.Configuration
{
    public class AppointmentHistoryDetailsScreenDto
    {
        [JsonProperty("appointment_card")]
        public AppointmentHistoryDetailsScreenCardDto Card { get; set; }
        [JsonProperty("appointment_history_details_tabs")]
        public List<ApplicationServiceDTO> AppointmentHistoryDetailsTabs { get; set; }
        [JsonProperty("lab_configuration")]
        public AppointmentHistoryDetailsScreenTabConfigurationDto LabConfiguration { get; set; }
        [JsonProperty("radiology_configuration")]
        public AppointmentHistoryDetailsScreenTabConfigurationDto RadiologyConfiguration { get; set; }
        [JsonProperty("prescription_configuration")]
        public AppointmentHistoryDetailsScreenTabConfigurationDto PrescriptionConfiguration { get; set; }
        [JsonProperty("services_custom_theme")]
        public CustomThemeDTO ServicesCustomTheme { get; set; }

    }
}
