﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration
{
    public class AppointmentHistoryDetailsScreenTabConfigurationDto
    {
        [JsonProperty("tab_custom_theme")]
        public CustomThemeDTO TabCustomTheme { get; set; }
        [JsonProperty("cards_custom_theme")]
        public CustomThemeDTO CardsCustomTheme { get; set; }
    }
}
