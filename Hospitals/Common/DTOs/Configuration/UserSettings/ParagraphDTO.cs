﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.UserSettings
{
    public class ParagraphDTO
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("subtitle")]
        public string Subtitle { get; set; }
    }
}
