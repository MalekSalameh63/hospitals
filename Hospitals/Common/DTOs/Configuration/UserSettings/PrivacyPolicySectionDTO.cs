﻿using Common.DTOs.Configuration.Shared;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.UserSettings
{
    public class PrivacyPolicySectionDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("user_type_id")]
        public int UserTypeId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("privacy_policy_list")]
        public List<ParagraphDTO> PrivacyPolicies { get; set; }

        [JsonProperty("privacy_policy_button")]
        public SubmitButtonDTO PrivacyPolicyButton { get; set; }

        [JsonProperty("accept_privacy_policy_toggle")]
        public AcceptPrivacyPolicyToggleDTO AcceptPrivacyPolicyToggle { get; set; }

        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }
    }
}
