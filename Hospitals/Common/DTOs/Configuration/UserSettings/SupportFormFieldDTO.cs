﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.UserSettings
{
    public class SupportFormFieldDTO
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("hint")]
        public string Hint { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
