﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.UserSettings
{
    public class AcceptPrivacyPolicyToggleDTO
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("value")]
        public bool Value { get; set; }
    }
}
