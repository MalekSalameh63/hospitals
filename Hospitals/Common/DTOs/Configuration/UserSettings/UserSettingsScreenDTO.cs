﻿using Newtonsoft.Json;
using System;
using System.Text;

namespace Common.DTOs.Configuration.UserSettings
{
    public class UserSettingsScreenDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("language_select")]
        public LanguageSectionDTO LanguageSection { get; set; }

        [JsonProperty("privacy_policy")]
        public PrivacyPolicySectionDTO PrivacyPolicySection { get; set; }

        [JsonProperty("notifications_settings")]
        public NotificationsSectionDTO NotificationSettingsSection { get; set; }

        [JsonProperty("support")]
        public SupportSectionDTO SupportSection { get; set; }
    }
}
