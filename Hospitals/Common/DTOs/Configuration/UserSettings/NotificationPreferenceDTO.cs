﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.UserSettings
{
    public class NotificationPreferenceDTO
    {
        [JsonProperty("key")]
        public int Key { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("value")]
        public bool Value { get; set; }

        [JsonProperty("action")]
        public string Action { get; set; }
    }
}
