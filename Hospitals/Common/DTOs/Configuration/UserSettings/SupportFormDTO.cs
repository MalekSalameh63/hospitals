﻿using Common.DTOs.Configuration.Shared;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.UserSettings
{
    public class SupportFormDTO
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("background")]
        public string Background { get; set; }

        [JsonProperty("fields")]
        public List<SupportFormFieldDTO> Fields { get; set; }

        [JsonProperty("submit_button")]
        public SubmitButtonDTO SubmitButton { get; set; }
    }
}
