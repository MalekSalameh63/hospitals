﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.Configuration.UserSettings
{
    public class SupportSectionDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("user_type_id")]
        public int UserTypeId { get; set; }
        [JsonProperty("user_email")]
        public string UserEmail { get; set; }
        [JsonProperty("user_name")]
        public string Username { get; set; }
        [JsonProperty("phone")]
        public string PhoneNumber { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("main_title")]
        public string MainTitle { get; set; }

        [JsonProperty("main_subtitle")]
        public string MainSubtitle { get; set; }

        [JsonProperty("rate_count")]
        public int RateCount { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("support_form")]
        public SupportFormDTO SupportForm { get; set; }

        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }
    }
}
