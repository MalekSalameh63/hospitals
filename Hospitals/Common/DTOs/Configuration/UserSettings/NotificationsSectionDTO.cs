﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.UserSettings
{
    public class NotificationsSectionDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("user_type_id")]
        public int UserTypeId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("notification_preferences")]
        public List<NotificationPreferenceDTO> Preferences { get; set; }

        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }
    }
}
