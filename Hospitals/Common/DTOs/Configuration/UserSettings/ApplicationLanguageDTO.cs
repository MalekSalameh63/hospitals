﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.UserSettings
{
    public class ApplicationLanguageDTO
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("value")]
        public int Value { get; set; }

        [JsonProperty("is_selected")]
        public bool IsSelected { get; set; }

    }
}
