﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.UserSettings
{
    public class LanguageSectionDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("user_type_id")]
        public int UserTypeId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("languages")]
        public List<ApplicationLanguageDTO> Languages { get; set; }

        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }
    }
}
