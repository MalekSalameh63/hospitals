﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class UserLevelNavigationBarDTO
    {
        [JsonProperty("dashboard")]
        public bool Dashboard { get; set; }

        [JsonProperty("requests")]
        public bool Requests { get; set; }

        [JsonProperty("working_hours")]
        public bool WorkingHours { get; set; }

        [JsonProperty("my_balance")]
        public bool MyBalance { get; set; }
    }
}
