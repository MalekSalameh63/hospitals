﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Configuration.Screens
{
    public class PhysicianAvailabilityDTO
    {
        [JsonProperty("doctor_availability")]
        public bool Available { get; set; }

        [JsonProperty("doctor_available_text")]
        public string AvailableText { get; set; }

        [JsonProperty("doctor_not_available_text")]
        public string NotAvailableText { get; set; }
    }
}
