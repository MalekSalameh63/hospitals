﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class UsersAvailabilityDTO
    {
        [JsonProperty("enabled")]
        public bool Enabled { get; set; }

        [JsonProperty("available_users_count")]
        public long UserCount { get; set; }

        [JsonProperty("available_text")]
        public string AvailableText { get; set; }
    }
}
