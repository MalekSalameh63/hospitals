﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs
{
    public class SliderExtrasDTO
    {
        [JsonProperty("modal_image")]
        public string ModalImage { get; set; }

        [JsonProperty("dots_active_color")]
        public string DotsActiveColor { get; set; }

        [JsonProperty("dots_deactive_color")]
        public string DotsDeactiveColor { get; set; }

        [JsonProperty("animation_start_after_milliseconds")]
        public long AnimationStartAfterMilliseconds { get; set; }

        [JsonProperty("slider_slide_after_milliseconds")]
        public long SliderSlideAfterMilliseconds { get; set; }
    }
}
