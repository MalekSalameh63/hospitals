﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class PhysicianInformationDTO
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("field")]
        public string Field { get; set; }

        [JsonProperty("country_flag")]
        public string CountryFlag { get; set; }

        [JsonProperty("specialty")]
        public string Specialty { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("years_experience")]
        public string ExperienceYears { get; set; }

        [JsonProperty("experience")]
        public string Experience { get; set; }
    }
}
