﻿using Common.DTOs.Configuration.Screens;
using Common.VOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.DTOs
{
    public class ScreenDTO
    {
        public Guid Id { get; set; }

        [JsonProperty("user_type")]
        public int UserType { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("sliders")]
        public List<SliderDTO> Sliders { get; set; }

        [JsonProperty("action_buttons")]
        public List<ActionButtonDTO> ActionButtons { get; set; }

        [JsonProperty("profile_information")]
        public FieldDTO ProfileInformation { get; set; }

        [JsonProperty("available_users")]
        public UsersAvailabilityDTO UsersAvailability { get; set; }

        [JsonProperty("application_services")]
        public List<ApplicationServiceDTO> ApplicationServices { get; set; }

        [JsonProperty("doctor_status")]
        public PhysicianAvailabilityDTO PhysicianAvailability { get; set; }

        [JsonProperty("static_navigation_bar")]
        public List<StaticNavigationBarDTO> StaticNavigationBar { get; set; }

        [JsonProperty("user_level_navigation_bar")]
        public List<UserLevelNavigationBarDTO> UserLevelNavigationBar { get; set; }
        [JsonProperty("slider_extras")]
        public List<SliderExtrasDTO> SliderExtras { get; set; }
    }
}
