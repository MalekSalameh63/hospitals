﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class SliderDTO
    {
        [JsonProperty("order")]
        public int Order { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("subtitle")]
        public string Subtitle { get; set; }

        [JsonProperty("title_color")]
        public string TitleColor { get; set; }

        [JsonProperty("subtitle_color")]
        public string SubtitleColor { get; set; }

        [JsonProperty("title_font_size")]
        public double TitleFontSize { get; set; }

        [JsonProperty("subtitle_font_size")]
        public double SubtitleFontSize { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }
    }
}
