﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class ApplicationServiceDTO
    {
        [JsonProperty("service_name")]
        public string ServiceName { get; set; }

        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("activated")]
        public bool Activated { get; set; }

        [JsonProperty("order")]
        public int Order { get; set; }
        [JsonProperty("route")]
        public string Route { get; set; }
    }
}
