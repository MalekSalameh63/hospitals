﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class ActionButtonDTO
    {
        [JsonProperty("key")]
        public string Key { get; set; }
        [JsonProperty("button_text")]
        public string Text { get; set; }
        [JsonProperty("button_text_color")]
        public string TextColor { get; set; }
        [JsonProperty("button_color")]
        public string ButtonColor { get; set; }
        [JsonProperty("button_route")]
        public string Route { get; set; }

        [JsonProperty("route_type")]
        public string RouteType { get; set; }
        [JsonProperty("toggle")]
        public bool Toggle { get; set; }

        //[JsonProperty("start_button_text")]
        //public string StartButtonText { get; set; }

        //[JsonProperty("start_button_text_color")]
        //public string StartButtonTextColor { get; set; }

        //[JsonProperty("start_button_route")]
        //public string StartButtonRoute { get; set; }

        //[JsonProperty("start_button_color")]
        //public string StartButtonColor { get; set; }

        //[JsonProperty("skip_button_text")]
        //public string SkipButtonText { get; set; }

        //[JsonProperty("skip_button_text_color")]
        //public string SkipButtonTextColor { get; set; }

        //[JsonProperty("skip_button_route")]
        //public string SkipButtonRoute { get; set; }

        //[JsonProperty("skip_button_color")]
        //public string SkipButtonColor { get; set; }

        //[JsonProperty("language_text")]
        //public string LanguageText { get; set; }

        //[JsonProperty("language_color")]
        //public string LanguageColor { get; set; }

        //[JsonProperty("registered_button_text")]
        //public string RegisteredButtonText { get; set; }

        //[JsonProperty("registered_button_text_color")]
        //public string RegisteredButtonTextColor { get; set; }

        //[JsonProperty("registered_button_route")]
        //public string RegisteredButtonRoute { get; set; }

        //[JsonProperty("dots_active_color")]
        //public string DotsActiveColor { get; set; }

        //[JsonProperty("dots_not_active_color")]
        //public string DotsNotActiveColor { get; set; }

        //[JsonProperty("animation_start_after_milliseconds")]
        //public long AnimationStartAfterMilliseconds { get; set; }

        //[JsonProperty("slider_slide_after_milliseconds")]
        //public long SliderSlideAfterMilliseconds { get; set; }

        //[JsonProperty("dots_active_color")]
        //public string DotsActiveColor { get; set; }

        //[JsonProperty("dots_not_active_color")]
        //public string DotsNotActiveColor { get; set; }
    }
}
