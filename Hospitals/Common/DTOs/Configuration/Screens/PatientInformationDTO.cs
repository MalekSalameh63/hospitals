﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class PatientInformationDTO
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("medical_file_number")]
        public string MedicalFileNumber { get; set; }

        [JsonProperty("complaint")]
        public string Complaint { get; set; }
        [JsonProperty("tags")]
        public string Tags { get; set; }

        [JsonProperty("age_value")]
        public int Age { get; set; }
    }
}
