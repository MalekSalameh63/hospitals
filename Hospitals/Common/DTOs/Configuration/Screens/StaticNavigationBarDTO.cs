﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class StaticNavigationBarDTO
    {
        [JsonProperty("home_screen")]
        public bool HomeScreen { get; set; }

        [JsonProperty("my_patients")]
        public bool MyPatients { get; set; }

        [JsonProperty("account_settings")]
        public bool AccountSettings { get; set; }

        [JsonProperty("notification")]
        public bool Notification { get; set; }
    }
}
