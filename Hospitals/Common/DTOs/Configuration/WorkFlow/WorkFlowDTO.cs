﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs
{
    public class WorkFlowDTO
    {
        public Guid Id { get; set; }
        [JsonProperty("user_type_id")]
        public int? UserTypeId { get; set; }
        [JsonProperty("stage_id")]
        public int StageId { get; set; }
        [JsonProperty("actions")]
        public List<ActionDTO> Actions { get; set; }
    }
}
