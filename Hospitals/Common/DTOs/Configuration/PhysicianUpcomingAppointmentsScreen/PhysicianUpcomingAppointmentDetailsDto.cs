﻿using Common.DTOs.Configuration.UpcomingAppointments;
using Newtonsoft.Json;

namespace Common.DTOs.Configuration.PhysicianUpcomingAppointmentsScreen
{
    public class PhysicianUpcomingAppointmentDetailsDto : UpcomingAppointmentDetailsDto
    {
        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }
    }
}
