﻿using Common.DTOs.Configuration.DoctorsList;
using Common.DTOs.Configuration.UpcomingAppointments;
using Newtonsoft.Json;
using System;

namespace Common.DTOs.Configuration
{
    public class PhysicianUpcomingAppointmentsScreenDto
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("call_to_action_button")]
        public CallToActionButtonDto CallToActionButton { get; set; }

        [JsonProperty("appointment_default_custom_theme")]
        public CustomThemeDTO AppointmentsDefaultCustomTheme { get; set; }
        [JsonProperty("today_appointments_custom_theme")]
        public CustomThemeDTO TodayAppointmentCustomTheme { get; set; }
        [JsonProperty("upcoming_appointments_configuration")]
        public UpcomingAppointmentsConfigurationDto AppointmentsConfiguration { get; set; }
    }
}
