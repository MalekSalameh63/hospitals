﻿using Common.DTOs.Configuration.DoctorsList;
using Common.DTOs.Configuration.PhysicianUpcomingAppointmentsScreen;
using Common.DTOs.Configuration.UpcomingAppointments;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs.Configuration
{
    public class PhysicianUpcomingAppointmentsScreenDtoWithData
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("call_to_action_button")]
        public CallToActionButtonDto CallToActionButton { get; set; }
        [JsonProperty("upcoming_appointments_configuration")]
        public UpcomingAppointmentsConfigurationDto AppointmentsConfiguration { get; set; }
        [JsonProperty("appointments_list")]
        public List<PhysicianUpcomingAppointmentDetailsDto> AppointmentsList { get; set; }
    }
}
