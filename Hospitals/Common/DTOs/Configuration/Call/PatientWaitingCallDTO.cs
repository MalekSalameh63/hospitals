﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Configuration.Call
{
    public class PatientWaitingCallDTO 
    {
        public Guid Id { get; set; }

        [JsonProperty("screen_config")]
        public ScreenConfigDTO ScreenConfig { get; set; }

        [JsonProperty("number_of_online_physician")]
        public int OnlinePhysicians { get; set; }

        [JsonProperty("patients_in_queue")]
        public int PatientsInQueueCount { get; set; }

        [JsonProperty("position_in_queue")]
        public int PositionInQueue { get; set; }

        [JsonProperty("promotion_tips")]
        public List<PromotionTipDTO> PromotionTips { get; set; }
    }
}
