﻿using Common.DTOs.Configuration.Screens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.Call
{
    public class PatientQueueDTO
    {
        public Guid Id { get; set; }

        [JsonProperty("screen_config")]
        public ScreenConfigDTO ScreenConfig { get; set; }

        [JsonProperty("profile_information")]
        public FieldDTO ProfileInformation { get; set; }

        [JsonProperty("doctor_status")]
        public PhysicianAvailabilityDTO PhysicianAvailability { get; set; }

        [JsonProperty("patient_queue_list")]
        public List<PatientInformationDTO> PatientsInformation { get; set; }
    }
    public class PatientQueueDtoWebsocket
    {
        [JsonProperty("patient_queue_list")]
        public List<PatientInformationDTO> PatientsInformation { get; set; }
    }
}
