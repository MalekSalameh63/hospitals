﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Configuration.Call
{
    public class PhysicianWaitingCallDTO
    {
        public Guid Id { get; set; }

        [JsonProperty("screen_config")]
        public ScreenConfigDTO ScreenConfig { get; set; }

        [JsonProperty("modal_config")]
        public ModalConfigDTO ModalConfig { get; set; }

        [JsonProperty("patient_information")]
        public PatientInformationDTO PatientInformation { get; set; }

    }
}
