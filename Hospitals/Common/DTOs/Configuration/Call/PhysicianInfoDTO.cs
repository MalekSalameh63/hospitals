﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.Configuration.Call
{
    public class PhysicianInfoDTO
    {
        public Guid Id { get; set; }

        [JsonProperty("screen_config")]
        public ScreenConfigDTO ScreenConfig { get; set; }

        [JsonProperty("modal_config")]
        public ModalConfigDTO ModalConfig { get; set; }

        [JsonProperty("exit_visit_modal")]
        public ModalConfigDTO ExitVisitModal { get; set; }

        [JsonProperty("physician_information")]
        public PhysicianInformationDTO PhysicianInformation { get; set; }
    }
}
