﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.DoctorsList
{
    public class DoctorDetailsDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("doctor_name")]
        public string DoctorName { get; set; }

        [JsonProperty("doctor_experience")]
        public string DoctorExperience { get; set; }

        [JsonProperty("doctor_bio")]
        public string DoctorBio { get; set; }
        [JsonProperty("icon")]
        public string Icon { get; set; }
        [JsonProperty("price")]
        public float Price { get; set; }
        [JsonProperty("rate")]
        public float Rate { get; set; }
        [JsonProperty("specialty")]
        public string Specialty { get; set; }
        [JsonProperty("clinic_name")]
        public string ClinicName { get; set; }
        [JsonProperty("nationality")]
        public string Nationality { get; set; }
        [JsonProperty("clinic_id")]
        public int ClinicId { get; set; }
        [JsonProperty("project_id")]
        public int ProjectId { get; set; }
        [JsonProperty("currency")]
        public string Currency { get; set; }
        [JsonProperty("years_label")]
        public string YearsLabel { get; set; }

        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }
    }
}
