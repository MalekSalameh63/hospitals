﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.DoctorsList
{
    public class CardConfigurationDto
    {

        [JsonProperty("call_to_action_button")]
        public CallToActionButtonDto CallToActionButton { get; set; }
        [JsonProperty("doctor_visit_fees_activated")]
        public bool DoctorVisitFeesActivated { get; set; }
    }
}
