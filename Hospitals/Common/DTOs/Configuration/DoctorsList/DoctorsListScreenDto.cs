﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.Configuration.DoctorsList
{
    public class DoctorsListScreenDto
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("card_cofigurations")]
        public CardConfigurationDto CardConfigurationDto { get; set; }

        [JsonProperty("doctors_details_configuration")]
        public CustomThemeDTO DoctorsDetailsDto { get; set; }
    }
}
