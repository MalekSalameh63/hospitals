﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.DoctorsList
{
    public class CallToActionButtonDto
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("title_color")]
        public string TitleColor { get; set; }
        [JsonProperty("title_font_size")]
        public double TitleFontSize { get; set; }
        [JsonProperty("background_color")]
        public string BackgroundColor { get; set; }

        [JsonProperty("activated")]
        public bool Activated { get; set; }

        [JsonProperty("route")]
        public string Route { get; set; }

    }
}
