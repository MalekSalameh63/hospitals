﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.DoctorsList
{
    public class DoctorsListScreenWithDataDto
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("card_cofigurations")]
        public CardConfigurationDto CardConfigurationDto { get; set; }
        [JsonProperty("doctors_details_configuration")]
        public CustomThemeDTO DoctorsDetailsConfigurationDto { get; set; }

        [JsonProperty("doctors_details")]
        public List<DoctorDetailsDto> DoctorsDetailsDto { get; set; }
        [JsonProperty("page_count")]
        public int PageCount { get; set; }
    }
}
