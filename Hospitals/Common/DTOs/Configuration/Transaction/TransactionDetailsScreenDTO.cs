﻿using Common.DTOs.Configuration.Payment;
using Newtonsoft.Json;
using System;

namespace Common.DTOs.Configuration.Transaction
{
    public class TransactionDetailsScreenDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("transaction_details")]
        public TransactionDetailsDTO TransactionDetails { get; set; }

        [JsonProperty("consultation_details")]
        public TransactionDetailsDTO ConsultationDetails { get; set; }
        [JsonProperty("transaction_summary")]
        public TransactionSummaryDTO TransactionSummary { get; set; }
    }
}
