﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.Transaction
{
    public class TransactionDetailsDTO
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("date", NullValueHandling = NullValueHandling.Ignore)]
        public long? Date { get; set; }

        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }
    }
}
