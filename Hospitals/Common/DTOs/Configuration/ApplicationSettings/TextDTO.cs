﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.ApplicationSettings
{
    public partial class TextDTO
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("title")]
        public string Value { get; set; }
    }
}
