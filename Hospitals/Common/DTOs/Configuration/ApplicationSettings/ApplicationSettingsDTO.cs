﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Configuration.ApplicationSettings
{
    public class ApplicationSettingsDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("texts")]
        public List<TextDTO> ApplicationTexts { get; set; }

        [JsonProperty("forms")]
        public List<FormDetailsDTO> FormDetails { get; set; }
    }
}
