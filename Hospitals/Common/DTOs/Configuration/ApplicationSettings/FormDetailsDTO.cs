﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.ApplicationSettings
{
    public class FormDetailsDTO
    {
        [JsonProperty("key")]
        public string Key { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
    }
}
