﻿using Newtonsoft.Json;

namespace Common.DTOs.Wallet
{
    public class DateRangeDTO
    {
        [JsonProperty("min_date")]
        public string MinDate { get; set; }

        [JsonProperty("max_date")]
        public string MaxDate { get; set; }

        [JsonProperty("min_date_text")]
        public string MinDateText { get; set; }

        [JsonProperty("max_date_text")]
        public string MaxDateText { get; set; }

        [JsonProperty("text_color")]
        public string TextColor { get; set; }

        [JsonProperty("date_color")]
        public string DateColor { get; set; }
    }
}
