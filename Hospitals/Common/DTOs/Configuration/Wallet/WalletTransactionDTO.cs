﻿using Newtonsoft.Json;

namespace Common.DTOs.Wallet
{
    public class WalletTransactionDTO
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("date")]
        public long? Date { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("transaction_type")]
        public int TransactionType { get; set; }
        [JsonProperty("currency")]
        public string currency { get; set; }
    }
}
