﻿using Newtonsoft.Json;

namespace Common.DTOs.Wallet
{
    public class WalletCardDTO
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("background")]
        public string Background { get; set; }

        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }
    }
}
