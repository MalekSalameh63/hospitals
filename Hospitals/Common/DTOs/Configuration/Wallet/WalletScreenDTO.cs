﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs.Wallet
{
    public class WalletScreenDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
        [JsonProperty("email_confirmed")]
        public bool EmailConfirmed { get; set; }

        [JsonProperty("my_wallet_card")]
        public WalletCardDTO WalletCard { get; set; }

        [JsonProperty("date_range")]
        public DateRangeDTO DateRange { get; set; }

        [JsonProperty("transaction_card_theme")]
        public TransactionCardThemeDTO TransactionCardTheme { get; set; }

        [JsonProperty("wallet_card_items")]
        public List<WalletCardItemDTO> WalletCardItems { get; set; }

        [JsonProperty("tabs_items")]
        public List<TabItemDTO> TabItems { get; set; }

        [JsonProperty("transactions_list")]
        public List<WalletTransactionDTO> Transactions { get; set; }

        [JsonProperty("charge_wallet_model")]
        public ChargeWalletModalDTO ChargeWalletModal { get; set; }
    }
}
