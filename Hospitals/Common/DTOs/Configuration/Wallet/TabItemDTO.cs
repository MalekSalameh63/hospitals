﻿using Newtonsoft.Json;

namespace Common.DTOs.Wallet
{
    public class TabItemDTO
    {
        [JsonProperty("order")]
        public int Order { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("title_color")]
        public string TitleColor { get; set; }
    }
}
