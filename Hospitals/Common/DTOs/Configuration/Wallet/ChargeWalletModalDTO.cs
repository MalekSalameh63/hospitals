﻿using Common.DTOs.Configuration.Payment;
using Newtonsoft.Json;

namespace Common.DTOs.Wallet
{
    public class ChargeWalletModalDTO
    {
        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("initial_value")]
        public double InitialValue { get; set; }

        [JsonProperty("max_value")]
        public double MaxValue { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("charge_button")]
        public PaymentButtonDTO ChargeButton { get; set; }
    }
}
