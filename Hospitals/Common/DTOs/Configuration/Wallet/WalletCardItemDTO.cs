﻿using Newtonsoft.Json;

namespace Common.DTOs.Wallet
{
    public class WalletCardItemDTO
    {
        [JsonProperty("order")]
        public int Order { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("route")]
        public string Route { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("title_color")]
        public string TitleColor { get; set; }

        [JsonProperty("icon_color")]
        public string IconColor { get; set; }
    }
}
