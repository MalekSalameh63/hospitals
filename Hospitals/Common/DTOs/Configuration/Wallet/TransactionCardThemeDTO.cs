﻿using Newtonsoft.Json;

namespace Common.DTOs.Wallet
{
    public class TransactionCardThemeDTO
    {
        [JsonProperty("background_color")]
        public string BackgroundColor { get; set; }

        [JsonProperty("icon_color")]
        public string IconColor { get; set; }

        [JsonProperty("title_color")]
        public string TitleColor { get; set; }

        [JsonProperty("title_font_size")]
        public double TitleFontSize { get; set; }

        [JsonProperty("description_color")]
        public string DescriptionColor { get; set; }

        [JsonProperty("description_font_size")]
        public double DescriptionFontSize { get; set; }

        [JsonProperty("date_color")]
        public string DateColor { get; set; }

        [JsonProperty("date_font_size")]
        public double DateFontSize { get; set; }

        [JsonProperty("time_color")]
        public string TimeColor { get; set; }

        [JsonProperty("time_font_size")]
        public double TimeFontSize { get; set; }

        [JsonProperty("amount_color")]
        public string AmountColor { get; set; }

        [JsonProperty("amount_font_size")]
        public double AmountFontSize{ get; set; }
    }
}
