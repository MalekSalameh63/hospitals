﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.UpcomingAppointments
{
    public class UpcomingAppointmentDetailsDto
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("doctor_name")]
        public string DoctorName { get; set; }
        [JsonProperty("date")]
        public string Date { get; set; }
        [JsonProperty("start_time")]
        public string StartTime { get; set; }
        [JsonProperty("end_time")]
        public string EndTime { get; set; }
        [JsonProperty("image")]
        public string Image { get; set; }
        [JsonProperty("age")]
        public string Age { get; set; }
        [JsonProperty("clinic")]
        public string Clinic { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("is_details_available")]
        public bool IsDetailsAvailable { get; set; }
        [JsonProperty("clinic_id")]
        public long ClinicId { get; set; }
        [JsonProperty("doctor_id")]
        public long DoctorId { get; set; }
        [JsonProperty("patient_mrn")]
        public long PatientMrn { get; set; }
        [JsonProperty("episode")]
        public long Episode { get; set; }
        [JsonProperty("patient_id")]
        public string PatientId { get; set; }
        [JsonProperty("id_signed")]
        public bool IsSigned { get; set; }
    }
}
