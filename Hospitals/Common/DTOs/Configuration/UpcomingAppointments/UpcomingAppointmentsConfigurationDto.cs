﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.UpcomingAppointments
{
    public class UpcomingAppointmentsConfigurationDto
    {
        [JsonProperty("call_to_action_button_icon")]
        public string CallToActionButtonIcon { get; set; }
        [JsonProperty("call_to_action_button_icon_color")]
        public string CallToActionButtonIconColor { get; set; }
        [JsonProperty("appointment_icon")]
        public string AppointmentIcon { get; set; }
        [JsonProperty("more_icon")]
        public string MoreIcon { get; set; }
        [JsonProperty("more_route")]
        public string MoreRoute { get; set; }
        [JsonProperty("more_title")]
        public string MoreTitle { get; set; }

    }
}
