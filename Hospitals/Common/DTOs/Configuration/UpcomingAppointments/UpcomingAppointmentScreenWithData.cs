﻿using Common.DTOs.Configuration.DoctorsList;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.UpcomingAppointments
{
    public class UpcomingAppointmentScreenWithData
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("call_to_action_button")]
        public CallToActionButtonDto CallToActionButton { get; set; }

        [JsonProperty("appointment_details_configuration")]
        public CustomThemeDTO CustomThemeDto { get; set; }
        [JsonProperty("upcoming_appointments_configuration")]
        public UpcomingAppointmentsConfigurationDto AppointmentsConfiguration { get; set; }
        [JsonProperty("appointments_list")]
        public List<UpcomingAppointmentDetailsDto> AppointmentsList { get; set; }
    }
}
