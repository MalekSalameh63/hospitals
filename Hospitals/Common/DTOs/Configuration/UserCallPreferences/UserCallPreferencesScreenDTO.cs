﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.UserCallPreferences
{
    public class UserCallPreferencesScreenDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("preferred_language_text")]
        public string PreferredLanguageText { get; set; }

        [JsonProperty("preferred_gender_text")]
        public string PreferredGenderText { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("preferred_languages")]
        public List<CheckBoxItem> PreferredLanguages { get; set; }

        [JsonProperty("preferred_preferredgender")]
        public List<CheckBoxItem> PreferredGenders { get; set; }
    }
}
