﻿using Common.DTOs.Configuration.Shared;
using Newtonsoft.Json;
using System;

namespace Common.DTOs.Configuration.CallAssessment
{
    public class PhysicianCallAssessmentScreenDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("subtitle")]
        public string Subtitle { get; set; }

        [JsonProperty("assessment_field")]
        public SOAPFieldDTO AssessmentField { get; set; }

        [JsonProperty("opinion_button")]
        public SubmitButtonDTO OpinionButton { get; set; }

        [JsonProperty("home_screen_button")]
        public SubmitButtonDTO HomeScreenButton { get; set; }
    }

}
