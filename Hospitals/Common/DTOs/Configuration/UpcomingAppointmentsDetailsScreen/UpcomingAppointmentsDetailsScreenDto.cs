﻿using Common.DTOs.File;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.UpcomingAppointmentsDetailsScreen
{
    public class UpcomingAppointmentsDetailsScreenDto
    {
        [JsonProperty("appointment_details_card")]
        public UpcomingAppointmentsDetailsCardDto UpcomingAppointmentsDetailsCard { get; set; }
        [JsonProperty("appointment_action_buttons")]
        public List<UpcomingAppointmentsDetailsActionButtonDto> UpcomingAppointmentsDetailsActionButtons { get; set; }
        [JsonProperty("appointment_details")]
        public UpcomingAppointmentsDetailsDto UpcomingAppointmentsDetails { get; set; }
        [JsonProperty("physician_call_button")]
        public UpcomingAppointmentsDetailsActionButtonDto PhysicianCallButton { get; set; }
        [JsonProperty("files")]
        public FileUploadDTO Files { get; set; }
        [JsonProperty("action_buttons_custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }

    }
}
