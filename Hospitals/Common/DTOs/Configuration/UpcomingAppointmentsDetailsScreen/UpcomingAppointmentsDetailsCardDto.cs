﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.UpcomingAppointmentsDetailsScreen
{
    public class UpcomingAppointmentsDetailsCardDto
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("doctor_name")]
        public string DoctorName { get; set; }
        [JsonProperty("doctor_specialty")]
        public string DoctorSpecialty { get; set; }
        [JsonProperty("doctor_language")]
        public string DoctorLanguage { get; set; }
        [JsonProperty("doctor_image")]
        public string DoctorImage { get; set; }
        [JsonProperty("appointment_date")]
        public string AppointmentDate { get; set; }
        [JsonProperty("appointment_time")]
        public string AppointmentTime { get; set; }
        [JsonProperty("rate")]
        public double Rate { get; set; }
        [JsonProperty("appointment_icon")]
        public string AppointmentIcon { get; set; }
        [JsonProperty("age_icon")]
        public string AgeIcon { get; set; }
        [JsonProperty("gender_icon")]
        public string GenderIcon { get; set; }
        [JsonProperty("number_of_votes")]
        public int NumberOfVotes { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("clinic_id")]
        public long ClinicId { get; set; }
        [JsonProperty("doctor_id")]
        public long DoctorId { get; set; }
        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }

    }
}
