﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.UpcomingAppointmentsDetailsScreen
{
    public class UpcomingAppointmentsDetailsDto
    {
        [JsonProperty("patient_id")]
        public string PatientId { get; set; }

        [JsonProperty("patient_mrn")]
        public string PatientMrn { get; set; }
        [JsonProperty("patient_name_title")]
        public string PatientNameTitle { get; set; }
        [JsonProperty("patient_name")]
        public string PatientName { get; set; }
        public string PatientAgeTitle { get; set; }
        [JsonProperty("patient_age")]
        public long PatientAge { get; set; }
        [JsonProperty("patient_gender_title")]
        public string PatientGenderTitle { get; set; }
        [JsonProperty("patient_gender")]
        public string PatientGender { get; set; }
        [JsonProperty("chief_complaint_title")]
        public string ChiefComplaintTitle { get; set; }
        [JsonProperty("chief_complaint_text")]
        public string ChiefComplaintText { get; set; }
        [JsonProperty("chief_complaint_tags")]
        public List<string> ChiefComplaintTags { get; set; }
        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }

    }
}
