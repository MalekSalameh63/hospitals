﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration
{
    public class UpcomingAppointmentsDetailsActionButtonDto
    {
        [JsonProperty("icon")]
        public string Icon { get; set; }
        [JsonProperty("route")]
        public string Route { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("icon_color")]
        public string IconColor { get; set; }

    }
}
