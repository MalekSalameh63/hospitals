﻿namespace Common.DTOs.Configuration.SOAP
{
    public class DiagnosisItem
    {
        public string IcdCode10Id { get; set; }
        public long ConditionId { get; set; }
        public long DiagnosisTypeId { get; set; }
        public string Description { get; set; }
        public string Remarks { get; set; }
    }
}
