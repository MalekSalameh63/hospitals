﻿namespace Common.DTOs
{
    public class AssessmentSectionDTO
    {
        public DiagnosisScreenDTO DiagnosisScreen { get; set; }
        public ProgressNoteScreenDTO ProgressNoteScreen { get; set; }
    }
}
