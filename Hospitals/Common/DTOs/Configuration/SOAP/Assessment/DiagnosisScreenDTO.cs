﻿using Common.DTOs.Configuration.Shared;
using Common.DTOs.Configuration.SOAP;
using Common.DTOs.EMR.Lookup;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs
{
    public class DiagnosisScreenDTO
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("is_update")]
        public bool IsUpdate { get; set; }

        [JsonProperty("diagnosis_field")]
        public AdvancedFieldDTO Diagnosis { get; set; }

        [JsonProperty("diagnosis_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> MasterDiagnosis { get; set; }

        [JsonProperty("patient_card", NullValueHandling = NullValueHandling.Ignore)]
        public PatientCardDTO PatientCard { get; set; }

        [JsonProperty("patient_card_details", NullValueHandling = NullValueHandling.Ignore)]
        public PatientCardDetailsDTO PatientCardDetails { get; set; }

        [JsonProperty("diagnosis_type_field")]
        public SOAPFieldDTO DiagnosisType { get; set; }

        [JsonProperty("diagnosis_type_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> MasterDiagnosisType { get; set; }

        [JsonProperty("condition_field")]
        public SOAPFieldDTO Condition { get; set; }

        [JsonProperty("condition_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> MasterCondition { get; set; }

        [JsonProperty("submit_button")]
        public SubmitButtonDTO SubmitButton { get; set; }
    }

}
