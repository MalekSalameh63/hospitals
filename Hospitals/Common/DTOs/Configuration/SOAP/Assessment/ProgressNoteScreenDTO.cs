﻿using Common.Common.Appointments;
using Common.DTOs.Configuration.Shared;
using Common.DTOs.Configuration.SOAP;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs
{
    public class ProgressNoteScreenDTO
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("is_update")]
        public bool IsUpdate { get; set; }

        [JsonProperty("progress_field")]
        public SOAPFieldDTO ProgressNote { get; set; }

        [JsonProperty("patient_card", NullValueHandling = NullValueHandling.Ignore)]
        public PatientCardDTO PatientCard { get; set; }

        [JsonProperty("patient_card_details", NullValueHandling = NullValueHandling.Ignore)]
        public PatientCardDetailsDTO PatientCardDetails { get; set; }

        [JsonProperty("submit_button")]
        public SubmitButtonDTO SubmitButton { get; set; }
    }

}
