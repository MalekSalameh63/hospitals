﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class SOAPDialogDTO
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("button_ok")]
        public string ButtonOk { get; set; }

        [JsonProperty("duration", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int? Duration { get; set; }

        [JsonProperty("button_cancel", NullValueHandling = NullValueHandling.Ignore)]
        public string ButtonCancel { get; set; }
    }
}

