﻿using Common.DTOs.Configuration.Shared;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class SOAPLayoutDTO
    {
        [JsonProperty("skip_dialog")]
        public SOAPDialogDTO SkipDialog { get; set; }

        [JsonProperty("close_episode_dialog")]
        public SOAPDialogDTO CloseEpisodeDialog { get; set; }

        [JsonProperty("error_episode_dialog")]
        public SOAPDialogDTO ErrorDialog { get; set; }

        [JsonProperty("sign_episode_button")]
        public SubmitButtonDTO SignEpisodeButton { get; set; }
    }
}

