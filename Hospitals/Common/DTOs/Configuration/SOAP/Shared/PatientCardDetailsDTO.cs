﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class PatientCardDetailsDTO
    {
        [JsonProperty("mrn_text")]
        public string MRNText { get; set; }

        [JsonProperty("year_text")]
        public string YearText { get; set; }
    }

}
