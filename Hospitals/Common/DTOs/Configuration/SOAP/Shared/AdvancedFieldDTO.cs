﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Configuration.SOAP
{
    public class AdvancedFieldDTO : SOAPFieldDTO
    {
        [JsonProperty("remark_text")]
        public string RemarksTitle { get; set; }

        [JsonProperty("type")]
        public int Type { get; set; }
    }
}
