﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class SOAPFieldDTO
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("hint")]
        public string Hint { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("value")]
        public dynamic Value { get; set; }
    }

}
