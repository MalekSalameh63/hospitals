﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class PatientCardDTO
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("medical_number")]
        public string MedicalNumber { get; set; }

        [JsonProperty("age")]
        public long Age { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }
    }

}
