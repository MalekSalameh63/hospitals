﻿using Common.DTOs;
using Newtonsoft.Json;
using System;

namespace Common.DTOs
{
    public class SOAPDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("chief_complaint_screen")]
        public ChiefComplaintScreenDTO ChiefComplaintScreen { get; set; }

        [JsonProperty("history_screen")]
        public HistoryScreenDTO HistoryScreen { get; set; }

        [JsonProperty("allergies_screen")]
        public AllergiesScreenDTO AllergiesScreen { get; set; }

        [JsonProperty("diagnosis_screen")]
        public DiagnosisScreenDTO DiagnosisScreen { get; set; }

        [JsonProperty("progress_note_screen")]
        public ProgressNoteScreenDTO ProgressNoteScreen { get; set; }

        [JsonProperty("prescription_screen")]
        public PrescriptionScreenDTO PrescriptionScreen { get; set; }

        [JsonProperty("radiology_screen")]
        public ProceduresScreenDTO RadiologyScreen { get; set; }

        [JsonProperty("laboratory_screen")]
        public ProceduresScreenDTO LaboratoryScreen { get; set; }

        [JsonProperty("clinical_screen")]
        public ProceduresScreenDTO ClinicalScreen { get; set; }

        [JsonProperty("layout")]
        public SOAPLayoutDTO Layout { get; set; }
    }
}
