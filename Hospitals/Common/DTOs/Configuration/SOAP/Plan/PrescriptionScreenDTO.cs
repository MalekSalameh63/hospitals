﻿using Common.Common.Appointments;
using Common.DTOs.Configuration.Shared;
using Common.DTOs.Configuration.SOAP;
using Common.DTOs.EMR.Lookup;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs
{
    public class PrescriptionScreenDTO
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("is_update")]
        public bool IsUpdate { get; set; }

        [JsonProperty("medicine_field")]
        public AdvancedFieldDTO Medicine { get; set; }

        [JsonProperty("medicine_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> MedicineList { get; set; }

        [JsonProperty("duration_field")]
        public SOAPFieldDTO Duration { get; set; }

        [JsonProperty("duration_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> DurationList { get; set; }

        [JsonProperty("dose_field")]
        public SOAPFieldDTO Dose { get; set; }

        //[JsonProperty("dose_list", NullValueHandling = NullValueHandling.Ignore)]
        //public List<EMRLookupDTO> DoseList { get; set; }

        [JsonProperty("dose_unit_field")]
        public SOAPFieldDTO DoseUnit { get; set; }

        [JsonProperty("dose_unit_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> DoseUnitList { get; set; }

        [JsonProperty("dose_time_field")]
        public SOAPFieldDTO DoseTime { get; set; }

        [JsonProperty("dose_time_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> DoseTimeList { get; set; }

        [JsonProperty("route_field")]
        public SOAPFieldDTO Route { get; set; }

        [JsonProperty("route_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> RouteList { get; set; }

        [JsonProperty("frequency_field")]
        public AdvancedFieldDTO Frequency { get; set; }

        [JsonProperty("frequency_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> FrequencyList { get; set; }

        [JsonProperty("patient_card", NullValueHandling = NullValueHandling.Ignore)]
        public PatientCardDTO PatientCard { get; set; }

        [JsonProperty("patient_card_details", NullValueHandling = NullValueHandling.Ignore)]
        public PatientCardDetailsDTO PatientCardDetails { get; set; }

        [JsonProperty("submit_button")]
        public SubmitButtonDTO SubmitButton { get; set; }
    }

}
