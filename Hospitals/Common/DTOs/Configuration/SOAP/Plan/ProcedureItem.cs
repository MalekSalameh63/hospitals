﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class ProcedureItem
    {
        [JsonProperty("orderNo")]
        public int OrderNo { get; set; }

        [JsonProperty("orderDate")]
        public DateTimeOffset orderDate { get; set; }

        [JsonProperty("lineItemNo")]
        public int LineItemNo { get; set; }

        [JsonProperty("procedureId")]
        public string ProcedureID { get; set; }

        [JsonProperty("procedureName")]
        public string ProcedureName { get; set; }

        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("orderType")]
        public int OrderType { get; set; }

        [JsonProperty("categoryID")]
        public string CategoryID { get; set; }

        [JsonProperty("template")]
        public string Template { get; set; }
    }
}
