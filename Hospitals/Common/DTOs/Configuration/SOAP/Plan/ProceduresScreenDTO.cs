﻿using Common.DTOs.Configuration.Shared;
using Common.DTOs.EMR.Lookup;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class ProceduresScreenDTO
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("template")]
        public string Template { get; set; }
        
        [JsonProperty("categoryId")]
        public string CategoryId { get; set; }

        [JsonProperty("is_update")]
        public bool IsUpdate { get; set; }

        [JsonProperty("field")]
        public SOAPFieldDTO ItemField { get; set; }

        [JsonProperty("list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> ItemList { get; set; }

        [JsonProperty("patient_card", NullValueHandling = NullValueHandling.Ignore)]
        public PatientCardDTO PatientCard { get; set; }

        [JsonProperty("patient_card_details", NullValueHandling = NullValueHandling.Ignore)]
        public PatientCardDetailsDTO PatientCardDetails { get; set; }

        [JsonProperty("submit_button")]
        public SubmitButtonDTO SubmitButton { get; set; }

    }
}
