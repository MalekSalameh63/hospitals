﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Configuration.SOAP.Subjective
{
    public class HistoryItem
    {
        public long Id { get; set; }
        public long Type { get; set; }
        public string HistoryName { get; set; }
        public string Remarks { get; set; }
    }
}
