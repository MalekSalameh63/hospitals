﻿namespace Common.DTOs.Configuration.SOAP
{
    public class AllergyItem
    {
        public long Id { get; set; }
        public long Type { get; set; }
        public long SeverityId { get; set; }
        public string SeverityName { get; set; }
        public string Title { get; set; }
        public string Remarks { get; set; }
    }
}
