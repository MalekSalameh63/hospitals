﻿using Common.Common.Appointments;
using Common.DTOs.Configuration.Shared;
using Common.DTOs.Configuration.SOAP;
using Common.DTOs.EMR.Lookup;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs
{
    public class HistoryScreenDTO
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("is_update")]
        public bool IsUpdate { get; set; }

        [JsonProperty("family_history_field")]
        public AdvancedFieldDTO FamilyHistory { get; set; }

        [JsonProperty("family_history_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> MasterFamilyHistories { get; set; }

        [JsonProperty("medical_history_field")]
        public AdvancedFieldDTO MedicalHistory { get; set; }

        [JsonProperty("medical_history_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> MasterMedicalHistories { get; set; }

        [JsonProperty("social_history_field")]
        public AdvancedFieldDTO SocialHistory { get; set; }

        [JsonProperty("social_history_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> MasterSocialHistories { get; set; }

        [JsonProperty("sports_history_field")]
        public AdvancedFieldDTO SportsHistory { get; set; }

        [JsonProperty("sports_history_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> MasterSportsHistories { get; set; }

        [JsonProperty("infection_history_field")]
        public AdvancedFieldDTO InfectionHistory { get; set; }

        [JsonProperty("infection_history_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> MasterInfectionHistories { get; set; }

        [JsonProperty("surgical_history_field")]
        public AdvancedFieldDTO SurgicalHistory { get; set; }

        [JsonProperty("surgical_history_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> MasterSurgicalHistories { get; set; }

        [JsonProperty("patient_card", NullValueHandling = NullValueHandling.Ignore)]
        public PatientCardDTO PatientCard { get; set; }

        [JsonProperty("patient_card_details", NullValueHandling = NullValueHandling.Ignore)]
        public PatientCardDetailsDTO PatientCardDetails { get; set; }

        [JsonProperty("submit_button")]
        public SubmitButtonDTO SubmitButton { get; set; }
    }

}
