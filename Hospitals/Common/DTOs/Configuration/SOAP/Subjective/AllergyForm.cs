﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.DTOs.EMR
{
    public class AllergyForm
    {
        [JsonProperty("patientMRN")]
        public long PatientMRN { get; set; }

        [JsonProperty("allergyDiseaseType")]
        public long AllergyDiseaseType { get; set; }

        [JsonProperty("allergyDiseaseId")]
        public long AllergyDiseaseId { get; set; }

        [JsonProperty("allergyDiseaseName")]
        public string AllergyDiseaseName { get; set; }

        [JsonProperty("episodeId")]
        public long EpisodeId { get; set; }

        [JsonProperty("appointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("severity")]
        public long Severity { get; set; }

        [JsonProperty("isChecked")]
        public bool IsChecked { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("severityName")]
        public string SeverityName { get; set; }

        [JsonProperty("createdByName")]
        public string CreatedByName { get; set; }

        [JsonProperty("editedByName")]
        public string EditedByName { get; set; }
    }
}
