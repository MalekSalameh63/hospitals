﻿using Common.DTOs.Configuration.Shared;
using Newtonsoft.Json;

namespace Common.DTOs
{
    public class ChiefComplaintScreenDTO
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("is_update")]
        public bool IsUpdate { get; set; }

        [JsonProperty("chief_field")]
        public SOAPFieldDTO ChiefField { get; set; }

        [JsonProperty("history_field")]
        public SOAPFieldDTO HistoryField { get; set; }

        [JsonProperty("patient_card", NullValueHandling = NullValueHandling.Ignore)]
        public PatientCardDTO PatientCard { get; set; }

        [JsonProperty("patient_card_details", NullValueHandling = NullValueHandling.Ignore)]
        public PatientCardDetailsDTO PatientCardDetails { get; set; }

        [JsonProperty("submit_button")]
        public SubmitButtonDTO SubmitButton { get; set; }
    }

}
