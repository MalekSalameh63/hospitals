﻿using Common.Common.Appointments;
using Common.DTOs.Configuration.Shared;
using Common.DTOs.Configuration.SOAP;
using Common.DTOs.EMR.Lookup;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs
{
    public class AllergiesScreenDTO
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("is_update")]
        public bool IsUpdate { get; set; }

        [JsonProperty("allergy_disease_type_field")]
        public SOAPFieldDTO AllergyDiseaseType { get; set; }

        [JsonProperty("allergy_disease_type_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> MasterAllergyDiseaseTypes { get; set; }

        [JsonProperty("allergy_disease_field")]
        public AdvancedFieldDTO AllergyDisease { get; set; }

        [JsonProperty("allergy_severity_field")]
        public SOAPFieldDTO AllergySeverity { get; set; }

        [JsonProperty("severity_list", NullValueHandling = NullValueHandling.Ignore)]
        public List<EMRLookupDTO> MasterAllergySeverities { get; set; }

        [JsonProperty("patient_card", NullValueHandling = NullValueHandling.Ignore)]
        public PatientCardDTO PatientCard { get; set; }

        [JsonProperty("patient_card_details", NullValueHandling = NullValueHandling.Ignore)]
        public PatientCardDetailsDTO PatientCardDetails { get; set; }

        [JsonProperty("submit_button")]
        public SubmitButtonDTO SubmitButton { get; set; }
    }

}
