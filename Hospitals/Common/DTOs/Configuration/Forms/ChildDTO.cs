﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class ChildDTO
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("icon", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Icon { get; set; }

        [JsonProperty("value")]
        public dynamic Value { get; set; }
    }
}
