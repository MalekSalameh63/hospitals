﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class ActionDTO
    {
        [JsonProperty("mode")]
        public string Mode { get; set; }
        [JsonProperty("value")]
        public dynamic Value { get; set; }
    }
}
