﻿using Common.VOs;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs
{
    public class FieldDTO
    {
        [JsonProperty("config_icon")]
        public string ConfigIcon { get; set; }

        [JsonProperty("config_label")] 
        public string ConfigLabel { get; set; }

        [JsonProperty("toggle")]
        public bool Toggle { get; set; }

        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("conditions")]
        public List<ConditionDTO> Conditions { get; set; }

        [JsonProperty("alignment")]
        public AlignmentDTO Alignment { get; set; }

        [JsonProperty("modal_fields")]
        public List<FieldDTO> ModalFields { get; set; }

        [JsonProperty("actions")]
        public List<ActionDTO> Actions { get; set; }

        [JsonProperty("activated")]
        public bool Activated { get; set; }

        [JsonProperty("read_only")]
        public bool ReadOnly { get; set; }

        [JsonProperty("flex")]
        public int? Flex { get; set; }

        [JsonProperty("order")]
        public int Order { get; set; }

        [JsonProperty("value")]
        public dynamic Value { get; set; }

        [JsonProperty("mode")]
        public dynamic Mode { get; set; }

        [JsonProperty("input_type")]
        public string InputType { get; set; }

        [JsonProperty("sub_fields")]
        public List<FieldDTO> SubFields { get; set; }

        [JsonProperty("count")]
        public int? Count { get; set; }

        [JsonProperty("selected_index")]
        public int? SelectedIndex { get; set; }

        [JsonProperty("item_mode")]
        public string ItemMode { get; set; }

        [JsonProperty("arrow_color")]
        public string ArrowColor { get; set; }

        [JsonProperty("vertical")]
        public bool Vertical { get; set; }
        
        [JsonProperty("children")]
        public List<ChildDTO> Children { get; set; }

        [JsonProperty("length")]
        public int? Length { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("additional_info")]
        public string AdditionalInfo { get; set; }

        [JsonProperty("approved_user")]
        public bool ApprovedUser { get; set; }

        [JsonProperty("profile_completion")]
        public double ProfileProgress { get; set; }

        [JsonProperty("is_horizontal")]
        public bool IsHorizontal { get; set; }

        [JsonProperty("hint")]
        public string Hint { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; }

        [JsonProperty("off_color")]
        public string OffColor { get; set; }

        [JsonProperty("text_off_color")]
        public string TextOffColor { get; set; }

        [JsonProperty("on_color")]
        public string OnColor { get; set; }
        [JsonProperty("text_on_color")]
        public string TextOnColor { get; set; }


    }
}
