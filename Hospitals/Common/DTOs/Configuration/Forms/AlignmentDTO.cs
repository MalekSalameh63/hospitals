﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class AlignmentDTO
    {
        [JsonProperty("x")]
        public double X { get; set; }

        [JsonProperty("y")]
        public double Y { get; set; }
    }
}
