﻿using Common.VOs;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs
{
    public class ResponseDTO
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("read_only")]
        public bool ReadOnly { get; set; }

        [JsonProperty("order")]
        public int Order { get; set; }

        [JsonProperty("auto_save")]
        public bool AutoSave { get; set; }

        [JsonProperty("bottom_alignment")]
        public bool BottomAlignment { get; set; }

        [JsonProperty("toggle")]
        public bool Toggle { get; set; }

        [JsonProperty("fields")]
        public List<FieldDTO> Fields { get; set; }
    }
}
