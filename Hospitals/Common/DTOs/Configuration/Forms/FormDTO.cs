﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.DTOs
{
    public class FormDTO
    {
        public Guid Id { get; set; }
        [JsonProperty("submitting_api")]
        public string SubmitUrl { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("response_list")]
        public List<ResponseDTO> Responses { get; set; }
    }
}
