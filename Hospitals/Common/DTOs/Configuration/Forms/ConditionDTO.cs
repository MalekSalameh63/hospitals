﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class ConditionDTO
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        public dynamic Value { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }
    }
}
