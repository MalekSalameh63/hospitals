﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.Payment
{
    public class ConsultationSummaryDTO
    {
        [JsonProperty("consultation_type")]
        public int ConsultationType { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("date_time")]
        public long DateTime { get; set; }

        [JsonProperty("patients_in_queue")]
        public long PatientsInQueue { get; set; }

        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }
    }
}
