﻿using Common.DTOs.Wallet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.Payment
{
    public class PaymentSummaryScreenDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
        [JsonProperty("email_confirmed")]
        public bool EmailConfirmed { get; set; }

        [JsonProperty("wallet_details")]
        public SummaryFieldDTO WalletDetails { get; set; }

        [JsonProperty("consultation_summary")]
        public ConsultationSummaryDTO ConsultationSummary { get; set; }

        [JsonProperty("transaction_summary")]
        public TransactionSummaryDTO TransactionSummary { get; set; }

        [JsonProperty("payment_options")]
        public List<PaymentOptionDTO> PaymentOptions { get; set; }

        [JsonProperty("privacy_policy")]
        public SummaryFieldDTO PrivacyPolicy { get; set; }

        [JsonProperty("charge_button")]
        public PaymentButtonDTO ChargeButton { get; set; }

        [JsonProperty("summary_button")]
        public PaymentButtonDTO SummaryButton { get; set; }

        [JsonProperty("charge_wallet_model")]
        public ChargeWalletModalDTO ChargeWalletModal { get; set; }
    }
}
