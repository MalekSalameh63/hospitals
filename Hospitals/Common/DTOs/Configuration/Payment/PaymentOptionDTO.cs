﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.Payment
{
    public class PaymentOptionDTO
    {
        [JsonProperty("type")]
        public int Type { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("min_charge_value")]
        public double MinChargeValue { get; set; }

        [JsonProperty("activated")]
        public bool Activated { get; set; }

        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }
    }
}
