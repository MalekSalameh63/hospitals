﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.Payment
{
    public class TransactionSummaryDTO
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("balance_text")]
        public string BalanceText { get; set; }

        [JsonProperty("balance_value")]
        public double Balance { get; set; }

        [JsonProperty("visit_fees_text")]
        public string VisitFeesText { get; set; }

        [JsonProperty("visit_fees_value")]
        public double VisitFees { get; set; }

        [JsonProperty("vat_fees_text")]
        public string VatFeesText { get; set; }

        [JsonProperty("vat_fees_value")]
        public double VatFees { get; set; }

        [JsonProperty("total_text")]
        public string TotalText { get; set; }

        [JsonProperty("total_value")]
        public double Total { get; set; }

        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }
        [JsonProperty("discount_value")]
        public double Discount { get; set; }
        [JsonProperty("discount_percentage")]
        public string DiscountPercentage { get; set; }
    }
}
