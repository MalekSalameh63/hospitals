﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Payment
{
    public class PaymentMethodScreenDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("bottom_image")]
        public string BottomImage { get; set; }

        [JsonProperty("credit_cards_text")]
        public string CreditCardsText { get; set; }

        [JsonProperty("credit_cards_icon1")]
        public string CreditCardsIcon1 { get; set; }

        [JsonProperty("credit_cards_icon2")]
        public string CreditCardsIcon2 { get; set; }

        [JsonProperty("mada_text")]
        public string MadaText { get; set; }

        [JsonProperty("mada_icon")]
        public string MadaIcon { get; set; }

        [JsonProperty("apple_text")]
        public string AppleText { get; set; }

        [JsonProperty("apple_icon")]
        public string AppleIcon { get; set; }

        [JsonProperty("button_title")]
        public string ButtonTitle { get; set; }
    }
}
