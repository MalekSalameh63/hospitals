﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Configuration.Notification
{
    public class NotificationPageDTO
    {
        public Guid Id { get; set; }

        [JsonProperty("dismissible")]
        public DismissibleDTO Dismissible { get; set; }

        [JsonProperty("notification_theme")]
        public NotificationThemeDTO NotificationTheme { get; set; }

        [JsonProperty("notifications")]
        public List<NotificationDTO> Notifications { get; set; }
    }

}
