﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.Notification
{
    public class NotificationDTO
    {
        [JsonProperty("notification_id")]
        public string Id { get; set; }

        [JsonProperty("card_color")]
        public string CardColor { get; set; }

        [JsonProperty("order")]
        public int Order { get; set; }

        [JsonProperty("category")]
        public string Category { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("can_delete")]
        public bool CanDelete { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }

        [JsonProperty("pagename")]
        public string PageName { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        public string IsNotificationRead { get; set; }
        
        [JsonProperty("is_read")]
        public bool IsRead { get; set; }
    }
}
