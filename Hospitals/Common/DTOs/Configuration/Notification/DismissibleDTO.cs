﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.Notification
{
    public class DismissibleDTO
    {
        [JsonProperty("direction")]
        public string Direction { get; set; }

        [JsonProperty("right_image")]
        public string RightImage { get; set; }

        [JsonProperty("right_text")]
        public string RightText { get; set; }

        [JsonProperty("right_text_color")]
        public string RightTextColor { get; set; }

        [JsonProperty("right_start_bg_color")]
        public string RightStartBgColor { get; set; }

        [JsonProperty("right_end_bg_color")]
        public string RightEndBgColor { get; set; }

        [JsonProperty("left_image")]
        public string LeftImage { get; set; }

        [JsonProperty("left_text")]
        public string LeftText { get; set; }

        [JsonProperty("left_text_color")]
        public string LeftTextColor { get; set; }

        [JsonProperty("left_start_bg_color")]
        public string LeftStartBgColor { get; set; }

        [JsonProperty("left_end_bg_color")]
        public string LeftEndBgColor { get; set; }
    }

}
