﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs.Configuration.Notification
{
    public class NotificationThemeDTO
    {
        [JsonProperty("time_Color")]
        public string TimeColor { get; set; }

        [JsonProperty("time_font_size")]
        public int TimeFontSize { get; set; }

        [JsonProperty("title_color")]
        public string TitleColor { get; set; }

        [JsonProperty("title_font_size")]
        public int TitleFontSize { get; set; }

        [JsonProperty("subtitle_font_size")]
        public int SubtitleFontSize { get; set; }

        [JsonProperty("subtitle_color")]
        public string SubtitleColor { get; set; }

        [JsonProperty("status_color")]
        public string StatusColor { get; set; }

        [JsonProperty("empty_text")]
        public string EmptyText { get; set; }
        [JsonProperty("notification_icons")]
        public List<NotificationIconDTO> NotificationIcons { get; set; }
    }

}
