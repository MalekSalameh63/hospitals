﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Configuration.Notification
{
    public class NotificationIconDTO
    {
        [JsonProperty("notification_type")]
        public int NotificationType { get; set; }
        [JsonProperty("icon")]
        public string Icon { get; set; }
    }
}
