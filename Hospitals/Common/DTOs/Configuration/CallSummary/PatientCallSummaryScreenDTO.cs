﻿using Common.DTOs.Configuration.Shared;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Configuration.CallAssessment
{
    public class PatientCallSummaryScreenDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("patient_summary_titles")]
        public PatientSummaryTitlesDTO PatientSummaryTitles { get; set; }

        [JsonProperty("submit_button")]
        public SubmitButtonDTO SubmitButton { get; set; }

        [JsonProperty("patient_summary")]
        public PatientCallSummaryDTO PatientSummary { get; set; }
    }
    public class PatientCallSummaryDTO
    {
        [JsonProperty("doctor_name")]
        public string DoctorName { get; set; }

        [JsonProperty("call_date")]
        public long CallDate { get; set; }

        [JsonProperty("call_time")]
        public double CallTime { get; set; }

        [JsonProperty("call_duration")]
        public double CallDuration { get; set; }

        [JsonProperty("consultation_fee")]
        public double ConsultationFee { get; set; }

        [JsonProperty("tax")]
        public double Tax { get; set; }

        [JsonProperty("discount_code")]
        public double DiscountCode { get; set; }

        [JsonProperty("health_insurance")]
        public double HealthInsurance { get; set; }

        [JsonProperty("total")]
        public double Total { get; set; }

        [JsonProperty("current_balance")]
        public double CurrentBalance { get; set; }
    }

    public class PatientSummaryTitlesDTO
    {
        [JsonProperty("post_call_massage")]
        public string PostCallMassage { get; set; }

        [JsonProperty("description_text")]
        public string DescriptionText { get; set; }

        [JsonProperty("call_type")]
        public string CallType { get; set; }

        [JsonProperty("consultation_fee_title")]
        public string ConsultationFeeTitle { get; set; }

        [JsonProperty("tax_title")]
        public string TaxTitle { get; set; }

        [JsonProperty("discount_code_title")]
        public string DiscountCodeTitle { get; set; }

        [JsonProperty("health_insurance_title")]
        public string HealthInsuranceTitle { get; set; }

        [JsonProperty("total_title")]
        public string TotalTitle { get; set; }

        [JsonProperty("current_balance_title")]
        public string CurrentBalanceTitle { get; set; }

        [JsonProperty("call_duration_title")]
        public string CallDurationTitle { get; set; }

        [JsonProperty("time_unit_title")]
        public string TimeUnitTitle { get; set; }

        [JsonProperty("currency_title")]
        public string CurrencyTitle { get; set; }

        [JsonProperty("payment_info_title")]
        public string PaymentInfoTitle { get; set; }
    }

}
