﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class ItemDTO
    {
        [JsonProperty("config_icon")]
        public string ConfigIcon { get; set; }

        [JsonProperty("config_label")]
        public string ConfigLabel { get; set; }

        [JsonProperty("disabled")]
        public bool Disabled { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("route")]
        public string Route { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }
    }
}
