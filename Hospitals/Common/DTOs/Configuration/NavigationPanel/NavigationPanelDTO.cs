﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class NavigationPanelDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
        [JsonProperty("user_type")]
        public int UserType { get; set; }

        [JsonProperty("selected_index")]
        public int SelectedIndex { get; set; }

        [JsonProperty("navigation_bar_theme")]
        public NavigationBarThemeDTO NavigationBarTheme { get; set; }

        [JsonProperty("navigation_items")]
        public List<ItemDTO> NavigationItems { get; set; }

        [JsonProperty("navigation_bar_items")]
        public List<ItemDTO> NavigationBarItems { get; set; }

        [JsonProperty("side_panel_items")]
        public List<ItemDTO> SidePanelItems { get; set; }
    }
}
