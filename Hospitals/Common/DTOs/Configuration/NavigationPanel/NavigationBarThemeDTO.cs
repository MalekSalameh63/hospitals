﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class NavigationBarThemeDTO
    {
        [JsonProperty("selected_item_color")]
        public string SelectedItemColor { get; set; }

        [JsonProperty("unselected_item_color")]
        public string UnselectedItemColor { get; set; }

        [JsonProperty("selected_bg_color")]
        public string SelectedBackgroundColor { get; set; }

        [JsonProperty("unselected_bg_color")]
        public string UnselectedBackgroundColor { get; set; }

        [JsonProperty("item_font_size")]
        public double ItemFontSize { get; set; }
    }
}
