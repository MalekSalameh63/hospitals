﻿using Common.DTOs.Configuration.AppointmentHistory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs.Configuration
{
    public class PhysicianHistoryAppointmentsScreenWithData
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
        [JsonProperty("route")]
        public string Route { get; set; }
        [JsonProperty("appointment_history_configuration")]
        public AppointmentHistoryConfigurationDto AppointmentsConfiguration { get; set; }
        [JsonProperty("appointments_list")]
        public List<PhysicianHistoryAppointmentsDetailsDto> AppointmentsList { get; set; }
    }
}
