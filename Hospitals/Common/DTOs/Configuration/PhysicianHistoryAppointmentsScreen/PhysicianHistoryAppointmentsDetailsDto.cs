﻿using Common.DTOs.Configuration.UpcomingAppointments;
using Newtonsoft.Json;

namespace Common.DTOs.Configuration
{
    public class PhysicianHistoryAppointmentsDetailsDto : UpcomingAppointmentDetailsDto
    {
        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }
       
    }
}
