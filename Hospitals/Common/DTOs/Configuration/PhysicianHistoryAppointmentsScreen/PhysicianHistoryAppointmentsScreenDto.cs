﻿using Common.DTOs.Configuration.AppointmentHistory;
using Newtonsoft.Json;
using System;

namespace Common.DTOs.Configuration
{
    public class PhysicianHistoryAppointmentsScreenDto
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
        [JsonProperty("route")]
        public string Route { get; set; }
        [JsonProperty("canceled_appointments_custom_theme")]
        public CustomThemeDTO CanceledAppointmentsCustomTheme { get; set; }
        [JsonProperty("no_show_appointments_custom_theme")]
        public CustomThemeDTO NoShowAppointmentsCustomTheme { get; set; }
        [JsonProperty("inprogress_appointments_custom_theme")]
        public CustomThemeDTO InProgressAppointmentsCustomTheme { get; set; }
        [JsonProperty("signed_appointments_custom_theme")]
        public CustomThemeDTO SignedAppointmentsCustomTheme { get; set; }
        [JsonProperty("appointment_history_configuration")]
        public AppointmentHistoryConfigurationDto AppointmentsConfiguration { get; set; }

    }
}
