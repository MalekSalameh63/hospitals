﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class PromotionTipDTO
    {
        [JsonProperty("tips_image")]
        public string Image { get; set; }

        [JsonProperty("tips_text")]
        public string Text { get; set; }
    }
}
