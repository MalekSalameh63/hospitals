﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Configuration.Shared
{
    public class LookupDTO
    {
        public LookupCategory LookupCategory { get; set; }
        public int EnumReference { get; set; }
    }
}
