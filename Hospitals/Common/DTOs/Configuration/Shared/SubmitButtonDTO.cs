﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.Shared
{
    public class SubmitButtonDTO
    {
        [JsonProperty("button_text")]
        public string ButtonText { get; set; }

        [JsonProperty("button_text_size")]
        public double ButtonTextSize { get; set; }

        [JsonProperty("button_text_color")]
        public string ButtonTextColor { get; set; }

        [JsonProperty("button_color")]
        public string ButtonColor { get; set; }

        [JsonProperty("button_route")]
        public string ButtonRoute { get; set; }
    }
}
