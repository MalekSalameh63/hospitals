﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class PointDTO
    {
        [JsonProperty("x")]
        public long X { get; set; }

        [JsonProperty("y")]
        public long Y { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }
    }
}
