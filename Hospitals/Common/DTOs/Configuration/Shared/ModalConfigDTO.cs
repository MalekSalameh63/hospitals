﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class ModalConfigDTO
    {
        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("subtitle")]
        public string Subtitle { get; set; }

        [JsonProperty("button_back_title")]
        public string BackButtonText { get; set; }

        [JsonProperty("button_exit_title")]
        public string ExitButtonText { get; set; }
    }
}
