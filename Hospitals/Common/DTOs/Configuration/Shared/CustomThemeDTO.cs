﻿using Common.VOs;
using Newtonsoft.Json;

namespace Common.DTOs
{
    public class CustomThemeDTO
    {
        [JsonProperty("container_background_color")]
        public string ContainerBackgroundColor { get; set; }

        [JsonProperty("background_color")]
        public string BackgroundColor { get; set; }
       
        [JsonProperty("icon_color")]
        public string IconColor { get; set; }

        [JsonProperty("title")]
        public TextThemeDTO Title { get; set; }

        [JsonProperty("description")]
        public TextThemeDTO Description { get; set; }

        [JsonProperty("padding")]
        public PaddingDTO Padding { get; set; }
    }
}
