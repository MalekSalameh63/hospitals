﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class PaddingDTO
    {
        [JsonProperty("top")]
        public double Top { get; set; }
        [JsonProperty("bottom")]
        public double Bottom { get; set; }
        [JsonProperty("right")]
        public double Right { get; set; }
        [JsonProperty("left")]
        public double Left{ get; set; }
    }
}
