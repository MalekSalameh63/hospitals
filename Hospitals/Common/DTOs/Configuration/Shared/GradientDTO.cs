﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class GradientDTO
    {
        [JsonProperty("point1")]
        public PointDTO Point1 { get; set; }

        [JsonProperty("point2")]
        public PointDTO Point2 { get; set; }
    }
}
