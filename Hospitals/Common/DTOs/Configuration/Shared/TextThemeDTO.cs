﻿using Common.VOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.DTOs
{ 
    public class TextThemeDTO
    {
        public double Size { get; set; }
        public string Color { get; set; }
        public int Weight { get; set; }
    }
}
