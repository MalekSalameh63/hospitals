﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class ScreenConfigDTO
    {
        [JsonProperty("call_button_text")]
        public string CallButtonText { get; set; }

        [JsonProperty("call_button_icon")]
        public string CallButtonIcon { get; set; }

        [JsonProperty("call_button_rout")]
        public string CallButtonRout { get; set; }

        [JsonProperty("calling_text")]
        public string CallingText { get; set; }

        [JsonProperty("medical_file_text")]
        public string MedicalFileText { get; set; }

        [JsonProperty("counter_text")]
        public string CounterText { get; set; }

        [JsonProperty("counter_unit_text")]
        public string CounterUnitText { get; set; }

        [JsonProperty("counter_value")]
        public int? CounterValue { get; set; }

        [JsonProperty("appointment_link_text")]
        public string AppointmentLinkText { get; set; }

        [JsonProperty("appointment_link_route")]
        public string AppointmentLinkRoute { get; set; }

        [JsonProperty("postpone_call_text")]
        public string PostponeCallText { get; set; }

        [JsonProperty("postpone_call_route")]
        public string PostponeCallRoute { get; set; }

        [JsonProperty("card_background")]
        public string CardBackground { get; set; }

        [JsonProperty("card_text_color")]
        public string CardTextColor { get; set; }

        [JsonProperty("age_text")]
        public string AgeText { get; set; }

        [JsonProperty("background_image")]
        public string BackgroundImage { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("number_of_online_physician_text")]
        public string OnlinePhysiciansText { get; set; }

        [JsonProperty("patients_in_queue_text")]
        public string PatientsInQueueCountText { get; set; }

        [JsonProperty("position_in_queue_text")]
        public string PositionInQueueText { get; set; }

        [JsonProperty("remain_time_text")]
        public string RemainTimeText { get; set; }

        [JsonProperty("location_icon")]
        public string LocationIcon { get; set; }

        [JsonProperty("image_border_color")]
        public string ImageBorderColor { get; set; }

        [JsonProperty("description_text_color")]
        public string DescriptionTextColor { get; set; }

        [JsonProperty("gradient")]
        public GradientDTO Gradient { get; set; }

        [JsonProperty("empty_text")]
        public string EmptyText { get; set; }
        [JsonProperty("experience_message")]
        public string ExperienceMessage { get; set; }
        [JsonProperty("years_label")]
        public string YearsLabel { get; set; }
    }
}
