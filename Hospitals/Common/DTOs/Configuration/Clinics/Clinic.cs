﻿using Newtonsoft.Json;

namespace Common.DTOs.Configuration.Clinics
{
    public class Clinic
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("icon")]
        public string Icon { get; set; }
    }
}
