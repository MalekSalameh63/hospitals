﻿using System.Collections.Generic;

namespace Common.DTOs.Configuration.Clinics
{
    public class ClinicsScreenWithDataDto
    {
        public CustomThemeDTO CustomTheme { get; set; }
        public List<Clinic> Clinics { get; set; }
    }
}
