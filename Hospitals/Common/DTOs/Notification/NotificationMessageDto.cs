﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class NotificationMessageDto
    {
        public string[] RegistrationIds { get; set; }
        public NotificationDto NotificationDto { get; set; }
        public NotificationDataDto NotificationDataDto { get; set; }
    }
}
