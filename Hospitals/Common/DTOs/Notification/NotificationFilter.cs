﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Notification
{
    public class NotificationFilter
    {
        public string RegistrationId  { get;set; }
        public string UserId { get; set; }
        public int? Index { get; set; }
        public int? Count { get;  set; }
        public List<int> DisableCategories { get; set; }
        public string Language { get; set; }
    }
}
