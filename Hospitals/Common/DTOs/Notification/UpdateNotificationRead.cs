﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Notification
{
    public class UpdateNotificationRead
    {
        public Guid Id { get; set; }
    }
}
