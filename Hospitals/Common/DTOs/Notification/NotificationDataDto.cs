﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class NotificationDataDto
    {
        public string PageName { get; set; }
        public string CreationDate { get; set; }
        public string UserId { get; set; }
        [JsonProperty("category")]
        public string Category { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("can_delete")]
        public bool CanDelete { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }
        public string Language { get; set; }

    }
}
