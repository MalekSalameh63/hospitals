﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Common.DTOs
{
    public class InAppNotificationsDto
    {
        [Key]
        public Guid Id { get; set; }

        [JsonProperty("registration_ids")]
        public string[] RegistrationIds { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("clickAction")]
        public string ClickAction { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("androidChannelId")]
        public string AndroidChannelId { get; set; }

        [JsonProperty("pageName")]
        public string PageName { get; set; }

        public string UserName { get; set; }

        public string CreationDate { get; set; }

        [JsonProperty("category")]
        public string Category { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("can_delete")]
        public bool CanDelete { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }
    }
}
