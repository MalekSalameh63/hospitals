﻿using Common.Common;
using Common.Enums;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;

namespace Common.DTOs
{
    public class PushNotificationsDto
    {
        public PushNotificationsDto()
        {
        }

        [BsonId]
        public Guid Id { get; set; }

        [JsonProperty("registration_ids")]
        public string[] RegistrationIds { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("clickAction")]
        public string ClickAction { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("androidChannelId")]
        public string AndroidChannelId { get; set; }

        [JsonProperty("pageName")]
        public string PageName { get; set; }

        public string CreationDate { get; set; }

        [JsonProperty("category")]
        public string Category { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("can_delete")]
        public bool CanDelete { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }
        public string IsDeleted { get; set; }
        public string IsNotificationRead { get; set; }
        public string UserId { get; set; }
        public string Language { get; set; }

        public PushNotificationsDto(Guid id, Message notificationMessageDto)
        {
            Id = id;
            Title = notificationMessageDto.Notification.Title;
            Text = notificationMessageDto.Notification.Text;
            ClickAction = notificationMessageDto.Notification.ClickAction;
            Icon = notificationMessageDto.Notification.Icon;
            AndroidChannelId = notificationMessageDto.Notification.AndroidChannelId;
            PageName = notificationMessageDto.Data.PageName;
            CreationDate = notificationMessageDto.Data.CreationDate;
            Status = NotificationStatus.New.ToString();
            CanDelete = true;
            Time = notificationMessageDto.Data.CreationDate;
            IsDeleted = "0";
            IsNotificationRead = "0";
            CanDelete = true;
            RegistrationIds = notificationMessageDto.RegistrationIds;
            UserId = notificationMessageDto.Data.UserId;
        }
    }
}
