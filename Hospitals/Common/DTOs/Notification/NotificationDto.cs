﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class NotificationDto
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public string ClickAction { get; set; }
        public string Icon { get; set; }
        public string AndroidChannelId { get; set; }
    }
}
