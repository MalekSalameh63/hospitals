﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Pricing
{
    public class PriceModelDTO
    {
        public long SpecialtyId { get; set; }
        public decimal Price { get; set; }
    }
}
