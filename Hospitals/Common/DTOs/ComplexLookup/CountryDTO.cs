﻿using Common.Common.Appointments;
using Common.DTOs.Lookup;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs
{
    public class CountryDTO
    {
        public Guid Id { get; set; }

        [JsonProperty("phone_key")]
        public string Key { get; set; }
        public string ISOCode { get; set; }
        public string Regex { get; set; }
        public string Flag { get; set; }

        [JsonProperty("full_name")]
        public List<LocalizationDTO> FullName { get; set; }

        [JsonProperty("short_name")]
        public List<LocalizationDTO> ShortName { get; set; }

        [JsonProperty("local_full_name", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string LocalFullName { get; set; }

        [JsonProperty("local_short_name", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string LocalShortName { get; set; }
    }
}
