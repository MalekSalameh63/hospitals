﻿using Common.DTOs.Configuration.Shared;
using Common.VOs;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.DTOs
{
    public class CultureEditDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("abbreviation")]
        public string Abbreviation { get; set; }
        [JsonProperty("date_format")]
        public int DateFormatId { get; set; }
        [JsonProperty("month_name_format")]
        public int MonthNameFormatId { get; set; }
        [JsonProperty("language")]
        public int DefaultLanguageId { get; set; }
        [JsonProperty("currency")]
        public int CurrencyId { get; set; }
        [JsonProperty("numeric")]
        public int NumericId { get; set; }
        [JsonProperty("unit")]
        public int UnitId { get; set; }
        [JsonProperty("country")]
        public int CountryId { get; set; }
    }
}
