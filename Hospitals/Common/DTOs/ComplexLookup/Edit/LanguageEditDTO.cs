﻿using Common.DTOs.Configuration.Shared;
using Newtonsoft.Json;

namespace Common.DTOs
{
    public class LanguageEditDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("abbreviation")]
        public string Abbreviation { get; set; }
        [JsonProperty("direction")]
        public string Direction { get; set; }
        [JsonProperty("name")]
        public LookupDTO Name { get; set; }
    }
}
