﻿using Common.DTOs.Configuration.Shared;
using Common.VOs;
using Newtonsoft.Json;

namespace Common.DTOs
{
    public class NumericEditDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("name")]
        public LookupDTO Name { get; set; }
    }
}
