﻿using Common.DTOs.Configuration.Shared;
using Newtonsoft.Json;

namespace Common.DTOs
{
    public class CurrencyEditDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("decimals")]
        public int Decimals { get; set; }
        [JsonProperty("full_name")]
        public string FullName { get; set; }
        [JsonProperty("short_name")]
        public string ShortName { get; set; }
        [JsonProperty("abbreviation")]
        public LookupDTO Abbreviation { get; set; }
    }
}
