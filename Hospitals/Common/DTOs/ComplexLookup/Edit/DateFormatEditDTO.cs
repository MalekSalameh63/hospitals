﻿using Common.DTOs.Configuration.Shared;
using Common.VOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.DTOs
{
    public class DateFormatEditDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("format")]
        public string Format { get; set; }
        [JsonProperty("name")]
        public LookupDTO Name { get; set; }
    }
}
