﻿using Common.DTOs.Configuration.Shared;
using Newtonsoft.Json;

namespace Common.DTOs
{
    public class UnitEditDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("length_unit")]
        public LookupDTO LengthUnit { get; set; }
        [JsonProperty("weight_unit")]
        public LookupDTO WeightUnit { get; set; }
    }
}
