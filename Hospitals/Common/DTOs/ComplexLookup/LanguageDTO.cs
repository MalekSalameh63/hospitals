﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class LanguageDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("abbreviation")]
        public string Abbreviation { get; set; }
        [JsonProperty("direction")]
        public string Direction { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
