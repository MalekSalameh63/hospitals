﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.ComplexLookup
{
    public class DropdownListDTO
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("icon")]
        public object Icon { get; set; }

        [JsonProperty("value")]
        public dynamic Value { get; set; }
    }
}
