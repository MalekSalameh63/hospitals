﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.ComplexLookup
{
    public class AddressDDLDTO
    {
        [JsonProperty("children")]
        public List<DropdownListDTO> Children { get; set; }
    }
}
