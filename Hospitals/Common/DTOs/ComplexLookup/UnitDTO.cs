﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class UnitDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("length_unit")]
        public string LengthUnit { get; set; }
        [JsonProperty("weight_unit")]
        public string WeightUnit { get; set; }
    }
}
