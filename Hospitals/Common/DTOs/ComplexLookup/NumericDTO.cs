﻿using Common.VOs;
using Newtonsoft.Json;

namespace Common.DTOs
{
    public class NumericDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
