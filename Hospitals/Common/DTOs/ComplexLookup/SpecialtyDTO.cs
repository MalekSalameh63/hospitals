﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class SpecialtyDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("number")]
        public int Number { get; set; }
        [JsonProperty("default_visit_fee")]
        public double DefaultVisitFee { get; set; }
        [JsonProperty("full_name")]
        public string FullName { get; set; }
        [JsonProperty("short_name")]
        public string ShortName { get; set; }
    }
}
