﻿using Common.DTOs.Lookup;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs
{
    public class CityDTO
    {
        public Guid Id { get; set; }
        public string CountryCode { get; set; }
        public int Number { get; set; }
        public string Code { get; set; }

        [JsonProperty("local_name", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string LocalName { get; set; }
        public List<LocalizationDTO> Name { get; set; }
    }
}
