﻿using Common.VOs;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.DTOs
{
    public class CultureDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("abbreviation")]
        public string Abbreviation { get; set; }
        [JsonProperty("date_format")]
        public DateFormatDTO DateFormat { get; set; }
        [JsonProperty("month_name_format")]
        public MonthNameFormatDTO MonthNameFormat { get; set; }
        [JsonProperty("language")]
        public LanguageDTO Language { get; set; }
        [JsonProperty("currency")]
        public CurrencyDTO Currency { get; set; }
        [JsonProperty("numeric")]
        public NumericDTO Numeric { get; set; }
        [JsonProperty("unit")]
        public UnitDTO Unit { get; set; }
        [JsonProperty("country")]
        public CountryDTO Country { get; set; }
    }
}
