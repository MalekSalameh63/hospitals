﻿using Common.Common.Appointments;
using Common.DTOs.Lookup;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs
{
    public class RankDTO
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public List<LocalizationDTO> Name { get; set; }
        public string LocalName { get; set; }
    }
}
