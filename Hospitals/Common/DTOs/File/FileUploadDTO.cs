﻿using Common.DTOs.File;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.DTOs.File
{
    public class FileUploadDTO
    {
        [JsonProperty("list")]
        public List<DocumentDTO> Documents { get; set; }
    }
}
