﻿using Common.Enums;
using Newtonsoft.Json;
using System;

namespace Common.DTOs.File
{
    public class DocumentDTO
    {
        public Guid Id { get; set; }
        [JsonProperty("user_id")]
        public string UserId { get; set; }
        [JsonProperty("content")]
        public byte[] Content { get; set; }
        [JsonProperty("file_extension")]
        public string Extension { get; set; }
        [JsonProperty("file_name")]
        public string FileName { get; set; }
        [JsonProperty("file_size")]
        public int Size { get; set; }
        public FileType FileType { get; set; }
        public FileParentType ParentType { get; set; }
        public string ParentId { get; set; }
        public bool Approved { get; set; }

    }
}
