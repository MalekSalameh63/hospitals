﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Patient
{
    public class PatientQueueListDto
    {
        [JsonProperty("patient_queue_list")]
        public List<PatientQueueList> PatientQueueList { get; set; }
    }

    public class PatientQueueList
    {
        [JsonProperty("patient_name")]
        public string PatientName { get; set; }

        [JsonProperty("patient_image")]
        public string PatientImage { get; set; }

        [JsonProperty("patient_gender")]
        public string PatientGender { get; set; }
    }
}
