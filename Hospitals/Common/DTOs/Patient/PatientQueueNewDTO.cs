﻿using Common.Enums;
using System;

namespace Common.DTOs.Patient
{
    public class PatientQueueNewDTO
    {
        public string PatientId { get; set; }
        public string ComplaintId { get; set; }
        public bool IsActive { get; set; }
        public string Partition { get; set; }
        public string Offset { get; set; }
        public string PreferredLanguage { get; set; }
        public string PreferredGender { get; set; }
        public long EpochTime { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
