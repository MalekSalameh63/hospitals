﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Patient
{
    public class AddPatientQueueDTO
    {
        public string PatientId { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
