﻿using Newtonsoft.Json;

namespace Common.DTOs.Patient
{
    public class PickPatientDTO
    {
        // DEV-NOTE: This out to be refactored to PatientMRN for better readability and consistency with the code.
        [JsonProperty("patient_id")]
        public string PatientId { get; set; }
    }
}
