﻿namespace Common.DTOs.Dictionary
{
    public class WordDTO
    {
        public string Word { get; set; }
    }
}
