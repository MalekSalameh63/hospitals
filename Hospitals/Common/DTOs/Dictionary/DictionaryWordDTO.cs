﻿using Newtonsoft.Json;

namespace Common.DTOs.Dictionary
{
    public class DictionaryWordDTO
    {
        [JsonProperty("language")]
        public string Language { get; set; }
        [JsonProperty("translation")]
        public string Translation { get; set; }
    }
}
