﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.DTOs.Dictionary
{
    public class DictionaryEntryDTO
    {
        [JsonProperty("key")]
        public string Key { get; set; }
        [JsonProperty("translations")]
        public DictionaryWordDTO [] DictionaryEntries { get; set; }
    }
}
