﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Template.Transaction
{
    public class TransactionTemplateDTO
    {
        public Guid Id { get; set; }
        public int TypeId { get; set; }
        public string Language { get; set; }
        public string Icon { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
