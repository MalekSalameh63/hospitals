﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class DailyTipDTO
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<int> Tags { get; set; }
    }
}
