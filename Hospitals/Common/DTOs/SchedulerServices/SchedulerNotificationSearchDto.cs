﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.SchedulerServices
{
    public class SchedulerNotificationSearchDto
    {
        public Guid Id { get; set; }
        public int? Type { get; set; }
        public long ExecutionDate { get; set; }
        public long CreationDate { get; set; }
        public int? Status { get; set; }
        public long NotificationTime { get; set; }
        public string PatientId { get; set; }
        public string PhysicianId { get; set; }
        public long AppointmentNo { get; set; }
        public long InMinutesPeriod { get; set; }
    }
}
