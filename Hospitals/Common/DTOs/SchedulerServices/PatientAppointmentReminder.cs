﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.SchedulerServices
{
    public class PatientAppointmentReminder
    {
        public string UserName { get; set; }
        public string AppName { get; set; }
        public string AppointmentTime { get; set; }
        public string AppointmentDate { get; set; }
    }
}
