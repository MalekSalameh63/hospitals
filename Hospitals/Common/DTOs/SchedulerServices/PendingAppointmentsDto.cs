﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.SchedulerServices
{
    public class PendingAppointmentsDto
    {
        public long AppointmentNo { get; set; }
        public long NotificationTime { get; set; }
    }
}
