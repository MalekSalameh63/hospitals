﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.SchedulerServices
{
    public class PhysicianAppointmentReminder
    {
        public string PhysicianName { get; set; }
        public string AppointmentRemainingTime { get; set; }
        public string PatientName { get; set; }
    }
}
