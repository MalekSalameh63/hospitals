﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.SchedulerServices
{
    public class AppointmentNotificationDto
    {
        public long AppointmentNo { get; set; }
    }
}
