﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.SchedulerServices
{
    public class UserAppointmentNotificationDto
    {
        public string UserId { get; set; }
        public string NotificationToken { get; set; }
        public string UILanguage { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}
