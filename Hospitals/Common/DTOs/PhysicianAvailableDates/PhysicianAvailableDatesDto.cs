﻿using Common.DTOs.Configuration.Shared;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs.PhysicianAvailableDatesDto
{
    public class PhysicianAvailableDatesDto
    {
        [JsonProperty("dates_list")]
        public List<long> DatesLists { get; set; }
        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }
        [JsonProperty("submit_button")]
        public SubmitButtonDTO SubmitButton { get; set; }
        [JsonProperty("cancel_button")]
        public SubmitButtonDTO CancelButton { get; set; }

    }
}
