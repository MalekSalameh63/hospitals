﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.PhysicianAvailableDates.DatesListDto
{
    public class DatesListDto
    {
        [JsonProperty("date")]
        public DateTime Date { get; set; }
    }
}
