﻿using Common.DTOs.Configuration.Shared;
using System.Collections.Generic;

namespace Common.DTOs.DoctorsListFilterScreen
{
    public class DoctorsListFilterScreenDto
    {
        public CustomThemeDTO CustomTheme { get; set; }
        public string Title { get; set; }
        public SubmitButtonDTO SubmitButton { get; set; }
        public List<FilterScreenListDto> FilterScreenList { get; set; }
    }
}
