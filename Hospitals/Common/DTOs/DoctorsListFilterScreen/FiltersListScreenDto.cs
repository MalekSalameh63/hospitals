﻿namespace Common.DTOs.DoctorsListFilterScreen
{
    public class FilterScreenListDto
    {
        public string Title { get; set; }
        public int Value { get; set; }
        public bool Selected { get; set; }
    }
}
