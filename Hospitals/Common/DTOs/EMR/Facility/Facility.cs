﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.DTOs.EMR.Facility
{
    public class Facility
    {
        [JsonProperty("facilityGroupId")]
        public string FacilityGroupId { get; set; }

        [JsonProperty("facilityId")]
        public long FacilityId { get; set; }

        [JsonProperty("facilityName")]
        public string FacilityName { get; set; }
    }
}
