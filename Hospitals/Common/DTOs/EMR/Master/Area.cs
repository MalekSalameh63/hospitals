﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.Master
{
    public class Area
    {
        [JsonProperty("areaID")]
        public int AreaID { get; set; }

        /// <summary>
        /// Nullable.
        /// </summary>
        [JsonProperty("areaName")]
        public string AreaName { get; set; }
    }
}
