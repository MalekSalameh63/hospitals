﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.Master
{ 
    public class ICDCode
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
