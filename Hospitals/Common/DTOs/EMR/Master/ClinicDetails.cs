﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.Master
{
    public class ClinicDetails
    {
        [JsonProperty("clinicID")]
        public int ClinicID { get; set; }

        /// <summary>
        /// Nullable
        /// </summary>
        [JsonProperty("clinicNameEnglish")]
        public string ClinicNameEnglish { get; set; }

        /// <summary>
        /// Nullable
        /// </summary>
        [JsonProperty("clinicNameArabic")]
        public string ClinicNameArabic { get; set; }

        [JsonProperty("clinicGroupID")]
        public int ClinicGroupID { get; set; }

        /// <summary>
        /// Nullable
        /// </summary>
        [JsonProperty("clinicGroupName")]
        public string ClinicGroupName { get; set; }
    }
}
