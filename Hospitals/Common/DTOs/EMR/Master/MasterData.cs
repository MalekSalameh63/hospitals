﻿using Newtonsoft.Json;
using System;
using System.Security.Policy;

namespace Common.DTOs.EMR.Master
{
    public class MasterData
    {
        /// <summary>
        /// Master data lookup key
        /// </summary>
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
