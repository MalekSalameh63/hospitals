﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.Master
{
    public class City
    {
        /// <summary>
        /// Nullable.
        /// </summary>
        [JsonProperty("isoCityID")]
        public string IsoCityID { get; set; }

        /// <summary>
        /// Nullable.
        /// </summary>
        [JsonProperty("cityName")]
        public string CityName { get; set; }
    }
}
