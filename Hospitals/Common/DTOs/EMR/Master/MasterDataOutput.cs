﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.EMR.Master
{
    public class MasterDataOutput
    {
        /// <summary>
        /// Parameter ID to be used in transaction.
        /// </summary>
        [JsonProperty("id")]
        public int ID { get; set; }

        /// <summary>
        /// English name.
        /// Nullable.
        /// </summary>
        [JsonProperty("nameEn")]
        public string NameEnglish { get; set; }

        /// <summary>
        /// Arabic name.
        /// Nullable
        /// </summary>
        [JsonProperty("nameAr")]
        public string NameArabic { get; set; }

        /// <summary>
        /// Group ID associated with master data.
        /// </summary>
        [JsonProperty("groupID")]
        public int GroupID { get; set; }

        /// <summary>
        /// Type ID associated with master data.
        /// </summary>
        [JsonProperty("typeId")]
        public int TypeID { get; set; }

        /// <summary>
        /// Additional attribute. Used as a per need basis.
        /// Nullable.
        /// </summary>
        [JsonProperty("alias")]
        public string Alias { get; set; }

        /// <summary>
        /// Alias in Arabic.
        /// Nullable.
        /// </summary>
        [JsonProperty("aliasN")]
        public string AliasArabic { get; set; }

        /// <summary>
        /// Nullable.
        /// </summary>
        [JsonProperty("valueList")]
        public string ValueList { get; set; }

        /// <summary>
        /// Nullable.
        /// </summary>
        [JsonProperty("detail1")]
        public string Detail1 { get; set; }

        /// <summary>
        /// Nullable.
        /// </summary>
        [JsonProperty("detail2")]
        public string Detail2 { get; set; }

        /// <summary>
        /// Nullable.
        /// </summary>
        [JsonProperty("detail3")]
        public string Detail3 { get; set; }

        /// <summary>
        /// Nullable.
        /// </summary>
        [JsonProperty("detail4")]
        public string Detail4 { get; set; }

        /// <summary>
        /// Nullable.
        /// </summary>
        [JsonProperty("detail5")]
        public string Detail5 { get; set; }

        /// <summary>
        /// Nullable.
        /// </summary>
        [JsonProperty("remarks")]
        public string Remarks { get; set; }
    }
}
