﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.EMR.Patient
{
    public class PatientBannerVM
    {
        [JsonProperty("patientMRN")]
        public int PatientMRN { get; set; }

        [JsonProperty("fullName")]
        public string Fullname { get; set; }

        [JsonProperty("fullNameN")]
        public string FullnameArabic { get; set; }

        [JsonProperty("gender")]
        public int Gender { get; set; }

        [JsonProperty("companyName")]
        public string CompanyName { get; set; }

        [JsonProperty("companyNameN")]
        public string CompanyNameArabic { get; set; }

        [JsonProperty("age")]
        public string Age { get; set; }

        [JsonProperty("dob")]
        public DateTimeOffset DoB { get; set; }

        [JsonProperty("dobHijri")]
        public string DoB_Hijri { get; set; }

        [JsonProperty("bloodGroup")]
        public string BloodGroup { get; set; }

        [JsonProperty("nationalityId")]
        public string NationalityID { get; set; }

        [JsonProperty("mobileNumber")]
        public string MobileNumber { get; set; }

        [JsonProperty("allergy")]
        public string Allergy { get; set; }

        [JsonProperty("showImage")]
        public bool ShowImage { get; set; }

        [JsonProperty("nationalityName")]
        public string NationalityName { get; set; }

        [JsonProperty("weightKg")]
        public double WeightKg { get; set; }

        [JsonProperty("heightCm")]
        public double HeightCm { get; set; }

        [JsonProperty("bodyMassIndex")]
        public double BodyMassIndex { get; set; }

        [JsonProperty("temperatureCelcius")]
        public double TemperatureCelcius { get; set; }

        [JsonProperty("pulseBeatPerMinute")]
        public int PulseBPM { get; set; }

        [JsonProperty("pulseRhythm")]
        public int PulseRhythm { get; set; }

        [JsonProperty("respirationBeatPerMinute")]
        public double RespirationBPM { get; set; }

        [JsonProperty("respirationPattern")]
        public int RespirationPattern { get; set; }

        [JsonProperty("bloodPressureLower")]
        public int BloodPressureLower { get; set; }

        [JsonProperty("bloodPressureHigher")]
        public int BloodPressureHigher { get; set; }

        /// <summary>
        /// Oxygen saturation.
        /// </summary>
        [JsonProperty("sao2")]
        public int SaO2 { get; set; }

        /// <summary>
        /// Fraction of inspired oxygen.
        /// </summary>
        [JsonProperty("fio2")]
        public int FiO2 { get; set; }
    }
}
