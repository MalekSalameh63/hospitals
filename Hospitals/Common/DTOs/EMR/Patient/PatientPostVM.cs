﻿using Common.DTOs.User;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Common.DTOs.EMR.Patient
{ 
    public class PatientPostVM
    {
        [JsonProperty("patientMRN")]
        public int PatientMRN { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("middleName")]
        public string MiddleName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        // Note: Nullable in vba.hmg.com
        [JsonProperty("firstNameArabic")]
        public string FirstNameArabic { get; set; }

        // Note: Nullable in vba.hmg.com
        [JsonProperty("middleNameArabic")]
        public string MiddleNameArabic { get; set; }

        // Note: Nullable in vba.hmg.com
        [JsonProperty("lastNameArabic")]
        public string LastNameArabic { get; set; }

        // Note: Nullable in vba.hmg.com
        [JsonProperty("familyName")]
        public string FamilyName { get; set; }

        [JsonProperty("gender")]
        public int? Gender { get; set; }

        [JsonProperty("marital")]
        public int Marital { get; set; }

        [JsonProperty("dateofBirth")]
        public string DateOfBirth { get; set; }

        [JsonProperty("nationalityID")]
        public string NationalityID { get; set; }

        [JsonProperty("mobileNumber")]
        public string MobileNumber { get; set; }

        [JsonProperty("emergencyContactName")]
        public string EmergencyContactName { get; set; }

        [JsonProperty("emergencyContactNo")]
        public string EmergencyContactNo { get; set; }

        [JsonProperty("patientIdentificationType")]
        public int PatientIdentificationType { get; set; }

        [JsonProperty("patientIdentificationNo")]
        public string PatientIdentificationNo { get; set; }

        [JsonProperty("ResidenceCountry")]
        [Required]
        public string ResidenceCountry { get; set; }

        [JsonProperty("ResidenceCity")]
        [Required]
        public string ResidenceCity { get; set; }

        [JsonProperty("ResidenceArea")]
        [Required]
        public int ResidenceArea { get; set; }

        public PatientPostVM()
        { }

        public PatientPostVM(UserResponseDto user)
        {
            PatientMRN = 0;                     // DEV-NOTE: Must be 0 or VIDA will do update on user instead.
            FirstName = user.FirstName;
            MiddleName = user.SecondName;
            LastName = user.LastName;
            Gender = user.Gender;
            DateOfBirth = (new DateTime(1970, 1, 1)).AddMilliseconds(user.DateOfBirth.Value).ToString("yyyy-MM-dd");
            NationalityID = user.Nationality;
            MobileNumber = String.Concat("0", user.PhoneNumber);
            PatientIdentificationNo = user.NationalId;
            Marital = user.MaritalStatus ?? default;
            ResidenceCountry = "SAU";// user.Country;
            ResidenceCity = "10";               // DEV-NOTE: Required by VIDA.
            ResidenceArea = 1;                  // DEV-NOTE: Required by VIDA.
            string _EmergencyContactNo = user.EmergencyCountryCode + user.EmergencyPhoneNumber;
            EmergencyContactNo = string.IsNullOrEmpty(_EmergencyContactNo) ? "1111111111" : _EmergencyContactNo;  // DEV-NOTE: Required by VIDA.
            EmergencyContactName = string.IsNullOrEmpty(user.EmergencyName) ? "Trachea" : user.EmergencyName;   // DEV-NOTE: Required by VIDA.
            PatientIdentificationType = 3;      // DEV-NOTE: Required by VIDA. 1 = "Saudi National", 2 = "Iqama", 3 = "Passport Number"
        }
    }
}
