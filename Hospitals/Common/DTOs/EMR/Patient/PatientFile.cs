﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace Common.DTOs.EMR.Patient
{
    #region Helper classes

    #region Lab Order
    public class LstHistory
    {
        [JsonProperty("orderNo")]
        public int OrderNo { get; set; }

        [JsonProperty("testID")]
        public int TestID { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("resultValue")]
        public string ResultValue { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }

        [JsonProperty("referenceRange")]
        public string ReferenceRange { get; set; }

        [JsonProperty("createdOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("duration")]
        public string Duration { get; set; }

        [JsonProperty("xAxis")]
        public string XAxis { get; set; }

        [JsonProperty("tooltip")]
        public string Tooltip { get; set; }

        [JsonProperty("resultCriticalFlag")]
        public string ResultCriticalFlag { get; set; }
    }

    public class LstGeneralLabResult
    {
        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("testID")]
        public int TestID { get; set; }

        [JsonProperty("orderNo")]
        public int OrderNo { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("resultValue")]
        public string ResultValue { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }

        [JsonProperty("referenceRange")]
        public string ReferenceRange { get; set; }

        [JsonProperty("referenceRange_Low")]
        public int ReferenceRangeLow { get; set; }

        [JsonProperty("referenceRange_high")]
        public int ReferenceRangeHigh { get; set; }

        [JsonProperty("resultValueFlag")]
        public string ResultValueFlag { get; set; }

        [JsonProperty("resultCriticalFlag")]
        public string ResultCriticalFlag { get; set; }

        [JsonProperty("resultDeltaFlag")]
        public string ResultDeltaFlag { get; set; }

        [JsonProperty("isAmended")]
        public string IsAmended { get; set; }

        [JsonProperty("verifiedBy")]
        public string VerifiedBy { get; set; }

        [JsonProperty("reviewedby")]
        public string Reviewedby { get; set; }

        [JsonProperty("reviewedOn")]
        public string ReviewedOn { get; set; }

        [JsonProperty("explainedby")]
        public string Explainedby { get; set; }

        [JsonProperty("explainedOn")]
        public string ExplainedOn { get; set; }

        [JsonProperty("createdOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("duration")]
        public string Duration { get; set; }

        [JsonProperty("myProperty")]
        public int MyProperty { get; set; }

        [JsonProperty("lstHistory")]
        public List<LstHistory> LstHistory { get; set; }

        [JsonProperty("verifiedOn")]
        public string VerifiedOn { get; set; }

        [JsonProperty("isPackage")]
        public bool IsPackage { get; set; }

        [JsonProperty("packageID")]
        public int PackageID { get; set; }

        [JsonProperty("isCritical")]
        public bool IsCritical { get; set; }

        [JsonProperty("isAbnormal")]
        public bool IsAbnormal { get; set; }

        [JsonProperty("packageName")]
        public string PackageName { get; set; }

        [JsonProperty("testCode")]
        public string TestCode { get; set; }
    }

    public class LstSpecialLabResult
    {
        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("orderNo")]
        public int OrderNo { get; set; }

        [JsonProperty("orderDate")]
        public DateTimeOffset OrderDate { get; set; }

        [JsonProperty("testID")]
        public int TestID { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("resultData")]
        public string ResultData { get; set; }

        [JsonProperty("isAmended")]
        public string IsAmended { get; set; }

        [JsonProperty("clinicId")]
        public string ClinicId { get; set; }

        [JsonProperty("clinicName")]
        public string ClinicName { get; set; }

        [JsonProperty("doctorID")]
        public string DoctorID { get; set; }

        [JsonProperty("doctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("verifiedBy")]
        public string VerifiedBy { get; set; }

        [JsonProperty("reviewedOn")]
        public string ReviewedOn { get; set; }

        [JsonProperty("explainedOn")]
        public string ExplainedOn { get; set; }

        [JsonProperty("rtfToHtml")]
        public string RtfToHtml { get; set; }

        [JsonProperty("rtfToHtmlOnlyBody")]
        public string RtfToHtmlOnlyBody { get; set; }

        [JsonProperty("rtfToText")]
        public string RtfToText { get; set; }

        [JsonProperty("encounterNumber")]
        public int EncounterNumber { get; set; }

        [JsonProperty("verifiedOn")]
        public string VerifiedOn { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class LabOrder
    {
        [JsonProperty("type")]
        public int Type { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("clinicId")]
        public int ClinicId { get; set; }

        [JsonProperty("orderNo")]
        public int OrderNo { get; set; }

        [JsonProperty("orderDate")]
        public DateTimeOffset OrderDate { get; set; }

        [JsonProperty("doctorID")]
        public int DoctorID { get; set; }

        [JsonProperty("doctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("isControlTest")]
        public string IsControlTest { get; set; }

        [JsonProperty("clinicName")]
        public string ClinicName { get; set; }

        [JsonProperty("lstGeneralLabResult")]
        public List<LstGeneralLabResult> LstGeneralLabResult { get; set; }

        [JsonProperty("lstSpecialLabResult")]
        public List<LstSpecialLabResult> LstSpecialLabResult { get; set; }
    }

    #endregion

    #region Admission
    public class LstDischargeSummary
    {
        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("dischargeNo")]
        public int DischargeNo { get; set; }

        [JsonProperty("admissionNo")]
        public int AdmissionNo { get; set; }

        [JsonProperty("dischargeDate")]
        public DateTimeOffset DischargeDate { get; set; }

        [JsonProperty("finalDiagnosis")]
        public string FinalDiagnosis { get; set; }

        [JsonProperty("persentation")]
        public string Persentation { get; set; }

        [JsonProperty("pastHistory")]
        public string PastHistory { get; set; }

        [JsonProperty("planOfCare")]
        public string PlanOfCare { get; set; }

        [JsonProperty("investigations")]
        public string Investigations { get; set; }

        [JsonProperty("followupPlan")]
        public string FollowupPlan { get; set; }

        [JsonProperty("conditionOnDischarge")]
        public string ConditionOnDischarge { get; set; }

        [JsonProperty("significantFindings")]
        public string SignificantFindings { get; set; }

        [JsonProperty("planedProcedure")]
        public string PlanedProcedure { get; set; }

        [JsonProperty("daysStayed")]
        public string DaysStayed { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("erCare")]
        public string ErCare { get; set; }
    }

    public class LstDischargeDiag
    {
        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("dischargeNo")]
        public int DischargeNo { get; set; }

        [JsonProperty("admissionNo")]
        public int AdmissionNo { get; set; }

        [JsonProperty("codeID")]
        public string CodeID { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }
    }

    public class Admission
    {
        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("admissionNo")]
        public int AdmissionNo { get; set; }

        [JsonProperty("admissionDate")]
        public DateTimeOffset AdmissionDate { get; set; }

        [JsonProperty("clinic")]
        public string Clinic { get; set; }

        [JsonProperty("doctor")]
        public string Doctor { get; set; }

        [JsonProperty("lstDischargeSummary")]
        public List<LstDischargeSummary> LstDischargeSummary { get; set; }

        [JsonProperty("lstDischargeDiag")]
        public List<LstDischargeDiag> LstDischargeDiag { get; set; }
    }
    #endregion

    #region Radiology Report

    public class RadReport
    {
        [JsonProperty("navId")]
        public int NavId { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("restultType")]
        public string RestultType { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("clinicName")]
        public string ClinicName { get; set; }

        [JsonProperty("invoiceNo")]
        public string InvoiceNo { get; set; }

        [JsonProperty("invoiceLineItemNo")]
        public string InvoiceLineItemNo { get; set; }

        [JsonProperty("patientType")]
        public string PatientType { get; set; }

        [JsonProperty("patientID")]
        public string PatientID { get; set; }

        [JsonProperty("imageURLContainPDF")]
        public bool ImageURLContainPDF { get; set; }

        [JsonProperty("procedureID")]
        public int ProcedureID { get; set; }

        [JsonProperty("procedure")]
        public string Procedure { get; set; }

        [JsonProperty("reportData")]
        public string ReportData { get; set; }

        [JsonProperty("imageURL")]
        public string ImageURL { get; set; }

        [JsonProperty("diaPacsURL")]
        public string DiaPacsURL { get; set; }

        [JsonProperty("createdBy")]
        public string CreatedBy { get; set; }

        [JsonProperty("createdOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("editedBy")]
        public string EditedBy { get; set; }

        [JsonProperty("editedOn")]
        public string EditedOn { get; set; }

        [JsonProperty("rowver")]
        public string Rowver { get; set; }

        [JsonProperty("encounterNumber")]
        public int EncounterNumber { get; set; }

        [JsonProperty("doctorID")]
        public int DoctorID { get; set; }

        [JsonProperty("doctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("clinicId")]
        public int ClinicId { get; set; }
    }

    #endregion

    #region Clinical Summary

    public class LstAllergy
    {
        [JsonProperty("allergy")]
        public string Allergy { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
    }

    public class LstDiagnosis
    {
        [JsonProperty("diagnosis")]
        public string Diagnosis { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
    }

    public class LstMedication
    {
        [JsonProperty("date")]
        public DateTimeOffset Date { get; set; }

        [JsonProperty("medication")]
        public string Medication { get; set; }

        [JsonProperty("prescription")]
        public string Prescription { get; set; }

        [JsonProperty("lastFilled")]
        public string LastFilled { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("doseDurationDays")]
        public int DoseDurationDays { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
    }

    public class LstGPDetail
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
    }

    public class LstOtherProvider
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("disp")]
        public string Disp { get; set; }

        [JsonProperty("lastEncounter")]
        public string LastEncounter { get; set; }

        [JsonProperty("nextencounter")]
        public string Nextencounter { get; set; }

        [JsonProperty("rightOfAccess")]
        public string RightOfAccess { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
    }

    public class LstEncounterHistory
    {
        [JsonProperty("date")]
        public DateTimeOffset Date { get; set; }

        [JsonProperty("speciality")]
        public string Speciality { get; set; }

        [JsonProperty("clinician")]
        public string Clinician { get; set; }

        [JsonProperty("reason")]
        public string Reason { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("encounterType")]
        public string EncounterType { get; set; }
    }

    public class LstCsVisitDetail
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("apptType")]
        public string ApptType { get; set; }

        [JsonProperty("appointmentDate")]
        public DateTimeOffset AppointmentDate { get; set; }

        [JsonProperty("clinic")]
        public string Clinic { get; set; }

        [JsonProperty("doctor")]
        public string Doctor { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
    }

    public class LstPastHistory
    {
        [JsonProperty("historyType")]
        public string HistoryType { get; set; }

        [JsonProperty("history")]
        public string History { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("historyTypeName")]
        public string HistoryTypeName { get; set; }

        [JsonProperty("historyTypeID")]
        public string HistoryTypeID { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
    }

    public class LstCsVitalSign
    {
        [JsonProperty("createdOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("weightKg")]
        public int WeightKg { get; set; }

        [JsonProperty("heightCm")]
        public int HeightCm { get; set; }

        [JsonProperty("temperatureCelcius")]
        public int TemperatureCelcius { get; set; }

        [JsonProperty("pulseBeatPerMinute")]
        public int PulseBeatPerMinute { get; set; }

        [JsonProperty("respirationBeatPerMinute")]
        public int RespirationBeatPerMinute { get; set; }

        [JsonProperty("bloodPressureLower")]
        public int BloodPressureLower { get; set; }

        [JsonProperty("bloodPressureHigher")]
        public int BloodPressureHigher { get; set; }
    }

    public class ObjCsVitalSign
    {
        [JsonProperty("createdOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("weightKg")]
        public int WeightKg { get; set; }

        [JsonProperty("heightCm")]
        public int HeightCm { get; set; }

        [JsonProperty("temperatureCelcius")]
        public int TemperatureCelcius { get; set; }

        [JsonProperty("pulseBeatPerMinute")]
        public int PulseBeatPerMinute { get; set; }

        [JsonProperty("respirationBeatPerMinute")]
        public int RespirationBeatPerMinute { get; set; }

        [JsonProperty("bloodPressureLower")]
        public int BloodPressureLower { get; set; }

        [JsonProperty("bloodPressureHigher")]
        public int BloodPressureHigher { get; set; }
    }

    public class BloodPressureHigher
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("createdOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
    }

    public class BloodPressureLower
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("createdOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
    }

    public class Temperature
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("createdOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
    }

    public class Respiration
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("createdOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
    }

    public class HeartRate
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("createdOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
    }

    public class Height
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("createdOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
    }

    public class Weight
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("createdOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
    }

    public class ObjLastNonZeroVitalSign
    {
        [JsonProperty("bloodPressureHigher")]
        public BloodPressureHigher BloodPressureHigher { get; set; }

        [JsonProperty("bloodPressureLower")]
        public BloodPressureLower BloodPressureLower { get; set; }

        [JsonProperty("temperature")]
        public Temperature Temperature { get; set; }

        [JsonProperty("respiration")]
        public Respiration Respiration { get; set; }

        [JsonProperty("heartRate")]
        public HeartRate HeartRate { get; set; }

        [JsonProperty("height")]
        public Height Height { get; set; }

        [JsonProperty("weight")]
        public Weight Weight { get; set; }
    }

    public class LstComplaintsDetail
    {
        [JsonProperty("ccDate")]
        public DateTimeOffset CcDate { get; set; }

        [JsonProperty("rtfChiefComplaint")]
        public string RtfChiefComplaint { get; set; }

        [JsonProperty("plainChiefComplaint")]
        public string PlainChiefComplaint { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("createdBy")]
        public string CreatedBy { get; set; }
    }

    public class LstVaccinationDetail
    {
        [JsonProperty("templateName")]
        public string TemplateName { get; set; }

        [JsonProperty("procedureID")]
        public string ProcedureID { get; set; }

        [JsonProperty("vaccinationCode")]
        public string VaccinationCode { get; set; }

        [JsonProperty("procedure")]
        public string Procedure { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
    }

    public class LstNonZeroVitalSign
    {
        [JsonProperty("criteria")]
        public string Criteria { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("createdOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("cssClass")]
        public string CssClass { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("isActive")]
        public bool IsActive { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }
    }

    public class LabAbnormalResult
    {
        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("testID")]
        public int TestID { get; set; }

        [JsonProperty("orderNo")]
        public int OrderNo { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("resultValue")]
        public string ResultValue { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }

        [JsonProperty("referenceRange")]
        public string ReferenceRange { get; set; }

        [JsonProperty("referenceRange_Low")]
        public int ReferenceRangeLow { get; set; }

        [JsonProperty("referenceRange_high")]
        public int ReferenceRangeHigh { get; set; }

        [JsonProperty("resultValueFlag")]
        public string ResultValueFlag { get; set; }

        [JsonProperty("resultCriticalFlag")]
        public string ResultCriticalFlag { get; set; }

        [JsonProperty("createdOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("verifiedOn")]
        public string VerifiedOn { get; set; }

        [JsonProperty("isPackage")]
        public bool IsPackage { get; set; }

        [JsonProperty("packageID")]
        public int PackageID { get; set; }

        [JsonProperty("isCritical")]
        public bool IsCritical { get; set; }

        [JsonProperty("isAbnormal")]
        public bool IsAbnormal { get; set; }

        [JsonProperty("packageName")]
        public string PackageName { get; set; }

        [JsonProperty("testCode")]
        public string TestCode { get; set; }
    }

    public class ClinicalSummary
    {
        [JsonProperty("lstAllergy")]
        public List<LstAllergy> LstAllergy { get; set; }

        [JsonProperty("lstDiagnosis")]
        public List<LstDiagnosis> LstDiagnosis { get; set; }

        [JsonProperty("lstMedication")]
        public List<LstMedication> LstMedication { get; set; }

        [JsonProperty("lstGPDetail")]
        public List<LstGPDetail> LstGPDetail { get; set; }

        [JsonProperty("lstOtherProvider")]
        public List<LstOtherProvider> LstOtherProvider { get; set; }

        [JsonProperty("lstEncounterHistory")]
        public List<LstEncounterHistory> LstEncounterHistory { get; set; }

        [JsonProperty("lstCsVisitDetail")]
        public List<LstCsVisitDetail> LstCsVisitDetail { get; set; }

        [JsonProperty("lstPastHistory")]
        public List<LstPastHistory> LstPastHistory { get; set; }

        [JsonProperty("lstCsVitalSign")]
        public List<LstCsVitalSign> LstCsVitalSign { get; set; }

        [JsonProperty("objCsVitalSign")]
        public ObjCsVitalSign ObjCsVitalSign { get; set; }

        [JsonProperty("objLastNonZeroVitalSign")]
        public ObjLastNonZeroVitalSign ObjLastNonZeroVitalSign { get; set; }

        [JsonProperty("lstComplaintsDetail")]
        public List<LstComplaintsDetail> LstComplaintsDetail { get; set; }

        [JsonProperty("lstVaccinationDetail")]
        public List<LstVaccinationDetail> LstVaccinationDetail { get; set; }

        [JsonProperty("lstNonZeroVitalSigns")]
        public List<LstNonZeroVitalSign> LstNonZeroVitalSigns { get; set; }

        [JsonProperty("labAbnormalResults")]
        public List<LabAbnormalResult> LabAbnormalResults { get; set; }
    }

    #endregion

    #region Doctor-wise Patient Episodes

    public class LstAssessment
    {
        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("dispalyName")]
        public string DispalyName { get; set; }

        [JsonProperty("episodeID")]
        public int EpisodeID { get; set; }

        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [JsonProperty("patientID")]
        public int PatientID { get; set; }

        [JsonProperty("icD10")]
        public string IcD10 { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("condition")]
        public string Condition { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("patientType")]
        public string PatientType { get; set; }
    }

    public class LstCheifComplaint
    {
        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("dispalyName")]
        public string DispalyName { get; set; }

        [JsonProperty("episodeID")]
        public int EpisodeID { get; set; }

        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [JsonProperty("patientType")]
        public string PatientType { get; set; }

        [JsonProperty("patientID")]
        public int PatientID { get; set; }

        [JsonProperty("ccDate")]
        public DateTimeOffset CcDate { get; set; }

        [JsonProperty("chiefComplaint")]
        public string ChiefComplaint { get; set; }

        [JsonProperty("hopi")]
        public string Hopi { get; set; }

        [JsonProperty("currentMedication")]
        public string CurrentMedication { get; set; }
    }

    public class LstMedicalHistory
    {
        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("dispalyName")]
        public string DispalyName { get; set; }

        [JsonProperty("episodeID")]
        public int EpisodeID { get; set; }

        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [JsonProperty("patientType")]
        public string PatientType { get; set; }

        [JsonProperty("patientID")]
        public int PatientID { get; set; }

        [JsonProperty("history")]
        public string History { get; set; }

        [JsonProperty("checked")]
        public string Checked { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }
    }

    public class LstMedicine
    {
        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("dispalyName")]
        public string DispalyName { get; set; }

        [JsonProperty("episodeID")]
        public int EpisodeID { get; set; }

        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [JsonProperty("patientType")]
        public string PatientType { get; set; }

        [JsonProperty("patientID")]
        public int PatientID { get; set; }

        [JsonProperty("prescriptionType")]
        public string PrescriptionType { get; set; }

        [JsonProperty("medicineCode")]
        public string MedicineCode { get; set; }

        [JsonProperty("medicineDesc")]
        public string MedicineDesc { get; set; }

        [JsonProperty("dailyDose")]
        public string DailyDose { get; set; }

        [JsonProperty("duration")]
        public string Duration { get; set; }

        [JsonProperty("frequencyID")]
        public string FrequencyID { get; set; }

        [JsonProperty("instruction")]
        public string Instruction { get; set; }

        [JsonProperty("prescripedOn")]
        public string PrescripedOn { get; set; }
    }

    public class LstProcedure
    {
        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("dispalyName")]
        public string DispalyName { get; set; }

        [JsonProperty("episodeID")]
        public int EpisodeID { get; set; }

        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [JsonProperty("patientType")]
        public string PatientType { get; set; }

        [JsonProperty("patientID")]
        public int PatientID { get; set; }

        [JsonProperty("procedureId")]
        public string ProcedureId { get; set; }

        [JsonProperty("procName")]
        public string ProcName { get; set; }

        [JsonProperty("orderDate")]
        public DateTimeOffset OrderDate { get; set; }
    }

    public class LstPhysicalExam
    {
        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("dispalyName")]
        public string DispalyName { get; set; }

        [JsonProperty("episodeID")]
        public int EpisodeID { get; set; }

        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [JsonProperty("patientType")]
        public string PatientType { get; set; }

        [JsonProperty("patientID")]
        public int PatientID { get; set; }

        [JsonProperty("examID")]
        public string ExamID { get; set; }

        [JsonProperty("examType")]
        public string ExamType { get; set; }

        [JsonProperty("examDesc")]
        public string ExamDesc { get; set; }

        [JsonProperty("abnormal")]
        public string Abnormal { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }
    }

    public class LstTreatmentPlan
    {
        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("dispalyName")]
        public string DispalyName { get; set; }

        [JsonProperty("episodeID")]
        public int EpisodeID { get; set; }

        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [JsonProperty("patientType")]
        public string PatientType { get; set; }

        [JsonProperty("patientID")]
        public int PatientID { get; set; }

        [JsonProperty("planDate")]
        public DateTimeOffset PlanDate { get; set; }

        [JsonProperty("planNote")]
        public string PlanNote { get; set; }
    }

    public class LstVitalSign
    {
        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [JsonProperty("patientType")]
        public string PatientType { get; set; }

        [JsonProperty("patientID")]
        public int PatientID { get; set; }

        [JsonProperty("weightKg")]
        public string WeightKg { get; set; }

        [JsonProperty("bodyMassIndex")]
        public string BodyMassIndex { get; set; }

        [JsonProperty("leanBodyWeightLbs")]
        public string LeanBodyWeightLbs { get; set; }

        [JsonProperty("heightCm")]
        public string HeightCm { get; set; }

        [JsonProperty("temperatureCelcius")]
        public string TemperatureCelcius { get; set; }

        [JsonProperty("tempMethod")]
        public string TempMethod { get; set; }

        [JsonProperty("pulseBeatPerMinute")]
        public string PulseBeatPerMinute { get; set; }

        [JsonProperty("pulseRhythmDesc")]
        public string PulseRhythmDesc { get; set; }

        [JsonProperty("respirationPerMinute")]
        public string RespirationPerMinute { get; set; }

        [JsonProperty("bpPatientPosition")]
        public string BpPatientPosition { get; set; }

        [JsonProperty("bloodPressureHigher")]
        public string BloodPressureHigher { get; set; }

        [JsonProperty("bloodPressureLower")]
        public string BloodPressureLower { get; set; }
    }

    public class LstEpisode
    {
        [JsonProperty("dispalyName")]
        public string DispalyName { get; set; }

        [JsonProperty("doctorID")]
        public int DoctorID { get; set; }

        [JsonProperty("patientID")]
        public int PatientID { get; set; }

        [JsonProperty("doctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [JsonProperty("episodeID")]
        public int EpisodeID { get; set; }

        [JsonProperty("admissionNo")]
        public int AdmissionNo { get; set; }

        [JsonProperty("appointmentDate")]
        public DateTimeOffset AppointmentDate { get; set; }

        [JsonProperty("episodeDate")]
        public DateTimeOffset EpisodeDate { get; set; }

        [JsonProperty("appointmentType")]
        public string AppointmentType { get; set; }

        [JsonProperty("clinicID")]
        public string ClinicID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("clinicName")]
        public string ClinicName { get; set; }

        [JsonProperty("startTime")]
        public string StartTime { get; set; }

        [JsonProperty("endTime")]
        public string EndTime { get; set; }

        [JsonProperty("visitType")]
        public string VisitType { get; set; }

        [JsonProperty("visitFor")]
        public string VisitFor { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("lstAssessments")]
        public List<LstAssessment> LstAssessments { get; set; }

        [JsonProperty("lstCheifComplaint")]
        public List<LstCheifComplaint> LstCheifComplaint { get; set; }

        [JsonProperty("lstMedicalHistory")]
        public List<LstMedicalHistory> LstMedicalHistory { get; set; }

        [JsonProperty("lstMedicine")]
        public List<LstMedicine> LstMedicine { get; set; }

        [JsonProperty("lstProcedure")]
        public List<LstProcedure> LstProcedure { get; set; }

        [JsonProperty("lstPhysicalExam")]
        public List<LstPhysicalExam> LstPhysicalExam { get; set; }

        [JsonProperty("lstTreatmentPlan")]
        public List<LstTreatmentPlan> LstTreatmentPlan { get; set; }

        [JsonProperty("lstVitalSign")]
        public List<LstVitalSign> LstVitalSign { get; set; }
    }

    public class DoctorWisePatientEpisode
    {
        [JsonProperty("doctorID")]
        public int DoctorID { get; set; }

        [JsonProperty("doctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("clinicName")]
        public string ClinicName { get; set; }

        [JsonProperty("lastVisitDate")]
        public DateTimeOffset LastVisitDate { get; set; }

        [JsonProperty("lstEpisodes")]
        public List<LstEpisode> LstEpisodes { get; set; }
    }

    #endregion

    #region Procedures

    public class Procedure
    {
        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("dispalyName")]
        public string DispalyName { get; set; }

        [JsonProperty("episodeID")]
        public int EpisodeID { get; set; }

        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [JsonProperty("patientType")]
        public string PatientType { get; set; }

        [JsonProperty("patientID")]
        public int PatientID { get; set; }

        [JsonProperty("procedureId")]
        public string ProcedureId { get; set; }

        [JsonProperty("procName")]
        public string ProcName { get; set; }

        [JsonProperty("orderDate")]
        public DateTimeOffset OrderDate { get; set; }
    }

    #endregion

    #region All Episodes

    public class LstAllEpisode : LstEpisode
    { }

    #endregion

    #region ER Data

    public class LstErDischargeSummary
    {
        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("conclusionDiagnosis")]
        public string ConclusionDiagnosis { get; set; }

        [JsonProperty("conditionOnDischarge")]
        public string ConditionOnDischarge { get; set; }

        [JsonProperty("followupCarePlan")]
        public string FollowupCarePlan { get; set; }

        [JsonProperty("whenToSeekEmergnecy")]
        public string WhenToSeekEmergnecy { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("isDAMA")]
        public int IsDAMA { get; set; }

        [JsonProperty("dischargeTime")]
        public string DischargeTime { get; set; }

        [JsonProperty("createdBy")]
        public string CreatedBy { get; set; }

        [JsonProperty("createdOn")]
        public string CreatedOn { get; set; }

        [JsonProperty("assessmentDate")]
        public DateTimeOffset AssessmentDate { get; set; }

        [JsonProperty("assessmentNo")]
        public string AssessmentNo { get; set; }

        [JsonProperty("appointmentID")]
        public int AppointmentID { get; set; }

        [JsonProperty("triageScore")]
        public int TriageScore { get; set; }

        [JsonProperty("clinicName")]
        public string ClinicName { get; set; }

        [JsonProperty("doctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("clinicId")]
        public int ClinicId { get; set; }

        [JsonProperty("doctorId")]
        public int DoctorId { get; set; }
    }

    public class Consulation
    {
        [JsonProperty("dispalyName")]
        public string DispalyName { get; set; }

        [JsonProperty("doctorID")]
        public int DoctorID { get; set; }

        [JsonProperty("patientID")]
        public int PatientID { get; set; }

        [JsonProperty("doctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [JsonProperty("episodeID")]
        public int EpisodeID { get; set; }

        [JsonProperty("admissionNo")]
        public int AdmissionNo { get; set; }

        [JsonProperty("appointmentDate")]
        public DateTimeOffset AppointmentDate { get; set; }

        [JsonProperty("episodeDate")]
        public DateTimeOffset EpisodeDate { get; set; }

        [JsonProperty("appointmentType")]
        public string AppointmentType { get; set; }

        [JsonProperty("clinicID")]
        public string ClinicID { get; set; }

        [JsonProperty("projectID")]
        public int ProjectID { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("setupID")]
        public string SetupID { get; set; }

        [JsonProperty("clinicName")]
        public string ClinicName { get; set; }

        [JsonProperty("startTime")]
        public string StartTime { get; set; }

        [JsonProperty("endTime")]
        public string EndTime { get; set; }

        [JsonProperty("visitType")]
        public string VisitType { get; set; }

        [JsonProperty("visitFor")]
        public string VisitFor { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("lstAssessments")]
        public List<LstAssessment> LstAssessments { get; set; }

        [JsonProperty("lstCheifComplaint")]
        public List<LstCheifComplaint> LstCheifComplaints { get; set; }

        [JsonProperty("lstMedicalHistory")]
        public List<LstMedicalHistory> LstMedicalHistories { get; set; }

        [JsonProperty("lstMedicine")]
        public List<LstMedicine> LstMedicines { get; set; }

        [JsonProperty("lstProcedure")]
        public List<LstProcedure> LstProcedures { get; set; }

        [JsonProperty("lstPhysicalExam")]
        public List<LstPhysicalExam> LstPhysicalExams { get; set; }

        [JsonProperty("lstTreatmentPlan")]
        public List<LstTreatmentPlan> LstTreatmentPlans { get; set; }

        [JsonProperty("lstVitalSign")]
        public List<LstVitalSign> ListVitalSigns { get; set; }

    }

    public class ERData
    {
        [JsonProperty("lstErDischargeSummary")]
        public List<LstErDischargeSummary> LstErDischargeSummaries { get; set; }

        [JsonProperty("consulations")]
        public List<Consulation> Consulations { get; set; }

        [JsonProperty("labOrders")]
        public List<LabOrder> LabOrders { get; set; }

        [JsonProperty("radReports")]
        public List<RadReport> RadReports { get; set; }

        [JsonProperty("admissions")]
        public List<Admission> Admissions { get; set; }
    }

    #endregion

    #endregion

    public class PatientFile
    {
        // Skipping Timelines

        [JsonProperty("labOrders")]
        public List<LabOrder> LabOrders { get; set; }

        [JsonProperty("admissions")]
        public List<Admission> Admissions { get; set; }

        [JsonProperty("radReports")]
        public List<RadReport> RadReports { get; set; }

        [JsonProperty("clinicalSummary")]
        public ClinicalSummary ClinicalSummary { get; set; }

        [JsonProperty("doctorWisePatientEpisodes")]
        public List<DoctorWisePatientEpisode> DoctorWisePatientEpisodes { get; set; }

        [JsonProperty("procedures")]
        public List<Procedure> Procedures { get; set; }

        [JsonProperty("lstAllEpisodes")]
        public List<LstAllEpisode> LstAllEpisodes { get; set; }

        [JsonProperty("erData")]
        public ERData ERData { get; set; }

    }

}
