﻿using Newtonsoft.Json;
using System.Linq;
using System.Collections.Generic;

namespace Common.DTOs.EMR.Patient
{
    public class PatientResponseMessage : VidaValidationErrors
    {
        [JsonProperty("isValid")]
        public bool isValid { get; set; }

        [JsonProperty("patientMRN")]
        public int PatientMRN { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        /// Error messages specific to the Patient API
        /// </summary>
        [JsonProperty("patientValidationErrors")]
        public List<PatientValidationError> PatientValidationErrors { get; set; }

        /// <summary>
        /// Validation errors represented in a single string format.
        /// </summary>
        public string RawPatientValidationErrors
        {
            get
            {
                const string NEWLINE = "\n";
                return PatientValidationErrors?
                    .Select(e => e.ErrorMessage)
                    .Aggregate((i, j) => i + NEWLINE + j);            
            }
        }

        public class PatientValidationError
        {
            [JsonProperty("errorCode")]
            public string ErrorCode { get; set; }

            [JsonProperty("errorMessage")]
            public string ErrorMessage { get; set; }
        }


    }
}
