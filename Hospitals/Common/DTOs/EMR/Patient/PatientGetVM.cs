﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.EMR.Patient
{
    public class PatientGetVM
    {
        [JsonProperty("patientId")]
        public int PatientMRN { get; set; }

        [JsonProperty("firstName")]
        public string Firstname { get; set; }

        [JsonProperty("middleName")]
        public string Middlename { get; set; }

        [JsonProperty("lastName")]
        public string Lastname { get; set; }

        [JsonProperty("firstNameN")]
        public string FirstnameN { get; set; }

        [JsonProperty("middleNameN")]
        public string MiddlenameN { get; set; }

        [JsonProperty("lastNameN")]
        public string LastnameN { get; set; }

        [JsonProperty("fullName")]
        public string Fullname { get; set; }

        [JsonProperty("fullNameN")]
        public string FullnameN { get; set; }

        [JsonProperty("gender")]
        public int Gender { get; set; }

        [JsonProperty("age")]
        public string Age { get; set; }

        [JsonProperty("dob")]
        public DateTimeOffset? DoB { get; set; }

        [JsonProperty("dobHijri")]
        public DateTimeOffset? DoB_Hijri { get; set; }

        [JsonProperty("bloodGroup")]
        public string BloodGroup { get; set; }

        [JsonProperty("rhfactor")]
        public string RhFactor { get; set; }

        [JsonProperty("phoneResi")]
        public string PhoneResidence { get; set; }

        [JsonProperty("phoneOffice")]
        public string PhoneOffice { get; set; }

        [JsonProperty("mobileNumber")]
        public string MobileNumber { get; set; }

        [JsonProperty("faxNumber")]
        public string FaxNumber { get; set; }

        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("nationalityId")]
        public string NationalityID { get; set; }

        [JsonProperty("nationalityName")]
        public string NationalityName { get; set; }

        [JsonProperty("areaID")]
        public string AreaID { get; set; }

        [JsonProperty("countryID")]
        public string CountryID { get; set; }

        [JsonProperty("cityID")]
        public string CityID { get; set; }

        [JsonProperty("area")]
        public string Area { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }
    }
}
