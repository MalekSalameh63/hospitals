﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.EMR.Patient
{
    public class PatientSearch
    {
        [JsonProperty("pageNumber")]
        public long PageNumber { get; set; }

        [JsonProperty("pageSize")]
        public long PageSize { get; set; }

        [JsonProperty("firstPage")]
        public Uri FirstPage { get; set; }

        [JsonProperty("lastPage")]
        public Uri LastPage { get; set; }

        [JsonProperty("pageUrl")]
        public Uri PageUrl { get; set; }

        [JsonProperty("totalPages")]
        public long TotalPages { get; set; }

        [JsonProperty("rowCount")]
        public long RowCount { get; set; }

        [JsonProperty("nextPage")]
        public object NextPage { get; set; }

        [JsonProperty("previousPage")]
        public object PreviousPage { get; set; }

        [JsonProperty("entityList")]
        public List<EntityList> EntityList { get; set; }
    }

    public partial class EntityList
    {
        [JsonProperty("patientMRN")]
        public long PatientMrn { get; set; }

        [JsonProperty("patientName")]
        public string PatientName { get; set; }

        [JsonProperty("mobileNumber")]
        public string MobileNumber { get; set; }
    }
}
