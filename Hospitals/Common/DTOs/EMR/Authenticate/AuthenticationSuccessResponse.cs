﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.EMR.Authenticate
{
    public class AuthenticationSuccessResponse
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("expiry_date")]
        public DateTimeOffset ExpiryDate { get; set; }
    }
}
