﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.Authenticate
{
    public class AuthenticateRequestDTO
    {
        [JsonProperty("userId")]
        public long UserId { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("facilityGroupId")]
        public string FacilityGroupId { get; set; }

        [JsonProperty("facilityId")]
        public int FacilityId { get; set; }
    }
}
