﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.Authenticate
{
    public class PasswordChangeDTO
    {
        [JsonProperty("oldPassword")]
        public string OldPassword { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("confirmPassword")]
        public string ConfirmPassword { get; set; }
    }
}
