﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace Common.DTOs.EMR.Authenticate
{
    public class MemberInformation : VidaValidationErrors
    {
        [JsonProperty("userId")]
        public long UserId { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("usernamearabic")]
        public string Usernamearabic { get; set; }

        [JsonProperty("employeeId")]
        public long EmployeeId { get; set; }

        [JsonProperty("preferredLanguage")]
        public string PreferredLanguage { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("doctorId")]
        public long DoctorId { get; set; }

        [JsonProperty("roles")]
        public List<MemberRole> Roles { get; set; }

        [JsonProperty("clinics")]
        public List<MemberClinic> Clinics { get; set; }

        [JsonProperty("passwordValidTo")]
        public DateTimeOffset PasswordValidTo { get; set; }

        [JsonProperty("accountValidTo")]
        public DateTimeOffset AccountValidTo { get; set; }

        [JsonProperty("activeSession")]
        public long ActiveSession { get; set; }
    }

    public class MemberClinic
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("defaultClinic")]
        public bool DefaultClinic { get; set; }
    }

    public class MemberRole
    {
        [JsonProperty("roleId")]
        public long RoleId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
