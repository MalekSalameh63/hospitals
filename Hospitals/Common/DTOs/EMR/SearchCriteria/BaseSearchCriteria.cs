﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs.EMR
{
    public class BaseMedicalRecordCriteria
    {
        [JsonProperty("patientMRN")]
        public long? PatientMRN { get; set; }

        [JsonProperty("appointmentNo")]
        public long? AppointmentNo { get; set; }

        [JsonProperty("episodeId")]
        public long? EpisodeId { get; set; }

        [JsonProperty("dateFrom")]
        public DateTime? DateFrom { get; set; }

        [JsonProperty("dateTo")]
        public DateTime? DateTo { get; set; }

        [JsonProperty("clinicId")]
        public long? ClinicId { get; set; }

        [JsonProperty("doctorId")]
        public long? DoctorId { get; set; }

        public BaseMedicalRecordCriteria(long? patientMRN = null, long? appointmentNo = null, long? episodeId = null,
            DateTime? dateFrom = null, DateTime? dateTo = null, long? clinicId = null, long? doctorId = null)
        {
            PatientMRN = patientMRN;
            AppointmentNo = appointmentNo;
            EpisodeId = episodeId;
            DateFrom = dateFrom;
            DateTo = dateTo;
            ClinicId = clinicId;
            DoctorId = doctorId;
        }

        public Dictionary<string, object> GenerateDictionary()
        {
            return new Dictionary<string, object>()
            {
                ["patientMRN"] = this.PatientMRN,
                ["appointmentNo"] = this.AppointmentNo,
                ["episodeId"] = this.EpisodeId,
                ["dateFrom"] = this.DateFrom?.ToString("yyyy-MM-dd"),
                ["dateTo"] = this.DateTo?.ToString("yyyy-MM-dd"),
                ["clinicId"] = this.ClinicId,
                ["doctorId"] = this.DoctorId
            };
        }
    }
}
