﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.Doctor
{
    public class DoctorClinic
    {
        [JsonProperty("clinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("clinicName")]
        public string ClinicName { get; set; }

        [JsonProperty("isDefault")]
        public bool IsDefault { get; set; }

        [JsonProperty("active")]
        public bool Active { get; set; }

        public DoctorClinic(long clinicId, string clinicName, bool isDefault, bool active)
        {
            ClinicId    = clinicId;
            ClinicName  = clinicName;
            IsDefault   = isDefault;
            Active      = active;
        }
    }
}