﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.Doctor
{
    public class DoctorLite
    {
        [JsonProperty("doctorId")]
        public long DoctorId { get; set; }

        [JsonProperty("doctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("doctorNameN")]
        public string DoctorNameN { get; set; }
    }
}
