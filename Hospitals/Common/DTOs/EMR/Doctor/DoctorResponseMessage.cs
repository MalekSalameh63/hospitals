﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.DTOs.EMR.Doctor
{
    public class DoctorResponseMessage : VidaValidationErrors
    {
        [JsonProperty("isValid")]
        public bool IsValid { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        //[JsonProperty("errorList")]
        //public List<ValidationError> ErrorList { get; set; }
    }
}
