﻿using Newtonsoft.Json;
using System;
using System.Runtime.CompilerServices;

namespace Common.DTOs.EMR.Doctor
{
    public class RequisitionStatus
    {
        [JsonProperty("transactionNo")]
        public int TransactionNo { get; set; }

        // Nullable
        [JsonProperty("approvedbyName")]
        public string ApprovedByName { get; set; }

        /// <summary>
        /// ID of approver
        /// Nullable
        /// </summary>
        [JsonProperty("approvedby")]
        public int ApprovedBy { get; set; }

        // Nullable
        [JsonProperty("approvalDate")]
        public DateTimeOffset ApprovalDate { get; set; }

        // Nullable
        [JsonProperty("isApproved")]
        public bool isApproved { get; set; }

        // Nullable
        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        // Nullable
        [JsonProperty("groupName")]
        public string GroupName { get; set; }

        
    }
}
