﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.DTOs.EMR.Doctor
{
    public class RequisitionDetails
    {

        [JsonProperty("requisitionNo")]
        public int RequisitionNumber { get; set; }

        /// <summary>
        /// Requisition Type { OffTime = 1, Holiday = 2,ChangeOfSchedule = 3, 
        /// CloseSchedulePermanently = 4, NewSchedule = 5} 
        /// ref [GET] /api/Master/Data/{DoctorRequisitionType = 2013}
        /// </summary>
        [JsonProperty("requisitionType")]
        public int RequisitionType { get; set; }

        /// <summary>
        /// Clinic ID mapped to doctor
        /// </summary>
        [JsonProperty("clinicId")]
        public int ClinicID { get; set; }

        [JsonProperty("doctorId")]
        public int DoctorID { get; set; }

        [JsonProperty("dateTimeFrom")]
        public DateTimeOffset DateTimeFrom { get; set; }

        [JsonProperty("dateTimeTo")]
        public DateTimeOffset DateTimeTo { get; set; }

        [JsonProperty("date")]
        public DateTimeOffset Date { get; set; }

        /// <summary>
        /// Nullable
        /// Requisition reasonid based on Requisition Type - 
        /// ref - [GET] /api/Master/Data/({DoctorOffTime=18} or 
        /// {DoctorHolidayTime = 19} or 
        /// {DoctorChangeOfScheduleReason = 102} or 
        /// {DoctorCloseSchedulePermanentlyReason = 103}
        /// </summary>
        [JsonProperty("reasonId")]
        public int ReasonID { get; set; }

        /// <summary>
        /// Nullable
        /// Requisition Covering doctor id ref - [GET] /api/Doctor/CoveringDoctor
        /// </summary>
        [JsonProperty("coveringDoctorId")]
        public int CoveringDoctorID { get; set; }

        /// <summary>
        /// OffTimeRequisition Status - 2 active allow to edit
        /// </summary>
        [JsonProperty("status")]
        public int Status { get; set; }
    }
}
