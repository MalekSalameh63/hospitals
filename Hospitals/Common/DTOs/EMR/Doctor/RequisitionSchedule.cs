﻿using Common.Helper;
using Newtonsoft.Json;
using System;

namespace Common.DTOs.EMR.Doctor
{
    public class RequisitionSchedule
    {
        [JsonProperty("requisitionNo")]
        public int RequisitionNo { get; set; }

        /// <summary>
        /// Requisition Type { OffTime = 1, Holiday = 2,ChangeOfSchedule = 3, 
        /// CloseSchedulePermanently = 4, NewSchedule = 5} 
        /// ref [GET] /api/Master/Data/{DoctorRequisitionType = 2013}
        /// </summary>
        [JsonProperty("requisitionType")]
        public int RequisitionType { get; set; }

        /// <summary>
        /// WeekDayId { Code = 1, Description = "Sunday" } 
        /// { Code = 2, Description = "Monday" } 
        /// { Code = 3, Description = "Tuesday" } 
        /// { Code = 4, Description = "Wednesday" } 
        /// { Code = 5, Description = "Thursday" } 
        /// { Code = 6, Description = "Friday" } 
        /// { Code = 7, Description = "Saturday" } 
        /// ref [GET] /api/Master/Data/{WeekDaysValue = 2014}
        /// </summary>
        [JsonProperty("weekDayId")]
        public int WeekDayID { get; set; }

        /// <summary>
        /// ShiftId { Code = 1, Description = "SH-1" } 
        /// { Code = 2, Description = "SH-2" } 
        /// ref [GET] /api/Master/Data/{ShiftList = 2015}
        /// </summary>
        [JsonProperty("shiftId")]
        public int ShiftID { get; set; }

        // Nullable
        [JsonProperty("isOpen")]
        public bool isOpen { get; set; }

        [JsonProperty("timeFrom")]
        //[JsonConverter(typeof(TimeSpanConverter))]
        public TimeSpan? TimeFrom { get; set; }

        [JsonProperty("timeTo")]
        //[JsonConverter(typeof(TimeSpanConverter))]
        public TimeSpan? TimeTo { get; set; }
    }
}
