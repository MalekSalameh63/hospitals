﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.DTOs.EMR.Doctor
{ 
    public class Speciality
    {
        [JsonProperty("specialityID")]
        public long SpecialityId { get; set; }

        [JsonProperty("specialityName")]
        public string SpecialityName { get; set; }

        [JsonProperty("active")]
        public bool Active { get; set; }

        public Speciality(long specialityId, string specialityName, bool active)
        {
            SpecialityId = specialityId;
            SpecialityName = specialityName;
            Active = active;
        }
    }
}
