﻿using Common.DTOs.User;
using Common.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs.EMR.Doctor
{
    public class DoctorVM : VidaValidationErrors
    {
        [JsonProperty("doctorId")]
        public long DoctorId { get; set; }

        [JsonProperty("doctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("doctorNameArabic")]
        public string DoctorNameArabic { get; set; }

        [JsonProperty("shortName")]
        public string ShortName { get; set; }

        [JsonProperty("shortNameArabic")]
        public string ShortNameArabic { get; set; }

        [JsonProperty("nationalityId")]
        public string NationalityId { get; set; }

        [JsonProperty("maritalStatusID")]
        public long MaritalStatusId { get; set; }

        [JsonProperty("gender")]
        public long Gender { get; set; }

        [JsonProperty("dateofBirth")]
        public DateTimeOffset? DateofBirth { get; set; }

        [JsonProperty("physicianTypeID")]
        public long PhysicianTypeId { get; set; }

        [JsonProperty("employmentTypeID")]
        public long EmploymentTypeId { get; set; }

        [JsonProperty("joinDate")]
        public DateTimeOffset? JoinDate { get; set; }

        [JsonProperty("mobileNumber")]
        public string MobileNumber { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("idType")]
        public int? IdType { get; set; }

        [JsonProperty("idNumber")]
        public string IdNumber { get; set; }

        [JsonProperty("idExpiry")]
        public DateTimeOffset? IdExpiry { get; set; }

        [JsonProperty("profileInfo")]
        public string ProfileInfo { get; set; }

        [JsonProperty("profileInfoArabic")]
        public string ProfileInfoArabic { get; set; }

        [JsonProperty("clinics")]
        public List<DoctorClinic> Clinics { get; set; }

        [JsonProperty("speciality")]
        public List<Speciality> Speciality { get; set; }

        /// <summary>
        /// Nullable
        /// </summary>
        [JsonProperty("countryID")]
        public string CountryID { get; set; }

        [JsonProperty("cityID")]
        public string CityID { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        public DoctorVM()
        { }

        /// <summary>
        /// Assign PostgreSQL user to DoctorVM.
        /// </summary>
        /// <param name="user"></param>
        public DoctorVM(UserResponseDto user)
        {
            DoctorId = user.DoctorEMRNo ?? -1;
            DoctorName = user.FirstName + " " + user.SecondName + " " + user.LastName;
            DoctorNameArabic = user.FirstName + " " + user.SecondName + " " + user.LastName;
            ShortName = user.FirstName + " " + user.LastName;
            ShortNameArabic = user.FirstName + " " + user.LastName;

            // This country must be an ISO 3 Country Code.
            NationalityId = user.Nationality;

            // Default to "single" status.
            MaritalStatusId = user.MaritalStatus ?? 1;
            Gender = user.Gender ?? 1;
            DateofBirth = DateHelper.GetDate(user.DateOfBirth.Value);

            // 1 = "Consultant", 2 = "Specialist"
            PhysicianTypeId = 1;

            // 1 = "Permanent", 2 = "Part Timer"
            EmploymentTypeId = 1;
            JoinDate = DateTime.Now;
            MobileNumber = user.PhoneNumber;
            Email = user.Email;

            // DEV-NOTE: This needs to be reviewed. If we do not need ID validation then
            // hardcoding to 3 should be fine. Otherwise, we will need to update PostgreSQL users
            // and set up validation error handling from the user side.
            // 1 = "Saudi National", 2 = "IQamaNo", 3 = "PassportNo"
            IdType = 3;
            IdNumber = user.NationalId;

            // DEV-NOTE: We do not capture ID expiration dates yet. For now, I'll set the ID 
            // expiration dates to 5 years from their joining date. We will amend this later.
            IdExpiry = new DateTime(DateTime.Now.Year + 5, 1, 1, 0, 0, 0);
            ProfileInfo = "Virtual care doctor with 5 years experience";
            ProfileInfoArabic = "Virtual care doctor with 5 years experience";

            // Only ClinicID is needed. The other fields in clinic are NOT mandatory. VIDA will autofill them.
            Clinics = new List<DoctorClinic>();
            Clinics.Add(new DoctorClinic(user.ClinicID ?? default, "", default, true));

            // Only SpecialityID is needed. The other fields in specialties are NOT mandatory. VIDA will autofill them.
            Speciality = new List<Speciality>();
            foreach (var i in user.Specialties)
            {
                Speciality.Add(new Speciality(i, default, true));
            }
        }
    }
}
