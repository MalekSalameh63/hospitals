﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs.EMR
{
    public class VIDAEntityList<T> : VidaValidationErrors
    {
        [JsonProperty("rowCount")]
        public int RowCount { get; set; }

        // Nullable
        [JsonProperty("entityList")]
        public List<T> EntityList { get; set; }

        public VIDAEntityList()
        { }

        public VIDAEntityList(List<T> entityList, int rowCount)
        {
            EntityList = entityList;
            RowCount = rowCount;
        }
    }
}
