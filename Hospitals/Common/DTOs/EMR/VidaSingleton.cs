﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR
{
    public class VidaSingleton<T> : VidaValidationErrors
    {
        [JsonProperty("data")]
        public T Data { get; set; }

        /// <summary>
        /// Depending on the API, VIDA will use either statusMessage or message. ヽ(ಠ_ಠ)ノ
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }

        //[JsonProperty("statusMessage")]
        //public string StatusMessage { get; set; }
    }
}
