﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Common.DTOs.EMR
{
    public class VidaValidationErrors
    {
        [JsonProperty("statusMessage")]
        public string StatusMessage { get; set; }

        /// <summary>
        /// This is a kludge. VIDA error handling is inconsistent. In the Procedures API, success is the field used instead of statusMessage.
        /// Once VIDA team has patched this, we can remove this field.
        /// </summary>
        [JsonProperty("success")]
        public string Success { set { StatusMessage = value; } }

        [JsonProperty("validationErrors")]
        public List<ValidationError> ValidationErrors { get; set; }

        public class ValidationError
        {
            [JsonProperty("fieldName")]
            public string FieldName { get; set; }

            [JsonProperty("messages")]
            public List<string> Messages { get; set; }
        }

        public string RawValidationErrors 
        { 
            get 
            {
                const string NEWLINE = "\n";
                return ValidationErrors?
                    .Select(e => e.Messages
                    .Aggregate((i, j) => i + NEWLINE + j))
                    .Aggregate((i, j) => i + NEWLINE + j);
            }
        }

        public VidaValidationErrors()
        { }
    }
}
