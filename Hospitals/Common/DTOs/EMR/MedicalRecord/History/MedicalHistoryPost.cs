﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.EMR
{ 
    public class MedicalHistoryPost
    {
        [JsonProperty("patientMRN")]
        public long PatientMRN { get; set; }

        /// <summary>
        /// History Type ID for 36-Family, 37-Medical, 38-Social, 39-Sports, 61-Infection, 66-Surgical
        /// </summary>
        [JsonProperty("historyType")]
        public long HistoryType { get; set; }

        /// <summary>
        /// History ID corresponding to history type : [GET] /api/master/data/{HistoryType}
        /// </summary>
        [JsonProperty("historyId")]
        public long HistoryId { get; set; }

        [JsonProperty("episodeId")]
        public long EpisodeId { get; set; }

        [JsonProperty("appointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("isChecked")]
        public bool IsChecked { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }
    }
}
