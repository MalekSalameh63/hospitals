﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.EMR
{ 
    public class MedicalHistory
    {
        [JsonProperty("patientMRN")]
        public long PatientMrn { get; set; }

        [JsonProperty("episodeID")]
        public long EpisodeId { get; set; }

        [JsonProperty("appointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("isChecked")]
        public bool IsChecked { get; set; }

        [JsonProperty("historyType")]
        public long HistoryType { get; set; }

        [JsonProperty("historyTypeName")]
        public string HistoryTypeName { get; set; }

        [JsonProperty("historyId")]
        public long HistoryId { get; set; }

        [JsonProperty("historyName")]
        public string HistoryName { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("createdBy")]
        public long CreatedBy { get; set; }

        [JsonProperty("createdOn")]
        public DateTimeOffset CreatedOn { get; set; }

        [JsonProperty("createdByName")]
        public string CreatedByName { get; set; }

        [JsonProperty("editedOn")]
        public DateTimeOffset EditedOn { get; set; }

        [JsonProperty("editedByName")]
        public string EditedByName { get; set; }

    }
}
