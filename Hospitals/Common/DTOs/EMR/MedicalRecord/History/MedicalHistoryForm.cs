﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs.EMR
{
    public class MedicalHistoryForm
    {
        [JsonProperty("is_update")]
        public bool IsUpdate { get; set; }

        [JsonProperty("listMedicalHistoryVM")]
        public List<MedicalHistoryPost> MedicalHistories { get; set; }
    }
}
