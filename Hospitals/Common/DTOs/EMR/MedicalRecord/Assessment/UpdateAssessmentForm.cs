﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.MedicalRecord.Assessment
{
    public class UpdateAssessmentForm
    {
        [JsonProperty("patientMRN")]
        public long PatientMRN { get; set; }

        [JsonProperty("appointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("episodeId")]
        public long EpisodeId { get; set; }

        [JsonProperty("icdcode10Id")]
        public string Icdcode10Id { get; set; }

        [JsonProperty("prevIcdCode10ID")]
        public string PrevIcdCode10Id { get; set; }

        [JsonProperty("conditionId")]
        public long ConditionId { get; set; }

        [JsonProperty("diagnosisTypeId")]
        public long DiagnosisTypeId { get; set; }

        [JsonProperty("complexDiagnosis")]
        public bool ComplexDiagnosis { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }
}
