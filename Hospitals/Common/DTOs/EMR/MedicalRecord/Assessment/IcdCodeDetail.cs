﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.MedicalRecord.Assessment
{
    public class IcdCodeDetail
    {
        [JsonProperty("icdcode10Id")]
        public string Icdcode10Id { get; set; }

        /// <summary>
        /// Found in /api/Emr/EMRLookups/{34}
        /// </summary>
        [JsonProperty("conditionId")]
        public long ConditionId { get; set; }

        /// <summary>
        /// Found in /api/Emr/EMRLookups/{35}
        /// </summary>
        [JsonProperty("diagnosisTypeId")]
        public long DiagnosisTypeId { get; set; }

        [JsonProperty("complexDiagnosis")]
        public bool ComplexDiagnosis { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }
    }
}
