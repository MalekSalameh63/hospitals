﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.EMR.MedicalRecord.Assessment
{
    public class Assessment
    {
        [JsonProperty("icdCode10ID")]
        public string IcdCode10Id { get; set; }

        [JsonProperty("appointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("episodeId")]
        public long EpisodeId { get; set; }

        [JsonProperty("patientMRN")]
        public long PatientMrn { get; set; }

        [JsonProperty("conditionID")]
        public long ConditionId { get; set; }

        [JsonProperty("diagnosisTypeID")]
        public long DiagnosisTypeId { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("complexDiagnosis")]
        public bool ComplexDiagnosis { get; set; }

        [JsonProperty("createdBy")]
        public long CreatedBy { get; set; }

        [JsonProperty("createdByName")]
        public string CreatedByName { get; set; }

        [JsonProperty("createdOn")]
        public DateTimeOffset CreatedOn { get; set; }

        [JsonProperty("ascii_Desc")]
        public string AsciiDesc { get; set; }

        [JsonProperty("doctorID")]
        public long DoctorId { get; set; }

        [JsonProperty("clinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("doctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("clinicDescription")]
        public string ClinicDescription { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }
}
