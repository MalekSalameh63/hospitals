﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.MedicalRecord.Procedure
{
    public class ProcedureCategoryResponse
    {
        [JsonProperty("categoryId")]
        public string CategoryID { get; set; }

        [JsonProperty("categoryName")]
        public string CategoryName { get; set; }
    }
}
