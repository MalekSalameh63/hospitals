﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Common.DTOs.EMR.MedicalRecord.Procedure
{
    public class ProcedureEditRequest
    {
        /// <summary>
        /// Order number from the /api/medicalrecord/orderedprocedures.
        /// </summary>
        [JsonProperty("orderNo")]
        public int OrderNo { get; set; }

        /// <summary>
        /// Item No can be found in the api/medicalrecord/orderedprocedures.
        /// </summary>
        [JsonProperty("lineItemNo")]
        public int LineItemNo { get; set; }

        [Required]
        [JsonProperty("patientMRN")]
        public int PatientMRN { get; set; }

        [Required]
        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [Required]
        [JsonProperty("episodeNo")]
        public int EpisodeNo { get; set; }

        [JsonProperty("procedures")]
        public ProcedureEditDetails Procedure { get; set; }

        public class ProcedureEditDetails
        {
            [JsonProperty("Procedure")]
            public string ProcedureID { get; set; }

            [JsonProperty("Category")]
            public string CategoryID { get; set; }

            [JsonProperty("controls")]
            public ProcedureEditControls[] Controls { get; set; }
        }

        public class ProcedureEditControls
        {
            [JsonProperty("code")]
            public string Code { get; set; }

            [JsonProperty("controlValue")]
            public string ControlValue { get; set; }
        }
    }
}
