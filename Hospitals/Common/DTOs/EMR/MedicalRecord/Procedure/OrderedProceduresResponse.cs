﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.EMR.MedicalRecord.Procedure
{
    public class OrderedProceduresResponse
    {
        [JsonProperty("orderNo")]
        public int OrderNo { get; set; }

        [JsonProperty("orderDate")]
        public DateTimeOffset orderDate { get; set; }

        [JsonProperty("lineItemNo")]
        public int LineItemNo { get; set; }

        [JsonProperty("procedureId")]
        public string ProcedureID { get; set; }

        [JsonProperty("procedureName")]
        public string ProcedureName { get; set; }

        [JsonProperty("doctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("clinicDescription")]
        public string ClinicDescription { get; set; }

        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [JsonProperty("appointmentDate")]
        public DateTimeOffset AppointmentDate { get; set; }

        [JsonProperty("isInvoiced")]
        public bool IsInvoiced { get; set; }

        [JsonProperty("isApprovalCreated")]
        public bool IsApprovalCreated { get; set; }

        [JsonProperty("isReferralInvoiced")]
        public bool IsReferralInvoiced { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("isCovered")]
        public bool IsCovered { get; set; }

        [JsonProperty("isApprovalRequired")]
        public bool IsApprovalRequired { get; set; }

        [JsonProperty("orderType")]
        public int OrderType { get; set; }

        [JsonProperty("isUncoveredByDoctor")]
        public bool IsUncoveredByDoctor { get; set; }

        [JsonProperty("cptCode")]
        public string CptCode { get; set; }

        [JsonProperty("achiCode")]
        public string AchiCode { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        /// <summary>
        /// Yes, this is supposed to be an (int) according to VIDA.
        /// </summary>
        [JsonProperty("createdBy")]
        public int CreatedBy { get; set; }

        [JsonProperty("template")]
        public string Template { get; set; }

        [JsonProperty("categoryID")]
        public string CategoryID { get; set; }

    }
}
