﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.MedicalRecord.Procedure
{
    public class ProcedureValidation
    {
        [JsonProperty("procedureId")]
        public int ProcedureID { get; set; }

        [JsonProperty("warningMessages")]
        public string[] WarningMessages { get; set; }

    }
}
