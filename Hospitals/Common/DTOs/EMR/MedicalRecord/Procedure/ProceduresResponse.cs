﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.MedicalRecord.Procedure
{
    public class ProceduresResponse
    {
        [JsonProperty("procedureId")]
        public string ProcedureID { get; set; }

        [JsonProperty("procedureName")]
        public string ProcedureName { get; set; }

        [JsonProperty("category")]
        public string Category { get; set; }

        [JsonProperty("group")]
        public string Group { get; set; }

        [JsonProperty("subGroup")]
        public string SubGroup { get; set; }

        [JsonProperty("categoryID")]
        public string CategoryID { get; set; }

        [JsonProperty("specialPermission")]
        public string SpecialPermission { get; set; }

        [JsonProperty("allowedClinic")]
        public bool AllowedClinic { get; set; }

        [JsonProperty("genderValidation")]
        public string GenderValidation { get; set; }

        [JsonProperty("orderedValidation")]
        public string OrderedValidation { get; set; }

        [JsonProperty("template")]
        public string Template { get; set; }

        [JsonProperty("price")]
        public float Price { get; set; }
    }
}
