﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.MedicalRecord.Procedure
{
    public class ValidateProceduresRequest
    {
        [JsonProperty("patientMRN")]
        public int PatientMRN { get; set; }

        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [JsonProperty("episodeId")]
        public int EpisodeId { get; set; }

        /// <summary>
        /// Procedure ID.
        /// </summary>
        [JsonProperty("procedures")]
        public string[] Procedures { get; set; }
    }
}
