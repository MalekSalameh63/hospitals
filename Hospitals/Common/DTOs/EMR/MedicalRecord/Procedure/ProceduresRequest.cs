﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Common.DTOs.EMR.MedicalRecord.Procedure
{
    public class ProceduresRequest
    {
        [JsonProperty("is_update")]
        public bool IsUpdate { get; set; }

        [Required]
        [JsonProperty("patientMRN")]
        public int PatientMRN { get; set; }

        [Required]
        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [Required]
        [JsonProperty("episodeNo")]
        public int EpisodeNo { get; set; }

        [JsonProperty("procedures")]
        public List<ProcedureDetails> Procedures { get; set; }

        public class ProcedureDetails
        {
            [JsonProperty("Procedure")]
            public string ProcedureID { get; set; }

            [JsonProperty("Category")]
            public string CategoryID { get; set; }

            [JsonProperty("controls")]
            public ProcedureControls[] Controls { get; set; }
        }

        public class ProcedureControls
        {
            [JsonProperty("code")]
            public string Code { get; set; }

            [JsonProperty("controlValue")]
            public string ControlValue { get; set; }
        }
    }
}
