﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR
{
    /// <summary>
    /// Chief complaint class mapped to VIDA for POST/PUT requests.
    /// </summary>
    public class EMRChiefComplaintForm
    {
        [JsonProperty("is_update")]
        public bool IsUpdate { get; set; }

        [JsonProperty("appointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("episodeId")]
        public long EpisodeId { get; set; }

        [JsonProperty("patientMRN")]
        public long PatientMRN { get; set; }

        [JsonProperty("chiefComplaint")]
        public string ChiefComplaint { get; set; }

        [JsonProperty("hopi")]
        public string Hopi { get; set; }

        [JsonProperty("currentMedication")]
        public string CurrentMedication { get; set; }

        [JsonProperty("ispregnant")]
        public bool Ispregnant { get; set; }

        [JsonProperty("isLactation")]
        public bool IsLactation { get; set; }

        [JsonProperty("numberOfWeeks")]
        public long NumberOfWeeks { get; set; }
    }
}
