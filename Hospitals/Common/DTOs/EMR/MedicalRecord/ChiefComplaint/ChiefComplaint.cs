﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.EMR
{
    /// <summary>
    /// Chief complaint class mapped to VIDA for GET requests.
    /// </summary>
    public class EMRChiefComplaint
    {
        [JsonProperty("appointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("episodeId")]
        public long EpisodeId { get; set; }

        [JsonProperty("patientMRN")]
        public long PatientMRN { get; set; }

        [JsonProperty("ccdate")]
        public DateTimeOffset? ChiefComplaintDate { get; set; }

        [JsonProperty("chiefComplaint")]
        public string ChiefComplaint { get; set; }

        [JsonProperty("hopi")]
        public string Hopi { get; set; }

        [JsonProperty("currentMedication")]
        public string CurrentMedication { get; set; }

        [JsonProperty("ispregnant")]
        public bool Ispregnant { get; set; }

        [JsonProperty("isLactation")]
        public bool IsLactation { get; set; }

        [JsonProperty("numberOfWeeks")]
        public long NumberOfWeeks { get; set; }

        [JsonProperty("doctorID")]
        public long DoctorId { get; set; }

        [JsonProperty("doctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("clinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("clinicDescription")]
        public string ClinicDescription { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }
    }
}
