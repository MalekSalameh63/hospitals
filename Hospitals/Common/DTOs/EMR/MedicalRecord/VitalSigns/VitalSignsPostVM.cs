﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR
{
    public class VitalSignsPostVM
    {

        [JsonProperty("episodeId")]
        public int EpisodeID { get; set; }

        [JsonProperty("patientMRN")]
        public long PatientMrn { get; set; }

        [JsonProperty("appointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("weightKg")]
        public double WeightKg { get; set; }

        [JsonProperty("heightCm")]
        public double HeightCm { get; set; }

        [JsonProperty("bodyMassIndex")]
        public double BodyMassIndex { get; set; }

        [JsonProperty("headCircumCm")]
        public double HeadCircumCm { get; set; }

        [JsonProperty("leanBodyWeightLbs")]
        public double LeanBodyWeightLbs { get; set; }

        [JsonProperty("idealBodyWeightLbs")]
        public double IdealBodyWeightLbs { get; set; }

        [JsonProperty("temperatureCelcius")]
        public double TemperatureCelcius { get; set; }

        [JsonProperty("temperatureCelciusMethod")]
        public long TemperatureCelciusMethod { get; set; }

        [JsonProperty("pulseBeatPerMinute")]
        public long PulseBeatPerMinute { get; set; }

        [JsonProperty("pulseRhythm")]
        public long PulseRhythm { get; set; }

        [JsonProperty("respirationBeatPerMinute")]
        public double RespirationBeatPerMinute { get; set; }

        [JsonProperty("respirationPattern")]
        public long RespirationPattern { get; set; }

        [JsonProperty("bloodPressureLower")]
        public long BloodPressureLower { get; set; }

        [JsonProperty("bloodPressureHigher")]
        public long BloodPressureHigher { get; set; }

        [JsonProperty("bloodPressureCuffLocation")]
        public long BloodPressureCuffLocation { get; set; }

        [JsonProperty("bloodPressureCuffSize")]
        public double BloodPressureCuffSize { get; set; }

        [JsonProperty("bloodPressurePatientPosition")]
        public long BloodPressurePatientPosition { get; set; }

        [JsonProperty("sao2")]
        public long Sao2 { get; set; }

        [JsonProperty("fio2")]
        public long Fio2 { get; set; }

        [JsonProperty("painScore")]
        public long PainScore { get; set; }

        [JsonProperty("painLocation")]
        public string PainLocation { get; set; }

        [JsonProperty("painDuration")]
        public string PainDuration { get; set; }

        [JsonProperty("painCharacter")]
        public string PainCharacter { get; set; }

        [JsonProperty("painFrequency")]
        public string PainFrequency { get; set; }

        [JsonProperty("isPainManagementDone")]
        public bool IsPainManagementDone { get; set; }

        [JsonProperty("gcscore")]
        public long Gcscore { get; set; }

        [JsonProperty("painPolicyNo")]
        public long PainPolicyNo { get; set; }

        [JsonProperty("painScale")]
        public long PainScale { get; set; }

        [JsonProperty("consciousness")]
        public string Consciousness { get; set; }

        [JsonProperty("skinColor")]
        public string SkinColor { get; set; }

        [JsonProperty("waistSizeInch")]
        public double WaistSizeInch { get; set; }

        [JsonProperty("nurseComment")]
        public string NurseComment { get; set; }

        [JsonProperty("doctorComment")]
        public string DoctorComment { get; set; }

        [JsonProperty("memberId")]
        public long MemberID { get; set; }

        [JsonProperty("isVitalsRequired")]
        public bool IsVitalsRequired { get; set; }
    }
}
