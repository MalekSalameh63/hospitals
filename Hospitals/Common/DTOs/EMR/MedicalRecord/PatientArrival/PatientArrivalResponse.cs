﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.EMR.MedicalRecord.PatientArrival
{
    public class PatientArrivalResponse
    {
        /// <summary>
        /// Patient's medical record number.
        /// </summary>
        [JsonProperty("patientMRN")]
        public int PatientMRN { get; set; }

        /// <summary>
        /// Patient's full name.
        /// </summary>
        [JsonProperty("patientName")]
        public string PatientName { get; set; }

        /// <summary>
        /// Apppointment date in the format YYYY-MM-DD.
        /// </summary>
        [JsonProperty("appointmentDate")]
        public DateTimeOffset AppointmentDate { get; set; }

        /// <summary>
        /// Regular or Walkin or Waiting
        /// </summary>
        [JsonProperty("appointmentType")]
        public string AppointmentType { get; set; }

        /// <summary>
        /// Format: HH:mm
        /// </summary>
        [JsonProperty("startTime")]
        public string StartTime { get; set; }

        /// <summary>
        /// Format: HH:mm
        /// </summary>
        [JsonProperty("endTime")]
        public string EndTime { get; set; }

        /// <summary>
        /// Age in the format: years, months, days.
        /// </summary>
        [JsonProperty("age")]
        public string Age { get; set; }

        [JsonProperty("mobileNumber")]
        public string MobileNumber { get; set; }

        /// <summary>
        /// Arrival datetime.
        /// </summary>
        [JsonProperty("arrivedOn")]
        public DateTimeOffset ArrivedOn { get; set; }

        /// <summary>
        /// Example values: 
        /// Initial Consultation,
        /// Follow up,
        /// Consultation,
        /// etc...
        /// </summary>
        [JsonProperty("visitType")]
        public string VisitType { get; set; }

        /// <summary>
        /// Insurance company of a patient as registered in EMR.
        /// </summary>
        [JsonProperty("companyName")]
        public string CompanyName { get; set; }

        /// <summary>
        /// ISO-3 country ID for patient.
        /// </summary>
        [JsonProperty("nationality")]
        public string Nationality { get; set; }

        /// <summary>
        /// Appointment number for patient.
        /// </summary>
        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        /// <summary>
        /// Not sure honestly... ¯\_(ツ)_/¯
        /// </summary>
        [JsonProperty("fallRiskScore")]
        public int FallRiskScore { get; set; }

        [JsonProperty("medicationOrders")]
        public int MedicationOrders { get; set; }

        /// <summary>
        /// Episode linked with the appointment.
        /// If returned as 0, call [POST[/api/medicalrecord/episode]
        /// to link.
        /// </summary>
        [JsonProperty("episodeNo")]
        public int EpisodeNo { get; set; }

        [JsonProperty("isSigned")]
        public bool IsSigned { get; set; }

        [JsonProperty("clinicName")]
        public string ClinicName { get; set; }

        [JsonProperty("clinicID")]
        public int ClinicID { get; set; }
    }
}
