﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.EMR.MedicalRecord.PatientArrival
{
    public class PatientArrivalResponseListPaged : VIDAEntityList<PatientArrivalResponse>
    {
        /// <summary>
        /// Page number for request. First request begins at 1.
        /// </summary>
        [JsonProperty("pageNumber")]
        public int PageNumber { get; set; }

        /// <summary>
        /// Size of page returned.
        /// </summary>
        [JsonProperty("pageSize")]
        public int PageSize { get; set; }

        /// <summary>
        /// URI for the first page.
        /// </summary>
        [JsonProperty("firstPage")]
        public string FirstPage { get; set; }

        /// <summary>
        /// URI for the last page.
        /// </summary>
        [JsonProperty("lastPage")]
        public string LastPage { get; set; }

        /// <summary>
        /// Paging URL to replace "$pageIndex$" parameter to 
        /// be replaced with expected page number data to return.
        /// </summary>
        [JsonProperty("pageUrl")]
        public string PageUrl { get; set; }

        /// <summary>
        /// Total number of records in result.
        /// </summary>
        [JsonProperty("totalPages")]
        public int TotalPages { get; set; }

        /// <summary>
        /// URI for next page.
        /// </summary>
        [JsonProperty("nextPage")]
        public string NextPage { get; set; }

        /// <summary>
        /// URI for previous page.
        /// </summary>
        [JsonProperty("previousPage")]
        public string PreviousPage { get; set; }
    }
}
