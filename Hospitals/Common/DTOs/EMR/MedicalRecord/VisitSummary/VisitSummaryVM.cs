﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.DTOs.EMR.MedicalRecord.VisitSummary
{
    public class VisitSummaryVM
    {
        //
        // Patient demographic information
        //
        public int PatientMRN { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public DateTimeOffset DoB { get; set; }
        public int Gender { get; set; }
        public string GenderName 
        {
            get
            {
                return this.Gender switch
                {
                    1 => "Male",
                    2 => "Female",
                    _ => "N/A",
                };
            }
        }
        public string Nationality { get; set; }
        //public int NationalID { get; set; }
        public string PhoneNumber { get; set; }

        //
        // Encounter details
        //
        // Chief complaint info
        public List<EMRChiefComplaint> ChiefComplaint { get; set; }

        // Progress note info
        public List<ProgressNote> ProgressNote { get; set; }

        // History info
        public List<MedicalHistory> PatientHistories { get; set; }

        // Allergies info
        public List<Allergy> Allergies { get; set; }

        // Assessment/diagnosis info
        public List<Assessment.Assessment> Assessments { get; set; }

        // Procedures info
        public List<Procedure.OrderedProceduresResponse> Procedures { get; set; }

        // Prescription info
        public List<Prescription.Prescription> Prescriptions { get; set; }

        //
        // Appointment details
        //
        public DateTimeOffset? AppointmentDate { get; set; }
        public string AppointmentType { get; set; }
        public string ClinicName { get; set; }
        public string DoctorName { get; set; }

        public VisitSummaryVM()
        { }
    }
}
