﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.EMR.MedicalRecord.Episode
{
    public class PatientEpisode
    {
        [JsonProperty("patientMRN")]
        public int PatientMRN { get; set; }

        [JsonProperty("episodeNo")]
        public int EpisodeNo { get; set; }

        [JsonProperty("episodeDate")]
        public DateTimeOffset EpisodeDate { get; set; }

        /// <summary>
        /// Doctor's ID
        /// </summary>
        [JsonProperty("createdBy")]
        public int CreatedBy { get; set; }

        [JsonProperty("createdOn")]
        public DateTimeOffset CreatedOn { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        /// <summary>
        /// Doctor's ID
        /// </summary>
        [JsonProperty("editedBy")]
        public int EditedBy { get; set; }

        [JsonProperty("editedOn")]
        public DateTimeOffset EditedOn { get; set; }

        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }
    }
}
