﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Common.DTOs.EMR.MedicalRecord.Episode
{
    public class EpisodePost
    {
        [JsonProperty("appointmentNo")]
        [Required]
        public long AppointmentNo { get; set; }

        [JsonProperty("patientMRN")]
        [Required]
        public long PatientMRN { get; set; }

        /// <summary>
        /// Required for nurses
        /// </summary>
        [JsonProperty("doctorId")]
        public long DoctorID { get; set; }
    }
}
