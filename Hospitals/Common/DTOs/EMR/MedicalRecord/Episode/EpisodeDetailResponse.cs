﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.EMR.MedicalRecord.Episode
{
    public class EpisodeDetailResponse
    {
        [JsonProperty("episodeId")]
        public int EpisodeId { get; set; }

        [JsonProperty("episodeDate")]
        public DateTimeOffset EpisodeDate { get; set; }

        // Nullable
        [JsonProperty("problem")]
        public string Problem { get; set; }

        // Nullable
        [JsonProperty("status")]
        public string Status { get; set; }

    }
}
