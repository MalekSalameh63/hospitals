﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs.EMR.MedicalRecord.Episode
{
    public class PatientEpisodeDetails
    {
        public class PatientAppointmentVM
        {
            /// <summary>
            /// Episode linked w/ appointment
            /// </summary>
            [JsonProperty("epsoideId")]
            public int EpisodeID { get; set; }

            [JsonProperty("appointmentNo")]
            public int AppointmentNo { get; set; }

            [JsonProperty("appointmentDate")]
            public DateTimeOffset AppointmentDate{ get; set; }

            [JsonProperty("startTime")]
            public string StartTime { get; set; }

            [JsonProperty("doctor")]
            public string Doctor { get; set; }

            [JsonProperty("clinic")]
            public string Clinic { get; set; }

            /// <summary>
            /// Status can be either: Draft/Signed
            /// </summary>
            [JsonProperty("status")]
            public string Status { get; set; }
        }

        /// <summary>
        /// Episode ID of a visit
        /// </summary>
        [JsonProperty("epsoideId")]
        public int EpisodeID { get; set; }

        /// <summary>
        /// Episode created date YYYY-MM-DD
        /// </summary>
        [JsonProperty("visitDate")]
        public DateTimeOffset VisitDate { get; set; }

        /// <summary>
        /// Doctor's name
        /// </summary>
        [JsonProperty("createdBy")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// YYYY-MM-DD HH:mm:ss
        /// </summary>
        [JsonProperty("createdOn")]
        public DateTimeOffset CreatedOn { get; set; }

        /// <summary>
        /// Draft / Signed
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        /// <summary>
        /// Episode created with the initial appointment number.
        /// </summary>
        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        [JsonProperty("appointments")]
        public List<PatientAppointmentVM> Appointments { get; set; }
    }
}
