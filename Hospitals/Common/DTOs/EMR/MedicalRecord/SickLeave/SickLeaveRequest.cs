﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Common.DTOs.EMR.MedicalRecord.SickLeave
{
    public class SickLeaveRequest
    {
        /// <summary>
        /// Patient Medical Record Number
        /// </summary>
        [JsonProperty("patientMRN")]
        [Required]
        public int PatientMRN { get; set; }

        /// <summary>
        /// Current visit Appointment Number to issue the sick leave
        /// </summary>
        [JsonProperty("appointmentNo")]
        [Required]
        public int AppointmentNo { get; set; }

        /// <summary>
        /// Sick Leave Start Date yyyy-MM-ddTHH:mm:ss
        /// </summary>
        [JsonProperty("startDate")]
        [Required]
        public DateTimeOffset StartDate { get; set; }

        /// <summary>
        /// Number of sick leave days
        /// </summary>
        [JsonProperty("noOfDays")]
        [Required]
        public int NoOfDays { get; set; }

        /// <summary>
        /// Remarks
        /// </summary>
        [JsonProperty("remarks")]
        public string Remarks { get; set; }
    }
}
