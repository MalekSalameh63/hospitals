﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.EMR.MedicalRecord.SickLeave
{
    public class SickLeaveResponse
    {
        [JsonProperty("patientMRN")]
        public long PatientMRN { get; set; }

        /// <summary>
        /// Leave request number
        /// </summary>
        [JsonProperty("requestNo")]
        public int RequestNo { get; set; }

        /// <summary>
        /// Appointment number of the sick leave
        /// </summary>
        [JsonProperty("appointmentNo")]
        public long AppointmentNo { get; set; }

        /// <summary>
        /// Date in format: yyyy-MM-ddTHH:mm:ss
        /// </summary>
        [JsonProperty("startDate")]
        public DateTimeOffset StartDate { get; set; }

        /// <summary>
        /// Number of days for sick leave
        /// </summary>
        [JsonProperty("noOfDays")]
        public long NoOfDays { get; set; }

        /// <summary>
        /// Remarks
        /// </summary>
        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("isExtendedLeave")]
        public bool IsExtendedLeave { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }
}
