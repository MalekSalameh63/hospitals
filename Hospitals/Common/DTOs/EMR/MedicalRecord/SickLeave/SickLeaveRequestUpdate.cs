﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.MedicalRecord.SickLeave
{
    public class SickLeaveRequestUpdate : SickLeaveRequest
    {
        /// <summary>
        /// Leave request number
        /// </summary>
        [JsonProperty("requestNo")]
        public int RequestNo { get; set; }

        [JsonProperty("isExtendedLeave")]
        public bool IsExtendedLeave { get; set; }
    }
}
