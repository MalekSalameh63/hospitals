﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Common.DTOs.EMR.MedicalRecord.SickLeave
{
    public class ExtendSickLeaveRequest
    {
        [JsonProperty("patientMRN")]
        [Required]
        public int PatientMRN { get; set; }
        
        /// <summary>
        /// Previous sick leave request to extend from
        /// </summary>
        [JsonProperty("previousRequestNo")]
        [Required]
        public int PreviousRequestNo { get; set; }

        /// <summary>
        /// Number of days for sick leave
        /// </summary>
        [JsonProperty("noOfDays")]
        [Required]
        public int NoOfDays { get; set; }

        /// <summary>
        /// Remarks
        /// </summary>
        [JsonProperty("remarks")]
        public string Remarks { get; set; }
    }
}
