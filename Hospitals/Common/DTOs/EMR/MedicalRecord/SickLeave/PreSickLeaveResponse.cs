﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.MedicalRecord.SickLeave
{
    public class PreSickLeaveResponse
    {
        /// <summary>
        /// Recommended Sick Leave Days for current visit documented Diagnosis
        /// Nullable
        /// </summary>
        [JsonProperty("recommendedSickLeaveDays")]
        public string RecommendedSickLeaveDays { get; set; }

        /// <summary>
        /// Total Sick Leaves Issued by current visit appointment Doctor in last 6 Months
        /// Nullable
        /// </summary>
        [JsonProperty("totalLeavesByDoctor")]
        public int TotalLeavesByDoctor { get; set; }

        /// <summary>
        /// Total Sick Leves Issues By All Clinics in last 6 Months
        /// Nullable
        /// </summary>
        [JsonProperty("totalLeaveByAllClinics")]
        public int TotalLeaveByAllClinics { get; set; }
    }
}
