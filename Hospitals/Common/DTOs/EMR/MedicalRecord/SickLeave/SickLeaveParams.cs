﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Common.DTOs.EMR.MedicalRecord.SickLeave
{
    public class SickLeaveParams
    {
        [JsonProperty("patientMRN")]
        [Required]
        public int PatientMRN { get; set; }

        /// <summary>
        /// Appointment number of the sick leave request
        /// </summary>
        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }

        /// <summary>
        /// Values --> 1: Hold, 2: Active, 0: All
        /// </summary>
        [JsonProperty("status")]
        public int Status { get; set; }

        /// <summary>
        /// Date in format: yyyy-MM-ddTHH:mm:ss
        /// </summary>
        [JsonProperty("fromDate")]
        public DateTimeOffset? FromDate { get; set; }

        /// <summary>
        /// Date in format: yyyy-MM-ddTHH:mm:ss
        /// </summary>
        [JsonProperty("toDate")]
        public DateTimeOffset? ToDate { get; set; }
    }
}
