﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs.EMR.MedicalRecord.Prescription
{
    public class PrescriptionForm
    {
        [JsonProperty("is_update")]
        public bool IsUpdate { get; set; }

        [JsonProperty("clinicId")]
        public long ClinicId { get; set; }

        [JsonProperty("epsoideNo")]
        public long EpsoideNo { get; set; }

        [JsonProperty("appointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("patientMRN")]
        public long PatientMRN { get; set; }

        [JsonProperty("medicines")]
        public List<Medicine> Medicines { get; set; }
    }
}
