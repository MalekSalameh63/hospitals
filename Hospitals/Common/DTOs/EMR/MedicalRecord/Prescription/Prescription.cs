﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.EMR.MedicalRecord.Prescription
{
    public class Prescription
    {
        [JsonProperty("createdOn")]
        public DateTimeOffset CreatedOn { get; set; }

        [JsonProperty("medicationName")]
        public string MedicationName { get; set; }

        [JsonProperty("createdBy")]
        public string CreatedBy { get; set; }

        [JsonProperty("clinicName")]
        public string ClinicName { get; set; }

        [JsonProperty("medicineCode")]
        public long MedicineCode { get; set; }

        [JsonProperty("frequencyID")]
        public long FrequencyId { get; set; }

        [JsonProperty("doseDurationDays")]
        public long DoseDurationDays { get; set; }

        [JsonProperty("appointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("episodeID")]
        public long EpisodeId { get; set; }

        [JsonProperty("isSIG")]
        public bool IsSig { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("isMedicineCovered")]
        public bool IsMedicineCovered { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("doseDailyQuantity")]
        public long DoseDailyQuantity { get; set; }

        [JsonProperty("doseDailyUnitID")]
        public long DoseDailyUnitId { get; set; }

        [JsonProperty("doseTimingID")]
        public long DoseTimingId { get; set; }

        [JsonProperty("routeID")]
        public long RouteId { get; set; }

        [JsonProperty("qty")]
        public long Qty { get; set; }

        [JsonProperty("medicationPrice")]
        public long MedicationPrice { get; set; }

        [JsonProperty("uom")]
        public string Uom { get; set; }

        [JsonProperty("quantity")]
        public long Quantity { get; set; }

        [JsonProperty("doseDetail")]
        public string DoseDetail { get; set; }

        [JsonProperty("orderTypeDescription")]
        public string OrderTypeDescription { get; set; }

        [JsonProperty("startDate")]
        public DateTimeOffset StartDate { get; set; }

        [JsonProperty("isDispensed")]
        public bool IsDispensed { get; set; }

        [JsonProperty("indication")]
        public string Indication { get; set; }

        [JsonProperty("doctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("stopDate")]
        public DateTimeOffset StopDate { get; set; }
    }
}
