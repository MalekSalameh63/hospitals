﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.EMR.MedicalRecord.Prescription
{
    public class Medicine
    {
        [JsonProperty("itemId")]
        public long ItemId { get; set; }

        [JsonProperty("doseStartDate")]
        public DateTimeOffset DoseStartDate { get; set; }

        [JsonProperty("duration")]
        public long Duration { get; set; }

        [JsonProperty("dose")]
        public double Dose { get; set; }

        [JsonProperty("doseUnitId")]
        public long DoseUnitId { get; set; }

        [JsonProperty("route")]
        public long Route { get; set; }

        [JsonProperty("frequency")]
        public long Frequency { get; set; }

        [JsonProperty("doseTime")]
        public long DoseTime { get; set; }

        [JsonProperty("covered")]
        public bool Covered { get; set; }

        [JsonProperty("approvalRequired")]
        public bool ApprovalRequired { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("icdcode10Id")]
        public string Icdcode10Id { get; set; }
    }
}
