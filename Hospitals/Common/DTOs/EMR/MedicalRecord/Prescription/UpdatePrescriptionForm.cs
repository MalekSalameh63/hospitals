﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.MedicalRecord.Prescription
{
    public class UpdatePrescriptionForm
    {
        [JsonProperty("epsoideNo")]
        public long EpsoideNo { get; set; }

        [JsonProperty("appointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("patientMRN")]
        public long PatientMRN { get; set; }

        /// <summary>
        /// Not supposed to be a list. Single medicine.
        /// </summary>
        [JsonProperty("medicines")]
        public Medicine Medicines { get; set; }
    }
}
