﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.MedicalRecord.Medication
{
    public class MedicationItem
    {
        [JsonProperty("itemId")]
        public int ItemID { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("genericName")]
        public string GenericName { get; set; }

        [JsonProperty("keywords")]
        public string Keywords{ get; set; }

        [JsonProperty("quantity")]
        public float Quantity { get; set; }

        [JsonProperty("price")]
        public float Price { get; set; }
    }
}
