﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs.EMR.MedicalRecord.Medication
{
    public class MedicationProperties
    {
        public class MedProp
        {
            [JsonProperty("parameterCode")]
            public int ParameterCode { get; set; }

            [JsonProperty("description")]
            public string Description { get; set; }

            [JsonProperty("isDefault")]
            public bool IsDefault { get; set; }
        }

        [JsonProperty("routes")]
        public List<MedProp> Routes { get; set; }
        
        [JsonProperty("strengths")]
        public List<MedProp> Strengths { get; set; }

        [JsonProperty("frequencies")]
        public List<MedProp> Frequencies { get; set; }
    }
}
