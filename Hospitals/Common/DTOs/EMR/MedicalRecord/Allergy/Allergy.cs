﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.EMR
{
    public class Allergy
    {
        [JsonProperty("patientMRN")]
        public long PatientMRN { get; set; }

        /// <summary>
        /// Optional - It will be handled by API for POST / PUT request
        /// </summary>
        [JsonProperty("allergyDiseaseType")]
        public long AllergyDiseaseType { get; set; }

        /// <summary>
        /// Required for Saving Allergy, [GET]: /api/master/data/{11}
        /// </summary>
        [JsonProperty("allergyDiseaseId")]
        public long AllergyDiseaseId { get; set; }

        /// <summary>
        /// DEV-NOTE: Not needed by VIDA. This can be removed.
        /// </summary>
        [JsonProperty("allergyDiseaseName")]
        public string AllergyDiseaseName { get; set; }

        [JsonProperty("episodeId")]
        public long EpisodeId { get; set; }

        [JsonProperty("appointmentNo")]
        public long AppointmentNo { get; set; }

        /// <summary>
        /// Required for Saving Allergy, [GET]: /api/master/data/{55}
        /// </summary>
        [JsonProperty("severity")]
        public long Severity { get; set; }

        [JsonProperty("isChecked")]
        public bool IsChecked { get; set; }

        [JsonProperty("isUpdatedByNurse")]
        public bool IsUpdatedByNurse { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("createdBy")]
        public long CreatedBy { get; set; }

        [JsonProperty("createdOn")]
        public DateTimeOffset CreatedOn { get; set; }

        [JsonProperty("editedBy")]
        public long EditedBy { get; set; }

        [JsonProperty("editedOn")]
        public DateTimeOffset EditedOn { get; set; }

        /// <summary>
        /// DEV-NOTE: Not needed by VIDA. This can be removed.
        /// </summary>
        [JsonProperty("severityName")]
        public string SeverityName { get; set; }
    }
}
