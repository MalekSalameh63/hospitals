﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.EMR.MedicalRecord
{
    public class ProgressNote
    {
        [JsonProperty("appointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("episodeId")]
        public long EpisodeId { get; set; }

        [JsonProperty("patientMRN")]
        public long PatientMrn { get; set; }

        [JsonProperty("planNote")]
        public string PlanNote { get; set; }

        [JsonProperty("mName")]
        public string MName { get; set; }

        [JsonProperty("dName")]
        public string DName { get; set; }

        [JsonProperty("createdBy")]
        public long CreatedBy { get; set; }

        [JsonProperty("createdByName")]
        public string CreatedByName { get; set; }

        [JsonProperty("createdOn")]
        public DateTimeOffset CreatedOn { get; set; }

        [JsonProperty("editedByName")]
        public string EditedByName { get; set; }

        [JsonProperty("editedOn")]
        public DateTimeOffset EditedOn { get; set; }
    }
}
