﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.EMR.MedicalRecord
{
    public class ProgressNoteForm
    {
        [JsonProperty("is_update")]
        public bool IsUpdate { get; set; }

        [JsonProperty("appointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("episodeId")]
        public long EpisodeId { get; set; }

        [JsonProperty("patientMRN")]
        public long PatientMrn { get; set; }

        [JsonProperty("planNote")]
        public string PlanNote { get; set; }

        [JsonProperty("mName")]
        public string MName { get; set; }

        [JsonProperty("dName")]
        public string DName { get; set; }
    }
}
