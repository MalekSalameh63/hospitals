﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.MedicalRecord.Other
{
    public class PreviousDiagnosisVM
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("doctorID")]
        public long DoctorId { get; set; }

        [JsonProperty("doctorName")]
        public string DoctorName { get; set; }

        [JsonProperty("clinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("clinicDescription")]
        public string ClinicDescription { get; set; }

        [JsonProperty("appointmentNo")]
        public long AppointmentNo { get; set; }

        [JsonProperty("episodeID")]
        public long EpisodeId { get; set; }

        [JsonProperty("conditionID")]
        public long ConditionId { get; set; }

        [JsonProperty("diagnosisTypeID")]
        public long DiagnosisTypeId { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("complexDiagnosis")]
        public bool ComplexDiagnosis { get; set; }
    }
}
