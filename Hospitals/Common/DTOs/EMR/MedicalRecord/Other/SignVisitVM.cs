﻿using Newtonsoft.Json;

namespace Common.DTOs.EMR.MedicalRecord.Other
{
    public class SignVisitVM
    {
        [JsonProperty("patientMRN")]
        public int PatientMRN { get; set; }

        [JsonProperty("appointmentNo")]
        public int AppointmentNo { get; set; }
    }
}
