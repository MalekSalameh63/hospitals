﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Common.DTOs.EMR.MedicalRecord.PhysicalExam
{
    /// <summary>
    /// Used in GET, POST, and PUT requests for the Physical Exam endpoint.
    /// </summary>
    public class PhysicalExaminationVM
    {
        public class PhysicalExamDetails
        {
            [JsonProperty("episodeId")]
            public long EpisodeId { get; set; }

            [JsonProperty("appointmentNo")]
            public long AppointmentNo { get; set; }

            /// <summary>
            /// To be passed as 59 for physical exam
            /// </summary>
            [JsonProperty("examType")]
            public long ExamType { get; set; }

            /// <summary>
            /// IDs found in api/master/data/59
            /// </summary>
            [JsonProperty("examId")]
            public long ExamId { get; set; }

            [JsonProperty("patientMRN")]
            public long PatientMrn { get; set; }

            [JsonProperty("isNormal")]
            public bool IsNormal { get; set; }

            [JsonProperty("isAbnormal")]
            public bool IsAbnormal { get; set; }

            [JsonProperty("notExamined")]
            public bool NotExamined { get; set; }

            /// <summary>
            /// Unneeded. This is determined by VIDA via the examId.
            /// </summary>
            [JsonProperty("examName")]
            public string ExamName { get; set; }

            /// <summary>
            /// 1 - Normal, 2 - Abnormal, 3 - Not Examined.
            /// </summary>
            [JsonProperty("examinationTypeName")]
            public string ExaminationTypeName { get; set; }

            /// <summary>
            /// 1 - Normal, 2 - Abnormal, 3 - Not Examined.
            /// </summary>
            [JsonProperty("examinationType")]
            public long ExaminationType { get; set; }

            [JsonProperty("remarks")]
            public string Remarks { get; set; }

            /// <summary>
            /// Ignore for POST/PUT, VIDA will handle it.
            /// </summary>
            [JsonProperty("isNew")]
            public bool IsNew { get; set; }

            [JsonProperty("createdBy")]
            public long? CreatedBy { get; set; }

            [JsonProperty("createdOn")]
            public DateTimeOffset CreatedOn { get; set; }

            [JsonProperty("createdByName")]
            public string CreatedByName { get; set; }

            [JsonProperty("editedBy")]
            public long? EditedBy { get; set; }

            [JsonProperty("editedOn")]
            public DateTimeOffset EditedOn { get; set; }

            [JsonProperty("editedByName")]
            public string EditedByName { get; set; }
        }

        public List<PhysicalExamDetails> listHisProgNotePhysicalExaminationVM { get; set; }
    }
}
