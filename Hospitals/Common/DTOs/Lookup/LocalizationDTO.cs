﻿using Newtonsoft.Json;

namespace Common.DTOs.Lookup
{
    public class LocalizationDTO
    {
        [JsonProperty("language")]
        public string Language { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }
    }
}
