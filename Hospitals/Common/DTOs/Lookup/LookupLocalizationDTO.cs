﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Lookup
{
    public class LookupLocalizationDTO
    {
        [BsonId]
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("category_id")]
        public int CategoryId { get; set; }
        [JsonProperty("enum_reference")]
        public int EnumReference { get; set; }
        [JsonProperty("local_name")]
        public string LocalName { get; set; }
        [JsonProperty("localizations")]
        public List<LocalizationDTO> Localizations { get; set; }
    }
}
