﻿using Common.Common;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.DTOs
{
    public class EmailMessageDto
    {
        public EmailMessageDto()
        {
        }

        public EmailMessageDto(EmailMessage message)
        {
            Id = Guid.NewGuid();
            Subject = message.Subject;
            Body = message.Body;
            From = message.From;
            To = message.To.Select(x => x.EmailAddress).ToList();
            Cc = message.Cc.Select(x => x.EmailAddress).ToList();
            Attachments = message.Attachments.Select(x =>
                new Attachments() { Content = x.Content, Name = x.Name, MimeType = x.MimeType })
                .ToList();
            IsBodyHtml = message.IsBodyHtml;
            Type = message.Type;
        }
        [BsonId]
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string From { get; set; }
        public List<string> To { get; set; }
        public List<string> Cc { get; set; }
        public List<Attachments> Attachments { get; set; }
        public bool IsBodyHtml { get; set; }
        public int? Type { get; set; }
    }
}
