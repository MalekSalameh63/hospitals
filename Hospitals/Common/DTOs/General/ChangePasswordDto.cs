﻿using Newtonsoft.Json;

namespace Common.DTOs
{
    public class ChangePasswordDto
    {

        [JsonProperty("old_password")]
        public string OldPassword { set; get; }
        [JsonProperty("new_password")]
        public string NewPassword { get; set; }
        [JsonProperty("confirm_new_password")]
        public string ConfirmNewPassword { get; set; }
    }
}
