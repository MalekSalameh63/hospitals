﻿using Common.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class PatientQueueInfoDTO
    {
        public string PatientId { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
