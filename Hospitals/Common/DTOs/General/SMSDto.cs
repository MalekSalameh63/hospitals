﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class SMSDto
    {
        public string Message { get; set; }
        public string[] To { get; set; }
    }
}
