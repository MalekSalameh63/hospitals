﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class UserRolesDto
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public bool IsSelected { get; set; }
    }
}
