﻿using Common.Enums;

namespace Common.DTOs
{
    public class TransactionResponseDto
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public long? Date { get; set; }
        public string TransactionType { get; set; }
        public float Amount { get; set; }
        public string Currency { get; set; }
        public string PaymentMethod { get; set; }
        public float Balance { get; set; }
        public float Points { get; set; }
        public ServiceType ServiceType { get; set; }
        public string ServiceId { get; set; }


    }
}
