﻿using Common.Enums;

namespace Common.DTOs
{
    public class TransactionRequestDto
    {
        public float Amount { get; set; }
        public string UserId { get; set; }
        public string Currency { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public ServiceType ServiceType { get; set; }
        public string ServiceId { get; set; }
    }
}
