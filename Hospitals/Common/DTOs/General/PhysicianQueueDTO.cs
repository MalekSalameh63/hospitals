﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class PhysicianQueueDTO
    {
        [JsonProperty("physician_id")]
        public string PhysicianId { get; set; }
        public DateTime CreationDate { get; set; }

    }
}
