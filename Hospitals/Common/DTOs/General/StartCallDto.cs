﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class StartCallDto
    {
        public string SessionId { get; set; }
        public string TokenId { get; set; }
        public long PhysicianId { get; set; }
        public decimal Duration { get; set; }
        public decimal WarningTime { get; set; }
        public int Language { get; set; }
    }
}
