﻿using Common.Common;
using Common.Enums;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Common.DTOs
{
    public class ChiefComplaintDto
    {
        public ChiefComplaintDto()
        { }

        public ChiefComplaintDto(ChiefComplaint chiefComplaint)
        {
            Id = chiefComplaint.ComplaintId.Guid;
            Complaint = chiefComplaint.Complaint.TextValue;
            Tags = chiefComplaint.Tags;
            UserId = chiefComplaint.UserId.Guid.ToString();
            CreationDate = DateTime.Now;
            AppointmentNumber = chiefComplaint.AppointmentNumber;
            EpisodeNumber = chiefComplaint.EpisodeNumber;
            PhysicianId = PhysicianId;
            CallStart = chiefComplaint.CallStart;
            CallEnd = chiefComplaint.CallEnd;
            VisitType = chiefComplaint.VisitType;
            EndBy = chiefComplaint.EndBy;
        }

        [BsonId]
        public Guid Id { get; set; }
        [JsonProperty("complaint")]
        public string Complaint { get; set; }
        [JsonProperty("tags")]
        public List<string> Tags { get; set; }
        [JsonProperty("user_id")]
        public string UserId { get; set; }
        [JsonProperty("physician_id")]
        public string PhysicianId { get; set; }
        public long AppointmentNumber { get; set; }
        public long EpisodeNumber { get; set; }
        public VisitType VisitType { get; set; }
        public DateTime CallStart { get; set; }
        public DateTime CallEnd { get; set; }
        public UserType EndBy { get; set; }
        public DateTime CreationDate { get; set; }
        [JsonIgnore]
        public bool IsDeleted { get; set; }
    }
}

