﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class SSLCertificateDTO
    {
        [BsonId]
        public Guid Id { get; set; }
        public byte[] Certificate { get; set; }
        public string Password { get; set; }
        public bool Enabled { get; set; }
    }
}
