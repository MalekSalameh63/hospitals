﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Common.DTOs
{
    public class OtpVerificationDto
    {
        [BsonId]
        public Guid Id { get; set; }
        public string Otp { get; set; }
       
        public int? NumberOfSentOTP { get; set; }
        public string phone { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
