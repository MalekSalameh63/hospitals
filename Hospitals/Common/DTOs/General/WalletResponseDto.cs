﻿using Common.Common;
using System;

namespace Common.DTOs
{
    public class WalletResponseDto
    {
        public string UserId { get; set; }
        public Balance AvailableBalance { get; set; }
        public float Points { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}
