﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Common.DTOs
{
    public class SMSDetails
    {
        [Key]
        public Guid Id { get; set; }
        public string Message { get; set; }
        public string[] To { get; set; }
        public string Status { get; set; }
        public DateTime CreationDate { get; set; }
        protected SMSDetails()
        {

        }

        public SMSDetails(SMSDto smsDto)
        {
            Id = Guid.NewGuid();
            Message = smsDto.Message;
            To = smsDto.To;
            CreationDate = DateTime.Now;
            Status = GlobalStatus.Success.ToString();
        }

        public SMSDetails(IEnumerable<SMSDetails> sMSDetails)
        {
    
        }
    }
}
