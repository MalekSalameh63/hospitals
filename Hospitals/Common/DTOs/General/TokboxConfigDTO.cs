﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class TokboxConfigDTO
    {
        [JsonProperty("key_api_key")]
        public long KeyApiKey { get; set; }

        [JsonProperty("key_session_id")]
        public string KeySessionId { get; set; }

        [JsonProperty("key_token")]
        public string KeyToken { get; set; }

        [JsonProperty("call_duration")]
        public long CallDuration { get; set; }

        [JsonProperty("warning_duration")]
        public long WarningDuration { get; set; }
    }
}
