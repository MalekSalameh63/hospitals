﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class WebSocketMessageDTO
    {
        public string message { get; set; }
    }
}
