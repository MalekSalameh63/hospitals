﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class RequsetDto<T>
    {
        [JsonProperty("form_name")]
        public string FormName { get; set; }

        [JsonProperty("key")]
        public string key { get; set; }

        [JsonProperty("data")]
        public T Data { get; set; }
    }
}
