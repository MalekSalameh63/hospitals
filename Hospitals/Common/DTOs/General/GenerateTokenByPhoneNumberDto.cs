﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class GenerateTokenByPhoneNumberDto
    {
        public string PhoneNumber { get; set; }
    }
}
