﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class KafkaProducer
    {
        public string offset { get; set; }
        public string partition { get; set; }
    }
}
