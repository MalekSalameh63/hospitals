﻿using Newtonsoft.Json;

namespace Common.DTOs.PhysicianInfo
{
    public class PhysicianInfoDetailsDto
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("specialty")]
        public string Specialty { get; set; }
        [JsonProperty("language")]
        public string Language { get; set; }
        [JsonProperty("icon")]
        public string Icon { get; set; }
        [JsonProperty("rate_count")]
        public long RateCount { get; set; }
        [JsonProperty("appointment_title")]
        public string AppointmentsTitle { get; set; }
        [JsonProperty("appointments_interval")]
        public string AppointmentsInterval { get; set; }
        [JsonProperty("price")]
        public float Price { get; set; }
        [JsonProperty("clinic")]
        public int ClinicId { get; set; }
        [JsonProperty("project_id")]
        public int ProjectId { get; set; }
        [JsonProperty("interval_icon")]
        public string IntervalIcon { get; set; }
        [JsonProperty("currency")]
        public string Currency { get; set; }
        [JsonProperty("bio")]
        public string Bio { get; set; }

        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }
    }
}
