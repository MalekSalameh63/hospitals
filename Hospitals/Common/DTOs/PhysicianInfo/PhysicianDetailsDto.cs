﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs.PhysicianInfo
{
    public class PhysicianDetailsDto
    {
        [JsonProperty("details_list")]
        public List<PhysicianInfoDetailsListDto> DetailsList { get; set; }
        [JsonProperty("custom_theme")]
        public CustomThemeDTO CustomTheme { get; set; }

    }
}
