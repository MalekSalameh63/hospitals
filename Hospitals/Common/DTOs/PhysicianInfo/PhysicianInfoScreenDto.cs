﻿using Common.DTOs.Configuration.Shared;
using Newtonsoft.Json;

namespace Common.DTOs.PhysicianInfo
{
    public class PhysicianInfoScreenDto
    {
        [JsonProperty("physician_info")]
        public PhysicianInfoDetailsDto PhysicianInfoDetails { get; set; }
        [JsonProperty("physician_details")]
        public PhysicianDetailsDto PhysicianDetails { get; set; }
        [JsonProperty("submit_button")]
        public SubmitButtonDTO SubmitButton { get; set; }

    }
}
