﻿namespace Common.DTOs.PhysicianInfo
{
    public class PhysicianInfoDetailsListDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }

    }
}
