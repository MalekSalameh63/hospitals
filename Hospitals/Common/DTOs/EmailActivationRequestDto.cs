﻿using Common.Common;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.DTOs
{
    [Table("EmailActivationRequest")]
    public class EmailActivationRequestDto
    {
        public EmailActivationRequestDto()
        {

        }
        public EmailActivationRequestDto(EmailActivationRequest emailActivationRequest)
        {
            Email = emailActivationRequest.Email.ToString();
            Token = emailActivationRequest.Token.TextValue;
            CreatedOn = emailActivationRequest.CreatedOn;
            NumberOfSentEmails = emailActivationRequest.NumberOfSentEmails;
        }

        [BsonId]
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? NumberOfSentEmails { get; set; }
    }
}
