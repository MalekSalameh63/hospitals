﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class ChangePasswordForAdmin
    {
        public string UserId { get; set; }
        public string NewPassword { get; set; }
    }
}
