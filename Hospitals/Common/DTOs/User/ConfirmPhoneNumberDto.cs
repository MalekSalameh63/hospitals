﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class ConfirmPhoneNumberDto
    {
        public string UserId { get; set; }
        public string PhoneNumber { get; set; }
    }
}
