﻿using Newtonsoft.Json;

namespace Common.DTOs.User
{
    public class OtpValidateRequestDto
    {
        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("otp_mode")]
        public int Mode { get; set; }
        public string Otp { get; set; }
        public string phone { get; set; }
    }
}
