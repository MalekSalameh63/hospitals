﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Common.DTOs.User
{
    public class Feedback
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public int? UserType { get; set; }
        public string UserId { get; set; }
        public string PhoneNumber { get; set; }
        public string Subject { get; set; }
        public int? Rating { get; set; }
    }
}
