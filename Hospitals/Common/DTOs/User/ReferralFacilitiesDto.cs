﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Common.DTOs.User
{
    [Table("ReferralFacilities")]
    public class ReferralFacilitiesDto
    {
        [Key]
        [JsonProperty("facilityId_id")]
        public int FacilityId { get; set; }

        [JsonProperty("is_active")]
        public bool IsActive { get; set; }

        [JsonProperty("name_ar")]
        public string NameAr { get; set; }

        [JsonProperty("name_en")]
        public string NameEn { get; set; }
    }
}
