﻿using Common.DTOs.Questionnaires;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.DTOs.User
{
    public class PhysicianUserMapper
    {
        public static IEnumerable<UserResponseDto> MapPhysicians(IEnumerable<UserResponseDto> lstUserResponseDto, List<AverageRatingDto> Ratings)
        {
            foreach (var user in lstUserResponseDto)
            {
                user.RatingAverage = GetPhysicianRatingAverage(Ratings.Where(r => r.RatingFor == user.UserId).ToList());
            }
            return lstUserResponseDto;
        }
        public static UserResponseDto MapPhysician(UserResponseDto userResponseDto, List<AverageRatingDto> ratings)
        {
            userResponseDto.RatingAverage = GetPhysicianRatingAverage(ratings.ToList());
            return userResponseDto;
        }
        public static decimal GetPhysicianRatingAverage(List<AverageRatingDto> Ratings)
        {
            if (Ratings.Count() > 0)
            {
                var NumberofOneAnswers = Ratings.Where(r => r.Answer == 1).Count();
                var NumberofTwoAnswers = Ratings.Where(r => r.Answer == 2).Count();
                var NumberofThreeAnswers = Ratings.Where(r => r.Answer == 3).Count();
                var NumberofFourAnswers = Ratings.Where(r => r.Answer == 4).Count();
                var NumberofFiveAnswers = Ratings.Where(r => r.Answer == 5).Count();
                decimal R = Convert.ToDecimal(1 * NumberofOneAnswers + 2 * NumberofTwoAnswers + 3 * NumberofThreeAnswers + 4 * NumberofFourAnswers + 5 * NumberofFiveAnswers) / Convert.ToDecimal(Ratings.Count());
                return R;
            }
            else
            {
                return 0;
            }
        }
    }
}
