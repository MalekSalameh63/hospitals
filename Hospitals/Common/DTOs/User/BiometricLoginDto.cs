﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class BiometricLoginDto
    {
        [JsonProperty("biometrics_token")]
        public string Token { get; set; }

        [JsonProperty("device_id")]
        public string DeviceId { get; set; }

        [JsonProperty("notification_id")]
        public string NotificationToken { get; set; }
    }
}
