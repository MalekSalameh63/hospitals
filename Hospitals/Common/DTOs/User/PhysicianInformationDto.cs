﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.DTOs.File;
using Newtonsoft.Json;

namespace Common.DTOs.User
{
    public class PhysicianInformationDto
    {
        [JsonProperty("license_number")]
        public string LicenseNumber { get; set; }
        [JsonProperty("license_expiry")]
        public long? LicenseExpiry { get; set; }

        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("files")]
        public List<DocumentDTO> Files { get; set; }

        [JsonProperty("rank")]
        public int? Rank { get; set; }

        [JsonProperty("qualification")]
        public string Qualification { get; set; }

        [JsonProperty("speciality")]
        public List<int> Specialties { get; set; }

        [JsonProperty("languages")]
        public List<int> Languages { get; set; }

        [JsonProperty("bio")]
        public string Bio { get; set; }

        [JsonProperty("clinic")]
        public int? ClinicID { get; set; }
    }
}
