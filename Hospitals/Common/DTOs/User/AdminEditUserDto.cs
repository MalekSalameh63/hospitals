﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class AdminEditUserDto
    {
        [JsonProperty("user_id")]
        public string UserId { get; set; }
        [JsonProperty("security_group_id")]
        public int? SecurityGroupId { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("second_name")]
        public string SecondName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        [JsonProperty("phone")]
        public string PhoneNumber { get; set; }
        [JsonProperty("dateofbirth")]
        public long? DateOfBirth { get; set; }
        [JsonProperty("gender")]
        public int? Gender { get; set; }
        [JsonProperty("national_id")]
        public string NationalId { get; set; }
        [JsonProperty("user_name")]
        public string UserName { get; set; }
        [JsonProperty("user_type")]
        public int? UserType { get; set; }
        [JsonProperty("image")]
        public string ProfilePicture { get; set; }
        [JsonProperty("country_code")]
        public string CountryCode { get; set; }
        [JsonProperty("address")]
        public UserAddressDto userAddressDto { get; set; }
    }
}
