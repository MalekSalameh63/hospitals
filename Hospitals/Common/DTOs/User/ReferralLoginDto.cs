﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class ReferralLoginDto
    {
        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("user_name")]
        public string UserName { get; set; }
    }
}
