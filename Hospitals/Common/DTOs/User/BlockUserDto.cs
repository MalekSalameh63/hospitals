﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class BlockUserDto
    {
        public string UserId { get; set; }

        [JsonProperty("value")]
        public bool Value { get; set; }
        public string Message { get; set; }
        public int TemplateId { get; set; }
    }
}
