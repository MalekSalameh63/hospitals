﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;

namespace Common.DTOs
{
    public class LoginDto
    {
        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("device_id")]
        public string DeviceId { get; set; }
        [JsonProperty("notification_id")]
        public string NotificationToken { get; set; }
        [JsonProperty("user_name")]
        public string UserName { get; set; }
        [JsonProperty("remember_me")]
        public bool RememberMe { get; set; }
        [JsonProperty("biometrics_enabled")]
        public bool BiometricsEnabled { get; set; }
        [JsonIgnore]
        public string Page { get; set; }

        [JsonProperty("ui_language")]
        public string UILanguage { get; set; }
    }
}
