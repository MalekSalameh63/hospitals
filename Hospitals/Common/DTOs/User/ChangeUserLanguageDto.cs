﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class ChangeUserLanguageDto
    {
        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("ui_language")]
        public string UILanguage { get; set; }
    }
}
