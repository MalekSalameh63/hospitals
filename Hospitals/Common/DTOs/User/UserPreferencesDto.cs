﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class UserPreferencesDto
    {
        [JsonProperty("preferred_languages")]
        public List<int> PreferredLanguages { get; set; }

        [JsonProperty("preferred_gender")]
        public int? PreferredGender { get; set; }

        [JsonProperty("user_id")]
        public string UserId { get; set; }
    }
}
