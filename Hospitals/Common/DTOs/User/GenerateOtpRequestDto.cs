﻿using Newtonsoft.Json;

namespace Common.DTOs.User
{
    public class GenerateOtpRequestDto
    {
        [JsonProperty("country_code")]
        public string CountryCode { get; set; }
        public string phone { get; set; }

        [JsonProperty("otp_mode")]      
        public int Mode { get; set; }
    }
}
