﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class BusinessValidationDto
    {
        public string UserId { get; set; }
        public string Value { get; set; }
        public string Key { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("user_type")]
        public int UserType { get; set; }
    }
}
