﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs.User
{
    public class UserResponseDto
    {
        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("middle_name")]
        public string SecondName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("phone")]
        public string PhoneNumber { get; set; }

        [JsonProperty("dateofbirth")]
        public long? DateOfBirth { get; set; }

        [JsonProperty("zip_code")]
        public string ZipCode { get; set; }

        [JsonProperty("gender")]
        public int? Gender { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("area")]
        public string Area { get; set; }

        [JsonProperty("Address")]
        public string Address { get; set; }

        [JsonProperty("national_id")]
        public string NationalId { get; set; }

        public string Captcha { get; set; }

        [JsonProperty("user_name")]
        public string UserName { get; set; }

        [JsonProperty("user_type")]
        public int? UserType { get; set; }

        public string ProfessionTitle { get; set; }

        public string Bio { get; set; }

        public List<int> Specialties { get; set; }

        public List<int> Languages { get; set; }

        public string QualificationTitle { get; set; }

        public string QualificationImages { get; set; }

        [JsonProperty("preferred_languages")]
        public List<int> PreferredLanguages { get; set; }

        [JsonProperty("preferred_gender")]
        public int? PreferredGender { get; set; }

        [JsonProperty("image")]
        public byte[] ProfilePicture { get; set; }

        public bool IsApproved { get; set; }

        public bool IsBlocked { get; set; }

        [JsonProperty("profile_completion")]
        public double ProfileCompletion { get; set; }

        [JsonProperty("additional_info")]
        public string AdditionalInfo { get; set; }

        [JsonProperty("mrn")]
        public string MRN { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("other_phone_number")]
        public string OtherPhoneNumber { get; set; }

        [JsonProperty("experience")]
        public int? Experience { get; set; }

        [JsonProperty("email_confirmed")]
        public bool EmailConfirmed { get; set; }

        [JsonProperty("other_country_code")]
        public string OtherCountryCode { get; set; }

        [JsonProperty("remember_me")]
        public bool RememberMe { get; set; }

        [JsonProperty("biometrics_enabled")]
        public bool BiometricsEnabled { get; set; }

        [JsonProperty("biometrics_token")]
        public string BiometricsToken { get; set; }

        [JsonProperty("rank")]
        public int? Rank { get; set; }

        [JsonProperty("rating_average")]
        public decimal RatingAverage { get; set; }

        [JsonProperty("doctorEMRNo")]
        public long? DoctorEMRNo { get; set; }

        [JsonProperty("ui_language")]
        public string UILanguage { get; set; }

        [JsonProperty("clinic")]
        public int? ClinicID { get; set; }

        [JsonProperty("marital_status")]
        public int? MaritalStatus { get; set; }
        [JsonProperty("nationality")]
        public string Nationality { get; set; }

        [JsonProperty("emergency_name")]
        public string EmergencyName { get; set; }

        [JsonProperty("emergency_phone_number")]
        public string EmergencyPhoneNumber { get; set; }

        [JsonProperty("emergency_contact_country_code")]
        public string EmergencyCountryCode { get; set; }

        [JsonProperty("emergency_contact_relationship")]
        public int? EmergencyContactRelationship { get; set; }

        [JsonProperty("is_referral")]
        public bool IsReferral { get; set; }

        [JsonProperty("physician_status")]
        public int? PhysicianStatus { get; set; }
        [JsonProperty("license_number")]
        public string LicenseNumber { get; set; }
        [JsonProperty("license_expiry")]
        public long? LicenseExpiry { get; set; }

        [JsonProperty("facilityId")]
        public int? FacilityId { get; set; }

        [JsonProperty("facilityGroupId")]
        public string FacilityGroupId { get; set; }
    }
}
