﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class ContactsDto
    {
        [JsonProperty("id")]
        public string UserId { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("second_name")]
        public string SecondName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("image")]
        public byte[] ProfilePicture { get; set; }

        [JsonProperty("phone")]
        public string PhoneNumber { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("user_name")]
        public string UserName { get; set; }

        [JsonProperty("birthdate")]
        public long? DateOfBirth { get; set; }

        [JsonProperty("user_type")]
        public int? UserType { get; set; }

        [JsonProperty("firebase_token")]
        public string FireBaseToken { get; set; }
        public bool IsApproved { get; set; }
        public bool IsBlocked { get; set; }
        [JsonProperty("gender")]
        public int? Gender { get; set; }
        [JsonProperty("ui_language")]
        public string UILanguage { get; set; }
        public int? PhysicianStatus { get; set; }
    }
}
