﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class HistoryLogsDto
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public int? SubType { get; set; }
        public string Description { get; set; }
        public long CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string TransactionNum { get; set; }
        public string CreatedByName { get; set; }
    }
}
