﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class UserSettingsDto
    {
        public int SettingId { get; set; }
        public bool IsEnabled { get; set; }
    }
}
