﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class AuthorizationDto
    {
        public int Id { get; set; }
        public string Action { get; set; }
        public string SubAction { get; set; }
        public string Controller { get; set; }
        public int? UserType { get; set; }
        public string UserId { get; set; }
        public bool Enabled { get; set; }
    }
}
