﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class ReferralUserDto
    {
        [JsonProperty("user_id")]
        public string UserId { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("second_name")]
        public string SecondName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        [JsonProperty("phone")]
        public string PhoneNumber { get; set; }
        [JsonProperty("dateofbirth")]
        public long? DateOfBirth { get; set; }
        [JsonProperty("gender")]
        public int? Gender { get; set; }
        [JsonProperty("national_id")]
        public string NationalId { get; set; }
        [JsonProperty("user_name")]
        public string UserName { get; set; }
        [JsonProperty("user_type_id")]
        public int? UserType { get; set; }
        [JsonProperty("image")]
        public string ProfilePicture { get; set; }
        [JsonProperty("country_code")]
        public string CountryCode { get; set; }
        [JsonProperty("address")]
        public UserAddressDto userAddressDto { get; set; }
        [JsonProperty("facility_id")]
        public int? FacilityId { get; set; }
        [JsonProperty("is_active")]
        public bool IsActive { get; set; }
    }
}
