﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class UserSettingsRequest
    {
        public List<UserSettingsDto> UserSettings { get; set; }
        public string UserId { get; set; }
    }
}
