﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class ContactUsTemplateDto
    {
        public string Message { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNumber { get; set; }
    }
}
