﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class ReferralUserResponseDto
    {
        [JsonProperty("user_id")]
        public string UserId { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("second_name")]
        public string SecondName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        [JsonProperty("phone")]
        public string PhoneNumber { get; set; }
        [JsonProperty("dateofbirth")]
        public long? DateOfBirth { get; set; }
        [JsonProperty("gender")]
        public int? Gender { get; set; }
        [JsonProperty("national_id")]
        public string NationalId { get; set; }
        [JsonProperty("user_name")]
        public string UserName { get; set; }
        [JsonProperty("user_type")]
        public int? UserType { get; set; }
        [JsonProperty("image")]
        public byte[] ProfilePicture { get; set; }
        [JsonProperty("country_code")]
        public string CountryCode { get; set; }
        [JsonProperty("address")]
        public UserAddressDto userAddressDto { get; set; }
        [JsonProperty("token")]
        public string Token { get; set; }
        [JsonProperty("zip_code")]
        public string ZipCode { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("area")]
        public string Area { get; set; }
        [JsonProperty("Address")]
        public string Address { get; set; }
        [JsonProperty("facility_id")]
        public int? FacilityId { get; set; }
        [JsonProperty("facility_name_en")]
        public string FacilityNameEn { get; set; }
    }
}
