﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class UserAddressDto
    {
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("zip_code")]
        public string ZipCode { get; set; }
        public string Area { get; set; }
        public string Address { get; set; }
        public decimal latitude { get; set; }
        public decimal longitude { get; set; }
    }
}
