﻿namespace Common.DTOs.User
{
    public class ApprovedPhysicianDto
    {
        public string[] UserId { get; set; }
        public int Value { get; set; }
        public string Message { get; set; }
        public int TemplateId { get; set; }
        public string Description { get; set; }
    }
}
