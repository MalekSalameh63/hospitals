﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class UpdateProfilePictureDto
    {
        public string UserId { get; set; }
        public byte[] ProfilePicture { get; set; }
    }
}
