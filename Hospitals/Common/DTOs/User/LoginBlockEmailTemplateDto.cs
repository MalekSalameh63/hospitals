﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class LoginBlockEmailTemplateDto
    {
        public string UserName { get; set; }
        public string EmailAccount { get; set; }
    }
}
