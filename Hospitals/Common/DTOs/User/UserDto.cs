﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs
{
    public class UserDto
    {
        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("second_name")]
        public string SecondName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("phone")]
        public string PhoneNumber { get; set; }

        [JsonProperty("dateofbirth")]
        public long? DateOfBirth { get; set; }

        [JsonProperty("gender")]
        public int? Gender { get; set; }

        [JsonProperty("national_id")]
        public string NationalId { get; set; }

        public string Captcha { get; set; }

        [JsonProperty("user_name")]
        public string UserName { get; set; }

        [JsonProperty("user_type")]
        public int? UserType { get; set; }

        [JsonProperty("professionTitle")]
        public string ProfessionTitle { get; set; }

        [JsonProperty("bio")]
        public string Bio { get; set; }

        public List<int> Specialties { get; set; }

        public List<int> Languages { get; set; }

        [JsonProperty("qualificationTitle")]
        public string QualificationTitle { get; set; }

        public string QualificationImages { get; set; }

        [JsonProperty("preferred_languages")]
        public List<int> PreferredLanguages { get; set; }

        [JsonProperty("preferred_gender")]
        public int PreferredGender { get; set; }

        [JsonProperty("image")]
        public byte[] ProfilePicture { get; set; }

        public bool IsApproved { get; set; }

        public bool IsBlocked { get; set; }

        [JsonProperty("additional_info")]
        public string AdditionalInfo { get; set; }

        [JsonProperty("mrn")]
        public string MRN { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("device_id")]
        public string DeviceId { get; set; }

        [JsonProperty("notification_id")]
        public string NotificationToken { get; set; }

        [JsonProperty("other_phone_number")]
        public string OtherPhoneNumber { get; set; }

        [JsonProperty("experience")]
        public int? Experience { get; set; }

        [JsonProperty("other_country_code")]
        public string OtherCountryCode { get; set; }

        [JsonProperty("rank")]
        public int? Rank { get; set; }

        [JsonProperty("doctorEMRNo")]
        public long? DoctorEMRNo { get; set; }

        [JsonProperty("ui_language")]
        public string UILanguage { get; set; }

        [JsonProperty("clinic")]
        public int? ClinicID { get; set; }

        [JsonProperty("marital_status")]
        public int? MaritalStatus { get; set; }

        [JsonProperty("nationality")]
        public string Nationality { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("zip_code")]
        public string ZipCode { get; set; }

        public string Area { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        public decimal latitude { get; set; }

        public decimal longitude { get; set; }

        [JsonProperty("emergency_name")]
        public string EmergencyName { get; set; }

        [JsonProperty("emergency_phone_number")]
        public string EmergencyPhoneNumber { get; set; }

        [JsonProperty("emergency_contact_country_code")]
        public string EmergencyCountryCode { get; set; }

        [JsonProperty("emergency_contact_relationship")]
        public int? EmergencyContactRelationship { get; set; }

        [JsonProperty("license_number")]
        public string LicenseNumber { get; set; }

        [JsonProperty("license_expiry")]
        public long? LicenseExpiry { get; set; }

        [JsonProperty("physician_status")]
        public int? PhysicianStatus { get; set; }

        /// <summary>
        /// This should be left blank. It will be handled by the backend.
        /// </summary>
        [JsonProperty("facilityId")]
        public int? FacilityId { get; set; }

        /// <summary>
        /// This should be left blank. It will be handled by the backend.
        /// </summary>
        [JsonProperty("facilityGroupId")]
        public string FacilityGroupId { get; set; }
    }
}
