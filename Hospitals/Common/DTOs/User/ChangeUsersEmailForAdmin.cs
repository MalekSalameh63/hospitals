﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class ChangeUsersEmailForAdmin
    {
        public string UserId { get; set; }
        public string NewEmail { get; set; }
    }
}
