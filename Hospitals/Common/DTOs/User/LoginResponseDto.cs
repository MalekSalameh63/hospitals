﻿using Newtonsoft.Json;

namespace Common.DTOs.User
{
    public class LoginResponseDto
    {
        [JsonProperty("user_id")]
        public string UserId { get; set; }
        [JsonProperty("user_name")]
        public string UserName { get; set; }
        [JsonProperty("user_type")]
        public int? UserType { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("middle_name")]
        public string SecondName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        [JsonProperty("image")]
        public byte[] ProfilePicture { get; set; }
        [JsonProperty("token")]
        public string Token { get; set; }
        [JsonProperty("remember_me")]
        public bool RememberMe { get; set; }
        [JsonProperty("biometrics_enabled")]
        public bool BiometricsEnabled { get; set; }
        [JsonProperty("biometrics_token")]
        public string BiometricsToken { get; set; }
        [JsonProperty("is_new_device")]
        public bool IsNewDevice { get; set; }
        [JsonProperty("old_device_firebase_token")]
        public string OldDeviceFirebaseToken { get; set; }
        [JsonProperty("Mrn")]
        public string Mrn { get; set; }
        [JsonProperty("doctor_id")]
        public long? DoctorId { get; set; }
        [JsonProperty("clinic")]
        public int? ClinicId { get; set; }
        [JsonProperty("ui_language")]
        public string UILanguage { get; set; }

        [JsonProperty("EMRToken")]
        public string EMRToken { get; set; }
    }
}
