﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.User
{
    public class RoleDto
    {
        public int UserType { get; set; }
        public string DescAr { get; set; }
        public string DescEn { get; set; }
        public bool IsActive { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
    }
}
