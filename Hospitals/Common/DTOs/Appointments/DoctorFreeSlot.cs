﻿using Newtonsoft.Json;

namespace Common.DTOs.Appointments
{
    public class DoctorFreeSlot
    {
        [JsonProperty("slot")]
        public long Slot { get; set; }
    }
}
