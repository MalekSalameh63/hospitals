﻿using System.Collections.Generic;

namespace Common.DTOs.Appointments
{
    public class DoctorFreeSlotsResponseDto
    {
        public List<DoctorFreeSlot> DoctorFreeSlotsLists { get; set; }

    }
}
