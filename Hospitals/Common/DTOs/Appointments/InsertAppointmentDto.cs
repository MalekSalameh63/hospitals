﻿using Newtonsoft.Json;

namespace Common.DTOs.Appointments
{
    public class InsertAppointmentDto
    {

        [JsonProperty("clinic_id")]
        public long ClinicId { get; set; }

        [JsonProperty("doctor_id")]
        public long DoctorId { get; set; }

        [JsonProperty("start_time")]
        public string StartTime { get; set; }

        [JsonProperty("date")]
        public long StrAppointmentDate { get; set; }
    }
}
