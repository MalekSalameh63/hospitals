﻿using Newtonsoft.Json;

namespace Common.DTOs.Appointments
{
    public class SearchDoctorByTimeDto
    {
        [JsonProperty("ClinicID")]
        public long ClinicId { get; set; }

        [JsonProperty("ProjectID")]
        public long ProjectId { get; set; }

        [JsonProperty("IsSearchAppointmnetByClinicID")]
        public bool IsSearchAppointmnetByClinicId { get; set; }

        [JsonProperty("LanguageID")]
        public long LanguageId { get; set; }
    }
}
