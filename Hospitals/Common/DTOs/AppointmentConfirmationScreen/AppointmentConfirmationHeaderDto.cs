﻿namespace Common.DTOs.AppointmentConfirmationScreen
{
    public class AppointmentConfirmationHeaderDto
    {
        public string AppointmentSuccessMessage { get; set; }
        public string AppointmentSuccessIcon { get; set; }
        public CustomThemeDTO CustomTheme { get; set; }

    }
}
