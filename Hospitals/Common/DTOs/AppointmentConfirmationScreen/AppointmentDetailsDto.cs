﻿namespace Common.DTOs.AppointmentConfirmationScreen
{
    public class AppointmentDetailsDto
    {
        public string DoctorName { get; set; }
        public string BookingSuccessMessage { get; set; }
        public string AppointmentDate { get; set; }
        public string AppointmentTime { get; set; }
        public string Image { get; set; }
        public CustomThemeDTO CustomTheme { get; set; }
    }
}
