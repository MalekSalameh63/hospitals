﻿using Common.DTOs.Configuration.Shared;

namespace Common.DTOs.AppointmentConfirmationScreen
{
    public class AppointmentConfirmationScreenDto
    {
        public AppointmentConfirmationHeaderDto AppointmentConfirmationHeader { get; set; }
        public AppointmentDetailsDto AppointmentDetails { get; set; }
        public AppointmentFeesDto AppointmentFees { get; set; }
        public SubmitButtonDTO SubmitButton { get; set; }
        public SubmitButtonDTO SkipMessage { get; set; }
    }
}
