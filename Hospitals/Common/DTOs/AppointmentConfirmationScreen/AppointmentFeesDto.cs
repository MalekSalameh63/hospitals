﻿namespace Common.DTOs.AppointmentConfirmationScreen
{
    public class AppointmentFeesDto
    {
        public string AppointmentDetailsTitle { get; set; }
        public double CurrentBalance { get; set; }
        public double Fees { get; set; }
        public double Tax { get; set; }
        public double Total { get; set; }
        public CustomThemeDTO CustomTheme { get; set; }
    }
}
