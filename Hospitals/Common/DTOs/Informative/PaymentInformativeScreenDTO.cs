﻿using Newtonsoft.Json;
using System;

namespace Common.DTOs.Informative
{
    public class PaymentInformativeScreenDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("sub_title")]
        public string SubTitle { get; set; }

        [JsonProperty("button_title")]
        public string ButtonTitle { get; set; }

        [JsonProperty("button_route")]
        public string ButtonRoute { get; set; }

        [JsonProperty("button_icon")]
        public string ButtonIcon { get; set; }

        [JsonProperty("link_title")]
        public string LinkTitle { get; set; }

        [JsonProperty("link_route")]
        public string LinkRoute { get; set; }
        [JsonProperty("button_title_wallet")]
        public string ButtonRouteWallet { get; set; }
    }
}
