﻿using Common.DTOs.Configuration.Shared;
using Newtonsoft.Json;
using System;

namespace Common.DTOs.Informative
{

    public class InformativeScreenDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("sub_title")]
        public string SubTitle { get; set; }

        [JsonProperty("button")]
        public SubmitButtonDTO Button { get; set; }
    }
}
