﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Questionnaires
{
    public class AddQuestionsDto
    {
        public int Type { get; set; }
        public string QuestionAR { get; set; }
        public string QuestionEN { get; set; }
        public bool IsActive { get; set; }
    }
}
