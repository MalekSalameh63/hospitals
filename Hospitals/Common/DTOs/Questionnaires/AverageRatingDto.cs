﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Questionnaires
{
    public class AverageRatingDto
    {
        [JsonProperty("rating_for")]
        public string RatingFor { get; set; }
        public int? QuestionId { get; set; }
        public decimal Answer { get; set; }
    }
}
