﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Common.DTOs.Questionnaires
{
    public class Questions
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QuestionId { get; set; }
        [ForeignKey("QuestionnaireId")]
        public int QuestionnaireId { get; set; }
        public int Type { get; set; }
        public string QuestionAR { get; set; }
        public string QuestionEN { get; set; }
        public bool IsActive { get; set; }
        public Questionnaires Questionnaires { get; set; }
    }
}
