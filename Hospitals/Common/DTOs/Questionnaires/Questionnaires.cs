﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Common.DTOs.Questionnaires
{
    public class Questionnaires
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QuestionnaireId { get; set; }
        public int? Type { get; set; }
        public List<Questions> Questions { get; set; }
        public string Title { get; set; }
    }
}
