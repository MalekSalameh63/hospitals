﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Questionnaires
{
    public  class RatingDto
    {
        [JsonProperty("id")]
        public int RatingId { get; set; }
        [JsonProperty("summury")]
        public string Summury { get; set; }
        [JsonProperty("rating_for")]
        public string RatingFor { get; set; }
        [JsonProperty("questions_list")]
        public List<QuestionAnswersDto> QuestionAnswersDto { get; set; }
        [JsonProperty("rating_by")]
        public string RatingBy { get; set; }
        [JsonProperty("rating_type")]
        public int? Type { get; set; }
    }
}
