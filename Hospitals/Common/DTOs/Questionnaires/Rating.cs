﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Common.DTOs.Questionnaires
{
    public class Rating
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RatingId { get; set; }
        public string Summury { get; set; }
        public string RatingBy { get; set; }
        public string RatingFor { get; set; }
        public List<RatingDetails> RatingDetails { get; set; }
        public int? Type { get; set; }
    }
}
