﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Questionnaires
{
    public class QuestionAnswersDto
    {
        [JsonProperty("key")]
        public int? Key { get; set; }
        [JsonProperty("value")]
        public int Value { get; set; }
    }
}
