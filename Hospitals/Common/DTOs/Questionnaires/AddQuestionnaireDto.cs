﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Questionnaires
{
    public class AddQuestionnaireDto
    {
        public int? Type { get; set; }
        public List<AddQuestionsDto> Questions { get; set; }
        public string Title { get; set; }
    }
}
