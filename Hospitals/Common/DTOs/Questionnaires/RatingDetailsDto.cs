﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Questionnaires
{
    public class RatingDetailsDto
    {
        public int RatingDetailsId { get; set; }
        public int RatingId { get; set; }
        public int QuestionId { get; set; }
        public string QuestionAR { get; set; }
        public string QuestionEN { get; set; }
        public decimal Answer { get; set; }
    }
}
