﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Common.DTOs.Questionnaires
{
    public class RatingDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RatingDetailsId { get; set; }
        [ForeignKey("RatingId")]
        public Rating Rating { get; set; }
        public int RatingId { get; set; }
        public int? QuestionId { get; set; }
        public decimal Answer { get; set; }
    }
}
