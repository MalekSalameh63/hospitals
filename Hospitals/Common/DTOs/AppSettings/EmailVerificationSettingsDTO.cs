﻿/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    public class EmailVerificationSettingsDTO
    {
        /// <summary>
        /// Gets or sets the activation requests to block.
        /// </summary>
        /// <value>The activation requests to block.</value>
        public int ActivationRequestsToBlock { get; set; }

        /// <summary>
        /// Gets or sets the email activation link expiration.
        /// </summary>
        /// <value>The email activation link expiration.</value>
        public int EmailActivationLinkExpiration { get; set; }

        /// <summary>
        /// Gets or sets the unblock user period.
        /// </summary>
        /// <value>The unblock user period.</value>
        public int UnBlockUserPeriod { get; set; }

        /// <summary>
        /// Gets or sets the email activation link.
        /// </summary>
        /// <value>The email activation link.</value>
        public string EmailActivationLink { get; set; }

        /// <summary>
        /// Gets or sets the encryption key.
        /// </summary>
        /// <value>The encryption key.</value>
        public string EncryptionKey { get; set; }

        /// <summary>
        /// Gets or sets the email allowed period in days.
        /// </summary>
        /// <value>The email allowed period in days.</value>
        public int EmailAllowedPeriodInDays { get; set; }

    }
}