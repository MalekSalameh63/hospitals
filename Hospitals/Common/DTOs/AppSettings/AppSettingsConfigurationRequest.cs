﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    [BsonIgnoreExtraElements]
    public class AppSettingsConfigurationRequest
    {
        /// <summary>
        /// Gets or sets value of the Application Name Identifier
        /// </summary>
        /// <value>The application name identifier.</value>
        /// <exception cref="System.Exception">Missing 'ApplicationNameIdentifier'as AppSettings items in the Configuration File</exception>
        public Guid ApplicationNameIdentifier { get; set; }

        /// <summary>
        /// Gets or sets value of the Environment Name Identifier
        /// </summary>
        /// <value>The environment name identifier.</value>
        /// <exception cref="System.Exception">Missing 'EnvironmentNameIdentifier'as AppSettings items in the Configuration File</exception>
        public Guid EnvironmentNameIdentifier { get; set; }
    }
}
