﻿/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    public class PaymentSettingsDTO
    {
        /// <summary>
        /// Gets or sets the access code.
        /// </summary>
        /// <value>The access code.</value>
        public string AccessCode { get; set; }

        /// <summary>
        /// Gets or sets the Merchant Identifier.
        /// </summary>
        /// <value>The Merchant Identifier.</value>
        public string MerchantIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the sha request phrase.
        /// </summary>
        /// <value>The sha request phrase.</value>
        public string ShaRequestPhrase { get; set; }

        /// <summary>
        /// Gets or sets the Language.
        /// </summary>
        /// <value>The Language.</value>
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the Currency.
        /// </summary>
        /// <value>The Currency.</value>
        public string Currency { get; set; }

        /// <summary>
        /// Gets or sets the currency code.
        /// </summary>
        /// <value>The currency code.</value>
        public int CurrencyCode { get; set; }
        /// <summary>
        /// Gets or sets the customer email.
        /// </summary>
        /// <value>The customer email.</value>
        public string CustomerEmail { get; set; }
        /// <summary>
        /// Gets or sets the customer email.
        /// </summary>
        /// <value>The merchant reference.</value>
        public string MerchantReference { get; set; }
    }
}
