﻿using Common.Common;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Common.DTOs
{
    [BsonIgnoreExtraElements]
    public class AppSettingsConfiguration
    {
        [BsonId]
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets value of the Application Name Identifier
        /// </summary>
        /// <value>The application name identifier.</value>
        /// <exception cref="System.Exception">Missing 'ApplicationNameIdentifier'as AppSettings items in the Configuration File</exception>
        public Guid ApplicationNameIdentifier { get; set; }

        /// <summary>
        /// Gets or sets value of the Application Name
        /// </summary>
        /// <value>The application name.</value>
        public string ApplicationName { get; set; }

        /// <summary>
        /// Gets or sets value of the Environment Name Identifier
        /// </summary>
        /// <value>The environment name identifier.</value>
        /// <exception cref="System.Exception">Missing 'EnvironmentNameIdentifier'as AppSettings items in the Configuration File</exception>
        public Guid EnvironmentNameIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the email settings.
        /// </summary>
        /// <value>The email settings.</value>
        public EmailSettingsDTO EmailSettings { get; set; }

        /// <summary>
        /// Gets or sets the email verification settings.
        /// </summary>
        /// <value>The email verification settings.</value>
        public EmailVerificationSettingsDTO EmailVerificationSettings { get; set; }

        /// <summary>
        /// Gets or sets the JWT Settings.
        /// </summary>
        /// <value>The JWT Settings.</value>
        public JWTSettingsDTO JWTSettings { get; set; }

        /// <summary>
        /// Gets or sets the Mongo settings.
        /// </summary>
        /// <value>The Mongo settings.</value>
        public MongoSettingsDTO MongoSettings { get; set; }

        /// <summary>
        /// Gets or sets the navigation settings.
        /// </summary>
        /// <value>The navigation settings.</value>
        public NavigationSettingsDTO NavigationSettings { get; set; }

        /// <summary>
        /// Gets or sets the notification settings.
        /// </summary>
        /// <value>The notification settings.</value>
        public NotificationSettingsDTO NotificationSettings { get; set; }

        /// <summary>
        /// Gets or sets the OpenTok settings.
        /// </summary>
        /// <value>The OpenTok settings.</value>
        public OpenTokSettingsDTO OpenTokSettings { get; set; }

        /// <summary>
        /// Gets or sets the OTP settings.
        /// </summary>
        /// <value>The OTP settings.</value>
        public OTPSettingsDTO OTPSettings { get; set; }

        /// <summary>
        /// Gets or sets the Payment settings.
        /// </summary>
        /// <value>The Payment settings.</value>
        public PaymentSettingsDTO PaymentSettings { get; set; }

        /// <summary>
        /// Gets or sets the Postgres settings.
        /// </summary>
        /// <value>The Postgres settings.</value>
        public PostgresSettingsDTO PostgresSettings { get; set; }

        /// <summary>
        /// Gets or sets the Redis settings.
        /// </summary>
        /// <value>The Redis settings.</value>
        public RedisSettingsDTO RedisSettings { get; set; }

        /// <summary>
        /// Gets or sets the Shared settings.
        /// </summary>
        /// <value>The Shared settings.</value>
        public SharedSettingsDTO SharedSettings { get; set; }

        /// <summary>
        /// Gets or sets the SMS settings.
        /// </summary>
        /// <value>The SMS settings.</value>
        public SMSSettingsDTO SMSSettings { get; set; }

        /// <summary>
        /// Gets or sets the User settings.
        /// </summary>
        /// <value>The User settings.</value>
        public UserSettingsDTO UserSettings { get; set; }

        /// <summary>
        /// Gets or sets the Kafka Settings.
        /// </summary>
        /// <value>The Kafka Settings.</value>
        public KafkaSettingsDTO KafkaSettings { get; set; }

        /// <summary>
        /// Gets or sets the Caching Settings.
        /// </summary>
        /// <value>The Caching Settings.</value>
        public CachingSettings CachingSettings { get; set; }
        /// <summary>
        /// Gets or sets the Aws S3 configuration.
        /// </summary>
        /// <value>The Aws S3 Configuration Settings.</value>
        public AwsS3Configuration AwsS3ServiceConfiguration { get; set; }
        /// <summary>
        /// Gets or sets the Elastic Apm Settings.
        /// </summary>
        /// <value>The Elastic Apm Settings.</value>
        public ElasticApmSettingsDTO ElasticApmSettings { get; set; }

        //public AppointmentsConfiguration AppointmentsConfiguration { get; set; }

    }
}
