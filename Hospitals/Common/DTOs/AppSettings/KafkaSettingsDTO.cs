﻿using Confluent.Kafka;
/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    /// <summary>
    /// Class KafkaSettingsDTO.
    /// </summary>
    public class KafkaSettingsDTO
    {
        //
        // Summary:
        //     Initial list of brokers as a CSV list of broker host or host:port. The application
        //     may also use `rd_kafka_brokers_add()` to add brokers during runtime. default:
        //     '' importance: high
        public string BootstrapServers { get; set; }

        //
        // Summary:
        //     SASL mechanism to use for authentication. Supported: GSSAPI, PLAIN, SCRAM-SHA-256,
        //     SCRAM-SHA-512. **NOTE**: Despite the name, you may not configure more than one
        //     mechanism.
        public SaslMechanism SaslMechanism { get; set; }

        //
        // Summary:
        //     Protocol used to communicate with brokers. default: plaintext importance: high
        public SecurityProtocol SecurityProtocol { get; set; }

        //
        // Summary:
        //     SASL username for use with the PLAIN and SASL-SCRAM-.. mechanisms default: ''
        //     importance: high
        public string SaslUsername { get; set; }
        //
        // Summary:
        //     SASL password for use with the PLAIN and SASL-SCRAM-.. mechanism default: ''
        //     importance: high
        public string SaslPassword { get; set; }

        /// <summary>
        ///  Gets or sets the notification title regex
        /// </summary>
        public string NotificationTitleRegex { get; set; }

        //
        // Summary:
        //     Request broker's supported API versions to adjust functionality to available
        //     protocol features. If set to false, or the ApiVersionRequest fails, the fallback
        //     version `broker.version.fallback` will be used. **NOTE**: Depends on broker version
        //     >=0.10.0. If the request is not supported by (an older) broker the `broker.version.fallback`
        //     fallback is used. default: true importance: high
        public bool? ApiVersionRequest { get; set; }





        //*****************************

        //
        // Summary:
        //     Automatically and periodically commit offsets in the background. Note: setting
        //     this to false does not prevent the consumer from fetching previously committed
        //     start offsets. To circumvent this behaviour set specific start offsets per partition
        //     in the call to assign(). default: true importance: high
        public bool? EnableAutoCommit { get; set; }

        //
        // Summary:
        //     The frequency in milliseconds that the consumer offsets are committed (written)
        //     to offset storage. (0 = disable). This setting is used by the high-level consumer.
        //     default: 5000 importance: medium
        public int? AutoCommitIntervalMs { get; set; }

        // Summary:
        //     Client group id string. All clients sharing the same group.id belong to the same
        //     group. default: '' importance: high
        public string GroupId { get; set; }

    }
}
