﻿/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    /// <summary>
    /// Class NavigationSettings.
    /// </summary>
    public class NavigationSettingsDTO
    {
        /// <summary>
        /// Gets or sets the Chief Complaint Url.
        /// </summary>
        /// <value>The Chief Complaint Url.</value>
        public string ChiefComplaintUrl { get; set; }

        /// <summary>
        /// Gets or sets the Complex Lookup Api Url.
        /// </summary>
        /// <value>The Complex Lookup Api Url.</value>
        public string ComplexLookupApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the Configuration Api Url.
        /// </summary>
        /// <value>The Configuration Api Url.</value>
        public string ConfigurationApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the Dictionary Api Url.
        /// </summary>
        /// <value>The Dictionary Api Url.</value>
        public string DictionaryApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the Email Activation Url.
        /// </summary>
        /// <value>The Email Activation Url.</value>
        public string EmailActivationUrl { get; set; }

        /// <summary>
        /// Gets or sets the Email Api Url.
        /// </summary>
        /// <value>The Email Api Url.</value>
        public string EmailApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the Emr Url.
        /// </summary>
        /// <value>The Emr Url.</value>
        public string EmrUrl { get; set; }

        /// <summary>
        /// Gets or sets the File Upload Url.
        /// </summary>
        /// <value>The File Upload Url.</value>
        public string FileUploadUrl { get; set; }

        /// <summary>
        /// Gets or sets the Lookup Api Url.
        /// </summary>
        /// <value>The Lookup Api Url.</value>
        public string LookupApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the MyWallet Url.
        /// </summary>
        /// <value>The MyWallet Url.</value>
        public string MyWalletUrl { get; set; }

        /// <summary>
        /// Gets or sets the Notification Api Url.
        /// </summary>
        /// <value>The Notification Api Url.</value>
        public string NotificationApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the Otp Verification Url.
        /// </summary>
        /// <value>The Otp Verification Url.</value>
        public string OtpVerificationUrl { get; set; }

        /// <summary>
        /// Gets or sets the Patient Api Url.
        /// </summary>
        /// <value>The Patient Api Url.</value>
        public string PatientApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the Physician Api Url.
        /// </summary>
        /// <value>The Physician Api Url.</value>
        public string PhysicianApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the Promotion Code Url.
        /// </summary>
        /// <value>The Promotion Code Url.</value>
        public string PromotionCodeUrl { get; set; }

        /// <summary>
        /// Gets or sets the SMS Api Url.
        /// </summary>
        /// <value>The SMS Api Url.</value>
        public string SMSApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the Template Format Url.
        /// </summary>
        /// <value>The Template Format Url.</value>
        public string TemplateFormatUrl { get; set; }

        /// <summary>
        /// Gets or sets the User Api Url.
        /// </summary>
        /// <value>The User Api Url.</value>
        public string UserApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the Web Socket Url.
        /// </summary>
        /// <value>The Web Socket Url.</value>
        public string WebSocketUrl { get; set; }

        /// <summary>
        /// Gets or sets the App settings Url.
        /// </summary>
        /// <value>The App settings Url.</value>
        public string AppsettingsUrl { get; set; }

        /// <summary>
        /// Gets or sets the Commercial api Url.
        /// </summary>
        /// <value>The App settings Url.</value>
        public string CommercialUrl { get; set; }

        /// <summary>
        /// Gets or sets the Appointments api Url.
        /// </summary>
        /// <value>The Appointments Url.</value>
        public string AppointmentsUrl { get; set; }
        /// <summary>
        /// Gets or sets the Appointments third party api Url.
        /// </summary>
        /// <value>The Appointments Url.</value>
        public string AppointmentsThirdPartyUrl { get; set; }

        /// <summary>
        /// Gets or sets the Questionnaires Url.
        /// </summary>
        /// <value>Questionnaires Url.</value>
        public string QuestionnairesUrl { get; set; }
        /// <summary>
        /// Gets or sets the Pricing Url.
        /// </summary>
        /// <value>Pricing Url.</value>
        public string PricingUrl { get; set; }
        /// <summary>
        /// Gets or sets the Api Gateway Url.
        /// </summary>
        /// <value>Api Gateway Url.</value>
        public string ApiGateway { get; set; }
        /// <summary>
        /// Gets or sets the Scheduler Notification Url.
        /// </summary>
        /// <value>Scheduler Notification Url.</value>
        public string SchedulerNotificationUrl { get; set; }
        /// <summary>
        /// Gets or sets the HmgWebServices Url.
        /// </summary>
        /// <value>Scheduler HmgWebServices Url.</value>
        public string HmgWebService { get; set; }

        /// <summary>
        /// Gets or sets the VIDA WebEMRUrl Url.
        /// </summary>
        /// <value>Web EMR Url.</value>
        public string WebEMRUrl { get; set; }
    }
}
