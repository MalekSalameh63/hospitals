﻿/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    public class MongoSettingsDTO
    {
        /// <summary>
        /// Gets or sets the mongoDB connection string.
        /// </summary>
        /// <value>The mongoDB connection string.</value>
        public string Connection
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the mongoDB database name.
        /// </summary>
        /// <value>The mongoDB database name.</value>
        public string DatabaseName
        {
            get;
            set;
        }
    }
}
