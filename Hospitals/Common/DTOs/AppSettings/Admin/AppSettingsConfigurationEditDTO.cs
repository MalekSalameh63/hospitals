﻿using Common.Common;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;

namespace Common.DTOs
{
    [BsonIgnoreExtraElements]
    public class AppSettingsConfigurationEditDTO
    {
        [JsonProperty("applicationIdentifier", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public ApplicationIdentityDTO ApplicationIdentity { get; set; }
        /// <summary>
        /// Gets or sets the email settings.
        /// </summary>
        /// <value>The email settings.</value>
        [JsonProperty("emailSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public EmailSettingsDTO EmailSettings { get; set; }

        /// <summary>
        /// Gets or sets the email verification settings.
        /// </summary>
        /// <value>The email verification settings.</value>
        [JsonProperty("emailVerificationSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public EmailVerificationSettingsDTO EmailVerificationSettings { get; set; }

        /// <summary>
        /// Gets or sets the JWT Settings.
        /// </summary>
        /// <value>The JWT Settings.</value>
        [JsonProperty("jwtSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public JWTSettingsDTO JWTSettings { get; set; }

        /// <summary>
        /// Gets or sets the Mongo settings.
        /// </summary>
        /// <value>The Mongo settings.</value>
        [JsonProperty("mongoSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public MongoSettingsDTO MongoSettings { get; set; }

        /// <summary>
        /// Gets or sets the navigation settings.
        /// </summary>
        /// <value>The navigation settings.</value>
        [JsonProperty("navigationSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public NavigationSettingsDTO NavigationSettings { get; set; }

        /// <summary>
        /// Gets or sets the notification settings.
        /// </summary>
        /// <value>The notification settings.</value>
        [JsonProperty("notificationSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public NotificationSettingsDTO NotificationSettings { get; set; }

        /// <summary>
        /// Gets or sets the OpenTok settings.
        /// </summary>
        /// <value>The OpenTok settings.</value>
        [JsonProperty("openTokSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public OpenTokSettingsDTO OpenTokSettings { get; set; }

        /// <summary>
        /// Gets or sets the OTP settings.
        /// </summary>
        /// <value>The OTP settings.</value>
        [JsonProperty("otpSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public OTPSettingsDTO OTPSettings { get; set; }

        /// <summary>
        /// Gets or sets the Payment settings.
        /// </summary>
        /// <value>The Payment settings.</value>
        [JsonProperty("paymentSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public PaymentSettingsDTO PaymentSettings { get; set; }

        /// <summary>
        /// Gets or sets the Postgres settings.
        /// </summary>
        /// <value>The Postgres settings.</value>
        [JsonProperty("postgresSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public PostgresSettingsDTO PostgresSettings { get; set; }

        /// <summary>
        /// Gets or sets the Redis settings.
        /// </summary>
        /// <value>The Redis settings.</value>
        [JsonProperty("redisSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public RedisSettingsDTO RedisSettings { get; set; }

        /// <summary>
        /// Gets or sets the Shared settings.
        /// </summary>
        /// <value>The Shared settings.</value>
        [JsonProperty("sharedSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public SharedSettingsDTO SharedSettings { get; set; }

        /// <summary>
        /// Gets or sets the SMS settings.
        /// </summary>
        /// <value>The SMS settings.</value>
        [JsonProperty("smsSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public SMSSettingsDTO SMSSettings { get; set; }

        /// <summary>
        /// Gets or sets the User settings.
        /// </summary>
        /// <value>The User settings.</value>
        [JsonProperty("userSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public UserSettingsDTO UserSettings { get; set; }

        /// <summary>
        /// Gets or sets the Kafka Settings.
        /// </summary>
        /// <value>The Kafka Settings.</value>
        [JsonProperty("kafkaSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public KafkaSettingsDTO KafkaSettings { get; set; }

        ///// <summary>
        ///// Gets or sets the Caching Settings.
        ///// </summary>
        ///// <value>The Caching Settings.</value>
        //[JsonProperty("cachingSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        //public CachingSettings CachingSettings { get; set; }
        /// <summary>
        /// Gets or sets the Aws S3 configuration.
        /// </summary>
        /// <value>The Aws S3 Configuration Settings.</value>
        [JsonProperty("awsS3ServiceConfiguration", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public AwsS3Configuration AwsS3ServiceConfiguration { get; set; }
        /// <summary>
        /// Gets or sets the Elastic Apm Settings.
        /// </summary>
        /// <value>The Elastic Apm Settings.</value>
        [JsonProperty("ElasticApmSettings", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public ElasticApmSettingsDTO ElasticApmSettings { get; set; }
    }
}
