﻿using MongoDB.Bson.Serialization.Attributes;
using System;
/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    public class ApplicationDTO
    {
        [BsonId]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets value of the Application Name Identifier
        /// </summary>
        /// <value>The application name identifier.</value>
        public Guid ApplicationNameIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the Application Name.
        /// </summary>
        /// <value>The Application Name.</value>
        public string Name { get; set; }
    }
}
