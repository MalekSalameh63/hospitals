﻿using System.Collections.Generic;
/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    public class OpenTokSettingsDTO
    {
        /// <summary>
        /// Gets or sets the Api Key.
        /// </summary>
        /// <value>The Api Key.</value>
        public int ApiKey { get; set; }

        /// <summary>
        /// Gets or sets the Api Secret.
        /// </summary>
        /// <value>The Api Secret.</value>
        public string ApiSecret { get; set; }

        /// <summary>
        /// Gets or sets the role.
        /// </summary>
        /// <value>The role.</value>
        public int Role { get; set; }

        /// <summary>
        /// Gets or sets the expire Time.
        /// </summary>
        /// <value>The expire Time.</value>
        public double ExpireTime { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>The data.</value>
        public string Data { get; set; }

        /// <summary>
        /// Gets or sets the initial Layout Class List.
        /// </summary>
        /// <value>The initial Layout Class List.</value>
        public List<string> InitialLayoutClassList { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>The location.</value>
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the media mode.
        /// </summary>
        /// <value>The media mode.</value>
        public int MediaMode { get; set; }

        /// <summary>
        /// Gets or sets the archive mode.
        /// </summary>
        /// <value>The archive mode.</value>
        public int ArchiveMode { get; set; }

    }
}
