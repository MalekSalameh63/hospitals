﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Common.DTOs
{
    /// <summary>
    /// The Encryption keys to be stored in the database
    /// </summary>
    public class MicroserviceEncryptionDTO
    {
        [BsonId]
        public Guid Id { get; set; }
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public int TokenExpiry { get; set; }
        public MicroserviceKeyDTO APIGateWay { get; set; }
        public MicroserviceKeyDTO Configuration { get; set; }
        public MicroserviceKeyDTO ComplexLookup { get; set; }
        public MicroserviceKeyDTO Commercial { get; set; }
        public MicroserviceKeyDTO ChiefComplaint { get; set; }
        public MicroserviceKeyDTO Dictionary { get; set; }
        public MicroserviceKeyDTO Email { get; set; }
        public MicroserviceKeyDTO EmailVerification { get; set; }
        public MicroserviceKeyDTO EMR { get; set; }
        public MicroserviceKeyDTO FileUpload { get; set; }
        public MicroserviceKeyDTO IdentityVerification { get; set; }
        public MicroserviceKeyDTO InAppNotifications { get; set; }
        public MicroserviceKeyDTO Lookup { get; set; }
        public MicroserviceKeyDTO PushNotifications { get; set; }
        public MicroserviceKeyDTO Patient { get; set; }
        public MicroserviceKeyDTO MyWallet { get; set; }
        public MicroserviceKeyDTO Physician { get; set; }
        public MicroserviceKeyDTO PhysicianLicenseVerification { get; set; }
        public MicroserviceKeyDTO PromotionCode { get; set; }
        public MicroserviceKeyDTO OtpVerification { get; set; }
        public MicroserviceKeyDTO SMS { get; set; }
        public MicroserviceKeyDTO TemplateFormat { get; set; }
        public MicroserviceKeyDTO Users { get; set; }
        public MicroserviceKeyDTO WebSocket { get; set; }
        public MicroserviceKeyDTO Appointments { get; set; }
        public MicroserviceKeyDTO Questionnaires { get; set; }
        public MicroserviceKeyDTO Pricing { get; set; }
        public MicroserviceKeyDTO SchedulerNotifications { get; set; }
    }
}
