﻿using Microsoft.IdentityModel.Tokens;

namespace Common.DTOs
{
    public class MicroserviceKeyDTO
    {
        public byte[] PrivateKey { get; set; }
        public byte[] PublicKey { get; set; }
    }
}
