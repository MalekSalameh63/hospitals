﻿/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    public class UserSettingsDTO
    {
         /// <summary>
        /// Gets or sets the active devices.
        /// </summary>
        /// <value>The active devices.</value>
        public bool GetActiveDevices { get; set; }

        /// <summary>
        /// Gets or sets the number of user fields.
        /// </summary>
        /// <value>The number of user fields.</value>
        public int NumberOfUserFields { get; set; }

        /// <summary>
        /// Gets or sets the number of patient fields.
        /// </summary>
        /// <value>The number of patient fields.</value>
        public int NumberOfPatientFields { get; set; }

        /// <summary>
        /// Gets or sets the number of physician fields.
        /// </summary>
        /// <value>The number of physician fields.</value>
        public int NumberOfPhysicianFields { get; set; }

        /// <summary>
        /// Gets or sets the password expires in days.
        /// </summary>
        /// <value>The password expires in days.</value>
        public int PasswordExpiresInDays { get; set; }

        /// <summary>
        /// Gets or sets the locked out time in minutes.
        /// </summary>
        /// <value>The locked out time in minutes.</value>
        public int LockedOutTimeInMinutes { get; set; }

        /// <summary>
        /// Gets or sets the max failed access attempts to lock account.
        /// </summary>
        /// <value>The max failed access attempts to lock account.</value>
        public int MaxFailedAccessAttemptsToLockAccount { get; set; }

        /// <summary>
        /// Gets or sets the max failed access attempt to block account.
        /// </summary>
        /// <value>The emax failed access attempt to block account.</value>
        public int MaxFailedAccessAttemptToBlockAccount { get; set; }

        /// <summary>
        /// Gets or sets the disable multi login.
        /// </summary>
        /// <value>The disable multi login.</value>
        public bool DisableMultiLogin { get; set; }

        /// <summary>
        /// Gets or sets the Phone Regex.
        /// </summary>
        /// <value>The Phone Regex.</value>
        public string PhoneRegex { get; set; }

        /// <summary>
        /// Gets or sets the Email Regex.
        /// </summary>
        /// <value>The Email Regex.</value>
        public string EmailRegex { get; set; }

        /// <summary>
        /// Gets or sets the direct login.
        /// </summary>
        /// <value>The direct login.</value>
        public bool DirectLogin { get; set; }
    }
}
