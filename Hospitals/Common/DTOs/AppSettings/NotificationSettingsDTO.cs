﻿/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    /// <summary>
    /// Class NotificationSettings.
    /// </summary>
    public class NotificationSettingsDTO
    {
        /// <summary>
        /// Gets or sets the mail credential Server Key.
        /// </summary>
        /// <value>The mail credential ServerKey.</value>
        public string ServerKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the mail credential ServerURL.
        /// </summary>
        /// <value>The mail credential ServerURL.</value>
        public string ServerURL
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the notification Min Length
        /// </summary>
        public string MinLength { get; set; }

        /// <summary>
        /// Gets or sets the notification Max Length
        /// </summary>
        public string MaxLength { get; set; }

        /// <summary>
        ///  Gets or sets the notification title regex
        /// </summary>
        public string NotificationTitleRegex { get; set; }

        /// <summary>
        /// Gets or sets the notification number min length.
        /// </summary>
        /// <value>The notification number min length.</value>
        public string NumberMinLength { get; set; }

        /// <summary>
        /// Gets or sets the notification date Format.
        /// </summary>
        /// <value>The notification date Format.</value>
        public string DateFormat { get; set; }

        /// <summary>
        /// Gets or sets the schedule notification period in minutes.
        /// </summary>
        /// <value>The schedule notification period in minutes.</value>
        public string ScheduleNotificationPeriodInMinutes { get; set; }
        public string AppName { get; set; }
    }
}
