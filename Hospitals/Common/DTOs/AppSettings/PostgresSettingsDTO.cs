﻿/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    public class PostgresSettingsDTO
    {
        /// <summary>
        /// Gets or sets the mongoDB connection string.
        /// </summary>
        /// <value>The mongoDB connection string.</value>
        public string Connection
        {
            get;
            set;
        }
    }
}
