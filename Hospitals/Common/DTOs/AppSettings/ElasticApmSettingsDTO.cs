﻿/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    public class ElasticApmSettingsDTO
    {
        /// <summary>
        /// Gets or sets the ElasticApm Enable.
        /// </summary>
        /// <value>The ElasticApm status.</value>
        public bool ElasticApmEnable { get; set; }

        /// <summary>
        /// Gets or sets the Secret Token.
        /// </summary>
        /// <value>The Secret Token.</value>
        public string SecretToken { get; set; }

        /// <summary>
        /// Gets or sets the Server Urls.
        /// </summary>
        /// <value>The Server Urls.</value>
        public string ServerUrls { get; set; }

        /// <summary>
        /// Gets or sets the Service Name.
        /// </summary>
        /// <value>The Service Name.</value>
        public string ServiceName { get; set; }
    }
}
