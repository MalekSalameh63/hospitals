﻿/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    public class OTPSettingsDTO
    {
        /// <summary>
        /// Gets or sets the valid until.
        /// </summary>
        /// <value>The valid until.</value>
        public int ValidUntil { get; set; }

        /// <summary>
        /// Gets or sets the date format.
        /// </summary>
        /// <value>The date format.</value>
        public string DateFormat { get; set; }

        /// <summary>
        /// Gets or sets the otp period in days.
        /// </summary>
        /// <value>The otp period in days.</value>
        public int OTPPeriodInDays { get; set; }

        /// <summary>
        /// Gets or sets the number of otp in period.
        /// </summary>
        /// <value>The number of otp in period.</value>
        public int NumberOfOTPInPeriod { get; set; }

        /// <summary>
        /// Gets or sets the otp size.
        /// </summary>
        /// <value>The otp size.</value>
        public int OtpSize { get; set; }

        /// <summary>
        /// Gets or sets the otp message.
        /// </summary>
        /// <value>The otp message.</value>
        public string OtpMessage { get; set; }

        /// <summary>
        /// Gets or sets the otp test.
        /// </summary>
        /// <value>The otp test.</value>
        public string OtpTest { get; set; }
    }
}
