﻿/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    public class JWTSettingsDTO
    {
        /// <summary>
        /// Gets or sets thekey.
        /// </summary>
        /// <value>The key.</value>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the Issuer.
        /// </summary>
        /// <value>The Issuer.</value>
        public string Issuer { get; set; }

        /// <summary>
        /// Gets or sets the Audience.
        /// </summary>
        /// <value>The Audience.</value>
        public string Audience { get; set; }

        /// <summary>
        /// Gets or sets the expires token time in minutes.
        /// </summary>
        /// <value>The expires token in minutes.</value>
        public int ExpiresToken { get; set; }
    }
}
