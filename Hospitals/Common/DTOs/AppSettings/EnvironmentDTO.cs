﻿using MongoDB.Bson.Serialization.Attributes;
using System;
/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    public class EnvironmentDTO
    {
        [BsonId]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets value of the Environment Name Identifier
        /// </summary>
        /// <value>The environment name identifier.</value>
        public Guid EnvironmentNameIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the Environment Name.
        /// </summary>
        /// <value>The Environment Name.</value>
        public string Name { get; set; }
    }
}
