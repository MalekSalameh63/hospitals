﻿/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    public class EmailSettingsDTO
    {
        /// <summary>
        /// Gets or sets the subject min length.
        /// </summary>
        /// <value>The subject min length.</value>
        public int SubjectMinLength { get; set; }

        /// <summary>
        /// Gets or sets the subject max length.
        /// </summary>
        /// <value>The subject max length.</value>
        public int SubjectMaxLength { get; set; }

        /// <summary>
        /// Gets or sets the body min length.
        /// </summary>
        /// <value>The body min length.</value>
        public int BodyMinLength { get; set; }

        /// <summary>
        /// Gets or sets the body max length.
        /// </summary>
        /// <value>The body max length.</value>
        public int BodyMaxLength { get; set; }

        /// <summary>
        /// Gets or sets the attachments max size.
        /// </summary>
        /// <value>The attachments max size.</value>
        public int AttachmentsMaxSize { get; set; }

        /// <summary>
        /// Gets or sets the attachment max number.
        /// </summary>
        /// <value>The attachment max number.</value>
        public int AttachmentMaxNumber { get; set; }

        /// <summary>
        /// Gets or sets the attachment max size.
        /// </summary>
        /// <value>The attachment max size.</value>
        public int AttachmentMaxSize { get; set; }

        /// <summary>
        /// Gets or sets the smtp host.
        /// </summary>
        /// <value>The smtp host.</value>
        public string SmtpHost { get; set; }

        /// <summary>
        /// Gets or sets the smtp port.
        /// </summary>
        /// <value>The smtp port.</value>
        public int SmtpPort { get; set; }

        /// <summary>
        /// Gets or sets the smtp user.
        /// </summary>
        /// <value>The smtp user.</value>
        public string SmtpUser { get; set; }

        /// <summary>
        /// Gets or sets the smtp password.
        /// </summary>
        /// <value>The smtp password.</value>
        public string SmtpPassword { get; set; }
        /// <summary>
        /// Gets or sets the from email.
        /// </summary>
        /// <value>The smtp password.</value>
        public string FromEmail { get; set; }
        public string FeedbackEmailSubject { get; set; }
        public string FeedbackToEmail { get; set; }

    }
}
