﻿/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    public class RedisSettingsDTO
    {
        /// <summary>
        /// Gets or sets the mongoDB connection string.
        /// </summary>
        /// <value>The mongoDB connection string.</value>
        public string Connection
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the mongoDB database id.
        /// </summary>
        /// <value>The mongoDB database id.</value>
        public int DatabaseId
        {
            get;
            set;
        }
    }
}
