﻿/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    public class SharedSettingsDTO
    {
        /// <summary>
        /// Gets or sets the secret key.
        /// </summary>
        /// <value>The secret key.</value>
        public string Secret { get; set; }

        /// <summary>
        /// Gets or sets the error log path.
        /// </summary>
        /// <value>The error log path.</value>
        public string ErrorLogPath { get; set; }

        /// <summary>
        /// Gets or sets the cache enabled.
        /// </summary>
        /// <value>The cache enabled.</value>
        public bool CacheEnabled { get; set; }

        /// <summary>
        /// Gets or sets the cache duration.
        /// </summary>
        /// <value>The cache duration.</value>
        public int CacheDuration { get; set; }

    }
}
