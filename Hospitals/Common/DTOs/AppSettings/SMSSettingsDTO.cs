﻿/// <summary>
/// The Configuration namespace.
/// </summary>
namespace Common.DTOs
{
    public class SMSSettingsDTO
    {
        /// <summary>
        /// Gets or sets the From.
        /// </summary>
        /// <value>The From.</value>
        public string From { get; set; }

        /// <summary>
        /// Gets or sets the Api Key.
        /// </summary>
        /// <value>The Api Key.</value>
        public string ApiKey { get; set; }

        /// <summary>
        /// Gets or sets the Api Secret.
        /// </summary>
        /// <value>The Api Secret.</value>
        public string ApiSecret { get; set; }

        /// <summary>
        /// Gets or sets the mobile phone regex.
        /// </summary>
        /// <value>The mobile phone regex.</value>
        public string MobilePhoneRegex { get; set; }

        /// <summary>
        /// Gets or sets the SMS server url.
        /// </summary>
        /// <value>Server Url.</value>
        public string ServerUrl { get; set; }
    }
}

