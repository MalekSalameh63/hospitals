﻿using Common.DTOs.InitiatePatientCall;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Common.DTOs.InitiatePhysicianScreen
{
    public class InitiatePhysicianCallDto
    {
        [Key]
        [JsonProperty("id")]
        public Guid Id { get; set; }
        [JsonProperty("user_id")]
        public string PatientId { get; set; }
        [JsonProperty("session_data")]
        public SessionDataDto SessionData { get; set; }
        [JsonProperty("user_info")]
        public InitiatePhysicianCallInfoDto PatientInfo { get; set; }
        [JsonProperty("call_configuration")]
        public CallConfigurationDto CallConfiguration { get; set; }
        [JsonProperty("dialog_config")]
        public DialogConfigurationDto DialogConfiguration { get; set; }
        [JsonProperty("episode_info")]
        public EpisodeInfo EpisodeInfo { get; set; }

    }
}
