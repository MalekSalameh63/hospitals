﻿using Newtonsoft.Json;

namespace Common.DTOs.InitiatePhysicianScreen
{
    public class InitiatePhysicianCallInfoDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("age")]
        public int Age { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("user_type")]
        public int? UserType { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("age_label")]
        public string AgeLabel { get; set; }
        [JsonProperty("assessment")]
        public string Assessment { get; set; }
        [JsonProperty("allergies")]
        public string Allergies { get; set; }
        [JsonProperty("medicines")]
        public string Medicines { get; set; }

    }
}
