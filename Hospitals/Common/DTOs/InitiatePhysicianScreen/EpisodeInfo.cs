﻿using Newtonsoft.Json;

namespace Common.DTOs.InitiatePhysicianScreen
{
    public class EpisodeInfo
    {
        [JsonProperty("appointment_number")]
        public long AppointmentNumber { get; set; }
        [JsonProperty("episode_number")]
        public long EpisodeNumber { get; set; }
        [JsonProperty("patient_mrn")]
        public long PatientMrn { get; set; }
    }
}
