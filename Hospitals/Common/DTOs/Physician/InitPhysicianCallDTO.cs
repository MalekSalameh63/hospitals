﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Common.DTOs.Physician
{
    public class InitPhysicianCallDTO
    {
        [Key]
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("physician_id")]
        public string PhysicianId { get; set; }
        
        [JsonProperty("key_api_key")]
        public long KeyApiKey { get; set; }

        [JsonProperty("key_session_id")]
        public string KeySessionId { get; set; }

        [JsonProperty("key_token")]
        public string KeyToken { get; set; }

        [JsonProperty("call_duration")]
        public long CallDuration { get; set; }

        [JsonProperty("warning_duration")]
        public long WarningDuration { get; set; }
    }
}
