﻿using Common.DTOs.EMR;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.DTOs.Physician
{
    public class AllergiesForm
    {
        [JsonProperty("is_update")]
        public bool IsUpdate { get; set; }

        [JsonProperty("allergies")]
        public List<Allergy> Allergies { get; set; }
    }
}
