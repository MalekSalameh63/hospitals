﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Physician
{
    public class PhysicianStateRequst
    {
        public string PhysicianId { get; set; }
        public bool PhysicianState { get; set; }
    }
}
