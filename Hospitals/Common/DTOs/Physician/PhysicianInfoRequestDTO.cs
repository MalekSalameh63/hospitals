﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.DTOs.Physician
{
    public class PhysicianInfoRequestDTO
    {
        public string PhysicianId { get; set; }
    }
}
