﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Physician
{
    public class PhysicianStateRequstDTO
    {
        [JsonProperty("value")]
        public bool PhysicianState { get; set; }
    }
}
