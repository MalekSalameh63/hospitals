using Common.DTOs;
using Confluent.Kafka;
using System.Threading.Tasks;

namespace Common.Kafka
{
    public class ProducerWrapper
    {
        private readonly string _topicName;
        private readonly ProducerConfig _config;

        public ProducerWrapper(ProducerConfig config, string topicName)
        {
            this._topicName = topicName;
            this._config = config;
        }
        public async Task<KafkaProducer> WriteMessage(string message)
        {
            using var p = new ProducerBuilder<Null, string>(this._config).Build();
            try
            {
                var dr = await p.ProduceAsync(this._topicName, new Message<Null, string> { Value = message });

                var kafkaProducer = new KafkaProducer
                {
                    offset = dr.Offset.ToString(),
                    partition = dr.Partition.Value.ToString()
                };
                return kafkaProducer;
            }
            catch (ProduceException<Null, string>)
            {
                return null;
            }
        }
    }
}