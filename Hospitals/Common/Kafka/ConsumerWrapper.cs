using Confluent.Kafka;
using System;
using System.Threading;

namespace Common.Kafka
{
    public class ConsumerWrapper
    {
        private readonly string _topicName;
        private readonly ConsumerConfig _consumerConfig;
        public ConsumerWrapper(ConsumerConfig config, string topicName)
        {
            this._topicName = topicName;
            this._consumerConfig = config;
        }
        public string ReadMessage(ref long offsetNo, ref long partitionNo)
        {
            using var consumer = new ConsumerBuilder<Ignore, string>(this._consumerConfig).Build();
            consumer.Subscribe(this._topicName);

            CancellationTokenSource cts = new CancellationTokenSource();
            Console.CancelKeyPress += (_, e) =>
            {
                e.Cancel = true; // prevent the process from terminating.
                cts.Cancel();
            };

            try
            {
                //while (true)
                //{
                try
                {
                    var consumeResult = consumer.Consume(cts.Token);
                    if (!consumeResult.IsPartitionEOF)
                    {
                        try
                        {
                            consumer.Commit(consumeResult);
                            offsetNo = consumeResult.Offset.Value;
                            partitionNo = consumeResult.Partition.Value;
                            return consumeResult.Message.Value;
                        }
                        catch (KafkaException e)
                        {
                            return "Commit error: " + e.Error.Reason;
                        }
                    }
                    return null;
                }
                catch (ConsumeException e)
                {
                    return "Error occured: " + e.Error.Reason;
                }
                catch (Exception ex)
                {
                    return "Error occured: " + ex.Message;
                }
                //}
            }
            catch (OperationCanceledException ex)
            {
                // Ensure the consumer leaves the group cleanly and final offsets are committed.
                consumer.Close();
                return "Error occured: " + ex.Message;
            }
        }
        public bool CheckMessageExist()
        {
            using var consumer = new ConsumerBuilder<Ignore, string>(this._consumerConfig).Build();
            consumer.Subscribe(this._topicName);

            CancellationTokenSource cts = new CancellationTokenSource();
            Console.CancelKeyPress += (_, e) =>
            {
                e.Cancel = true; // prevent the process from terminating.
                cts.Cancel();
            };

            try
            {
                //while (true)
                //{
                try
                {
                    var consumeResult = consumer.Consume(cts.Token);
                    if (!consumeResult.IsPartitionEOF)
                    {
                        return true;
                    }
                    return false;
                }
                catch (ConsumeException)
                {
                    return false;
                }
                catch (Exception)
                {
                    return false;
                }
                //}
            }
            catch (OperationCanceledException)
            {
                // Ensure the consumer leaves the group cleanly and final offsets are committed.
                consumer.Close();
                return false;
            }
        }
    }
}