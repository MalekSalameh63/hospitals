﻿using Common.Authorization;
using Common.Common;
using Common.Common.Configuration;
using Common.DTOs;
using Common.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;

namespace Common.Filters
{
    public class CustomActionFilter : IActionFilter
    {
        private bool _SessionEnabled;
        private readonly MicroserviceKeyDTO _microserviceKey;
        public CustomActionFilter(MicroserviceKeyDTO microserviceKey, bool sessionEnabled)
        {
            _SessionEnabled = sessionEnabled;
            _microserviceKey = microserviceKey;
        }
        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (!string.IsNullOrEmpty(context.HttpContext.Request.Headers[SystemConstants.Authorization]))
            {
                var token = context.HttpContext.Request.Headers[SystemConstants.Authorization].ToString();

                JwtSecurityToken validatedToken = JwtTokenManager.ValidateToken(_microserviceKey, token);

                if (validatedToken != null)
                {
                    if (_SessionEnabled)
                    {
                        string user = UserContext.GetUserFromToken(validatedToken);

                        context.HttpContext.Session.SetString(SystemConstants.SessionUserKey, user);
                    }
                }
                else
                {
                    context.Result = new ObjectResult(new ResponseResult<string>(GlobalStatus.Failed, ApiStatus.Unauthorized, null, FriendlyStatus.GeneralError, null))
                    {
                        StatusCode = StatusCodes.Status401Unauthorized
                    };
                }
            }
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
            // if (!SharedSettings.CacheEnabled)
            // {
            //     context.HttpContext.Response.Headers.Remove("Cache-Control");
            //     context.HttpContext.Response.Headers.Add("Cache-Control", "no-cache,no-store");
            //
            // }
        }
    }
}
