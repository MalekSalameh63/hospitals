﻿using Common.VOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace Common.Configurations
{
    public class Configuration : IDisposable
    {
        private readonly TextVO _configFileName;
        private static readonly object _syncObject = new object();
        private static readonly Dictionary<TextVO, dynamic> configContentDictionary = new Dictionary<TextVO, dynamic>();
        private Configuration()
        {

        }
        public Configuration(TextVO configFilePath)
        {
            try
            {
                _configFileName = configFilePath;
                if (!configContentDictionary.ContainsKey(_configFileName))
                {
                    lock (_syncObject)
                    {
                        if (!configContentDictionary.ContainsKey(_configFileName))
                        {
                            var readString = File.ReadAllText(_configFileName.TextValue);
                            var configContent = JsonConvert.DeserializeObject<Dictionary<string, string>>(readString);

                            var configurationValueList = new List<ConfigurationValues>();
                            configContent.AsQueryable().ToList().ForEach(x =>
                            {
                                var record = new ConfigurationValues
                                {
                                    Key = new TextVO(x.Key),
                                    Value = new TextVO(x.Value, 1)
                                };
                                configurationValueList.Add(record);
                            });
                            configContentDictionary.Add(_configFileName, configurationValueList);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw new Exception("There is error in reading config file or the config path is not correct");
            }
        }
        public TextVO GetConfigValue<TextVO>(string key)
        {
            TextVO configValue;
            try
            {
                configValue = configContentDictionary[_configFileName][key];
            }
            catch (Exception)
            {
                throw new Exception("config key is not found");
            }
            return configValue;
        }
        public void Dispose()
        {
        }
        public T GetConfigFile<T>()
        {
            return (T)configContentDictionary[_configFileName];
        }
    }
    public class ConfigurationValues
    {
        public TextVO Key { get; set; }
        public TextVO Value { get; set; }
    }
}
