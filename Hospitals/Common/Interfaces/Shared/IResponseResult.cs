﻿using Common.Enums;

namespace Common.Interfaces.Shared
{
    public interface IResponseResult<T>
    {
        /// <summary>
        /// Business Error Code
        /// </summary>
        FriendlyStatus BusinessError { get; set; }
        /// <summary>
        /// Unhandled Exception Messages
        /// </summary>
        string ExceptionMessage { get; set; }
        /// <summary>
        /// Api Status
        /// </summary>
        ApiStatus ApiStatus { get; set; }
        /// <summary>
        /// Global Status
        /// </summary>
        GlobalStatus GlobalStatus { get; set; }
        /// <summary>
        /// Dynamic Data Object
        /// </summary>
        T Data { get; set; }
        /// <summary>
        /// Checks if GlobalStatus is success, ApiStatus is ok, and data was returned. Otherwise, return false.
        /// </summary>
        /// <returns></returns>
        bool AllGreen();
    }
}
