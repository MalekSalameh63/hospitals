﻿using Common.Authorization;
using Common.Common.Configuration;
using Common.DTOs;
using Microsoft.AspNetCore.Http;
using System.Net.Http;

namespace Common.Clients
{
    public class CustomHttpClient : HttpClient
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public CustomHttpClient(MicroserviceKeyDTO microserviceKey = null)
        {
            _httpContextAccessor = new HttpContextAccessor();

            if (SharedSettings.SessionEnabled && _httpContextAccessor.HttpContext != null && _httpContextAccessor.HttpContext.Session != null)
            {
                string user = _httpContextAccessor.HttpContext.Session.GetString(SystemConstants.SessionUserKey);


                if (microserviceKey != null)
                {
                    this.DefaultRequestHeaders.Add(SystemConstants.Authorization, JwtTokenManager.GenerateJWEToken(microserviceKey, user));
                }

                this.DefaultRequestHeaders.Add(SystemConstants.Language, _httpContextAccessor.HttpContext.Request.Headers[SystemConstants.Language].ToString());
            }
        }
        public CustomHttpClient(MicroserviceKeyDTO microserviceKey = null, HttpMessageHandler handler = null) : base(handler)
        {
            _httpContextAccessor = new HttpContextAccessor();

            if (SharedSettings.SessionEnabled && _httpContextAccessor.HttpContext != null && _httpContextAccessor.HttpContext.Session != null)
            {
                string user = _httpContextAccessor.HttpContext.Session.GetString(SystemConstants.SessionUserKey);

                if (microserviceKey != null)
                {
                    this.DefaultRequestHeaders.Add(SystemConstants.Authorization, JwtTokenManager.GenerateJWEToken(microserviceKey, user));
                }

                this.DefaultRequestHeaders.Add(SystemConstants.Language, _httpContextAccessor.HttpContext.Request.Headers[SystemConstants.Language].ToString());
            }
        }
    }
}
