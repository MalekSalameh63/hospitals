﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Common.Enums
{
    public enum Actions
    {
        Register = 1,
        Login = 2,
        Get = 3,
        Post = 4,
        Put = 5,
        Delete = 6,
        BiometricLogin =7
    }

    public enum SubActions : short
    {
        None = 1,
        ById = 2,
        Range =3,
        ByType =4,
        Count =5
    }
}
