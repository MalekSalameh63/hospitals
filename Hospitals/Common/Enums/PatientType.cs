﻿namespace Common.Enums
{
    public enum PatientType
    {
        Citizen = 1,
        NonCitizen = 2
    }
}
