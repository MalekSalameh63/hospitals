﻿using System.ComponentModel.DataAnnotations;

namespace Common.Enums
{
    public enum GlobalStatus
    {
        Success = 1,
        Failed = 2,
        LoggedOut = 3,
        Unauthorized = 4,
        OldConfigs = 5,
        OldApp = 6,
        OldApi = 7
    }

    public enum ApiStatus
    {
        #region Login and Registration
        None = 0,
        Ok = 1,
        LoginFailed = 2,
        PasswordExpired = 3,
        OtpFailed = 4,
        OtpExpired = 5,
        EmailVerificationFailed = 6,
        RegistrationFailed = 7,
        ChangePhoneNumberFailed = 8,
        BusinessValidationFailed = 9,
        PhoneNumberDoesNotExist = 10,
        BiometricLoginFailed = 11,
        UpdateUserFailed = 12,
        UpdateUserEntityErrors = 13,
        #endregion

        #region common
        AlreadyExists = 21,
        #endregion

        #region ChiefComplaint
        ChiefComplientTextAndTagsAreNull = 20,
        #endregion

        #region EMR
        CouldNotPostNewPatientToVIDA = 100,
        CouldNotUpdateNewPatientInVIDA,
        CouldNotPostNewPhysicianToVIDA,
        CouldNotUpdatePhysicianInVIDA,
        ValidatePatientInVIDA = 103,
        #endregion

        #region General
        NoContent = 204,
        BadRequest = 400,
        Unauthorized = 401,
        Forbidden = 403,
        NotFound = 404,
        UnprocessableEntity = 422,
        InternalServerError = 500,
        BadGateway = 502,
        #endregion

        #region Questionnaire
        QuestionAlreadyUsed,
        QuestionnaireAlreadyUsed,
        CannotMakeChangeONThisQuestionBecauseItHasRating,
        YouCannotDeleteThisQuestionnaireBecauseItsContainsQuestions,
        #endregion 

        FeedbackDataCannotBeEmpty,
        SchedulerNotificationFailed,

        VidaSOAPValidationError = 900,
        IncorrectOldPassword,
        UserIsBlocked
    }

    public enum LookupCategory
    {
        None = 0,
        FriendlyStatus = 1,
        Culture = 2,
        Language = 3,
        Currency = 4,
        Numeric = 5,
        WeightUnit = 6,
        LengthUnit = 7,
        DateFormat = 8,
        MonthFormat = 9,
        Gender = 10,
        NotificationPreference = 11,
        MaritalStatus = 12,
        Relation = 13
    }
    public enum MaritalStatus
    {
        Unknown = 0,
        Single = 1,
        Married = 2,
        Divorced = 3,
        Widowed = 4,
        NotApplicable = 5
    }
    public enum FriendlyStatus
    {
        None = 0,
        Ok = 1,
        GeneralError = 2,
        ChiefComplaintNotFound = 3,
        EmptyRecipientList = 4,
        EmailAlreadyActivated = 5,
        EmailDoesNotExist = 6,
        EmailIsBlocked = 7,
        ExceededActivationRequests = 8,
        EmailVerificationFailed = 9,
        WalletDoesNotExist = 10,
        IdShouldMatchWalletId = 11,
        WalletAlreadyExists = 12,
        TransactionDoesNotExists = 13,
        InsufficientBalance = 14,
        IdShouldMatchTransactionId = 15,
        OtpDoesNotExists = 16,
        OtpExpired = 17,
        ThisEmailDoesNotExist = 18,
        ThisPhoneNumberDoesNotExist = 19,
        ThisPhoneNumberIsAlreadyRegistered = 20,
        ThisEmailIsAlreadyRegistered = 21,
        PasswordIsExpired = 22,
        ThisUserDoesNotExist = 23,
        EmailOrPasswordIncorrect = 24,
        PassedSMSMessageCannotBeEmpty = 25,
        NoPatientInQueue = 26,
        PhysicianNotFound = 27,
        TheAccountIsLockedOut = 28,
        TheAccountIsBlocked = 29,
        CultureAbbreviationExists = 30,
        UnitNameExists = 31,
        DateFormatExists = 32,
        FormAlreadyExists = 33,
        LanguageAbbreviationExists = 34,
        MonthNameFormatExists = 35,
        NumericCodeExists = 36,
        ScreenAlreadyExists = 37,
        KeyAlreadyExists = 38,
        KeyDoesntExist = 39,
        LookupAlreadyExists = 40,
        NotificationPageExists = 41,
        NavigationPanelAlreadyExists = 42,
        YouAreNotAuthorizedToAccessTheApplication = 43,
        YouAreAlreadyLoggedInByAnotherAccount = 44,
        YouHaveExceedTheNumberOfAllowedToSentOTP = 45,
        InformativeScreenAlreadyExists = 46,
        InformativeScreenDoesNotExist = 47,
        WalletScreenAlreadyExists = 48,
        WalletScreenDoesNotExist = 50,
        PaymentMethodScreenAlreadyExists = 51,
        PaymentMethodScreenDoesNotExist = 52,
        YouHaveExceedTheNumberOfAllowedActivationLink,
        ApplicationSettingsAlreadyExist,
        ApplicationSettingsDoNotExist,
        PhoneNumberIsRequired,
        PhoneNumberDoesNotMatchRegularExpression,
        EmailDoesNotMatchRegularExpression,
        FileDoesNotExist,
        UserTypeIsRequired,
        InvalidGenderValue,
        AgeCouldNotBeLessThanMinAge,
        AgeCouldNotBeMoreThanMaxAge,
        FormDoesNotExist,
        FormConfigurationCannotBeEmpty,
        FormTypeDoesNotMatchAPI,
        LookupCategoryAlreadyExists,
        NationalIdCanNotBeEmpty,
        NationalIdShouldBeOnlyNumbers,
        NationalIdLengthShouldBe10Numbers,
        ThisNationalIdIsAlreadyRegistered,
        BusinessKeyCannotBeEmpty,
        CurrencyCodeAlreadyExists,
        CountryNumberAlreadyExists,
        SpecialtyNumberAlreadyExists,
        PaymentSummaryScreenDoesNotExist,
        ScreenTypeDoesNotMatchAPI,
        TransactionDetailsScreenAlreadyExists,
        AppSettingsDoesNotExist,
        AppSettingsAlreadyExists,
        ApplicationDoesNotExist,
        ApplicationAlreadyExists,
        EnvironmentDoesNotExist,
        EnvironmentAlreadyExists,
        ChiefComplientTextAndTagsAreNull,
        MicroserviceEncryptionAlreadyExists,
        MicroserviceEncryptionDoesNotExist,
        UserSettingsScreenAlreadyExists,
        UserSettingsScreenDoesNotExist,
        YourEmailIsAlreadyVerified,
        AboutSectionAlreadyExists,
        AboutSectionDoesNotExist,
        LanguageSectionAlreadyExists,
        LanguageSectionDoesNotExist,
        NotificationsSectionAlreadyExists,
        NotificationsSectionDoesNotExist,
        PrivacyPolicySectionAlreadyExists,
        PrivacyPolicySectionDoesNotExist,
        SupportSectionAlreadyExists,
        SupportSectionDoesNotExist,
        TransactionTemplateAlreadyExists,
        TransactionTemplateDoesNotExist,
        DeviceIdDoesNotMatchBiometricDeviceId,
        ThisOTPPhoneNumberDoesNotExist,
        CallAssessmentScreenAlreadyExists,
        CallAssessmentScreenDoesNotExist,
        PatientCallSummaryScreenAlreadyExists,
        PatientCallSummaryScreenDoesNotExist,
        DailyTipDoesNotExist,
        EMRLookupsNotFound,
        CityNumberAlreadyExists,
        RankNumberAlreadyExists,
        AreaNumberAlreadyExists,
        SOAPAlreadyExists,
        SOAPDoesNotExist,
        SOAPChiefComplaintScreenDoesNotExist,
        SOAPHistoryScreenDoesNotExist,
        SOAPAllergiesScreenDoesNotExist,
        SOAPDiagnosisScreenDoesNotExist,
        SOAPProgressNoteScreenDoesNotExist,
        SOAPPrescriptionScreenDoesNotExist,
        SOAPRadiologyScreenDoesNotExist,
        SOAPLaboratoryScreenDoesNotExist,
        SOAPClinicalScreenDoesNotExist,
        YouCannotDeleteThisQuestionnaireBecauseItsContainsQuestions,
        NoAvailableSlotsForDoctor,
        ClinicsScreenAlreadyExist,
        ClinicsScreenDoesNotExist,
        NameCannotBeEmpty,
        EmailCannotBeEmpty,
        PassedFeedbackMessageCannotBeEmpty,
        QuestionAlreadyUsed,
        QuestionnaireAlreadyUsed,
        YourMobileNumberIsAlreadyVerified,
        InvalidAppointmentNumber,
        SOAPLayoutDoesNotExist,
        AppointmentHistoryDetailsScreenDoesNotExist,
        AppointmentHistoryScreenDoesNotExist,
        AppointmentHistoryScreenAlreadyExist,
        AppointmentHistoryDetailsScreenAlreadyExist,
        AppointmentHomeScreenAlreadyExist,
        AppointmentHomeScreenDoesNotExist,
        AppointmentConfirmationScreenDoesNotExist,
        AppointmentConfirmationScreenAlreadyExist,
        DoctorsListFilterScreenDoesNotExist,
        DoctorsListFilterScreenAlreadyExist,
        DoctorListScreenAlreadyExist,
        DoctorListScreenDoesNotExist,
        InitiatePatientCallScreenDoesNotExist,
        InitiatePatientScreenAlreadyExist,
        InitiatePhysicianCallScreenAlreadyExist,
        InitiatePhysicianCallScreenDoesNotExist,
        PhysicianAvailableDatesScreenDoesNotExist,
        PhysicianAvailableDatesAlreadyExist,
        PhysicianHistoryAppointmentsScreenDoesNotExist,
        PhysicianHistoryAppointmentsScreenAlreadyExist,
        PhysicianUpcomingAppointmentsScreenDoesNotExist,
        PhysicianUpcomingAppointmentsScreenAlreadyExist,
        PhysicianWalletScreenAlreadyExist,
        PhysicianWalletScreenDoesNotExist,
        TransactionDetailsScreenDoesNotExist,
        UpcomingAppointmentsDetailsScreenDoesNotExist,
        UpcomingAppointmentsDetailsScreenAlreadyExist,
        UpcomingAppointmentsScreenDoesNotExist,
        UpcomingAppointmentsScreenAlreadyExist,
        VidaSOAPValidationError,
        CouldNotPostNewPatientToVIDA,
        CouldNotUpdatePatientInVIDA,
        IncorrectOldPassword,
        ThisUserNameNotExist,
        PhysicianCallAssessmentScreenAlreadyExists,
        PhysicianCallAssessmentScreenDoesNotExist,
        UserCallPreferencesScreenDoesNotExist,
        TemplateDoesNotExist,
        EpisodeWasNotCreatedSuccessfully,
        AppointmentWasNotConfirmedSuccessfully,
        AppointmentWasNotCreatedSuccessfully,
        PhysicianUpcomingAppointmentsDetailsScreenDoesNotExist,
        PhysicianUpcomingAppointmentsDetailsScreenAlreadyExist,
        CouldNotPostNewPhysicianToVIDA,
        CouldNotUpdatePhysicianInVIDA,
        UnauthorizedByVIDA,
        PhysicianQualificationScreenDoesNotExist,
        PhysicianQualificationScreenAlreadyExist,
        ActivationLinkIsInvalid,
        ActivationLinkIsExpired


    }

    public enum PatientQueueStatus
    {
        InProgress = 1,
        Completed = 2,
        Rejected = 3
    }

    public enum FormType
    {
        Login = 1,
        PatientRegistration = 2,
        Patient = 3,
        Physician = 4,
        PhysicianRegistration = 5,
        Chief = 6,
        ForgotPassword = 7,
        ChangePassword = 8,
        PaymentSummary
    }
    public enum ScreenType
    {
        Home = 1,
        PhysicianHome = 2,
        PatientHome = 3
    }
    public enum UserType
    {
        Patient = 1,
        Physician = 2,
        Admin = 3,
        Guest = 4,
        System = 5,
        SuperAdmin = 6,
        Pharmacists = 7,
        Radiologists = 8,
        Lab = 9
    }
    public enum WorkFlowStage
    {
        Login = 1,
        Registration = 2,
        BusinessValidation = 3,
        OtpGenerate = 4,
        OtpValidate = 5,
        GenerateActivationLink = 6,
        AutoSave = 7,
        ChangePassword = 8,
        ForgotPassword = 9,
        ChangePhoneNumberByOTP = 10,
        ChangePhoneNumber = 11,
        Cheifcomplaint = 12,
        OTPForgetPassword = 13,
        PasswordExpired = 14,
        ChiefComplaintAppointment = 15,
        LoginAfterRegistration = 16
    }
    public enum NotificationChannels
    {
        Call = 1,
        User = 2,
        Tips = 3,
        Marketing = 4,
        Promotion = 5,
        Alarms = 6,
        Timers = 7,
    }
    public enum NotificationCategory
    {
        GeneralInformation = 1,
        Promotion = 2,
        Appointment = 3,
        RequestMedicalRecord = 4,
        CompletedMedicalRecord = 5,
        SickLeave = 6,
        PaymentTransactions = 7,
        IncompleteProfile = 8,
        Security = 9,
        Call = 10,
        PatientAppointment = 11,
        PhysicianAppointment = 12
    }
    public enum Gender
    {
        None = 0,
        Male = 1,
        Female = 2,
    }

    public enum DropDownListType
    {
        None,
        Languages,
        Language,
        PreferredLanguage,
        Gender,
        PreferredGender,
        CountryCode,
        Specialties,
        Speciality,
        OtherCountryCode,
        Country,
        City,
        Area,
        Rank,
        Nationality,
        MaritalStatus,
        EmergencyContactRelationship,
        EmergencyContactCountryCode,
        Clinic
    }

    public enum BusinessValidationKey
    {
        Phone,
        Email,
        NationalId,
        PromotionCode,
        [Display(Name = "user_name")]
        UserName
    }

    public enum NotificationStatus
    {
        New,
        NotNew
    }

    public enum PaymentOptions
    {
        PayFromWallet = 1,
        ChargeWallet = 2
    }

    public enum OTPPageName
    {
        Registration = 1,
        ForgetPassword = 2,
        ChangePhoneNumber = 3,
        ReferralConfirmPhoneNumber = 4
    }

    public enum QuestionnairesType
    {
        Patient = 1,
        Physician = 2
    }

    public enum QuestionsType
    {
        GeneralSystem = 1,
        Physician = 2
    }

    public enum UserSettings
    {
        NotificationSound = 1,
        NewPromotion = 2,
        AppointmentReminder = 3,
        EmailReminder = 4
    }

    public enum EmailType
    {
        Verify = 1,
        Reminder = 2
    }

    public enum SchedulerNotificationType
    {
        Appointments = 1
    }

    public enum SchedulerNotificationStatus
    {
        Pending = 1,
        Completed = 2,
        Canceled = 3
    }

    public enum RatingType
    {
        Application = 1,
        Physician = 2
    }

    public enum PhysicianStatus
    {
        Approved = 1,
        Rejected = 2,
        RequestInformation = 3,
        Verified = 4
    }

    public enum HistoryLogsType
    {
        PhysicianApproval = 1
    }

    public enum HistoryLogsSubType
    {
        Approved = 1,
        Rejected = 2,
        RequestInformation = 3,
        Verified = 4
    }
}
