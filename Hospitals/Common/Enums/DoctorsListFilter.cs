﻿namespace Common.Enums
{
    public enum DoctorsListFilter
    {
        NameAToZ = 0,
        NameZToA = 1,
        PriceLowToHigh = 2,
        PriceHighToLow = 3,
        SpecialtyAToZ = 4,
        SpecialtyZToA = 5,
        Rating = 6,
        GenderMaleFirst = 7,
        GenderFemaleFirst = 8
    }
}
