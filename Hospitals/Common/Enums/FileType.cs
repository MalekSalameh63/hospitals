﻿namespace Common.Enums
{
    public enum FileType
    {
        None = 0,
        Private = 1,
        Public = 2,
        Shared = 3
    }
}
