﻿namespace Common.Enums
{
    public enum TransactionType
    {
        Charge,
        Credit,
        Reserve,
        Spare,
        Payment
    }
}
