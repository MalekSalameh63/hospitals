﻿namespace Common.Enums
{
    public enum ReferralUserType
    {
        LabTechnician = 1,
        RadiologyTechnician = 2,
        Pharmacy = 3
    }
}
