﻿namespace Common.Enums
{
    public enum FileParentType
    {
        None = 0,
        ChiefComplaint = 1,
        PhysicianProfile = 2,
        PhysicianApproval =3
    }
}
