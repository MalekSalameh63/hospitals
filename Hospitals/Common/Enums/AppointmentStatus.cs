﻿namespace Common.Enums
{
    public enum AppointmentStatus
    {
        All = 0,
        Confirmed = 2,
        Canceled = 4
    }
}