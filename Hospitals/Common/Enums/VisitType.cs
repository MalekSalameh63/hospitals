﻿namespace Common.Enums
{
    public enum VisitType
    {
        AdHocCall = 1,
        Appointment = 2
    }
}
