﻿namespace Common.Enums
{
    public enum TemplateFormatEnum
    {
        Otp = 1,
        EmailVerification = 2,
        VideoCallFeesSummary = 3,
        WelcomeEmail = 4,
        PhysicianApproval = 5,
        PhysicianReject = 6,
        BlockEmail = 7,
        UnblockEmail = 8,
        PhysicianAppointmentReminder = 9,
        PatientAppointmentReminder = 10,
        PatientCancelAppointment = 11,
        ContactUs = 12,
        LoginFromAnotherDevice = 13,
        LoginBlockEmail = 14,
        EmailVerificationDynamicLink = 15
    }
}
