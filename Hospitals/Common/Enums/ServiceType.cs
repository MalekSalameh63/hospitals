﻿namespace Common.Enums
{
    public enum ServiceType
    {
        ChargeWallet = 1,
        Visit = 2,
        PhysicianFees = 3,
        Appointment = 4
    }
}
