﻿namespace Common.Enums
{
    public enum PaymentMethod
    {
        visa = 0,
        mastercard = 1,
        mada = 2,
        sadad = 3,
        wallet = 4
    }
}