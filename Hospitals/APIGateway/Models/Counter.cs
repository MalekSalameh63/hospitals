﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace APIGateway.Models
{
    public class Counter
    {
        [BsonId]
        public Guid Id { get; set; }
        public string CounterType { get; set; }
        public int Count { get; set; }
    }
}
