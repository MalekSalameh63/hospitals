﻿using System;

namespace APIGateway.Models
{
    public class LoginResponse
    {
        public string UserId { get; set; }

        public string UserName { get; set; }
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
    }
}
