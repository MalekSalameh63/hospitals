﻿using APIGateway.Enums;
using Microsoft.AspNetCore.Identity;

namespace APIGateway.Models
{
    public class ApplicationUser : IdentityUser
    {
        public Gender Gender { get; set; }
    }
}
