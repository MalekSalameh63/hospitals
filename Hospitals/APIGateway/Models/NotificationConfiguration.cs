﻿namespace APIGateway.Models
{
    public class NotificationConfiguration
    {
        public static string ServerUrl { get; set; }
        public static string ServerKey { get; set; }
    }
}
