﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIGateway.Models
{
    public class TestClass
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public string FirstTest { get; set; }
        public string SecondTest { get; set; }
        public int ThirdTest { get; set; }
    }
}
