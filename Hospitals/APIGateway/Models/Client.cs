﻿namespace APIGateway.Models
{
    public class Client
    {
        public static string Server { get; set; }
        public static int Port { get; set; }
        public static string User { get; set; }
        public static string Password { get; set; }
    }
}
