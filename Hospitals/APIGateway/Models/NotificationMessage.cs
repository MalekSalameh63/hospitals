﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;

namespace APIGateway.Models
{
    public class NotificationMessage
    {
        [BsonId]
        public Guid Id { get; set; }
        [JsonProperty("to")]
        public string To { get; set; }
        [JsonProperty("notification")]
        public NotificationBody Notification { get; set; }
    }

    public class NotificationBody
    {
        [JsonProperty("body")]
        public string Body { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("icon")]
        public string Icon { get; set; }
    }
}
