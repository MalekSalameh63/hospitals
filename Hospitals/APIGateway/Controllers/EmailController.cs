﻿using APIGateway.Interfaces.Email;
using Common.DTOs;
using Common.Interfaces.Shared;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace APIGateway.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly IEmailService _emailService;

        public EmailController(IEmailService emailService)
        {
            _emailService = emailService;
        }

        [HttpPost("SendEmail")]
        public async Task<IResponseResult<EmailMessageDto>> SendEmail(EmailMessageDto messageDto)
        {
            return await _emailService.SendEmail(messageDto);
        }

        [HttpGet("GetAll")]
        public async Task<IResponseResult<IEnumerable<EmailMessageDto>>> GetAll()
        {
            return await _emailService.GetAllEmails();
        }
    }
}