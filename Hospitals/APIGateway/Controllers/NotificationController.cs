﻿using APIGateway.Interfaces.Notification;
using APIGateway.Models;
using Common.Common;
using Common.Enums;
using Common.Interfaces.Shared;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace APIGateway.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly INotificationService _notificationService;

        public NotificationController(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }
        [HttpPost("SendNotification")]
        public async Task<IResponseResult<bool>> Post(
            [FromBody] NotificationMessage notificationMessageDto)
        {
            try
            {
                var result = await _notificationService.SendMessage(notificationMessageDto);

                return result;
            }
            catch (Exception ex)
            {
                return new ResponseResult<bool>(GlobalStatus.Failed, ApiStatus.InternalServerError, false, FriendlyStatus.GeneralError, ex.Message);
            }
        }

        [HttpGet("GetAll")]
        public async Task<IResponseResult<IEnumerable<NotificationMessage>>> GetAllNotifications()
        {
            try
            {
                return await _notificationService.GetAllNotifications();
            }
            catch (Exception ex)
            {
                return new ResponseResult<IEnumerable<NotificationMessage>>(GlobalStatus.Failed, ApiStatus.InternalServerError, null, FriendlyStatus.GeneralError, ex.Message);

            }
        }
    }
}