﻿using Common.Authorization;
using Common.Authorization.Models;
using Microsoft.AspNetCore.Mvc;

namespace APIGateway.Controllers
{
    [ApiController]
    public class BaseController : ControllerBase
    {
        /// <summary>
        /// User object in the current request
        /// </summary>
        protected new ContextUser User { get; set; }
        /// <summary>
        /// Current Request Language
        /// </summary>
        protected string Language { get; set; }
        /// <summary>
        /// Creates new instance of BaseController
        /// </summary>
        /// <param name="userContext"></param>
        public BaseController(IUserContext userContext)
        {
            User = userContext.TryGetCurrentUser();
            Language = userContext.GetSessionLanguage();
        }
    }
}