﻿using APIGateway.Interfaces.TemplateFormat;
using Common.Common;
using Common.Interfaces.Shared;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace APIGateway.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TemplateFormatController : ControllerBase
    {
        private readonly ITemplateFormatService _templateFormatService;

        public TemplateFormatController(ITemplateFormatService templateFormatService)
        {
            _templateFormatService = templateFormatService;
        }

        [HttpPost("FormatTemplate")]
        public async Task<IResponseResult<TemplateFormatResponse>> TemplateFormat([FromQuery]int template, [FromBody] Dictionary<string, object> templateData, string language = "en")
        {

            return await _templateFormatService.TemplateFormat(template, templateData, language);
        }

        [HttpGet("GetAll")]
        public async Task<IResponseResult<IEnumerable<TemplateFormatDto>>> GetAll()
        {
            return await _templateFormatService.GetAllTemplates();
        }
        [HttpGet("Get")]

        public async Task<IResponseResult<TemplateFormatDto>> Get(int id, string language = "en")
        {
            return await _templateFormatService.GetTemplate(id, language);
        }
        [HttpPost]
        public async Task<IResponseResult<TemplateFormatDto>> Post([FromBody]TemplateFormatDto templateFormatDto)
        {
            return await _templateFormatService.PostTemplate(templateFormatDto);
        }
        [HttpPut]
        public async Task<IResponseResult<TemplateFormatDto>> Put([FromBody]
            TemplateFormatDto templateFormatDto)
        {
            return await _templateFormatService.PutTemplate(templateFormatDto);
        }
        [HttpDelete]
        public async Task<IResponseResult<TemplateFormatDto>> Delete(int id, string language = "en")
        {
            return await _templateFormatService.DeleteTemplate(id, language);
        }
    }
}