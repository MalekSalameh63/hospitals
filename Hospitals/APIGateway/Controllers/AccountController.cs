﻿using APIGateway.Interfaces.Account;
using APIGateway.Models;
using Common.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace APIGateway.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : BaseController
    {
        private readonly IAccountRepository _accountRepository;

        public AccountController(IAccountRepository accountRepository, IUserContext userContext) : base(userContext)
        {
            _accountRepository = accountRepository;
        }
        [HttpPost("Register")]
        public async Task Register(Register register)
        {
            await _accountRepository.Register(register);
        }

        [HttpPost("Logout")]
        public async Task Logout()
        {
            await _accountRepository.Logout();
        }

        [HttpPost("Login")]
        public async Task<LoginResponse> Login(Login login)
        {
            return await _accountRepository.Login(login);
        }
        [HttpGet("ForgotPassword")]
        public async Task ForgotPassword(string email)
        {
            await _accountRepository.ForgotPassword(email);
        }
        [HttpPost("ResetPassword")]
        public async Task ResetPassword(ResetPasswordRequest resetPassword)
        {
            await _accountRepository.ResetPassword(resetPassword);
        }
        [HttpPost("ChangePassword")]
        public async Task ChangePassword(ChangePasswordRequest changePasswordRequest)
        {
            await _accountRepository.ChangePassword(changePasswordRequest);
        }

    }
}