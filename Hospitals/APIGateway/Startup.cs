using APIGateway.Filters;
using APIGateway.Interfaces.Account;
using APIGateway.Interfaces.Counter;
using APIGateway.Interfaces.Email;
using APIGateway.Interfaces.Notification;
using APIGateway.Interfaces.TemplateFormat;
using APIGateway.Models;
using APIGateway.Services;
using APIGateway.Services.Account;
using APIGateway.Services.Counter;
using APIGateway.Services.Email;
using APIGateway.Services.TemplateFormat;
using Common.Authorization;
using Common.Common.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using MongoDB.Context;
using MongoDB.Interfaces;
using MongoDB.UoW;
using ServiceStack.Configuration;
using System;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace APIGateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();
            services.AddDistributedMemoryCache();

            #region Session Configuration

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(30);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            #endregion

            #region Postgres Configuration

            services.AddDbContext<ApplicationDbContext>(options => options.UseNpgsql(Configuration.GetConnectionString("ApplicationDbContext")));

            #endregion

            #region Identity Configuration

            services.AddIdentity<ApplicationUser, IdentityRole>(
                    options =>
                    {
                        options.Password.RequiredLength = 8;
                        options.Password.RequiredUniqueChars = 3;
                    })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            #endregion

            #region Mongo Configuration

            SharedSettings.MongoConnection = Configuration.GetSection("Mongo").GetValue<string>("Connection");
            SharedSettings.MongoDatabaseName = Configuration.GetSection("Mongo").GetValue<string>("Database");

            #endregion

            #region Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "API Gateway for Front-end Clients",
                    Description = "API Gateway for Front-end Clients",
                });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please insert JWT with Bearer into field",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                   {
                     new OpenApiSecurityScheme
                     {
                       Reference = new OpenApiReference
                       {
                         Type = ReferenceType.SecurityScheme,
                         Id = "Bearer"
                       }
                      },
                      new string[] { }
                    }
                });
                c.OperationFilter<AddRequiredHeaderParameter>();

                //var basePath = PlatformServices.Default.Application.ApplicationBasePath;

                //// Set the comments path for the Swagger JSON and UI.
                //c.IncludeXmlComments(basePath + "APIGateway.xml");
            });

            services.AddSwaggerGenNewtonsoftSupport();
            #endregion

            #region Email configuration
            services.AddSingleton((serviceProvider) => new SmtpClient()
            {
                Host = Configuration.GetSection("EmailConfiguration").GetValue<string>("Host"),
                Port = Configuration.GetSection("EmailConfiguration").GetValue<int>("Port"),
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(
                    Configuration.GetSection("EmailConfiguration").GetValue<string>("User"),
                    Configuration.GetSection("EmailConfiguration").GetValue<string>("Password")
                ),
                EnableSsl = true
            });

            Client.User = Configuration.GetSection("EmailConfiguration").GetValue<string>("User");
            Client.Password = Configuration.GetSection("EmailConfiguration").GetValue<string>("Password");
            #endregion

            #region Authentication

            // Adding Authentication  
            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                })

                // Adding Jwt Bearer  
                .AddJwtBearer(options =>
                {
                    options.SaveToken = true;
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidAudience = Configuration["JWT:ValidAudience"],
                        ValidIssuer = Configuration["JWT:ValidIssuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]))
                    };
                });

            #endregion

            #region Notification Configuration

            NotificationConfiguration.ServerKey =
                Configuration.GetSection("NotificationConfiguration").GetValue<string>("ServerKey");
            NotificationConfiguration.ServerUrl =
                Configuration.GetSection("NotificationConfiguration").GetValue<string>("ServerUrl");

            #endregion

            RegisterServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSession();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            #region Swagger Congiguration

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "MyAPI");
            });


            #endregion

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        private void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IUserContext, UserContext>();
            services.AddSingleton<ITemplateFormatService, TemplateFormatService>();
            services.AddSingleton<IUnitOfWork, UnitOfWork>();
            services.AddSingleton<IMongoContext, MongoContext>();
            services.AddSingleton<ICounterRepository, CounterRepository>();
            services.AddSingleton<IEmailService, EmailService>();
            services.AddSingleton<INotificationService, NotificationService>();

        }
    }
}
