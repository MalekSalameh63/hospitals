﻿using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;

namespace APIGateway.Filters
{
    public class AddRequiredHeaderParameter : IOperationFilter
    {
        /// <summary>
        /// Add custom headers on "try it out" in Swagger
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="context"></param>
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null) operation.Parameters = new List<OpenApiParameter>();

            var descriptor = context.ApiDescription.ActionDescriptor as ControllerActionDescriptor;

            if (descriptor != null && (descriptor.ControllerName.StartsWith("Configuration") || descriptor.ControllerName.StartsWith("Sms") || descriptor.ControllerName.StartsWith("Appointments") || descriptor.ControllerName.StartsWith("ComplexLookup") || descriptor.ControllerName.StartsWith("Lookup") || descriptor.ControllerName.StartsWith("Emr")))
            {
                operation.Parameters.Add(new OpenApiParameter()
                {
                    Name = "language",
                    In = ParameterLocation.Header,
                    Description = "Culture language",
                    Required = false
                });
            }
        }
    }
}
