﻿using Common.DTOs;
using Common.Interfaces.Shared;
using MongoDB.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace APIGateway.Interfaces.Email
{
    public interface IEmailService : IRepository<EmailMessageDto>
    {
        Task<IResponseResult<EmailMessageDto>> SendEmail(EmailMessageDto messageDto);
        Task<IResponseResult<IEnumerable<EmailMessageDto>>> GetAllEmails();

    }
}
