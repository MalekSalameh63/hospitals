﻿using APIGateway.Models;
using Common.Interfaces.Shared;
using MongoDB.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace APIGateway.Interfaces.Notification
{
    public interface INotificationService : IRepository<NotificationMessage>
    {
        Task<IResponseResult<bool>> SendMessage(NotificationMessage messageInformation);
        Task<IResponseResult<IEnumerable<NotificationMessage>>> GetAllNotifications();
    }
}
