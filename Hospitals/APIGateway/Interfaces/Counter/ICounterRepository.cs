﻿using MongoDB.Interfaces;
using System.Threading.Tasks;

namespace APIGateway.Interfaces.Counter
{
    public interface ICounterRepository : IRepository<Models.Counter>
    {
        Task<int> IncrementCounter(string counterType);
    }
}
