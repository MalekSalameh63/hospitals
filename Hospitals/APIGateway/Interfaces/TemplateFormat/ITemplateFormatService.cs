﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Common;
using Common.Interfaces.Shared;
using MongoDB.Interfaces;

namespace APIGateway.Interfaces.TemplateFormat
{
    public interface ITemplateFormatService : IRepository<TemplateFormatDto>
    {
        Task<IResponseResult<IEnumerable<TemplateFormatDto>>> GetAllTemplates();
        Task<IResponseResult<TemplateFormatDto>> GetTemplate(int id, string language);
        Task<IResponseResult<TemplateFormatDto>> PostTemplate(TemplateFormatDto templateFormatDto);
        Task<IResponseResult<TemplateFormatDto>> PutTemplate(TemplateFormatDto templateFormatDto);
        Task<IResponseResult<TemplateFormatDto>> DeleteTemplate(int id, string language);
        Task<IResponseResult<TemplateFormatResponse>> TemplateFormat(int template, Dictionary<string, object> templateData, string language);
    }
}
