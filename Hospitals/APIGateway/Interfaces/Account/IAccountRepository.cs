﻿using System.Threading.Tasks;
using APIGateway.Models;

namespace APIGateway.Interfaces.Account
{
    public interface IAccountRepository
    {
        Task Register(Register register);
        Task Logout();
        Task<LoginResponse> Login(Login login);
        Task ForgotPassword(string email);
        Task ResetPassword(ResetPasswordRequest resetPassword);
        Task ChangePassword(ChangePasswordRequest changePasswordRequest);
    }
}
