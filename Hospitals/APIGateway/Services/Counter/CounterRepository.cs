﻿using APIGateway.Interfaces.Counter;
using MongoDB.Interfaces;
using MongoDB.Repository;
using System.Threading.Tasks;

namespace APIGateway.Services.Counter
{
    public class CounterRepository : BaseRepository<Models.Counter>, ICounterRepository
    {
        public CounterRepository(IMongoContext context) : base(context)
        {

        }

        public async Task<int> IncrementCounter(string counterType)
        {
            var counter = await GetByProperty(counterType, nameof(Models.Counter.CounterType));

            if (counter != null)
            {
                counter.Count++;

                await UpdateAsync(counter);
            }
            else
            {
                counter = new Models.Counter()
                {
                    CounterType = counterType,
                    Count = 1
                };

                await AddAsync(counter);
            }

            return counter.Count;
        }
    }
}
