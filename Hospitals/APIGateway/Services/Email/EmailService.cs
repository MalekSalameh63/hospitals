﻿using APIGateway.Interfaces.Email;
using APIGateway.Models;
using Common.Common;
using Common.DTOs;
using Common.Enums;
using Common.Interfaces.Shared;
using MongoDB.Interfaces;
using MongoDB.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace APIGateway.Services.Email
{
    public class EmailService : BaseRepository<EmailMessageDto>, IEmailService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly SmtpClient _smtpClient;

        public EmailService(IMongoContext context, IUnitOfWork unitOfWork, SmtpClient smtpClient) : base(context)
        {
            _unitOfWork = unitOfWork;
            _smtpClient = smtpClient;
        }

        public async Task<IResponseResult<EmailMessageDto>> SendEmail(EmailMessageDto messageDto)
        {
            //Check if recipient list is null
            if (!messageDto.To.Any())
                //"Recipient list is empty."
                return new ResponseResult<EmailMessageDto>(GlobalStatus.Success, ApiStatus.Ok, null, FriendlyStatus.EmptyRecipientList, null);

            _smtpClient.Credentials = new NetworkCredential(Client.User, Client.Password);

            //Send email
            try
            {
                messageDto.From = Client.User;
                var message = new EmailMessage(messageDto);

                //Initialize message
                var mailMessage = new MailMessage
                {
                    From = new MailAddress(message.From),
                    Subject = message.Subject,
                    Body = message.Body,
                    IsBodyHtml = message.IsBodyHtml
                };

                //Check attachment count
                if (message.Attachments.Count > EmailSetting.AttachmentMaxNumber)
                    throw new Exception("Exceeded maximum number of attachments");

                //Fill attachment list
                if (message.Attachments.Any())
                {
                    foreach (var attachment in message.Attachments)
                    {
                        mailMessage.Attachments.Add(attachment.Attachment);
                    }
                }

                //Fill recipient list
                foreach (var to in message.To)
                {
                    mailMessage.To.Clear();
                    mailMessage.To.Add(new MailAddress(to.ToString()));

                    //Fill cc list
                    if (message.Cc.Any())
                    {
                        foreach (var cc in message.Cc)
                        {
                            mailMessage.CC.Add(new MailAddress(cc.ToString()));
                        }
                    }
                }

                //Send email
                await _smtpClient.SendMailAsync(mailMessage);

                //Add email to database
                await AddAsync(messageDto);
                await _unitOfWork.Commit();
                var responseMessageDto = new EmailMessageDto(message);

                return new ResponseResult<EmailMessageDto>(GlobalStatus.Success, ApiStatus.Ok, responseMessageDto, FriendlyStatus.None, null);
            }
            catch (Exception ex)
            {
                return new ResponseResult<EmailMessageDto>(GlobalStatus.Failed, ApiStatus.InternalServerError, null, FriendlyStatus.GeneralError, ex.Message);
            }
        }

        public async Task<IResponseResult<IEnumerable<EmailMessageDto>>> GetAllEmails()
        {
            try
            {
                var emails = await GetAll();
                return new ResponseResult<IEnumerable<EmailMessageDto>>(GlobalStatus.Success, ApiStatus.Ok, emails, FriendlyStatus.None, null);
            }
            catch (Exception ex)
            {
                return new ResponseResult<IEnumerable<EmailMessageDto>>(GlobalStatus.Failed, ApiStatus.InternalServerError, null, FriendlyStatus.GeneralError, ex.Message);
            }
        }
    }
}
