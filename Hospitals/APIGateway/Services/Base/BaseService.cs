﻿using APIGateway.Interfaces.Base;
using Common.Authorization;
using Common.Authorization.Models;

namespace APIGateway.Services.Base
{
    public class BaseService : IBaseService
    {
        /// <summary>
        /// User object in the current request
        /// </summary>
        protected ContextUser User { get; set; }
        /// <summary>
        /// Current Request Language
        /// </summary>
        protected string Language { get; set; }
        /// <summary>
        /// Creates new instance of BaseService
        /// </summary>
        /// <param name="userContext"></param>
        public BaseService(IUserContext userContext)
        {
            User = userContext.TryGetCurrentUser();
            Language = userContext.GetSessionLanguage();
        }
    }
}
