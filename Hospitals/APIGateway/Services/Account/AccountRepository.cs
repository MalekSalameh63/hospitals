﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using APIGateway.Interfaces.Account;
using APIGateway.Models;
using APIGateway.Services.Base;
using Common.Authorization;
using Common.Authorization.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace APIGateway.Services.Account
{
    public class AccountRepository : BaseService, IAccountRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IConfiguration _configuration;

        public AccountRepository(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IConfiguration configuration, IUserContext userContext) : base(userContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
        }

        public async Task Register(Register register)
        {
            var user = new ApplicationUser()
            {
                UserName = register.Email,
                Email = register.Email
            };
            var result = await _userManager.CreateAsync(user, register.Password);
            if (result.Succeeded)
            {
                await _signInManager.SignInAsync(user, isPersistent: false);
            }
        }

        public async Task Logout()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<LoginResponse> Login(Login login)
        {
            var user = await _userManager.FindByNameAsync(login.UserName);
            if (user != null && await _userManager.CheckPasswordAsync(user, login.Password))
            {
                var userRoles = await _userManager.GetRolesAsync(user);
                var contextUser = new ContextUser
                {
                    Id = user.Id,
                    UserName = user.UserName
                };
                var authClaims = new List<Claim>
                    {new Claim(nameof(ContextUser), JsonConvert.SerializeObject(contextUser))};
                authClaims.AddRange(userRoles.Select(userRole => new Claim(ClaimTypes.Role, userRole)));

                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

                var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );
                return new LoginResponse()
                {
                    Token = new JwtSecurityTokenHandler().WriteToken(token),
                    Expiration = token.ValidTo,
                    UserName = user.UserName,
                    UserId = user.Id
                };
            }
            else
            {
                return null;
            }
        }

        public async Task ForgotPassword(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
        }
        public async Task ResetPassword(ResetPasswordRequest resetPassword)
        {
            var user = await _userManager.FindByEmailAsync(resetPassword.Email);

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var passwordReset = await _userManager.ResetPasswordAsync(user, token, resetPassword.NewPassword);
        }

        public async Task ChangePassword(ChangePasswordRequest changePasswordRequest)
        {

            var user = await _userManager.FindByNameAsync(User.UserName);
            var result = await _userManager.ChangePasswordAsync(user, changePasswordRequest.CurrentPassword,
                changePasswordRequest.NewPassword);

        }
    }
}
