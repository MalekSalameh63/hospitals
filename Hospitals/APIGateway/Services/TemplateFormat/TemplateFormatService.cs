﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIGateway.Interfaces.Counter;
using APIGateway.Interfaces.TemplateFormat;
using Common.Common;
using Common.Enums;
using Common.Interfaces.Shared;
using FormatWith;
using MongoDB.Interfaces;
using MongoDB.Repository;

namespace APIGateway.Services.TemplateFormat
{
    public class TemplateFormatService : BaseRepository<TemplateFormatDto>, ITemplateFormatService
    {
        private readonly IUnitOfWork _iUnitOfWork;
        private readonly ICounterRepository _counterRepository;

        public TemplateFormatService(IUnitOfWork iUnitOfWork, ICounterRepository counterRepository, IMongoContext context) : base(context)
        {
            _iUnitOfWork = iUnitOfWork;
            _counterRepository = counterRepository;
        }

        public async Task<IResponseResult<TemplateFormatResponse>> TemplateFormat(int template, Dictionary<string, object> templateData, string language = "en")
        {
            try
            {
                var templateFormat = await GetTemplateById((int)template, language);



                if (!templateFormat.AllGreen())
                    return new ResponseResult<TemplateFormatResponse>(GlobalStatus.Failed,
                        ApiStatus.InternalServerError, null, FriendlyStatus.GeneralError, null);
                var templateFormatResponse = new TemplateFormatResponse
                {
                    EmailTemplate =
                        templateFormat.Data.EmailTemplate.FormatWith(templateData, MissingKeyBehaviour.Ignore, null, '(', ')'),
                    SmsTemplate = templateFormat.Data.SmsTemplate.FormatWith(templateData, MissingKeyBehaviour.Ignore, null, '(', ')'),
                    NotificationTemplate = templateFormat.Data.NotificationTemplate.FormatWith(templateData,
                        MissingKeyBehaviour.Ignore, null, '(', ')'),
                    TemplateName = templateFormat.Data.TemplateName,
                    Title = templateFormat.Data.Title
                };
                return new ResponseResult<TemplateFormatResponse>(GlobalStatus.Success, ApiStatus.Ok, templateFormatResponse, FriendlyStatus.Ok, null);
            }
            catch (Exception ex)
            {
                return new ResponseResult<TemplateFormatResponse>(GlobalStatus.Failed, ApiStatus.InternalServerError, null, FriendlyStatus.GeneralError, ex.Message);
            }
        }

        public async Task<IResponseResult<IEnumerable<TemplateFormatDto>>> GetAllTemplates()
        {
            try
            {
                var data = await GetAll();
                return new ResponseResult<IEnumerable<TemplateFormatDto>>(GlobalStatus.Success, ApiStatus.Ok, data, FriendlyStatus.Ok, null);
            }
            catch (Exception ex)
            {
                return new ResponseResult<IEnumerable<TemplateFormatDto>>(GlobalStatus.Failed, ApiStatus.InternalServerError, null, FriendlyStatus.Ok, ex.Message);
            }
        }
        public async Task<IResponseResult<TemplateFormatDto>> GetTemplateById(int id, string language)
        {
            try
            {
                var filters = new Dictionary<string, object>() { { nameof(TemplateFormatDto.TemplateId), id }, { nameof(TemplateFormatDto.Language), language } };
                var data = await GetAllWithFilters(filters);
                return new ResponseResult<TemplateFormatDto>(GlobalStatus.Success, ApiStatus.Ok, data.FirstOrDefault(), FriendlyStatus.Ok, null);
            }
            catch (Exception ex)
            {
                return new ResponseResult<TemplateFormatDto>(GlobalStatus.Failed, ApiStatus.InternalServerError, null, FriendlyStatus.Ok, ex.Message);
            }
        }

        public async Task<IResponseResult<TemplateFormatDto>> GetTemplate(int id, string language = "en")
        {
            try
            {
                var data = await GetTemplateById(id, language);
                return data;
            }
            catch (Exception ex)
            {
                return new ResponseResult<TemplateFormatDto>(GlobalStatus.Failed, ApiStatus.InternalServerError, null, FriendlyStatus.Ok, ex.Message);
            }
        }
        public async Task<IResponseResult<TemplateFormatDto>> PostTemplate(TemplateFormatDto templateFormatDto)
        {
            try
            {
                templateFormatDto.CreatedOn = DateTime.UtcNow;
                templateFormatDto.ModifiedOn = DateTime.UtcNow;
                templateFormatDto.TemplateId = await _counterRepository.IncrementCounter(nameof(templateFormatDto));

                await AddAsync(templateFormatDto);
                await _iUnitOfWork.Commit();
                return new ResponseResult<TemplateFormatDto>(GlobalStatus.Success, ApiStatus.Ok, null, FriendlyStatus.Ok, null);
            }
            catch (Exception ex)
            {
                return new ResponseResult<TemplateFormatDto>(GlobalStatus.Failed, ApiStatus.InternalServerError, null, FriendlyStatus.Ok, ex.Message);
            }
        }
        public async Task<IResponseResult<TemplateFormatDto>> PutTemplate(TemplateFormatDto templateFormatDto)
        {
            try
            {
                var filters = new Dictionary<string, object>() { { nameof(TemplateFormatDto.TemplateId), templateFormatDto.Id }, { nameof(TemplateFormatDto.Language), templateFormatDto.Language } };
                var template = (await GetAllWithFilters(filters)).FirstOrDefault();
                if (template == null)
                    return new ResponseResult<TemplateFormatDto>(GlobalStatus.Failed, ApiStatus.BadRequest, null,
                        FriendlyStatus.GeneralError, null);
                template.ModifiedOn = DateTime.UtcNow;
                template.EmailTemplate = templateFormatDto.EmailTemplate;
                template.NotificationTemplate = templateFormatDto.NotificationTemplate;
                template.SmsTemplate = templateFormatDto.SmsTemplate;
                template.TemplateName = templateFormatDto.TemplateName;
                await UpdateAsync(template);
                await _iUnitOfWork.Commit();
                return new ResponseResult<TemplateFormatDto>(GlobalStatus.Success, ApiStatus.Ok, templateFormatDto, FriendlyStatus.Ok, null);
            }
            catch (Exception ex)
            {
                return new ResponseResult<TemplateFormatDto>(GlobalStatus.Failed, ApiStatus.InternalServerError, null, FriendlyStatus.Ok, ex.Message);
            }
        }
        public async Task<IResponseResult<TemplateFormatDto>> DeleteTemplate(int id, string language = "en")
        {
            try
            {
                var filters = new Dictionary<string, object>() { { nameof(TemplateFormatDto.TemplateId), id }, { nameof(TemplateFormatDto.Language), language } };
                var template = (await GetAllWithFilters(filters)).FirstOrDefault();
                if (template == null)
                    return new ResponseResult<TemplateFormatDto>(GlobalStatus.Failed, ApiStatus.BadRequest, null,
                        FriendlyStatus.TemplateDoesNotExist, null);
                await RemoveAsync(template.Id);
                await _iUnitOfWork.Commit();
                return new ResponseResult<TemplateFormatDto>(GlobalStatus.Success, ApiStatus.Ok, template, FriendlyStatus.Ok, null);
            }
            catch (Exception ex)
            {
                return new ResponseResult<TemplateFormatDto>(GlobalStatus.Failed, ApiStatus.InternalServerError, null, FriendlyStatus.Ok, ex.Message);
            }
        }
    }
}
