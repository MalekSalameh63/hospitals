﻿using APIGateway.Interfaces.Notification;
using APIGateway.Models;
using Common.Authorization;
using Common.Common;
using Common.Enums;
using Common.Interfaces.Shared;
using MongoDB.Interfaces;
using MongoDB.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace APIGateway.Services
{
    public class NotificationService : BaseRepository<NotificationMessage>, INotificationService
    {
        private readonly IUnitOfWork _unitOfWork;
        public NotificationService(IMongoContext context, IUnitOfWork unitOfWork) : base(context)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<IResponseResult<bool>> SendMessage(NotificationMessage messageInformation)
        {
            try
            {
                var jsonMessage = JsonConvert.SerializeObject(messageInformation);
                var request = new HttpRequestMessage(HttpMethod.Post, NotificationConfiguration.ServerUrl);
                request.Headers.TryAddWithoutValidation(SystemConstants.ApiGatewayAuthorization, "key =" + NotificationConfiguration.ServerKey);
                request.Content = new StringContent(jsonMessage, Encoding.UTF8, "application/json");
                HttpResponseMessage result;
                using (var client = new HttpClient())
                {
                    result = await client.SendAsync(request);
                }


                await AddAsync(messageInformation);
                await _unitOfWork.Commit();
                return new ResponseResult<bool>(GlobalStatus.Success, ApiStatus.Ok, true, FriendlyStatus.None, null);

            }
            catch (Exception ex)
            {
                return new ResponseResult<bool>(GlobalStatus.Failed, ApiStatus.InternalServerError, false, FriendlyStatus.GeneralError, ex.Message);
            }
        }
        public async Task<IResponseResult<IEnumerable<NotificationMessage>>> GetAllNotifications()
        {
            try
            {
                var listPushNotificationsDto = GetAll().Result.ToList();
                return new ResponseResult<IEnumerable<NotificationMessage>>(GlobalStatus.Success, ApiStatus.Ok, listPushNotificationsDto, FriendlyStatus.None, null);
            }
            catch (Exception)
            {
                throw new Exception("There is error in reading notifications");
            }
        }
    }
}
