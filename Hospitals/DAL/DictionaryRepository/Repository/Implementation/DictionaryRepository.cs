﻿using Common.Common.Configuration;
using Common.DTOs.Dictionary;
using Common.Helper;
using DictionaryRepository.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace DictionaryRepository.Repository.Implementation
{
    public class DictionaryRepository : IDictionaryRepository
    {
        public DictionaryRepository()
        {

        }
        public WordDTO GetWordByKey(string key, string language)
        {
            try
            {
                return HttpClientWrapper<WordDTO>.GetRequest(SharedSettings.MicroserviceEncryption.Dictionary, SharedSettings.DictionaryServiceBaseUrl + "api/Dictionary/translate?key=" + key + "&language=" + language, "");
            }
            catch (Exception)
            {
                return new WordDTO() { Word = key };
            }
        }
    }
}
