﻿using Common.Common;
using Common.Common.Configuration;
using Common.DTOs.Dictionary;
using Common.DTOs.Lookup;
using Common.Enums;
using Common.Helper;
using Common.Interfaces.Shared;
using DictionaryRepository.Repository.Interfaces;
using System;
using System.Collections.Generic;

namespace DictionaryRepository.Repository.Implementation
{
    public class LookupRepository : ILookupRepository
    {
        public LookupLocalizationDTO GetLookupByCategoryEnum(LookupCategory categoryId, int enumReference, string language)
        {
            try
            {

                if (!string.IsNullOrEmpty(language))
                {
                    IResponseResult<LookupLocalizationDTO> response =
                        HttpClientWrapper<ResponseResult<LookupLocalizationDTO>>.GetRequest(SharedSettings.MicroserviceEncryption.Lookup, SharedSettings.LookupServiceBaseUrl, "api/lookup/" + (int)categoryId + "/" + enumReference + "/" + language);

                    if (response != null)
                    {
                        return response.Data;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<LookupLocalizationDTO> GetLookupsByCategoryId(LookupCategory categoryId, string language)
        {
            try
            {
                IResponseResult<List<LookupLocalizationDTO>> response =
                    HttpClientWrapper<ResponseResult<List<LookupLocalizationDTO>>>.GetRequest(SharedSettings.MicroserviceEncryption.Lookup, SharedSettings.LookupServiceBaseUrl, "api/lookup/" + (int)categoryId + "/" + language);

                if (response != null)
                {
                    return response.Data;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<LookupLocalizationDTO> GetLookupsByCategoryId(LookupCategory categoryId)
        {
            try
            {
                IResponseResult<List<LookupLocalizationDTO>> response =
                    HttpClientWrapper<ResponseResult<List<LookupLocalizationDTO>>>.GetRequest(SharedSettings.MicroserviceEncryption.Lookup, SharedSettings.LookupServiceBaseUrl, "api/lookup/" + (int)categoryId);

                if (response != null)
                {
                    return response.Data;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public LookupLocalizationDTO GetLookupById(int lookupId, string language)
        {
            try
            {
                IResponseResult<LookupLocalizationDTO> response =
                    HttpClientWrapper<ResponseResult<LookupLocalizationDTO>>.GetRequest(SharedSettings.MicroserviceEncryption.Lookup, SharedSettings.LookupServiceBaseUrl, "api/lookup/LookupById/" + lookupId + "/" + language);

                if (response != null)
                {
                    return response.Data;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
