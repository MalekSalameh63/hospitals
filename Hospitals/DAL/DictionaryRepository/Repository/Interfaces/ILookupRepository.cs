﻿using Common.DTOs.Dictionary;
using Common.DTOs.Lookup;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DictionaryRepository.Repository.Interfaces
{
    public interface ILookupRepository
    {
        List<LookupLocalizationDTO> GetLookupsByCategoryId(LookupCategory categoryId, string language);
        LookupLocalizationDTO GetLookupByCategoryEnum(LookupCategory categoryId, int enumReference, string language);
        List<LookupLocalizationDTO> GetLookupsByCategoryId(LookupCategory categoryId);
        LookupLocalizationDTO GetLookupById(int lookupId, string language);
    }
}
