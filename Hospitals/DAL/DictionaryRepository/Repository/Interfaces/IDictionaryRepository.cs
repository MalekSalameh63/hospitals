﻿using Common.DTOs.Dictionary;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DictionaryRepository.Repository.Interfaces
{
    public interface IDictionaryRepository
    {
        WordDTO GetWordByKey(string key, string language);
    }
}
