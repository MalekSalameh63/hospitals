﻿using Common.Authorization;
using Common.Common.Configuration;
using Common.Helper;
using DictionaryRepository.Services;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DictionaryRepository.Models
{
    public class DictionaryVO
    {
        public string Key { get; }
        public DictionaryVO(string key)
        {
            Key = key;
        }
        public string Translate(string language)
        {
            //return "key";
            if (language != null)
            {
                return DictionaryService.GetTranslation(Key, language);
            }
            else
            {
                return Key;
            }
        }
    }
}
