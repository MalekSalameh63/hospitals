﻿using Common.Authorization;
using Common.Common.Configuration;
using Common.Enums;
using DictionaryRepository.Services;
using Microsoft.AspNetCore.Http;

namespace DictionaryRepository.Models
{
    public class LookupVO
    {
        public LookupCategory LookupCategory { get; set; }
        public int EnumReference { get; set; }

        public LookupVO(LookupCategory lookupCategory, int enumReference)
        {
            LookupCategory = lookupCategory;
            EnumReference = enumReference;
        }
        public string Translate(string language)
        {
            if (!string.IsNullOrEmpty(language))
            {
                return LookupService.GetTranslationByCategoryEnum(LookupCategory, EnumReference, language);
            }
            else
            {
                return LookupCategory.ToString();
            }
        }
    }
}
