﻿using Common.Helper;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;
using DictionaryRepository.Repository.Interfaces;
using DictionaryRepository.Repository.Implementation;
using Common.DTOs.Dictionary;
using System.Text.RegularExpressions;
using Common.Enums;
using Common.DTOs.Lookup;

namespace DictionaryRepository.Services
{
    public static class LookupService
    {
        private static ILookupRepository _lookupRepository = new LookupRepository();
        public static string GetTranslationByCategoryEnum(LookupCategory category, int enumReference, string language)
        {
            try
            {
                LookupLocalizationDTO lookup = _lookupRepository.GetLookupByCategoryEnum(category, enumReference, language);

                if (lookup == null || string.IsNullOrEmpty(lookup.LocalName))
                {
                    return string.Empty;
                }
                else
                {
                    return lookup.LocalName;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public static List<LookupLocalizationDTO> GetLookupsByCategoryId(LookupCategory category, string language)
        {
            try
            {
                List<LookupLocalizationDTO> lookups = _lookupRepository.GetLookupsByCategoryId(category, language);

                return lookups;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
