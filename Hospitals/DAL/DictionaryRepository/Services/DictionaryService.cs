﻿using Common.Helper;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;
using DictionaryRepository.Repository.Interfaces;
using DictionaryRepository.Repository.Implementation;
using Common.DTOs.Dictionary;
using System.Text.RegularExpressions;

namespace DictionaryRepository.Services
{
    public static class DictionaryService
    {
        private static IDictionaryRepository dictionaryRepository = new Repository.Implementation.DictionaryRepository();
        public static string GetTranslation(string key, string language)
        {
            try
            {
                if (string.IsNullOrEmpty(key) || !Regex.IsMatch(key, "^[a-zA-Z0-9_]+$"))
                {
                    return key;
                }
                else
                {
                    WordDTO word = dictionaryRepository.GetWordByKey(key, language);

                    if (word == null || string.IsNullOrEmpty(word.Word))
                    {
                        return key;
                    }
                    else
                    {
                        return word.Word;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
