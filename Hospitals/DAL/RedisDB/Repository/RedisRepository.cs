﻿using Common.Common.Configuration;
using RedisDB.Interfaces;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedisDB.Repository
{
    public class RedisRepository : IRedisRepository
    {
        private readonly IConnectionMultiplexer _connectionMultiplexer;
        protected readonly IDatabase _redisDb;

        public RedisRepository()
        {
            if (string.IsNullOrEmpty(SharedSettings.RedisConnection))
            {
                throw new Exception("Redis Connection String Empty");
            }

            _connectionMultiplexer = ConnectionMultiplexer.Connect(SharedSettings.RedisConnection);

            _redisDb = _connectionMultiplexer.GetDatabase(SharedSettings.RedisDatabaseId);
        }
        /// <summary>
        /// Inserts key with an array of hash entries, 'key' as key, 'hashEntries' as value.
        /// </summary>
        /// <param name="key">Key to store and access the HashEntries.</param>
        /// <param name="hashEntries">Array of Hash Entry, Stored as the 'Value' for the key.</param>
        public void AddHash(string key, HashEntry[] hashEntries)
        {
            _redisDb.HashSetAsync(key, hashEntries);
        }
        /// <summary>
        /// Gets the array of hash entries that is stored as the 'value' for the specified key.
        /// </summary>
        /// <param name="key">Key to access the HashEntry array.</param>
        /// <returns></returns>
        public HashEntry[] GetHashEntries(string key)
        {
            return _redisDb.HashGetAll(key);
        }
        /// <summary>
        /// Gets the array of hash entries that is stored as the 'value' for the specified key.
        /// </summary>
        /// <param name="key">Key to access the HashEntry array.</param>
        /// <returns></returns>
        public async Task<HashEntry[]> GetHashEntriesAsync(string key)
        {
            return await _redisDb.HashGetAllAsync(key);
        }
        /// <summary>
        /// Gets one of the hash entries inside a hash entry array using the key that points to that array and the hashkey that points to a specific index inside the array. 
        /// </summary>
        /// <param name="key">Key that points to the array of HashEntries</param>
        /// <param name="hashKey">Key that represents the index of a specific HashEntry to get</param>
        /// <returns></returns>
        public async Task<string> GetHashEntryAsync(string key, string hashKey)
        {
            return await _redisDb.HashGetAsync(key, hashKey);
        }
        /// <summary>
        /// Adds specified entry to database under the provided key
        /// </summary>
        /// <param name="key">Key to store the entry under</param>
        /// <param name="entry">String entry to store under provided key</param>
        public void AddString(string key, string entry)
        {
            _redisDb.StringSetAsync(key, entry);
        }
        /// <summary>
        /// Returns the string that is stored under the specified key
        /// </summary>
        /// <param name="key">Key to find the value under</param>
        /// <returns></returns>
        public async Task<string> GetStringAsync(string key)
        {
            return await _redisDb.StringGetAsync(key);
        }
        /// <summary>
        /// Gets all the existing Keys in the current database.
        /// </summary>
        /// <returns>IEnumerable of RedisKeys</returns>
        public IEnumerable<RedisKey> GetAllKeys()
        {
            var endPoints = _connectionMultiplexer.GetEndPoints();

            return _connectionMultiplexer.GetServer(endPoints[0]).Keys();
        }
        /// <summary>
        /// Deletes all entries under the provided key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task DeleteEntryByKey(string key)
        {
            await _redisDb.KeyDeleteAsync(key);
        }
    }
}
