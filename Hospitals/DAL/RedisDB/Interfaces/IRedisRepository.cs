﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedisDB.Interfaces
{
    public interface IRedisRepository
    {
        /// <summary>
        /// Inserts key with an array of hash entries, 'key' as key, 'hashEntries' as value.
        /// </summary>
        /// <param name="key">Key to store and access the HashEntries.</param>
        /// <param name="hashEntries">Array of Hash Entry, Stored as the 'Value' for the key.</param>
        public void AddHash(string key, HashEntry[] hashEntries);
        /// <summary>
        /// Gets the array of hash entries that is stored as the 'value' for the specified key.
        /// </summary>
        /// <param name="key">Key to access the HashEntry array.</param>
        /// <returns></returns>
        public Task<HashEntry[]> GetHashEntriesAsync(string key);
        /// <summary>
        /// Gets the array of hash entries that is stored as the 'value' for the specified key.
        /// </summary>
        /// <param name="key">Key to access the HashEntry array.</param>
        /// <returns></returns>
        public HashEntry[] GetHashEntries(string key);
        /// <summary>
        /// Gets one of the hash entries inside a hash entry array using the key that points to that array and the hashkey that points to a specific index inside the array. 
        /// </summary>
        /// <param name="key">Key that points to the array of HashEntries</param>
        /// <param name="hashKey">Key that represents the index of a specific HashEntry to get</param>
        /// <returns></returns>
        public Task<string> GetHashEntryAsync(string key, string hashKey);
        /// <summary>
        /// Adds specified entry to database under the provided key
        /// </summary>
        /// <param name="key">Key to store the entry under</param>
        /// <param name="entry">String entry to store under provided key</param>
        public void AddString(string key, string entry);
        /// <summary>
        /// Returns the string that is stored under the specified key
        /// </summary>
        /// <param name="key">Key to find the value under</param>
        /// <returns></returns>
        public Task<string> GetStringAsync(string key);
        /// <summary>
        /// Gets all the existing Keys in the current database.
        /// </summary>
        /// <returns>IEnumerable of RedisKeys</returns>
        IEnumerable<RedisKey> GetAllKeys();
        /// <summary>
        /// Deletes all entries under the provided key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Task DeleteEntryByKey(string key);
    }
}
