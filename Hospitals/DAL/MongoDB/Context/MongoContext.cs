﻿using Common.Common.Configuration;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using MongoDB.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoDB.Context
{
    public class MongoContext : IMongoContext
    {
        private IMongoDatabase Database { get; set; }
        public IClientSessionHandle Session { get; set; }
        public MongoClient MongoClient { get; set; }
        private readonly List<Func<Task>> _commands;


        public MongoContext(IConfiguration configuration)
        {
            // Every command will be stored and it'll be processed at SaveChanges
            _commands = new List<Func<Task>>();
        }

        
        // DEV-NOTE: (SAA) this does nothing useful right now. Should be removed or modified.
        public async Task<int> SaveChanges()
        {
            ConfigureMongo();

            //using (Session = await MongoClient.StartSessionAsync())
            //{
            //    Session.StartTransaction();

            //    var commandTasks = _commands.Select(c => c());

            //    await Task.WhenAll(commandTasks);

            //    await Session.CommitTransactionAsync();
            //}

            //return _commands.Count;

            return 1;
        }

        private void ConfigureMongo()
        {
            if (MongoClient != null)
                return;

            // Configure mongo (You can inject the config, just to simplify)

            if(string.IsNullOrEmpty(SharedSettings.MongoConnection) || string.IsNullOrEmpty(SharedSettings.MongoDatabaseName))
            {
                throw new Exception("Mongo Connection String and/or Database Name Empty");
            }

            MongoClient = new MongoClient(SharedSettings.MongoConnection);

            Database = MongoClient.GetDatabase(SharedSettings.MongoDatabaseName);

        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            ConfigureMongo();
            return Database.GetCollection<T>(name);
        }

        public void Dispose()
        {
            Session?.Dispose();
            GC.SuppressFinalize(this);
        }

        public void AddCommand(Func<Task> func)
        {
            _commands.Add(func);
        }
    }
}
