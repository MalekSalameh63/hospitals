﻿using System;
using System.Threading.Tasks;

namespace MongoDB.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        Task<bool> Commit();
    }
}
