﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MongoDB.Interfaces
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        void Add(TEntity obj);
        Task AddAsync(TEntity obj);
        Task AddManyAsync(IEnumerable<TEntity> lst);
        void UpsertRecord(Guid id, TEntity record);
        Task<TEntity> GetById(dynamic id);
        Task<List<TEntity>> GetListByProperty(object value, string property);
        Task<TEntity> GetByProperty(object value, string property);
        Task<TEntity> GetWithFilters(Dictionary<string, object> filters);
        Task<TEntity> GetFirstOrDefault();
        Task<IEnumerable<TEntity>> GetAll();
        Task<IEnumerable<TEntity>> GetAllWithFilters(Dictionary<string, object> filters);
        void RemoveWithFilters(Dictionary<string, object> filters);
        void RemoveAllWithFilters(Dictionary<string, object> filters);
        void Update(TEntity obj);
        Task UpdateAsync(TEntity obj);
        Task UpdateManyAsync(FilterDefinition<TEntity> filter, UpdateDefinition<TEntity> update);
        void Remove(dynamic id);
        Task RemoveAsync(dynamic id);
        Task<IEnumerable<TEntity>> GetRange(int index, int count);
        Task<long> GetCount();
        Task<long> GetCountWithFilters(Dictionary<string, object> filters);
    }
}
