﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Interfaces;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MongoDB.Repository
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly IMongoContext Context;
        protected IMongoCollection<TEntity> DbSet;

        protected BaseRepository(IMongoContext context)
        {
            Context = context;
        }

        public virtual void Add(TEntity obj)
        {
            ConfigDbSet();
            DbSet.InsertOneAsync(obj);
            //Context.AddCommand(() => DbSet.InsertOneAsync(obj));
        }

        public virtual async Task AddAsync(TEntity obj)
        {
            ConfigDbSet();

            // N.B. If database and/or document doesn't exist, Mongo will create it automatically.
            await DbSet.InsertOneAsync(obj);
        } 
        
        public virtual async Task AddManyAsync(IEnumerable<TEntity> lst)
        {
            ConfigDbSet();

            await DbSet.InsertManyAsync(lst);
        }
        /// <summary>
        /// Updates existing record if the ID exists, Adds new if ID doesn't exist
        /// </summary>
        /// <param name="id">GUID ID</param>
        /// <param name="record">Dynamic object record</param>
        public virtual void UpsertRecord(Guid id, TEntity record)
        {
            ConfigDbSet();

            DbSet.ReplaceOne(new BsonDocument("_id", id), record, new ReplaceOptions { IsUpsert = true });
        }
        protected void ConfigDbSet()
        {
            DbSet = Context.GetCollection<TEntity>(typeof(TEntity).Name);
        }

        public virtual async Task<TEntity> GetById(dynamic id)
        {
            ConfigDbSet();
            var data = await DbSet.FindAsync(Builders<TEntity>.Filter.Eq("_id", id));
            return data.SingleOrDefault();
        }
        /// <summary>
        /// Gets object by property and the value of that property
        /// </summary>
        /// <param name="value">Value to look for</param>
        /// <param name="property">The property to look for the value under</param>
        /// <returns>TEntity item</returns>
        public virtual async Task<TEntity> GetByProperty(object value, string property)
        {
            ConfigDbSet();
            var data = await DbSet.FindAsync(Builders<TEntity>.Filter.Eq(property, value));
            return data.SingleOrDefault();
        }

        /// <summary>
        /// Gets object by Filters
        /// </summary>
        /// <param name="filters">Dictionary<string, object> </param>
        /// <returns>TEntity item</returns>
        public virtual async Task<TEntity> GetWithFilters(Dictionary<string, object> filters)
        {
            ConfigDbSet();

            FilterDefinition<TEntity> builders = Builders<TEntity>.Filter.Empty;
            foreach (var filter in filters)
            {
                builders &= Builders<TEntity>.Filter.Eq(filter.Key, filter.Value);
            }

            var data = await DbSet.FindAsync(builders);
            return data.FirstOrDefault();
        }

        /// <summary>
        /// Gets list of objects by property and the value of that property
        /// </summary>
        /// <param name="value">Value to look for</param>
        /// <param name="property">The property to look for the value under</param>
        /// <returns>List of TEntity items</returns>
        public virtual async Task<List<TEntity>> GetListByProperty(object value, string property)
        {
            ConfigDbSet();
            var data = await DbSet.FindAsync(Builders<TEntity>.Filter.Eq(property, value));
            return data.ToList();
        }

        public virtual async Task<IEnumerable<TEntity>> GetAll()
        {
            ConfigDbSet();
            var all = await DbSet.FindAsync(Builders<TEntity>.Filter.Empty);
            return all.ToList();
        }
        public virtual async Task<TEntity> GetFirstOrDefault()
        {
            ConfigDbSet();

            var all = await DbSet.FindAsync(Builders<TEntity>.Filter.Empty);

            return all.FirstOrDefault();
        }
        public async Task<IEnumerable<TEntity>> GetAllWithFilters(Dictionary<string, object> filters)
        {
            ConfigDbSet();

            FilterDefinition<TEntity> builders = Builders<TEntity>.Filter.Empty;
            foreach (var filter in filters)
            {
                builders &= Builders<TEntity>.Filter.Eq(filter.Key, filter.Value);
            }
            var all = await DbSet.FindAsync(builders);
            return all.ToList();
        }
        public virtual void Update(TEntity obj)
        {
            ConfigDbSet();
            DbSet.ReplaceOneAsync(Builders<TEntity>.Filter.Eq("_id", obj.GetId()), obj);
            //Context.AddCommand(() => DbSet.ReplaceOneAsync(Builders<TEntity>.Filter.Eq("_id", obj.GetId()), obj));
        }
        public virtual async Task UpdateAsync(TEntity obj)
        {
            ConfigDbSet();

            await DbSet.ReplaceOneAsync(Builders<TEntity>.Filter.Eq("_id", obj.GetId()), obj);
        }
        public virtual async Task UpdateManyAsync(FilterDefinition<TEntity> filter, UpdateDefinition<TEntity> update)
        {
            ConfigDbSet();

            await DbSet.UpdateManyAsync(filter, update);
        }

        public virtual void Remove(dynamic id)
        {
            ConfigDbSet();
            DbSet.DeleteOneAsync(Builders<TEntity>.Filter.Eq("_id", id));
            //Context.AddCommand(() => DbSet.DeleteOneAsync(Builders<TEntity>.Filter.Eq("_id", id)));
        }
        public virtual async Task RemoveAsync(dynamic id)
        {
            ConfigDbSet();

            await DbSet.DeleteOneAsync(Builders<TEntity>.Filter.Eq("_id", id));
        }
        public virtual void RemoveWithFilters(Dictionary<string, object> filters)
        {
            ConfigDbSet();
            FilterDefinition<TEntity> builders = Builders<TEntity>.Filter.Empty;
            foreach (var filter in filters)
            {
                builders &= Builders<TEntity>.Filter.Eq(filter.Key, filter.Value);
            }
            DbSet.DeleteOneAsync(builders);
            //Context.AddCommand(() => DbSet.DeleteOneAsync(builders));
        }
        public virtual void RemoveAllWithFilters(Dictionary<string, object> filters)
        {
            ConfigDbSet();
            FilterDefinition<TEntity> builders = Builders<TEntity>.Filter.Empty;
            foreach (var filter in filters)
            {
                builders &= Builders<TEntity>.Filter.Eq(filter.Key, filter.Value);
            }
            DbSet.DeleteManyAsync(builders);
            //Context.AddCommand(() => DbSet.DeleteManyAsync(builders));
        }

        public virtual async Task<IEnumerable<TEntity>> GetRange(int index, int count)
        {
            ConfigDbSet();
            var all = await DbSet.FindAsync(Builders<TEntity>.Filter.Empty);
            return all.ToList().GetRange(index, count);
        }

        public virtual async Task<long> GetCount()
        {
            ConfigDbSet();
            var count = await DbSet.CountDocumentsAsync(Builders<TEntity>.Filter.Empty);
            return count;
        }

        public virtual async Task<long> GetCountWithFilters(Dictionary<string, object> filters)
        {
            ConfigDbSet();
            FilterDefinition<TEntity> builders = Builders<TEntity>.Filter.Empty;

            foreach (var filter in filters)
            {
                builders &= Builders<TEntity>.Filter.Eq(filter.Key, filter.Value);
            }

            var count = await DbSet.CountDocumentsAsync(builders);
            return count;
        }
        public void Dispose()
        {
            Context?.Dispose();
        }


    }
}
